import 'package:www_skillsmates_app/models/models.dart';

List<Account> accounts = [
  Account(
    uuid: 'uuid',
    active: true,
    email: 'account@skillsmates.com',
    firstname: 'Tiago',
    lastname: 'MOREAU',
    avatar: '',
  ),
  Account(
    uuid: 'uuid',
    active: true,
    email: 'account@skillsmates.com',
    firstname: 'Dassy',
    lastname: 'CHATCHUIN',
    avatar: '',
  ),
  Account(
    uuid: 'uuid',
    active: true,
    email: 'account@skillsmates.com',
    firstname: 'Belinda',
    lastname: 'MENGUE',
    avatar: '',
  ),
];
