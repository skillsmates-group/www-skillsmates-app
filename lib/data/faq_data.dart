import '../models/faq.dart';

List<Faq> faqs = [
  Faq(
      title: "Quels pays sont couvert par Skills Mates ?",
      content:
          "Le nom de domaine Skills-Mates.com ne fait l’objet d’aucune restriction, par conséquent il est accessible partout dans le monde. Cependant, la plateforme est pour le moment uniquement disponible en français et ainsi son usage est facilité au public francophone. Toutefois nous envisageons de nous ouvrir à de nouvelles langues dans les années à venir."),
  Faq(
      title: "Quel âge dois-je avoir pour m'inscrire ?",
      content:
          "Conformément aux exigences législatives relatives à l’utilisation des plateformes numériques, nous requérons que nos utilisateurs aient au moins 13 ans et plus."),
  Faq(
      title: "J'ai du mal à m'inscrire, que dois-je faire ?",
      content:
          " Veuillez nous envoyer un email à l’adresse suivante contact@skills-mates.com en précisant les difficultés ou points de blocages rencontrés lors de votre inscription. N’hésitez pas à y joindre des captures d’écran afin de nous aider à identifier et résoudre le problème dans les meilleurs délais."),
  Faq(
      title: "Dois-je payer des frais ?",
      content:
          " Non ! L'utilisation de Skills Mates est entièrement gratuite ! Nous envisageons de créer une cagnotte de financement participatif afin de permettre à ceux qui le souhaitent de soutenir le projet."),
  Faq(
      title: "Combien de temps devrais-je m'engager ?",
      content:
          "Skills Mates est un réseau social gratuit et libre d’accès. Chaque utilisateur dispose de la possibilité de se désinscrire librement de la plateforme et de supprimer son compte à tout moment sans préavis ni délai."),
  Faq(
      title: "Qui a accès à mes données ?",
      content:
          " Nous mobilisons vos données dans la limite des dispositions énoncées dans les conditions d’utilisation afin d’optimiser votre expérience sur la plateforme. Nous serions ainsi en mesure de vous proposer les profils, les contenus et les annonces publicitaires les mieux adaptés à vos besoins. Le cas échéant, nous partagerons aussi vos données avec des tiers dans le respect des contraintes légales afin de réaliser nos objectifs commerciaux et des partenariats nécessaires au fonctionnement de la plateforme. Par exemple nous pourrions souscrire à la régie publicitaire de Google afin de générer des revenus. En accord avec notre politique de confidentialité, toute désinscription et suppression de compte mettra un terme à l’usage de vos données sur la plateforme."),
  Faq(
      title: "Quelle est la politique concernant la propriété intellectuelle ?",
      content:
          " Conformément à notre charte utilisateur, vous êtes responsables des contenus que vous publiez. En ce sens, lorsque vous n’êtes pas l’auteur d’un contenu, il vous revient de vous assurer que les éléments partagés sont bien en accès libre « Creatives Commons » et non soumis à des droits d’auteur. A titre d’exemple, si vous souhaitez publier le cours d’un professeur, vous devez préalablement avoir obtenu son autorisation, auquel cas vous vous exposez personnellement aux éventuelles actions légales de ce dernier."),
  Faq(
      title: "Comment s’assurer de la validité et de la qualité des contenus publiés ?",
      content:
          " Skills Mates est un réseau social de partage de connaissances où chaque membre de la communauté apporte des contenus selon ses qualités et où chacun se sert selon ses besoins. En ce sens chaque utilisateur détient toute la liberté contribuer à ce principe sans discrimination ou restriction vis-à-vis de son degré d’appétence et d’expertise. Pour s’assurer de la qualité d’un contenu il revient à chacun de faire preuve de vigilance et de sagacité (discernement) afin d’évaluer la qualité des contenus ou des commentaires liés aux publications en adéquation avec les différents éléments liés au profil de l’expéditeur. Le parcours académique et professionnel, les diplômes publiés, le nombres d’abonnés, la description des contenus et la teneur des commentaires suscités sont autant d’éléments à prendre en compte. Il relève du devoir de chacun des membres de la communauté d’apporter des critiques constructives sur la base de sa maitrise des sujets et de contribuer à apprécier la qualité des contenus en toute courtoisie et bonne foi. Nous intégrerons progressivement différents outils de notation, de validation et de modérations des contenus."),
  Faq(
      title: "Comment puis-je remonter un bug sur la plateforme Skills Mates ?",
      content:
          " Vous pouvez nous faire part des anomalies et difficultés rencontrées sur plateforme (avec illustration si possible) grâce à la rubrique « contactez-nous » de l’assistance au sein de la plateforme ou en nous adressant un email à contact@skills-mates.com L’ensemble de vos remarques et propositions nous sont précieuses car elles nous aideront à améliorer l’outil. Nous nous efforcerons de vous apporter des solutions dans les meilleurs délais et dans la mesure du possible.")
];
