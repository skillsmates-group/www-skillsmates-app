import 'package:www_skillsmates_app/data/account_data.dart';
import 'package:www_skillsmates_app/models/forum.dart';

List<Forum> forums = [
  Forum(
    account: accounts[0],
    content: 'How to convert image to URI to Byte[] (Byte array) in React Native?',
    discipline: 'Informatique',
    createdAt: '2022-10-16',
    comments: 6,
  ),
  Forum(
    account: accounts[1],
    content: 'Comment accelerer la bio-decomposition des dechets ?',
    discipline: 'Energie',
    createdAt: '2022-09-16',
    comments: 13,
  ),
  Forum(
    account: accounts[2],
    content: 'Comment calculer le délai de recuperation d\'un investissement?',
    discipline: 'Finance',
    createdAt: '2022-10-11',
    comments: 2,
  ),
];
