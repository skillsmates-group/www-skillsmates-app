import 'package:www_skillsmates_app/enums/media_type_enum.dart';

import '../models/media.dart';
import '../themes/picture_file.dart';

List<Media> medias = [
  Media(
    code: MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name,
    label: 'Documents',
    image: document_bleu,
  ),
  Media(
    code: MediaTypeEnum.MEDIA_TYPE_VIDEO.name,
    label: 'Vidéos',
    image: video_rouge,
  ),
  Media(
    code: MediaTypeEnum.MEDIA_TYPE_LINK.name,
    label: 'Liens',
    image: lien_jaune,
  ),
  Media(
    code: MediaTypeEnum.MEDIA_TYPE_IMAGE.name,
    label: 'Images',
    image: photo_vert,
  ),
  Media(
    code: MediaTypeEnum.MEDIA_TYPE_AUDIO.name,
    label: 'Audios',
    image: audio_rose,
  ),
];

Media getMediaByCode(String code) {
  return medias.where((element) => element.code == code).first;
}
