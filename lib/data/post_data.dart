import 'package:www_skillsmates_app/models/models.dart';

List<Post> posts = [
  Post(
    uuid: 'uuid',
    description: 'Optimisation du rangement par la maison',
    createdAt: '2022-10-12',
    comments: 2,
    shares: 5,
    likes: 52,
  ),
  Post(
    uuid: 'uuid',
    description: 'Illustration du theoreme de Pythagore',
    createdAt: '2022-08-20',
    comments: 8,
    shares: 2,
    likes: 26,
  ),
  Post(
    uuid: 'uuid',
    description: 'Pourquoi Bill Gates soutient-il la nasa?',
    createdAt: '2022-09-30',
    comments: 1,
    shares: 7,
    likes: 9,
  ),
];
