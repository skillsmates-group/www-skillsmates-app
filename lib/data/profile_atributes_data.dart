import 'package:www_skillsmates_app/enums/account_attribute_enum.dart';

import '/models/profile_attribute.dart';
import '../themes/picture_file.dart';

List<ProfileAttribute> profileAttributes = [
  ProfileAttribute(
    code: AccountAttribute.resume,
    label: 'Résumé',
    image: document_bleu,
  ),
  ProfileAttribute(
    code: AccountAttribute.general_infos,
    label: 'Informations génrales',
    image: video_rouge,
  ),
  ProfileAttribute(
    code: AccountAttribute.trainings,
    label: 'Formations',
    image: lien_jaune,
  ),
  ProfileAttribute(
    code: AccountAttribute.jobs,
    label: 'Parcours professionnel',
    image: photo_vert,
  ),
  ProfileAttribute(
    code: AccountAttribute.skills,
    label: 'Compétences',
    image: audio_rose,
  ),
  ProfileAttribute(
    code: AccountAttribute.biography,
    label: 'Biographie',
    image: audio_rose,
  ),
];
