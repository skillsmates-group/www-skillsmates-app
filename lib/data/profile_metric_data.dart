import '../enums/status.dart';
import '../models/documents/document_type_metric.dart';
import '../models/parameter_response.dart';

List<DocumentTypeMetric> profileMetrics = [
  DocumentTypeMetric(
    uuid: 'uuid',
    code: Status.STUDENT.name,
    label: Status.STUDENT.value,
    total: 0,
    mediaType: ParameterResponse(uuid: '', code: 'PROFILE', label: 'Profile'),
  ),
  DocumentTypeMetric(
    uuid: 'uuid',
    code: Status.TEACHER.name,
    label: Status.TEACHER.value,
    total: 0,
    mediaType: ParameterResponse(uuid: '', code: 'PROFILE', label: 'Profile'),
  ),
  DocumentTypeMetric(
    uuid: 'uuid',
    code: Status.PROFESSIONAL.name,
    label: Status.PROFESSIONAL.value,
    total: 0,
    mediaType: ParameterResponse(uuid: '', code: 'PROFILE', label: 'Profile'),
  ),
];
