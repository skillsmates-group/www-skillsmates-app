import 'package:www_skillsmates_app/enums/setting_attribute.dart';
import 'package:www_skillsmates_app/enums/setting_code.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

List<SettingAttribute> settingAttributes = [
  SettingAttribute(code: SettingCode.account, label: "Compte", image: profile),
  SettingAttribute(code: SettingCode.security, label: "Sécurité & connexions", image: security),
  SettingAttribute(code: SettingCode.confidentiality, label: "Confidentialité", image: confidentiality),
  SettingAttribute(code: SettingCode.location, label: "Localisation", image: location)
];
