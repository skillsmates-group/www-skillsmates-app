import 'package:www_skillsmates_app/data/account_data.dart';
import 'package:www_skillsmates_app/data/post_data.dart';
import 'package:www_skillsmates_app/models/skillbox.dart';

List<Skillbox> skillboxes = [
  Skillbox(
    account: accounts[0],
    post: posts[0],
  ),
  Skillbox(
    account: accounts[1],
    post: posts[1],
  ),
  Skillbox(
    account: accounts[2],
    post: posts[2],
  ),
];
