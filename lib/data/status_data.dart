import 'package:www_skillsmates_app/models/parameter_response.dart';

import '../enums/status.dart';

List<ParameterResponse> statuses = [
  ParameterResponse(
    uuid: 'uuid',
    code: Status.STUDENT.name,
    label: Status.STUDENT.value,
  ),
  ParameterResponse(
    uuid: 'uuid',
    code: Status.TEACHER.name,
    label: Status.TEACHER.value,
  ),
  ParameterResponse(
    uuid: 'uuid',
    code: Status.PROFESSIONAL.name,
    label: Status.PROFESSIONAL.value,
  ),
];
