enum SkillLevel {
  LEVEL_1("Notions de base", "assets/images/red-arrow.svg"),
  LEVEL_2("A approfondir", "assets/images/orange-arrow.svg"),
  LEVEL_3("Bien", "assets/images/green-arrow.svg"),
  LEVEL_4("Très bien", "assets/images/green-arrow-plus.svg");

  const SkillLevel(this.value, this.image);
  final String value;
  final String image;
}
