enum AccountAttribute {
  resume,
  general_infos,
  trainings,
  jobs,
  skills,
  biography,
}
