enum Direction {
  ASC("Les plus anciens"),
  DESC("Les plus récents");

  const Direction(this.value);

  final String value;
}
