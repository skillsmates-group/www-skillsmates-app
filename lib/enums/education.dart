enum Education {
  SECONDARY("Enseignement secondaire"),
  HIGHER("Enseignement superieur");

  const Education(this.value);
  final String value;
}
