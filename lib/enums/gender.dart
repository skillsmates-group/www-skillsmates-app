enum Gender {
  MALE("Homme"),
  FEMALE("Femme");

  const Gender(this.value);
  final String value;

  Gender getByValue(String value) {
    return Gender.values.where((element) => element.value == value).first;
  }
}
