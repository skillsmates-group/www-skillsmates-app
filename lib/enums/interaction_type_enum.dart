enum InteractionTypeEnum {
  INTERACTION_TYPE_LIKE('LIKE'),
  INTERACTION_TYPE_COMMENT('COMMENT'),
  INTERACTION_TYPE_SHARE('SHARE'),
  INTERACTION_TYPE_FOLLOWER('FOLLOWER'),
  INTERACTION_TYPE_FAVORITE('FAVORITE'),
  INTERACTION_TYPE_POST('POST');

  const InteractionTypeEnum(this.value);
  final String value;
}
