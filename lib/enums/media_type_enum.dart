enum MediaTypeEnum {
  MEDIA_TYPE_DOCUMENT('DOCUMENT'),
  MEDIA_TYPE_IMAGE('IMAGE'),
  MEDIA_TYPE_VIDEO('VIDEO'),
  MEDIA_TYPE_LINK('LINK'),
  MEDIA_TYPE_AUDIO('AUDIO');

  const MediaTypeEnum(this.value);
  final String value;
}
