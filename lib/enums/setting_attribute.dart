import 'package:www_skillsmates_app/enums/setting_code.dart';

class SettingAttribute {
  final SettingCode code;
  final String label;
  final String image;

  const SettingAttribute({
    required this.code,
    required this.label,
    required this.image,
  });
}
