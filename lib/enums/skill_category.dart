enum SkillCategory {
  CATEGORY_1("A developper"),
  CATEGORY_2("Maitrisée");

  const SkillCategory(this.value);
  final String value;
}
