enum Status {
  STUDENT("Etudiant", "assets/images/student.svg"),
  TEACHER("Enseignant", "assets/images/teacher.svg"),
  PROFESSIONAL("Professionnel", "assets/images/professional.svg");

  const Status(this.value, this.image);
  final String value;
  final String image;

  Status getByValue(String value) {
    return Status.values.where((element) => element.value == value).first;
  }
}
