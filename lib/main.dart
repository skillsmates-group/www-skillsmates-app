import 'package:flutter/material.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/account_activated/account_activated_page.dart';
import 'package:www_skillsmates_app/pages/account_created/account_created_page.dart';
import 'package:www_skillsmates_app/pages/assistance/assistance_page.dart';
import 'package:www_skillsmates_app/pages/contact/contact_page.dart';
import 'package:www_skillsmates_app/pages/dashboard/dashboard_page.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/documents_shelf_page.dart';
import 'package:www_skillsmates_app/pages/faq/faq_page.dart';
import 'package:www_skillsmates_app/pages/forgotten_password/forgotten_password_page.dart';
import 'package:www_skillsmates_app/pages/login/login_page.dart';
import 'package:www_skillsmates_app/pages/messagerie/messagerie_page.dart';
import 'package:www_skillsmates_app/pages/network/network_page.dart';
import 'package:www_skillsmates_app/pages/notifications/notification_page.dart';
import 'package:www_skillsmates_app/pages/password_deleted/password_deleted_page.dart';
import 'package:www_skillsmates_app/pages/post_add/post_add_page.dart';
import 'package:www_skillsmates_app/pages/post_details/post_details_page.dart';
import 'package:www_skillsmates_app/pages/post_details/post_details_provider.dart';
import 'package:www_skillsmates_app/pages/post_display/post_display_page.dart';
import 'package:www_skillsmates_app/pages/post_edit/post_edit_page.dart';
import 'package:www_skillsmates_app/pages/profile/profile_page.dart';
import 'package:www_skillsmates_app/pages/profile_edit/profile_edit_page.dart';
import 'package:www_skillsmates_app/pages/reset-password/reset_password_page.dart';
import 'package:www_skillsmates_app/pages/search/SearchProvider.dart';
import 'package:www_skillsmates_app/pages/search/search_page.dart';
import 'package:www_skillsmates_app/pages/settings/settings_page.dart';
import 'package:www_skillsmates_app/pages/share_post/share_post_page.dart';
import 'package:www_skillsmates_app/providers/account_provider.dart';
import 'package:www_skillsmates_app/providers/attribute_provider.dart';
import 'package:www_skillsmates_app/providers/auth_provider.dart';
import 'package:www_skillsmates_app/providers/help_provider.dart';
import 'package:www_skillsmates_app/providers/interaction_provider.dart';
import 'package:www_skillsmates_app/providers/post_provider.dart';
import 'package:www_skillsmates_app/themes/sm_input_theme.dart';

void main() {
  runApp(const MyApp());
  setUrlStrategy(PathUrlStrategy());
}

final GoRouter _routers = GoRouter(routes: [
  GoRoute(path: "/", redirect: (context, state) => LoginPage.routeName),
  GoRoute(path: LoginPage.routeName, builder: (context, state) => const LoginPage()),
  GoRoute(path: DashboardPage.routeName, builder: (context, state) => const DashboardPage()),
  GoRoute(path: ProfilePage.routeName, builder: (context, state) => const ProfilePage()),
  GoRoute(path: ForgottenPasswordPage.routeName, builder: (context, state) => const ForgottenPasswordPage()),
  GoRoute(
      path: ResetPasswordPage.routeName,
      builder: (context, state) => ResetPasswordPage(token: state.pathParameters['token']!)),
  GoRoute(path: PostAddPage.routeName, builder: (context, state) => const PostAddPage()),
  GoRoute(path: AccountCreatedPage.routeName, builder: (context, state) => const AccountCreatedPage()),
  GoRoute(path: PasswordDeletedPage.routeName, builder: (context, state) => const PasswordDeletedPage()),
  GoRoute(path: AccountActivatedPage.routeName, builder: (context, state) => const AccountActivatedPage()),
  GoRoute(path: SearchPage.routeName, builder: (context, state) => const SearchPage()),
  GoRoute(path: NetworkPage.routeName, builder: (context, state) => const NetworkPage()),
  GoRoute(path: NotificationPage.routeName, builder: (context, state) => const NotificationPage()),
  GoRoute(path: MessageriePage.routeName, builder: (context, state) => const MessageriePage()),
  GoRoute(path: ProfileEditPage.routeName, builder: (context, state) => const ProfileEditPage()),
  GoRoute(path: DocumentsShelfPage.routeName, builder: (context, state) => const DocumentsShelfPage()),
  GoRoute(path: AssistancePage.routeName, builder: (context, state) => const AssistancePage()),
  GoRoute(path: FaqPage.routeName, builder: (context, state) => const FaqPage()),
  GoRoute(path: SettingsPage.routeName, builder: (context, state) => const SettingsPage()),
  GoRoute(path: ContactPage.routeName, builder: (context, state) => const ContactPage()),
  GoRoute(path: PostDetailsPage.routeName, builder: (context, state) => const PostDetailsPage()),
  GoRoute(path: SharePostPage.routeName, builder: (context, state) => const SharePostPage()),
  GoRoute(path: PostEditPage.routeName, builder: (context, state) => const PostEditPage()),
  GoRoute(
      path: PostDisplayPage.routeName,
      builder: (context, state) => PostDisplayPage(postId: state.pathParameters['postId']!)),
]);

bool isAuth = true;

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AuthProvider>(create: (context) => AuthProvider()),
          ChangeNotifierProvider<HelpProvider>(create: (context) => HelpProvider()),
          ChangeNotifierProvider<PostProvider>(create: (context) => PostProvider()),
          ChangeNotifierProvider<AccountProvider>(create: (context) => AccountProvider()),
          ChangeNotifierProvider<InteractionProvider>(create: (context) => InteractionProvider()),
          ChangeNotifierProvider<AttributeProvider>(create: (context) => AttributeProvider()),
          ChangeNotifierProvider<SearchProvider>(create: (context) => SearchProvider()),
          ChangeNotifierProvider<PostDetailsProvider>(create: (context) => PostDetailsProvider())
        ],
        child: MaterialApp.router(
            title: 'Skillsmates',
            theme: ThemeData(
                primarySwatch: Colors.blue, fontFamily: 'Raleway', inputDecorationTheme: SmInputTheme().theme()),
            routerConfig: _routers));
  }
}
