import 'package:intl/intl.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../enums/gender.dart';
import '../../enums/status.dart';

class Account {
  String uuid;
  String? createdAt;
  String? updatedAt;
  bool active;
  String firstname;
  String lastname;
  String email;
  String? address;
  String? phoneNumber;
  String? birthdate;
  bool? hideBirthdate;
  String? gender;
  String? biography;
  String? city;
  String? country;
  String? status;
  bool? connected;
  String? connectedAt;
  String? role;
  String? avatar;
  String? currentJob;
  String? currentCompany;
  int? posts;
  int? followers;
  int? followees;
  int? suggestions;
  int? messages;
  int? notifications;
  int? network;

  Account(
      {required this.uuid,
      this.createdAt,
      this.updatedAt,
      required this.active,
      required this.firstname,
      required this.lastname,
      required this.email,
      this.address,
      this.phoneNumber,
      this.birthdate,
      this.hideBirthdate,
      this.gender,
      this.biography,
      this.city,
      this.country,
      this.status,
      this.connected,
      this.connectedAt,
      this.role,
      this.avatar,
      this.currentJob,
      this.currentCompany,
      this.posts,
      this.followers,
      this.followees,
      this.suggestions,
      this.messages,
      this.notifications,
      this.network});

  factory Account.fromJson(Map<String, dynamic> data) {
    return Account(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'],
      updatedAt: data['updatedAt'],
      active: true,
      firstname: data['firstname'] ?? '',
      lastname: data['lastname'] ?? '',
      email: data['email'] ?? '',
      address: data['address'],
      phoneNumber: data['phoneNumber'],
      birthdate: data['birthdate'],
      hideBirthdate: data['hideBirthdate'],
      gender: data['gender'],
      biography: data['biography'],
      city: data['city'],
      country: data['country'],
      status: data['status'],
      connected: data['connected'],
      connectedAt: data['connectedAt'],
      role: data['role'],
      avatar: data['avatar'],
      currentJob: data['currentJob'],
      currentCompany: data['currentCompany'],
      posts: data['posts'],
      followers: data['followers'],
      followees: data['followees'],
      suggestions: data['suggestions'],
      messages: data['messages'],
      notifications: data['notifications'],
      network: data['network'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'active': active,
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
      'address': address,
      'phoneNumber': phoneNumber,
      'birthdate': birthdate,
      'hideBirthdate': hideBirthdate,
      'gender': gender,
      'biography': biography,
      'city': city,
      'country': country,
      'status': status,
      'connected': connected,
      'connectedAt': connectedAt,
      'role': role,
      'avatar': avatar,
      'currentJob': currentJob,
      'currentCompany': currentCompany,
      'posts': posts,
      'followers': followers,
      'followees': followees,
      'suggestions': suggestions,
      'messages': messages,
      'notifications': notifications,
      'network': network,
    };
  }

  String genderLabel() {
    if (this.gender == null || this.gender == '') return '';
    return Gender.values.where((element) => element.name == this.gender).first.value;
  }

  String birthDate() {
    if (this.birthdate == null || this.birthdate == '') return '';
    DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(birthdate!);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    return outputFormat.format(inputDate);
  }

  String statusLabel() {
    if (this.status == null || this.status == '') return '';
    return Status.values.where((element) => element.name == this.status).first.value;
  }

  String statusIcon() {
    if (this.status == null || this.status == '') return student;
    return Status.values.where((element) => element.name == this.status).first.image;
  }

  @override
  String toString() {
    return 'Account{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, active: $active, firstname: $firstname, lastname: $lastname, email: $email, address: $address, phoneNumber: $phoneNumber, birthdate: $birthdate, hideBirthdate: $hideBirthdate, gender: $gender, biography: $biography, city: $city, country: $country, status: $status, connected: $connected, connectedAt: $connectedAt, role: $role, avatar: $avatar, currentJob: $currentJob, currentCompany: $currentCompany, posts: $posts, followers: $followers, followees: $followees, suggestions: $suggestions}';
  }
}
