import 'package:www_skillsmates_app/models/account/certification.dart';
import 'package:www_skillsmates_app/models/account/diploma.dart';
import 'package:www_skillsmates_app/models/account/job.dart';
import 'package:www_skillsmates_app/models/account/skill.dart';
import 'package:www_skillsmates_app/models/account/training.dart';

class AccountAttributesResponse {
  final List<Skill>? skills;
  final int? totalSkills;
  final List<Diploma>? diplomas;
  final int? totalDiplomas;
  final List<Certification>? certifications;
  final int? totalCertifications;
  final List<Job>? jobs;
  final int? totalJobs;
  final List<Training>? trainings;
  final int? totalTrainings;

  const AccountAttributesResponse({
    this.skills,
    this.totalSkills,
    this.diplomas,
    this.totalDiplomas,
    this.certifications,
    this.totalCertifications,
    this.jobs,
    this.totalJobs,
    this.trainings,
    this.totalTrainings,
  });

  factory AccountAttributesResponse.fromJson(Map<String, dynamic> data) {
    var resource = data['resource'];
    var skills = resource['skills'] != null ? resource['skills'] as List : List.empty();
    var certifications = resource['certifications'] != null ? resource['certifications'] as List : List.empty();
    var diplomas = resource['diplomas'] != null ? resource['diplomas'] as List : List.empty();
    var jobs = resource['jobs'] != null ? resource['jobs'] as List : null;
    var trainings = resource['trainings'] != null ? resource['trainings'] as List : List.empty();
    return AccountAttributesResponse(
      skills: skills.map((skill) => Skill.fromJson(skill)).toList(),
      totalSkills: resource['totalSkills'],
      certifications: certifications.map((certification) => Certification.fromJson(certification)).toList(),
      totalCertifications: resource['totalCertifications'],
      diplomas: diplomas.map((diploma) => Diploma.fromJson(diploma)).toList(),
      totalDiplomas: resource['totalDiplomas'],
      jobs: jobs?.map((job) => Job.fromJson(job)).toList(),
      totalJobs: resource['totalJobs'],
      trainings: trainings.map((training) => Training.fromJson(training)).toList(),
      totalTrainings: resource['totalTrainings'],
    );
  }
}
