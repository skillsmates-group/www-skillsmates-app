class AccountAuthResponseResource {
  String firstname;
  String lastname;
  String email;

  AccountAuthResponseResource({
    required this.firstname,
    required this.lastname,
    required this.email,
  });

  factory AccountAuthResponseResource.fromJson(Map<String, dynamic> data) {
    return AccountAuthResponseResource(
      firstname: data['firstname'] ?? '',
      lastname: data['lastname'] ?? '',
      email: data['email'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
    };
  }

  @override
  String toString() {
    return 'firstname: $firstname, lastname: $lastname, email: $email}';
  }
}
