import '/models/meta.dart';
import '/models/models.dart';

class AccountResponse {
  final Meta? meta;
  final Account? resource;
  final List<Account>? resources;

  const AccountResponse({
    this.meta,
    this.resource,
    this.resources,
  });

  factory AccountResponse.fromJson(dynamic data) {
    var resources = data['resources'] != null ? data['resources'] as List : null;
    return AccountResponse(
      meta: Meta.fromJson(data['meta']),
      resource: data['resource'] != null ? Account.fromJson(data['resource']) : null,
      resources: resources?.map((post) => Account.fromJson(post)).toList(),
    );
  }

  @override
  String toString() {
    return '{meta: ${meta.toString()}, resource: ${resource}, resources: ${resources}}';
  }
}
