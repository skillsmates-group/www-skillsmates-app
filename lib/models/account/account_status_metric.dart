class AccountStatusMetric {
  final int total;
  // final String uuid;
  final String code;
  final String label;

  const AccountStatusMetric({
    this.total = 0,
    // required this.uuid,
    required this.code,
    required this.label,
  });

  factory AccountStatusMetric.fromJson(Map<String, dynamic> data) {
    return AccountStatusMetric(
      // uuid: data['uuid'],
      code: data['code'],
      label: data['label'],
      total: data['total'],
    );
  }

  @override
  String toString() {
    return 'AccountStatusMetric{total: $total, code: $code, label: $label}';
  }
}
