import '/models/meta.dart';
import 'account_status_metric.dart';

class AccountStatusMetricResponse {
  final Meta? meta;
  final AccountStatusMetric? resource;
  final List<AccountStatusMetric>? resources;

  const AccountStatusMetricResponse({
    this.meta,
    this.resource,
    this.resources,
  });

  factory AccountStatusMetricResponse.fromJson(dynamic data) {
    var resources = data['resources'] != null ? data['resources'] as List : List.empty();
    return AccountStatusMetricResponse(
      // meta: Meta.fromJson(data['meta']),
      resource: data['resource'] != null ? AccountStatusMetric.fromJson(data['resource']) : null,
      resources: resources.map((type) => AccountStatusMetric.fromJson(type)).toList(),
    );
  }

  static List<AccountStatusMetric> fromJsonList(List list) {
    return list.map((item) => AccountStatusMetric.fromJson(item)).toList();
  }

  @override
  String toString() {
    return '{meta: ${meta.toString()}, resource: ${resource}, resources: ${resources}}';
  }
}
