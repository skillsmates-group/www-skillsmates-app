import '/models/parameter_response.dart';

class Certification {
  String uuid;
  String? createdAt;
  String? updatedAt;
  String? title;
  String? description;
  String? account;
  bool? permanent;
  String? startDate;
  String? endDate;
  String? schoolName;
  ParameterResponse? schoolType;
  ParameterResponse? activityArea;

  Certification({
    required this.uuid,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.account,
    this.permanent,
    this.startDate,
    this.endDate,
    this.schoolName,
    this.schoolType,
    this.activityArea,
  });

  factory Certification.fromJson(Map<String, dynamic> data) {
    return Certification(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'],
      updatedAt: data['updatedAt'],
      title: data['title'] ?? '',
      description: data['description'] ?? '',
      account: data['account'] ?? '',
      startDate: data['startDate'] ?? '',
      endDate: data['endDate'] ?? '',
      permanent: data['permanent'] ?? '',
      schoolName: data['schoolName'],
      schoolType: data['schoolType'] != null ? ParameterResponse.fromJson(data['schoolType']) : null,
      activityArea: data['activityArea'] != null ? ParameterResponse.fromJson(data['activityArea']) : null,
    );
  }
}
