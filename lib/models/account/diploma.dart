import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import '/models/parameter_response.dart';

class Diploma {
  String uuid;
  String? createdAt;
  String? updatedAt;
  String? title;
  String? description;
  String? account;
  String? education;
  String? schoolName;
  String? city;
  String? startDate;
  String? endDate;
  ParameterResponse? schoolType;
  ParameterResponse? level;
  ParameterResponse? activityArea;

  Diploma({
    required this.uuid,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.account,
    this.education,
    this.schoolName,
    this.city,
    this.startDate,
    this.endDate,
    this.schoolType,
    this.activityArea,
    this.level,
  });

  factory Diploma.fromJson(Map<String, dynamic> data) {
    return Diploma(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'],
      updatedAt: data['updatedAt'],
      title: data['title'] ?? '',
      description: data['description'] ?? '',
      account: data['account'] ?? '',
      education: data['education'] ?? '',
      schoolName: data['schoolName'] ?? '',
      city: data['city'] ?? '',
      startDate: data['startDate'] ?? '',
      endDate: data['endDate'] ?? '',
      schoolType: data['schoolType'] != null ? ParameterResponse.fromJson(data['schoolType']) : null,
      activityArea: data['activityArea'] != null ? ParameterResponse.fromJson(data['activityArea']) : null,
      level: data['level'] != null ? ParameterResponse.fromJson(data['level']) : null,
    );
  }

  String? convertStringToDate(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return ' ';
    }
    try {
      DateTime dateTime = DateTime.parse(dateString);
      initializeDateFormatting('fr_FR', null);
      DateFormat outputFormat = DateFormat.yMMMMd('fr_FR');
      return outputFormat.format(dateTime);
    } catch (e) {
      return null;
    }
  }
}
