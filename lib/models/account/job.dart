import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import '/models/parameter_response.dart';

class Job {
  String? uuid;
  String? createdAt;
  String? updatedAt;
  String? title;
  String? description;
  String? account;
  String? company;
  String? startDate;
  String? endDate;
  String? city;
  bool? currentJob;
  ParameterResponse? activitySector;
  ParameterResponse? activityArea;

  Job({
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.account,
    this.company,
    this.startDate,
    this.endDate,
    this.city,
    this.currentJob,
    this.activitySector,
    this.activityArea,
  });

  factory Job.fromJson(Map<String, dynamic> data) {
    return Job(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'],
      updatedAt: data['updatedAt'],
      title: data['title'] ?? '',
      description: data['description'] ?? '',
      account: data['account'] ?? '',
      company: data['company'] ?? '',
      startDate: data['startDate'] ?? '',
      endDate: data['endDate'] ?? '',
      city: data['city'] ?? '',
      currentJob: data['currentJob'],
      activitySector: data['activitySector'] != null ? ParameterResponse.fromJson(data['activitySector']) : null,
      activityArea: data['activityArea'] != null ? ParameterResponse.fromJson(data['activityArea']) : null,
    );
  }

  String startdate() {
    DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(startDate!);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    return outputFormat.format(inputDate);
  }

  String enddate() {
    DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(endDate!);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    return outputFormat.format(inputDate);
  }

  @override
  String toString() {
    return 'Job{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, title: $title, description: $description, account: $account, company: $company, startDate: $startDate, endDate: $endDate, city: $city, currentJob: $currentJob, activitySector: $activitySector, activityArea: $activityArea}';
  }

  String? convertStringToDate(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return ' ';
    }
    try {
      DateTime dateTime = DateTime.parse(dateString);
      initializeDateFormatting('fr_FR', null);
      DateFormat outputFormat = DateFormat.yMMMMd('fr_FR');
      return outputFormat.format(dateTime);
    } catch (e) {
      return null;
    }
  }
}
