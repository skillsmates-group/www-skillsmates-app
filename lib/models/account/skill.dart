import '../../enums/skill_level.dart';

class Skill {
  String? uuid;
  String? createdAt;
  String? updatedAt;
  String? title;
  String? description;
  String? account;
  String? category;
  String? discipline;
  String? keywords;
  String? level;

  Skill({
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.account,
    this.category,
    this.discipline,
    this.keywords,
    this.level,
  });

  factory Skill.fromJson(Map<String, dynamic> data) {
    return Skill(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'],
      updatedAt: data['updatedAt'],
      title: data['title'] ?? '',
      description: data['description'] ?? '',
      account: data['account'] ?? '',
      category: data['category'] ?? '',
      discipline: data['discipline'] ?? '',
      keywords: data['keywords'] ?? '',
      level: data['level'] ?? '',
    );
  }

  String image() {
    return SkillLevel.values.where((element) => element.name == this.level).first.image;
  }

  String nameLevel() {
    return SkillLevel.values.where((element) => element.name == this.level).first.value;
  }

  @override
  String toString() {
    return 'Skill{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, title: $title, description: $description, account: $account, category: $category, discipline: $discipline, keywords: $keywords, level: $level}';
  }
}
