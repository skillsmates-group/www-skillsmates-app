import '../parameter_response.dart';

class Training {
  String? uuid;
  String? createdAt;
  String? updatedAt;
  String? title;
  String? description;
  String? account;
  String? education;
  String? schoolName;
  String? city;
  ParameterResponse? schoolType;
  ParameterResponse? activityArea;
  ParameterResponse? schoolClass;
  ParameterResponse? level;
  ParameterResponse? teachingName;

  Training({
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.description,
    this.account,
    this.education,
    this.schoolName,
    this.activityArea,
    this.city,
    this.schoolType,
    this.schoolClass,
    this.level,
    this.teachingName,
  });

  factory Training.fromJson(Map<String, dynamic> data) {
    return Training(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'],
      updatedAt: data['updatedAt'],
      title: data['title'] ?? '',
      description: data['description'] ?? '',
      account: data['account'] ?? '',
      education: data['education'] ?? '',
      schoolName: data['schoolName'] ?? '',
      city: data['city'] ?? '',
      schoolType: data['schoolType'] != null ? ParameterResponse.fromJson(data['schoolType']) : null,
      activityArea: data['activityArea'] != null ? ParameterResponse.fromJson(data['activityArea']) : null,
      schoolClass: data['schoolClass'] != null ? ParameterResponse.fromJson(data['schoolClass']) : null,
      level: data['level'] != null ? ParameterResponse.fromJson(data['level']) : null,
      teachingName: data['teachingName'] != null ? ParameterResponse.fromJson(data['teachingName']) : null,
    );
  }

  @override
  String toString() {
    return 'Training{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, title: $title, description: $description, account: $account, education: $education, schoolName: $schoolName, city: $city, schoolType: $schoolType, schoolClass: $schoolClass, level: $level, teachingName: $teachingName}';
  }
}
