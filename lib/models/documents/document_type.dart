import 'package:www_skillsmates_app/models/parameter_response.dart';

class DocumentType {
  final String? uuid;
  String? code;
  String? label;
  final ParameterResponse? mediaType;

  DocumentType({
    this.uuid,
    this.code,
    this.label,
    this.mediaType,
  });

  factory DocumentType.fromJson(Map<String, dynamic> data) {
    return DocumentType(
      uuid: data['uuid'],
      code: data['code'],
      label: data['label'],
      mediaType: ParameterResponse.fromJson(data['mediaType']),
    );
  }

  @override
  String toString() {
    return label ?? "";
  }
}
