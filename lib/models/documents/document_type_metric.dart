import '../parameter_response.dart';

class DocumentTypeMetric {
  final int total;
  final String uuid;
  final String code;
  final String label;
  final ParameterResponse mediaType;

  const DocumentTypeMetric({
    this.total = 0,
    required this.uuid,
    required this.code,
    required this.label,
    required this.mediaType,
  });

  factory DocumentTypeMetric.fromJson(Map<String, dynamic> data) {
    return DocumentTypeMetric(
      uuid: data['uuid'],
      code: data['code'],
      label: data['label'],
      total: data['total'],
      mediaType: ParameterResponse.fromJson(data['mediaType']),
    );
  }

  @override
  String toString() {
    return 'DocumentTypeMetric{total: $total, uuid: $uuid, code: $code, label: $label, mediaType: $mediaType}';
  }
}
