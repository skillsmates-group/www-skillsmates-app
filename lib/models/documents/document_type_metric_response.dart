import 'package:www_skillsmates_app/models/documents/document_type_metric.dart';

import '/models/meta.dart';

class DocumentTypeMetricResponse {
  final Meta? meta;
  final DocumentTypeMetric? resource;
  final List<DocumentTypeMetric>? resources;

  const DocumentTypeMetricResponse({
    this.meta,
    this.resource,
    this.resources,
  });

  factory DocumentTypeMetricResponse.fromJson(dynamic data) {
    var resources = data['resources'] != null ? data['resources'] as List : List.empty();
    return DocumentTypeMetricResponse(
      // meta: Meta.fromJson(data['meta']),
      resource: data['resource'] != null ? DocumentTypeMetric.fromJson(data['resource']) : null,
      resources: resources.map((type) => DocumentTypeMetric.fromJson(type)).toList(),
    );
  }

  static List<DocumentTypeMetric> fromJsonList(List list) {
    return list.map((item) => DocumentTypeMetric.fromJson(item)).toList();
  }

  @override
  String toString() {
    return '{meta: ${meta.toString()}, resource: ${resource}, resources: ${resources}}';
  }
}
