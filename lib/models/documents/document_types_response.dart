import 'package:www_skillsmates_app/models/documents/document_type.dart';

import '/models/meta.dart';

class DocumentTypesResponse {
  final Meta? meta;
  final DocumentType? resource;
  final List<DocumentType>? resources;

  const DocumentTypesResponse({
    this.meta,
    this.resource,
    this.resources,
  });

  factory DocumentTypesResponse.fromJson(dynamic data) {
    var resources = data['resources'] != null ? data['resources'] as List : List.empty();
    return DocumentTypesResponse(
      meta: Meta.fromJson(data['meta']),
      resource: data['resource'] != null ? DocumentType.fromJson(data['resource']) : null,
      resources: resources.map((type) => DocumentType.fromJson(type)).toList(),
    );
  }

  static List<DocumentType> fromJsonList(List list) {
    return list.map((item) => DocumentType.fromJson(item)).toList();
  }

  @override
  String toString() {
    return '{meta: ${meta.toString()}, resource: ${resource}, resources: ${resources}}';
  }
}
