class Faq {
  final String title;
  final String content;

  Faq({required this.title, required this.content});
}
