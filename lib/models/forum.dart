import 'package:timeago/timeago.dart' as timeago;
import 'package:www_skillsmates_app/models/account/account.dart';

class Forum {
  final Account account;
  final String content;
  final String discipline;
  final String? createdAt;
  final int? comments;

  const Forum({
    required this.account,
    required this.content,
    required this.discipline,
    this.createdAt,
    this.comments,
  });

  factory Forum.fromJson(Map<String, dynamic> data) {
    return Forum(
      account: Account.fromJson(data['account']),
      content: data['content'] ?? '',
      discipline: data['discipline'] ?? '',
      createdAt: data['createdAt'] ?? '',
      comments: data['comments'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'account': account.uuid,
      'content': content,
      'discipline': discipline,
    };
  }

  duration() {
    timeago.setLocaleMessages('fr', timeago.FrMessages());
    final now = DateTime.now();
    final other = DateTime.parse(this.createdAt ?? '');
    return '${timeago.format(now.subtract(now.difference(other)), locale: 'fr')}';
  }
}
