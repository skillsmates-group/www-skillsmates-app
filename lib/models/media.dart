class Media {
  final String code;
  final String label;
  final String image;

  const Media({
    required this.code,
    required this.label,
    required this.image,
  });

  factory Media.fromJson(Map<String, dynamic> data) {
    return Media(
      code: data['code'] ?? '',
      label: data['label'] ?? '',
      image: data['image'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'label': label,
      'image': image,
    };
  }

  @override
  String toString() {
    return 'Media{code: $code, label: $label, image: $image}';
  }
}
