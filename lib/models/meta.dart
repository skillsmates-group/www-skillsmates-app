class Meta {
  final bool? hasPrevious;
  final bool? hasNext;
  final int? total;
  final int? totalPages;

  const Meta({
    this.hasPrevious,
    this.hasNext,
    this.total,
    this.totalPages
  });

  factory Meta.fromJson(Map<String, dynamic> data) {
    return Meta(
      hasPrevious: data['hasPrevious'],
      hasNext: data['hasNext'],
      total: data['total'],
      totalPages: data['totalPages']
    );
  }

  @override
  String toString() {
    return '{hasPrevious: ${hasPrevious}, hasNext: ${hasNext}, total: ${total}, totalPages: ${totalPages}}';
  }
}
