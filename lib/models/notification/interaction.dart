import 'package:timeago/timeago.dart' as timeago;

import '../account/account.dart';

class Interaction {
  String? uuid;
  String? createdAt;
  String? updatedAt;
  bool? active;
  String entity;
  String interactionType;
  Account emitter;
  Account receiver;
  String? content;

  Interaction({
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.active,
    required this.entity,
    required this.interactionType,
    required this.emitter,
    required this.receiver,
    this.content,
  });

  String displayedMessage() {
    switch (this.interactionType) {
      case "INTERACTION_TYPE_LIKE":
        return ' a aimé votre publication ${this.content}';
      case "INTERACTION_TYPE_FOLLOWER":
        return ' est votre nouvel abbonné';
      case "INTERACTION_TYPE_FAVORITE":
        return ' vous a ajouté dans sa liste de favoris';
      case "INTERACTION_TYPE_COMMENT":
        return ' a commenté votre publication ${this.content}';
      case "INTERACTION_TYPE_SHARE":
        return ' a partagé votre publication ${this.content}';
      case "INTERACTION_TYPE_POST":
        return ' a effectué une nouvelle publication ${this.content}';
      default:
        return '';
    }
  }

  durationText() {
    timeago.setLocaleMessages('fr', timeago.FrMessages());
    final now = DateTime.now();
    final other = DateTime.parse(this.createdAt ?? '');
    return '${timeago.format(now.subtract(now.difference(other)), locale: 'fr')}';
  }

  factory Interaction.fromJson(Map<String, dynamic> json) {
    return Interaction(
      uuid: json['uuid'] ?? '',
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'] ?? '',
      active: json['active'],
      entity: json['entity'],
      interactionType: json['interactionType'],
      emitter: Account.fromJson(json['emitter']),
      receiver: Account.fromJson(json['receiver']),
      content: json['content'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'active': active,
      'entity': entity,
      'interactionType': interactionType,
      'emitter': emitter.uuid,
      'receiver': receiver.uuid,
      'content': content,
    };
  }

  @override
  String toString() {
    return 'Interaction{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, active: $active, entity: $entity, interactionType: $interactionType, emitter: $emitter, receiver: $receiver, content: $content}';
  }
}
