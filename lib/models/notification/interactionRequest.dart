class InteractionRequest {
  String? uuid;
  String? createdAt;
  String? updatedAt;
  bool? active;
  String entity;
  String interactionType;
  String emitter;
  String receiver;
  String? content;

  InteractionRequest({
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.active,
    required this.entity,
    required this.interactionType,
    required this.emitter,
    required this.receiver,
    this.content,
  });

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'active': active,
      'entity': entity,
      'interactionType': interactionType,
      'emitter': emitter,
      'receiver': receiver,
      'content': content,
    };
  }

  @override
  String toString() {
    return 'Interaction{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, active: $active, entity: $entity, interactionType: $interactionType, emitter: $emitter, receiver: $receiver, content: $content}';
  }
}
