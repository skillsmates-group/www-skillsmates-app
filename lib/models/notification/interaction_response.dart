import 'package:www_skillsmates_app/models/meta.dart';
import 'package:www_skillsmates_app/models/notification/interaction.dart';

class InteractionResponse {
  final Meta? meta;
  final Interaction? resource;
  final List<Interaction>? resources;

  const InteractionResponse({
    this.meta,
    this.resource,
    this.resources,
  });

  factory InteractionResponse.fromJson(dynamic data) {
    var resources = data['resources'] != null ? data['resources'] as List : List.empty();
    return InteractionResponse(
        meta: data['meta'] != null ? Meta.fromJson(data['meta']) : null,
        resource: data['resource'] != null ? Interaction.fromJson(data['resource']) : null,
        resources: resources.map((interaction) => Interaction.fromJson(interaction)).toList());
  }
}
