class ParameterResponse {
  String? uuid;
  String? label;
  String? code;

  ParameterResponse({this.uuid, this.label, this.code});

  factory ParameterResponse.fromJson(Map<String, dynamic> data) {
    return ParameterResponse(
      uuid: data['uuid'],
      label: data['label'],
      code: data['code'],
    );
  }

  factory ParameterResponse.fromJsonV2(Map<String, dynamic> data) {
    var resource = data['resource'];
    return ParameterResponse(
      uuid: resource['uuid'],
      label: resource['label'],
      code: resource['code'],
    );
  }

  static List<ParameterResponse> fromJsonList(List list) {
    return list.map((item) => ParameterResponse.fromJson(item)).toList();
  }

  @override
  String toString() {
    return label ?? '';
  }
}
