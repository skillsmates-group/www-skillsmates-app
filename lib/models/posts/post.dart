import 'package:timeago/timeago.dart' as timeago;

import '../account/account.dart';

class Post {
  final String? uuid;
  final String? createdAt;
  final String? updatedAt;
  String? title;
  String? keywords;
  String? discipline;
  String? description;
  String? documentType;
  String? postType;
  String? mediaType;
  String? document;
  String? link;
  final String? thumbnail;
  final Account? account;
  final String? documentTitle;
  final String? documentDescription;
  int likes;
  int comments;
  int shares;
  bool liked;

  Post({
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.keywords,
    this.discipline,
    this.description,
    this.documentType,
    this.postType,
    this.mediaType,
    this.document,
    this.link,
    this.thumbnail,
    this.account,
    this.likes = 0,
    this.comments = 0,
    this.shares = 0,
    this.liked = false,
    this.documentTitle,
    this.documentDescription,
  });

  factory Post.fromJson(Map<String, dynamic> data) {
    return Post(
      uuid: data['uuid'] ?? '',
      createdAt: data['createdAt'] ?? '',
      updatedAt: data['updatedAt'] ?? '',
      title: data['title'] ?? '',
      keywords: data['keywords'] ?? '',
      discipline: data['discipline'] ?? '',
      description: data['description'] ?? '',
      documentType: data['documentType'] ?? '',
      mediaType: data['mediaType'] ?? '',
      postType: data['postType'] ?? '',
      document: data['document'] ?? '',
      link: data['link'] ?? '',
      thumbnail: data['thumbnail'] ?? '',
      likes: data['likes'] ?? '',
      comments: data['comments'] ?? '',
      shares: data['shares'] ?? '',
      liked: data['liked'] ?? '',
      account: Account.fromJson(data['account']),
      documentTitle: data['documentTitle'] ?? '',
      documentDescription: data['documentDescription'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uuid': uuid,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'title': title,
      'keywords': keywords,
      'discipline': discipline,
      'description': description,
      'documentType': documentType,
      'mediaType': mediaType,
      'postType': postType,
      'document': document,
      'link': link,
      'thumbnail': thumbnail,
      'likes': likes,
      'comments': comments,
      'shares': shares,
      'liked': liked,
      'account': account?.toJson(),
      'documentTitle': documentTitle,
      'documentDescription': documentDescription,
    };
  }

  durationText() {
    timeago.setLocaleMessages('fr', timeago.FrMessages());
    final now = DateTime.now();
    final other = DateTime.parse(this.createdAt ?? '');
    return '${timeago.format(now.subtract(now.difference(other)), locale: 'fr')}';
  }

  @override
  String toString() {
    return 'Post{uuid: $uuid, createdAt: $createdAt, updatedAt: $updatedAt, title: $title, keywords: $keywords, discipline: $discipline, description: $description, documentType: $documentType, postType: $postType, mediaType: $mediaType, document: $document, link: $link, thumbnail: $thumbnail, account: $account, documentTitle: $documentTitle, documentDescription: $documentDescription, likes: $likes, comments: $comments, shares: $shares, liked: $liked}';
  }
}
