import '/models/meta.dart';
import '/models/models.dart';

class PostResponse {
  final Meta? meta;
  final Post? resource;
  final List<Post>? resources;

  const PostResponse({
    this.meta,
    this.resource,
    this.resources,
  });

  factory PostResponse.fromJson(dynamic data) {
    var resources = data['resources'] != null ? data['resources'] as List : null;
    return PostResponse(
      meta: Meta.fromJson(data['meta']),
      resource: data['resource'] != null ? Post.fromJson(data['resource']) : null,
      resources: resources?.map((post) => Post.fromJson(post)).toList(),
    );
  }

  @override
  String toString() {
    return '{meta: ${meta.toString()}, resource: ${resource}, resources: ${resources}}';
  }

  Account getPostOwner() {
    return getPost().account ?? Account(uuid: '', active: false, firstname: '', lastname: '', email: '');
  }

  String getOwnerFirstname() {
    return getPostOwner().firstname;
  }

  String getOwnerLastname() {
    return getPostOwner().lastname;
  }

  Post getPost() {
    return resource!;
  }

  List<Post> getOtherPosts() {
    return resources ?? List.empty();
  }
}
