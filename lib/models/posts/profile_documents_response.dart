import 'package:www_skillsmates_app/models/models.dart';

class ProfileDocumentsResponse {
  final List<Post>? documents;
  final int? totalDocuments;
  final List<Post>? links;
  final int? totalLinks;
  final List<Post>? videos;
  final int? totalVideos;
  final List<Post>? images;
  final int? totalImages;
  final List<Post>? audios;
  final int? totalAudios;

  const ProfileDocumentsResponse({
    this.documents,
    this.totalDocuments,
    this.links,
    this.totalLinks,
    this.videos,
    this.totalVideos,
    this.images,
    this.totalImages,
    this.audios,
    this.totalAudios,
  });

  factory ProfileDocumentsResponse.fromJson(Map<String, dynamic> data) {
    var resource = data['resource'];
    var documents = resource['documents'] != null ? resource['documents'] as List : null;
    var links = resource['links'] != null ? resource['links'] as List : null;
    var videos = resource['videos'] != null ? resource['videos'] as List : null;
    var images = resource['images'] != null ? resource['images'] as List : null;
    var audios = resource['audios'] != null ? resource['audios'] as List : null;
    return ProfileDocumentsResponse(
      documents: documents?.map((document) => Post.fromJson(document)).toList(),
      totalDocuments: resource['totalDocuments'],
      links: links?.map((link) => Post.fromJson(link)).toList(),
      totalLinks: resource['totalLinks'],
      videos: videos?.map((video) => Post.fromJson(video)).toList(),
      totalVideos: resource['totalVideos'],
      images: images?.map((image) => Post.fromJson(image)).toList(),
      totalImages: resource['totalImages'],
      audios: audios?.map((audio) => Post.fromJson(audio)).toList(),
      totalAudios: resource['totalAudios'],
    );
  }
}
