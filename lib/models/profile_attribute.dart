import '../enums/account_attribute_enum.dart';

class ProfileAttribute {
  final AccountAttribute code;
  final String label;
  final String image;

  const ProfileAttribute({
    required this.code,
    required this.label,
    required this.image,
  });
}
