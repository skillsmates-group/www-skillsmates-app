class SearchParam {
  String? account;
  String? content;
  String? country;
  String? city;
  String? direction;
  List<String>? documentTypes;
  List<String>? statuses;

  SearchParam({
    this.account,
    this.content,
    this.country,
    this.city,
    this.documentTypes,
    this.statuses,
    this.direction,
  });

  @override
  String toString() {
    return 'SearchParam{account: $account, content: $content, country: $country, city: $city, documentTypes: $documentTypes, statuses: $statuses, direction: $direction}';
  }
}
