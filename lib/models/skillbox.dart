import 'package:www_skillsmates_app/models/models.dart';

class Skillbox {
  final Account account;
  final Post post;

  const Skillbox({
    required this.account,
    required this.post,
  });

  factory Skillbox.fromJson(Map<String, dynamic> data) {
    return Skillbox(
      account: Account.fromJson(data['account']),
      post: Post.fromJson(data['post']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'account': account.uuid,
      'post': post.uuid,
    };
  }
}
