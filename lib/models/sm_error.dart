class SmError {
  String? title;
  String? link;
  String? category;

  SmError({
    this.title,
    this.link,
    this.category,
  });

  @override
  String toString() {
    return 'SmError{title: $title, link: $link, category: $category}';
  }
}
