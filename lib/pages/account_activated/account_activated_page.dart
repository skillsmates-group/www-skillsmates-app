import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/account_activated/desktop_account_activated_screen.dart';

class AccountActivatedPage extends StatelessWidget {
  const AccountActivatedPage({Key? key}) : super(key: key);

  static const routeName = '/confirm/activated';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as bool;
    return ResponsiveBuilder(
      builder: (BuildContext context, SizingInformation sizingInformation) {
        if (sizingInformation.isDesktop) {
          return DesktopAccountActivatedScreen(
            isresetPassword: args,
          );
        }
        if (sizingInformation.isTablet) {
          return DesktopAccountActivatedScreen(
            isresetPassword: args,
          );
        }
        return DesktopAccountActivatedScreen(
          isresetPassword: args,
        );
      },
    );
  }
}
