import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:www_skillsmates_app/pages/login/login_page.dart';

import '/themes/theme.dart';
import '../../providers/properties.dart';

class DesktopAccountActivatedScreen extends StatelessWidget {
  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';
  static const symboleSkillsmates = 'assets/images/symbole_skillsmates.svg';
  final bool isresetPassword;

  const DesktopAccountActivatedScreen({
    this.isresetPassword = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _navigateToLoginPage() {
      // Navigator.of(context).pushReplacementNamed(
      //   LoginPage.routeName,
      // );
      context.push(LoginPage.routeName);
    }

    return Scaffold(
        body: SingleChildScrollView(
      child: SizedBox(
        height: 900,
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Center(
                    child: SvgPicture.asset(
                      logoSkillsmates,
                      height: 100,
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Center(
                    child: SvgPicture.asset(
                      landingPagePeople,
                      height: 300,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
              side: const BorderSide(color: Colors.grey, width: 1),
            ),
            elevation: 1,
            child: Container(
              width: 700,
              padding: const EdgeInsets.all(30),
              child: Column(
                children: [
                  !isresetPassword
                      ? Row(
                          children: const [
                            Icon(Icons.person, size: 30),
                            Text(
                              "COMPTE ACTIVE",
                              style: TextStyle(
                                  fontSize: 20,
                                  color: primaryColor,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        )
                      : Row(
                          children: const [
                            Icon(Icons.key_sharp, size: 30),
                            Text(
                              "MOT DE PASSE REINITIALISE",
                              style: TextStyle(
                                fontSize: 20,
                                color: primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                  !isresetPassword
                      ? Container(
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                          child: const Text(
                            "Votre compte skillsmate a été activé "
                            "avec succès",
                            style: TextStyle(
                                fontSize: 18,
                                color: greenColor,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      : Container(
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                          child: const Text(
                            "Votre mot de passe a bien été réinitialisé "
                            "avec succès.",
                            style: TextStyle(
                                fontSize: 18,
                                color: greenColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                    child: Row(
                      children: [
                        // SmTextButton(label: "Cliquez ici", color: primaryColor),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            foregroundColor: Colors.white,
                            backgroundColor: primaryColor, // foreground
                          ),
                          onPressed: _navigateToLoginPage,
                          child: Text('Cliquez ici'),
                        ),
                        Text(
                          " pour accéder à la page de connexion de la plateforme.",
                          style: TextStyle(
                              fontSize: 18,
                              color: primaryColor,
                              fontWeight: FontWeight.normal),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                    child: const Text(
                      " Si le bouton ci dessus ne fonctionne "
                              "pas, cliquez directement sur ce lien ou copiez et collez "
                              "le dans votre navigateur: " +
                          skillsmates_url,
                      style: TextStyle(
                          fontSize: 18,
                          color: primaryColor,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 100),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
                "A PROPOS - AIDE -POLITIQUE DE CONFIDENTIALITE - \nCONDITIONS "
                "D'UTILISATION - FAQ",
                style: TextStyle(
                    fontSize: 18,
                    color: primaryColor,
                    fontWeight: FontWeight.normal)),
            const SizedBox(width: 20),
            Center(child: SvgPicture.asset(symboleSkillsmates, height: 100)),
            const Text("\u00a9 Skillsmates 2022",
                style: TextStyle(
                    fontSize: 18,
                    color: primaryColor,
                    fontWeight: FontWeight.normal))
          ])
        ]),
      ),
    ));
  }
}
