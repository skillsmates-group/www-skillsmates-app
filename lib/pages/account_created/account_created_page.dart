import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/account_created/desktop_account_created_screen.dart';

class AccountCreatedPage extends StatelessWidget {
  const AccountCreatedPage({Key? key}) : super(key: key);

  static const routeName = '/confirm/created';

  @override
  Widget build(BuildContext context) {
    // final args = ModalRoute.of(context)!.settings.arguments as bool;
    final args = false;
    return ResponsiveBuilder(
      builder: (BuildContext context, SizingInformation sizingInformation) {
        if (sizingInformation.isDesktop) {
          return DesktopAccountCreatedScreen(
            isForgottenpassword: args,
          );
        }
        if (sizingInformation.isTablet) {
          return DesktopAccountCreatedScreen(
            isForgottenpassword: args,
          );
        }
        return DesktopAccountCreatedScreen(
          isForgottenpassword: args,
        );
      },
    );
  }
}
