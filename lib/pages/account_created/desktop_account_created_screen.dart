import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class DesktopAccountCreatedScreen extends StatelessWidget {
  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';
  static const symboleSkillsmates = 'assets/images/symbole_skillsmates.svg';
  final bool isForgottenpassword;

  const DesktopAccountCreatedScreen({
    this.isForgottenpassword = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          height: 900,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Center(
                        child: SvgPicture.asset(logoSkillsmates, height: 100),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Center(
                        child: SvgPicture.asset(landingPagePeople, height: 300),
                      ),
                    ],
                  ),
                ],
              ),
              Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                    side: const BorderSide(color: Colors.grey, width: 1)),
                elevation: 1,
                child: Container(
                  width: 700,
                  padding: const EdgeInsets.all(30),
                  child: Column(
                    children: [
                      !isForgottenpassword
                          ? Row(
                              children: const [
                                Icon(Icons.person, size: 30),
                                Text(
                                  "COMPTE CREE",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )
                          : Row(
                              children: const [
                                Icon(
                                  Icons.mail,
                                  size: 30,
                                ),
                                Text(
                                  "MAIL ENVOYE",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                      if (!isForgottenpassword)
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.all(10),
                          child: const Text(
                            "Votre compte skillsmate a bien été crée",
                            style: TextStyle(
                                fontSize: 18,
                                color: primaryColor,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        child: const Text(
                          "Un mail de validation de votre compte "
                          "vous a été envoyé. Veillez consulter votre boite de "
                          "reception ou vos spams et cliquer sur le lien pour "
                          "valider le compte",
                          style: TextStyle(
                              fontSize: 18,
                              color: greenColor,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 100),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                      "A PROPOS - AIDE -POLITIQUE DE CONFIDENTIALITE - \nCONDITIONS "
                      "D'UTILISATION - FAQ",
                      style: TextStyle(
                          fontSize: 18,
                          color: primaryColor,
                          fontWeight: FontWeight.normal)),
                  const SizedBox(width: 20),
                  Center(
                      child: SvgPicture.asset(symboleSkillsmates, height: 100)),
                  const Text(
                    "\u00a9 Skillsmates 2022",
                    style: TextStyle(
                        fontSize: 18,
                        color: primaryColor,
                        fontWeight: FontWeight.normal),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
