import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/assistance/desktop_assistance_screen.dart';
import 'package:www_skillsmates_app/pages/assistance/mobile_assistance_screen.dart';

class AssistancePage extends StatelessWidget {
  const AssistancePage({Key? key}) : super(key: key);

  static const routeName = '/assistance';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopAssistanceScreen();
      }
      if (sizingInformation.isTablet) {
        return MobileAssistanceScreen();
      }
      return MobileAssistanceScreen();
    });
  }
}
