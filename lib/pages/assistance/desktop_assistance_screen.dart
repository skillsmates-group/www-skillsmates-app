import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:www_skillsmates_app/pages/contact/contact_page.dart';
import 'package:www_skillsmates_app/pages/faq/faq_page.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_button_back.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_footer_row.dart';
import 'package:www_skillsmates_app/themes/palette.dart';

import '../../themes/picture_file.dart';
import '../widgets/responsive.dart';

class DesktopAssistanceScreen extends StatelessWidget {
  const DesktopAssistanceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: padding(),
          ),
          child: Column(children: [
            Center(
              child: Text(
                "Besoin d'assistance ?".toUpperCase(),
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 100),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 80),
                  Container(
                    padding: EdgeInsets.all(10),
                    color: Palette.skillsmatesBlue,
                    child: Column(children: [
                      InkWell(
                        onTap: () {
                          context.push(FaqPage.routeName);
                          // Navigator.of(context).pushNamed(FaqPage.routeName);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(
                            10,
                          ),
                          child: Column(children: [
                            SvgPicture.asset(
                              assistanceQuestionMark,
                              height: 250,
                              width: 250,
                            ),
                            Text(
                              "FAQ",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ]),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(children: [
                      InkWell(
                        onTap: () {
                          context.push(ContactPage.routeName);
                          // Navigator.of(context).pushNamed(ContactPage.routeName);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(children: [
                            SvgPicture.asset(
                              assistanceHeadPhone,
                              height: 250,
                              width: 250,
                            ),
                            Text(
                              "CONTACTEZ-NOUS",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ]),
                  ),
                  SizedBox(
                    width: 80,
                  ),
                ]),
            SizedBox(
              height: 50,
            ),
            SmButtonBack(),
            SizedBox(
              height: 50,
            ),
            SmFooterRow()
          ]),
        ),
      ),
    );
  }
}
