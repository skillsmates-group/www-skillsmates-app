import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/classeur_docs/desktop_classeur_screen.dart';
import 'package:www_skillsmates_app/pages/classeur_docs/mobile_classeur_screen.dart';

class ClasseurDocsPage extends StatelessWidget {
  const ClasseurDocsPage({Key? key}) : super(key: key);

  static const routeName = '/classeur';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
        builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopClasseurScreen();
      }
      if (sizingInformation.isTablet) {
        return const MobileClasseurScreen();
      }
      return const MobileClasseurScreen();
    });
  }
}
