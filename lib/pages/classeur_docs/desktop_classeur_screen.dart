import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:www_skillsmates_app/pages/classeur_docs/widgets/classeur_content.dart';

import '../dashboard/widgets/desktop_nav_bar.dart';

class DesktopClasseurScreen extends StatefulWidget {
  const DesktopClasseurScreen({Key? key}) : super(key: key);

  @override
  State<DesktopClasseurScreen> createState() => _DesktopClasseurScreenState();
}

class _DesktopClasseurScreenState extends State<DesktopClasseurScreen> {
  final TrackingScrollController _trackingScrollController =
      TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 65,
                right: 65,
              ),
              child: ClasseurContent(),
            ),
          ],
        ),
      ),
    );
  }
}
