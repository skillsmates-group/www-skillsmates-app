import 'package:flutter/material.dart';

import 'package:www_skillsmates_app/pages/search/widgets/result_show_interface.dart';

import '../../search/widgets/filter_search_side.dart';

class ClasseurContent extends StatelessWidget {
  const ClasseurContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: FilterSearchSide(
              typeFilter: "classeur",
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Flexible(
            flex: 5,
            child: SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(10.0),
                child: ResultShowInterface(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
