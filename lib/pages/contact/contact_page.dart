import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'desktop_contact_screen.dart';
import 'mobile_contact_screen.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({Key? key}) : super(key: key);

  static const routeName = '/contact';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopContactScreen();
      }
      if (sizingInformation.isTablet) {
        return MobileContactScreen();
      }
      return MobileContactScreen();
    });
  }
}
