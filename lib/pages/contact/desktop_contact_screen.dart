import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:www_skillsmates_app/pages/assistance/assistance_page.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_button_back.dart';

import '../../themes/picture_file.dart';
import '../widgets/sm_elevated_button.dart';
import '../widgets/sm_footer_row.dart';
import '../widgets/sm_text_form_field.dart';

class DesktopContactScreen extends StatelessWidget {
  const DesktopContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    final _objet = TextEditingController();
    final _message = TextEditingController();

    return Scaffold(
        body: SingleChildScrollView(
            child: Column(children: [
      Center(
          child: Text("Besoin d'assistance ?".toUpperCase(),
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold))),
      SizedBox(height: 50),
      Row(children: [
        Flexible(
            child: Padding(
                padding: const EdgeInsets.only(left: 100, right: 100),
                child: Form(
                    key: _formKey,
                    child: Column(children: [
                      SmTextFormField(_objet, fieldName: "Objet du message"),
                      SizedBox(height: 20),
                      TextField(
                          controller: _message,
                          textInputAction: TextInputAction.newline,
                          keyboardType: TextInputType.multiline,
                          maxLines: 10,
                          decoration: InputDecoration(labelText: "Message")),
                      SizedBox(height: 20),
                      Row(children: [
                        Expanded(
                            child: SmElevatedButton(
                                label: "Envoyer",
                                color: Colors.grey,
                                handleButton: () {})),
                        SizedBox(width: 40),
                        Expanded(
                            child: SmElevatedButton(
                                label: "Annuler",
                                color: Colors.grey,
                                handleButton: () => context.push(AssistancePage.routeName)
                                    // Navigator.of(context)
                                    // .pushNamed(AssistancePage.routeName)
                            ) )
                      ])
                    ])))),
        Flexible(
            child: Column(children: [
          Text("Merci de nous écrire pour tout besoin",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 30)),
          SvgPicture.asset(assistanceLady, height: 300, width: 300)
        ]))
      ]),
      SizedBox(height: 50),
      SmButtonBack(),
      SizedBox(height: 50),
      SmFooterRow()
    ])));
  }
}
