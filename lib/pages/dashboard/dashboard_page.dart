import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/dashboard/tablet_dashboard_screen.dart';

import '/pages/dashboard/desktop_dashboard_screen.dart';
import '/pages/dashboard/mobile_dashboard_screen.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  static const routeName = '/dashboard';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopDashboardScreen();
      }
      if (sizingInformation.isTablet) {
        return const TabletDashboardScreen();
      }
      return const MobileDashboardScreen();
    });
  }
}
