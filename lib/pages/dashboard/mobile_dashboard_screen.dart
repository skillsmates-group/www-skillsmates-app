import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/mobile_nav_bar.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/mobile_top_nav_bar.dart';

import '/pages/dashboard/widgets/dashboard_content.dart';

class MobileDashboardScreen extends StatefulWidget {
  const MobileDashboardScreen({Key? key}) : super(key: key);

  @override
  State<MobileDashboardScreen> createState() => _MobileDashboardScreenState();
}

class _MobileDashboardScreenState extends State<MobileDashboardScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DashboardContent(
              scrollController: _trackingScrollController,
            ),
          ],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
