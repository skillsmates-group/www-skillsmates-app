import 'package:flutter/material.dart';

import '/pages/dashboard/widgets/dashboard_content.dart';
import '/pages/dashboard/widgets/desktop_nav_bar.dart';

class TabletDashboardScreen extends StatefulWidget {
  const TabletDashboardScreen({Key? key}) : super(key: key);

  @override
  State<TabletDashboardScreen> createState() => _TabletDashboardScreenState();
}

class _TabletDashboardScreenState extends State<TabletDashboardScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DashboardContent(
              scrollController: _trackingScrollController,
            ),
          ],
        ),
      ),
    );
  }
}
