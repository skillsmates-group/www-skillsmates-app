import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';

class Ambassadors extends StatefulWidget {
  const Ambassadors({Key? key}) : super(key: key);

  @override
  State<Ambassadors> createState() => _AmbassadorsState();
}

class _AmbassadorsState extends State<Ambassadors> {
  bool _customTileExpanded = false;

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    return Card(
      margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
      elevation: isDesktop ? 1.0 : 0.0,
      shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
      child: Container(
        width: 250,
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 8.0,
        ),
        color: Colors.white,
        child: Column(
          children: [
            ExpansionTile(
              leading: SvgPicture.asset(
                ambassador,
                width: 40,
                height: 40,
              ),
              title: const Text(
                'Ambassadeurs',
                style: const TextStyle(
                  color: primaryColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              trailing: Icon(
                _customTileExpanded ? Icons.arrow_drop_down_circle : Icons.arrow_drop_down,
              ),
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  child: SizedBox(
                    width: 200,
                    child: Image(
                      image: ExactAssetImage(
                        'assets/images/yamo_group.png',
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    "Nous travaillons ensemble Pour concevoir votre vision ",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: primaryColor,
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ],
              onExpansionChanged: (bool expanded) {
                setState(() => _customTileExpanded = expanded);
              },
            ),
          ],
        ),
      ),
    );
  }
}
