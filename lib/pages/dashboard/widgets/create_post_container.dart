import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '/pages/widgets/responsive.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/picture_file.dart';
import '../../post_add/post_add_page.dart';
import '../../widgets/sm_icon_text.dart';
import '../../widgets/sm_media_menu.dart';

class CreatePostContainer extends StatelessWidget {
  const CreatePostContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final bool isDesktop = Responsive.isDesktop(context);

    callbackMediaType(mediaType) {
      postProvider.setSelectedMediaType(mediaType).then((value) => {
            // Navigator.of(context).pushReplacementNamed(
            //   PostAddPage.routeName,
            // )
            context.push(PostAddPage.routeName)
          });
    }

    return Card(
      margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
      elevation: isDesktop ? 2.0 : 0.0,
      shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        color: Colors.white,
        child: Column(
          children: [
            Row(
              children: [
                const Expanded(
                  flex: 3,
                  child: TextField(
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    // minLines: null,
                    maxLines: 3,
                    // expands: true,
                    decoration: InputDecoration(
                      hintText: 'Exprimez-vous à votre réseau',
                      contentPadding: EdgeInsets.all(20.0),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: () {},
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        FittedBox(
                          fit: BoxFit.contain,
                          child: Text(
                            'Interrogez la \n communauté',
                            maxLines: 2,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        SvgPicture.asset(forum),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SmIconText(
              image: plus_colored,
              text: Text(
                'Selectionner le type de contenu à publier',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
              ),
              color: Colors.red,
            ),
            const Divider(height: 10.0, thickness: 0.5),
            SmMediaMenu(callback: callbackMediaType),
          ],
        ),
      ),
    );
  }
}
