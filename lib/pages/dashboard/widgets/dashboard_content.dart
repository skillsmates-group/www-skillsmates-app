import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/network/network_page.dart';

import '/pages/dashboard/widgets/widgets.dart';
import '../../../models/posts/post_response.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_footer_column.dart';
import 'dashboard_post.dart';

class DashboardContent extends StatefulWidget {
  final TrackingScrollController scrollController;

  DashboardContent({
    Key? key,
    required this.scrollController,
  }) : super(key: key);

  @override
  State<DashboardContent> createState() => _DashboardContentState();
}

class _DashboardContentState extends State<DashboardContent> {
  late Account loggedAccount;
  List<Post> postsToDisplay = [];
  int offset = 0;
  String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

  Future<PostResponse> fetchPagePost() async {
    await Future.delayed(Duration(seconds: 2));
    return Provider.of<PostProvider>(context, listen: false)
        .fetchDashboardPostsWithFilters(loggedAccountUuid ?? '', offset, 5);
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);
    final authProvider = Provider.of<AuthProvider>(context);

    void _navigateToSuggestions() {
      context.push(NetworkPage.routeName);
    }

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Container(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: padding()),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (isDesktop || isTablet)
                Flexible(
                    flex: 3,
                    child: SingleChildScrollView(
                        child: Column(children: [
                      Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: Column(children: [
                                VerticalLoggedAccountCard(),
                                const SizedBox(height: 5),
                                const ProfilesOnline(),
                                const SizedBox(height: 5),
                                const SmFooterColumn()
                              ])))
                    ]))),
              Flexible(
                  flex: 6,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CreatePostContainer(),
                        SizedBox(height: 10),
                        Card(
                            margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
                            elevation: isDesktop ? 1.0 : 0.0,
                            shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
                            child: Column(children: [
                              Row(children: [
                                IconButton(onPressed: () {}, icon: Icon(Icons.person_add_outlined)),
                                SizedBox(width: 8.0),
                                Expanded(
                                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                  Text('Ces profils vous intérèssent-ils ?',
                                      style: TextStyle(fontWeight: FontWeight.w600))
                                ])),
                                IconButton(onPressed: _navigateToSuggestions, icon: Icon(Icons.add_circle_rounded))
                              ]),
                              Suggestions()
                            ])),
                        Container(child: DashboardPost())
                      ])),
              if (isDesktop || isTablet)
                Flexible(
                    flex: 3,
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                            padding: EdgeInsets.all(12.0),
                            child: Column(children: const [
                              ForumsCard(),
                              SizedBox(height: 5),
                              SkillsboxCard(),
                              SizedBox(height: 5),
                              Ambassadors(),
                              SizedBox(height: 5),
                              Partners()
                            ]))))
            ]));
  }
}
