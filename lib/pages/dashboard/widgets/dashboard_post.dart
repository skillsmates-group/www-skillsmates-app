import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/post_container.dart';
import 'package:www_skillsmates_app/themes/palette.dart';

import '../../../models/posts/post.dart';
import '../../../providers/post_provider.dart';

class DashboardPost extends StatefulWidget {
  DashboardPost({Key? key}) : super(key: key);

  @override
  State<DashboardPost> createState() => _DashboardPostState();
}

class _DashboardPostState extends State<DashboardPost> {
  List<Post> _items = [];
  int _page = 0;
  int _limit = 5;
  bool _isFirstLoadRunning = false;
  bool _isLoadMoreRunning = false;
  bool _hasNextPage = true;
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _firstLoad();
    _scrollController = ScrollController()..addListener(_loadMore);
  }

  Future<List<Post>> _fetchPosts() async {
    var apiResponse = await Provider.of<PostProvider>(context, listen: false)
        .fetchDashboardPostsWithFilters(new LocalStorage('skills-mates_app').getItem("uuid") ?? '', _page, _limit);
    return apiResponse.resources!;
  }

  void _firstLoad() async {
    setState(() {
      _isFirstLoadRunning = true;
    });
    List<Post> posts = await _fetchPosts();
    setState(() {
      _items = posts;
    });
    setState(() {
      _isFirstLoadRunning = false;
    });
  }

  void _loadMore() async {
    if (_hasNextPage == true &&
            _isFirstLoadRunning == false &&
            _isLoadMoreRunning == false /*&&
        this.widget.scrollController.position.extentAfter < 300*/
        ) {
      setState(() {
        _isLoadMoreRunning = true;
      });
      _page += 1;
      List<Post> posts = await _fetchPosts();
      if (posts.isNotEmpty) {
        setState(() {
          _items.addAll(posts);
        });
      } else {
        setState(() {
          _hasNextPage = false;
        });
      }
      setState(() {
        _isLoadMoreRunning = false;
      });
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      _isFirstLoadRunning
          ? CircularProgressIndicator()
          : ListView.builder(
              shrinkWrap: true,
              itemCount: _items.length,
              controller: _scrollController,
              itemBuilder: (context, index) => PostContainer(post: _items[index])),
      if (_isLoadMoreRunning)
        Padding(padding: EdgeInsets.only(top: 10, bottom: 40), child: Center(child: CircularProgressIndicator())),
      _hasNextPage
          ? ElevatedButton(onPressed: _loadMore, child: Text('Charger plus...'))
          : Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              color: Palette.appGreen,
              child: Center(
                  child:
                      Text('Toutes les publications ont été affichées', style: TextStyle(fontWeight: FontWeight.bold))))
    ]);
  }
}
