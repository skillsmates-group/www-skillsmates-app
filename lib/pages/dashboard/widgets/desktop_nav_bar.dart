import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/sm_image_text.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/sm_svg_image_text.dart';
import 'package:www_skillsmates_app/pages/messagerie/messagerie_page.dart';
import 'package:www_skillsmates_app/pages/notifications/notification_page.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/providers/auth_provider.dart';
import 'package:www_skillsmates_app/providers/post_provider.dart';

import '/pages/dashboard/dashboard_page.dart';
import '/pages/post_add/post_add_page.dart';
import '/pages/profile/profile_page.dart';
import '../../../enums/menu_enum.dart';
import '../../../models/account/account.dart';
import '../../../models/media.dart';
import '../../../themes/palette.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../login/login_page.dart';
import '../../logout/logout_page.dart';
import '../../network/network_page.dart';
import '../../search/search_page.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_circle_button.dart';
import '../../widgets/sm_elevated_button.dart';

class DesktopNavBar extends StatefulWidget {
  const DesktopNavBar({Key? key}) : super(key: key);

  @override
  _DesktopNavBarState createState() => _DesktopNavBarState();
}

class _DesktopNavBarState extends State<DesktopNavBar> {
  Menu _selectedMenu = Menu.home;
  Menu _hoveredMenu = Menu.home;
  late Account loggedAccount;

  @override
  void initState() {
    super.initState();
    _getSelectedMenu();
  }

  void _getSelectedMenu() {
    String _smUrl = Uri.base.toString();
    if (_smUrl.contains(DashboardPage.routeName)) {
      _selectedMenu = Menu.home;
      _hoveredMenu = Menu.home;
    }

    if (_smUrl.contains(MessageriePage.routeName)) {
      _selectedMenu = Menu.messages;
      _hoveredMenu = Menu.messages;
    }

    if (_smUrl.contains(NotificationPage.routeName)) {
      _selectedMenu = Menu.notifications;
      _hoveredMenu = Menu.notifications;
    }

    if (_smUrl.contains(ProfilePage.routeName)) {
      _selectedMenu = Menu.profile;
      _hoveredMenu = Menu.profile;
    }

    if (_smUrl.contains(NetworkPage.routeName)) {
      _selectedMenu = Menu.network;
      _hoveredMenu = Menu.network;
    }

    if (_smUrl.contains(PostAddPage.routeName)) {
      _selectedMenu = Menu.posts;
      _hoveredMenu = Menu.posts;
    }

    if (_smUrl.contains(SearchPage.routeName)) {
      _selectedMenu = Menu.search;
      _hoveredMenu = Menu.search;
    }
  }

  void _navigateTo(String routeName) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    authProvider.setProfileAccount(loggedAccount).then((value) => {context.push(routeName)});
  }

  void _navigateToLogin() {
    context.push(LoginPage.routeName);
  }

  void _selectMenuOnTap(Menu menu) {
    this._selectedMenu = menu;
    switch (menu) {
      case Menu.search:
        _navigateTo(SearchPage.routeName);
        break;
      case Menu.home:
        _navigateTo(DashboardPage.routeName);
        break;
      case Menu.profile:
        _navigateTo(ProfilePage.routeName);
        break;
      case Menu.messages:
        _navigateTo(MessageriePage.routeName);
        break;
      case Menu.notifications:
        _navigateTo(NotificationPage.routeName);
        break;
      case Menu.network:
        _navigateTo(NetworkPage.routeName);
        break;
      case Menu.posts:
        Provider.of<PostProvider>(context, listen: false).setSelectedMediaType(
            Media(code: 'MEDIA_TYPE_DOCUMENT', label: 'DOCUMENT', image: 'assets/images/document-bleu.svg'));
        _navigateTo(PostAddPage.routeName);
        break;
      case Menu.plus:
        // TODO: Handle this case.
        break;
    }
  }

  void _onHoverMenu(Menu menu) {
    this._hoveredMenu = menu;
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final bool isDesktop = Responsive.isDesktop(context);
    return Container(
      decoration: const BoxDecoration(
        color: whitishColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black45,
            blurRadius: 15.0,
            offset: Offset(
              0.0,
              0.75,
            ),
          ),
        ],
      ),
      height: 70,
      padding: const EdgeInsets.symmetric(
        horizontal: 50,
      ),
      width: double.infinity,
      child: FutureBuilder<Account>(
        future: authProvider.getLoggedAccount(),
        builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
          if (snapshot.hasData) {
            loggedAccount = snapshot.data!;
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          _selectMenuOnTap(Menu.home);
                        });
                      },
                      child: Container(
                        child: Row(
                          children: [
                            SizedBox(
                              width: 40,
                              height: 40,
                              child: SvgPicture.asset('assets/images/symbole_skillsmates.svg'),
                            ),
                            const SizedBox(width: 8),
                            const SizedBox(
                              child: Text(
                                'Skills Mates',
                                style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 8),
                    Container(
                      height: 70,
                      width: 80,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: _selectedMenu == Menu.search ? Palette.skillsmatesBlue : Colors.transparent,
                            width: 3.0,
                          ),
                        ),
                      ),
                      child: Column(
                        children: [
                          SmCircleButton(
                            icon: search,
                            iconSize: 25.0,
                            onPressed: () {
                              setState(() {
                                _selectMenuOnTap(Menu.search);
                              });
                            },
                          ),
                          Text(
                            'Rechercher',
                            style: const TextStyle(
                              fontSize: 12,
                              color: primaryColor,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            _selectMenuOnTap(Menu.home);
                          });
                        },
                        onHover: (hover) {
                          setState(() {
                            _onHoverMenu(Menu.home);
                          });
                        },
                        child: SmSvgImageText(
                          isSelectedButton: _selectedMenu == Menu.home,
                          name: "Accueil",
                          iconImage: _hoveredMenu == Menu.home || _selectedMenu == Menu.home
                              ? "assets/images/home_selected.svg"
                              : "assets/images/home.svg",
                          smBorder: 'bottom',
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _selectMenuOnTap(Menu.messages);
                          });
                        },
                        onHover: (hover) {
                          setState(() {
                            _onHoverMenu(Menu.messages);
                          });
                        },
                        child: SmSvgImageText(
                          isSelectedButton: _selectedMenu == Menu.messages,
                          name: "Messages",
                          iconImage: _hoveredMenu == Menu.messages || _selectedMenu == Menu.messages
                              ? "assets/images/message_selected.svg"
                              : "assets/images/message.svg",
                          smBorder: 'bottom',
                          badge: loggedAccount.messages ?? 0,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _selectMenuOnTap(Menu.notifications);
                          });
                        },
                        onHover: (hover) {
                          setState(() {
                            _onHoverMenu(Menu.notifications);
                          });
                        },
                        child: SmSvgImageText(
                          isSelectedButton: _selectedMenu == Menu.notifications,
                          name: "Notifications",
                          iconImage: _hoveredMenu == Menu.notifications || _selectedMenu == Menu.notifications
                              ? "assets/images/notification_selected.svg"
                              : "assets/images/notification.svg",
                          smBorder: 'bottom',
                          badge: loggedAccount.notifications ?? 0,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _selectMenuOnTap(Menu.network);
                          });
                        },
                        onHover: (hover) {
                          setState(() {
                            _onHoverMenu(Menu.network);
                          });
                        },
                        child: SmSvgImageText(
                          isSelectedButton: _selectedMenu == Menu.network,
                          name: "Réseau",
                          iconImage: _hoveredMenu == Menu.network || _selectedMenu == Menu.network
                              ? "assets/images/network_selected.svg"
                              : "assets/images/network.svg",
                          smBorder: 'bottom',
                          badge: loggedAccount.network ?? 0,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _selectMenuOnTap(Menu.posts);
                          });
                        },
                        onHover: (hover) {
                          setState(() {
                            _onHoverMenu(Menu.posts);
                          });
                        },
                        child: SmImageText(
                          isSelectedButton: _selectedMenu == Menu.posts,
                          name: "Publication",
                          iconImage: _hoveredMenu == Menu.posts || _selectedMenu == Menu.posts
                              ? "assets/images/plus_colored.png"
                              : "assets/images/plus_colored.png",
                          smBorder: 'bottom',
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 70,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: _selectedMenu == Menu.profile ? Palette.skillsmatesBlue : Colors.transparent,
                            width: 3.0,
                          ),
                        ),
                      ),
                      child: PopupMenuButton(
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15.0),
                          ),
                        ),
                        offset: Offset(0, kToolbarHeight),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ProfileAvatar(
                              imageUrl: loggedAccount.avatar?.toLowerCase() ?? '',
                              isActive: loggedAccount.active,
                            ),
                            const SizedBox(width: 6.0),
                            if (isDesktop)
                              Flexible(
                                child: Text.rich(
                                  TextSpan(
                                    text: loggedAccount.firstname,
                                    style: const TextStyle(
                                      fontSize: 16.0,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    children: [
                                      TextSpan(text: ' '),
                                      TextSpan(
                                        text: loggedAccount.lastname,
                                        style: const TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w700,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                          ],
                        ),
                        onSelected: (result) {
                          if (result.toString() == LogoutPage.routeName) {
                            authProvider.logout().then((value) => {
                                  context.push(LoginPage.routeName)
                                  // Navigator.of(context).pushReplacementNamed(
                                  //   LoginPage.routeName,
                                  // )
                                });
                          } else {
                            _navigateTo(result.toString());
                          }
                        },
                        itemBuilder: (context) {
                          return [
                            PopupMenuItem(
                              child: ListTile(
                                leading: SvgPicture.asset(avatar, height: 25, width: 25),
                                title: Text("Mon profil"),
                              ),
                              value: ProfilePage.routeName,
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                leading: SvgPicture.asset(settings, height: 25, width: 25),
                                title: Text("Paramétres"),
                              ),
                              value: '/settings',
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                leading: SvgPicture.asset(assistance, height: 25, width: 25),
                                title: Text("Assistance"),
                              ),
                              value: '/assistance',
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                leading: SvgPicture.asset(logout, height: 25, width: 25),
                                title: Text("Deconnexion"),
                              ),
                              value: LogoutPage.routeName,
                            )
                          ];
                        },
                      ),
                    ),
                    const SizedBox(width: 12.0),
                  ],
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      _selectMenuOnTap(Menu.home);
                    });
                  },
                  child: Container(
                    child: Row(
                      children: [
                        SizedBox(
                          width: 40,
                          height: 40,
                          child: SvgPicture.asset('assets/images/symbole_skillsmates.svg'),
                        ),
                        const SizedBox(width: 8),
                        const SizedBox(
                          child: Text(
                            'Skills Mates',
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                              color: primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      SizedBox(
                        width: 160,
                        child: SmElevatedButton(
                          label: 'Connectez-vous',
                          color: primaryColor,
                          handleButton: () => _navigateToLogin(),
                        ),
                      ),
                      const SizedBox(width: 8),
                      SizedBox(
                        width: 160,
                        child: SmElevatedButton(
                          label: 'Inscrivez-vous',
                          color: greenColor,
                          handleButton: () => _navigateToLogin(),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return SmFutureBuilderWaiting();
          }
        },
      ),
    );
  }
}
