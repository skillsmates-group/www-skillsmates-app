import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/data/forum_data.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/sm_forum.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../themes/picture_file.dart';
import '../../widgets/responsive.dart';

class ForumsCard extends StatefulWidget {
  const ForumsCard({Key? key}) : super(key: key);

  @override
  State<ForumsCard> createState() => _ForumsCardState();
}

class _ForumsCardState extends State<ForumsCard> {
  bool _shouldExpand = false;

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);

    return Card(
        margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
        elevation: isDesktop ? 1.0 : 0.0,
        shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
        child: Container(
            width: 250,
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
            color: Colors.white,
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              ExpansionTile(
                  leading: SvgPicture.asset(forum, width: 40, height: 40),
                  title: Text('Forum (3)',
                      style: const TextStyle(
                          color: primaryColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          overflow: TextOverflow.ellipsis)),
                  trailing: Icon(_shouldExpand ? Icons.arrow_drop_down_circle : Icons.arrow_drop_down),
                  onExpansionChanged: (value) => setState(() => _shouldExpand = value),
                  children: [for (var forum in forums) SmForum(forum: forum)])
            ])));
  }
}
