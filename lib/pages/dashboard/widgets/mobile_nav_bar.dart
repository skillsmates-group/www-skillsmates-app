import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/sm_image_text.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/sm_svg_image_text.dart';
import 'package:www_skillsmates_app/pages/notifications/notification_page.dart';

import '../../../enums/menu_enum.dart';
import '../../../models/media.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../login/login_page.dart';
import '../../logout/logout_page.dart';
import '../../network/network_page.dart';
import '../../post_add/post_add_page.dart';
import '../../profile/profile_page.dart';
import '../../search/search_page.dart';
import '../dashboard_page.dart';

class MobileNavBar extends StatefulWidget {
  const MobileNavBar({Key? key}) : super(key: key);

  @override
  _MobileNavBarState createState() => _MobileNavBarState();
}

class _MobileNavBarState extends State<MobileNavBar> {
  Menu _selectedMenu = Menu.home;
  Menu _hoveredMenu = Menu.home;

  @override
  void initState() {
    super.initState();
    _getSelectedMenu();
  }

  void _getSelectedMenu() {
    String _smUrl = Uri.base.toString();
    if (_smUrl.contains(DashboardPage.routeName)) {
      _selectedMenu = Menu.home;
      _hoveredMenu = Menu.home;
    }

    if (_smUrl.contains(ProfilePage.routeName)) {
      _selectedMenu = Menu.profile;
      _hoveredMenu = Menu.profile;
    }

    if (_smUrl.contains(PostAddPage.routeName)) {
      _selectedMenu = Menu.posts;
      _hoveredMenu = Menu.posts;
    }

    if (_smUrl.contains(SearchPage.routeName)) {
      _selectedMenu = Menu.search;
      _hoveredMenu = Menu.search;
    }
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    void _navigateTo(String routeName) {
      context.push(routeName);
      // Navigator.of(context).pushNamed(
      //   routeName,
      //   arguments: {},
      // );
    }

    void _selectMenuOnTap(Menu menu) {
      this._selectedMenu = menu;
      switch (menu) {
        case Menu.search:
          _navigateTo(SearchPage.routeName);
          break;
        case Menu.home:
          _navigateTo(DashboardPage.routeName);
          break;
        case Menu.profile:
          _navigateTo(ProfilePage.routeName);
          break;
        case Menu.messages:
          // TODO: Handle this case.
          break;
        case Menu.notifications:
          _navigateTo(NotificationPage.routeName);
          break;
        case Menu.network:
          _navigateTo(NetworkPage.routeName);
          break;
        case Menu.posts:
          Provider.of<PostProvider>(context, listen: false).setSelectedMediaType(
              Media(code: 'MEDIA_TYPE_DOCUMENT', label: 'DOCUMENT', image: 'assets/images/document-bleu.svg'));
          _navigateTo(PostAddPage.routeName);
          // _navigateTo(PostAddPage.routeName);
          break;
        case Menu.plus:
          // TODO: Handle this case.
          break;
      }
    }

    void _onHoverMenu(Menu menu) {
      this._hoveredMenu = menu;
    }

    return Container(
      decoration: const BoxDecoration(
        color: whitishColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black45,
            blurRadius: 15.0,
            offset: Offset(
              0.0,
              0.75,
            ),
          ),
        ],
      ),
      height: 70,
      padding: const EdgeInsets.symmetric(
        horizontal: 2,
      ),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  _selectMenuOnTap(Menu.home);
                });
              },
              onHover: (hover) {
                setState(() {
                  _onHoverMenu(Menu.home);
                });
              },
              child: SmSvgImageText(
                isSelectedButton: _selectedMenu == Menu.home,
                name: "Accueil",
                iconImage: _hoveredMenu == Menu.home || _selectedMenu == Menu.home
                    ? "assets/images/home_selected.svg"
                    : "assets/images/home.svg",
                smBorder: 'top',
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  _selectMenuOnTap(Menu.search);
                });
              },
              onHover: (hover) {
                setState(() {
                  _onHoverMenu(Menu.search);
                });
              },
              child: SmSvgImageText(
                isSelectedButton: _selectedMenu == Menu.search,
                name: "Recherche",
                iconImage: _hoveredMenu == Menu.search || _selectedMenu == Menu.search
                    ? "assets/images/search_selected.svg"
                    : "assets/images/search.svg",
                smBorder: 'top',
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  _selectMenuOnTap(Menu.posts);
                });
              },
              onHover: (hover) {
                setState(() {
                  _onHoverMenu(Menu.posts);
                });
              },
              child: SmImageText(
                isSelectedButton: _selectedMenu == Menu.posts,
                name: "Publication",
                iconImage: _hoveredMenu == Menu.posts || _selectedMenu == Menu.posts
                    ? "assets/images/plus_colored.png"
                    : "assets/images/plus_colored.png",
                smBorder: 'top',
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  _selectMenuOnTap(Menu.network);
                });
              },
              onHover: (hover) {
                setState(() {
                  _onHoverMenu(Menu.network);
                });
              },
              child: SmSvgImageText(
                isSelectedButton: _selectedMenu == Menu.network,
                name: "Réseau",
                iconImage: _hoveredMenu == Menu.network || _selectedMenu == Menu.network
                    ? "assets/images/network_selected.svg"
                    : "assets/images/network.svg",
                smBorder: 'top',
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  _selectMenuOnTap(Menu.plus);
                });
              },
              onHover: (hover) {
                setState(() {
                  _onHoverMenu(Menu.plus);
                });
              },
              child: PopupMenuButton(
                child: SmSvgImageText(
                  isSelectedButton: _selectedMenu == Menu.plus,
                  name: "Plus",
                  iconImage: _hoveredMenu == Menu.plus || _selectedMenu == Menu.plus
                      ? "assets/images/plus-mobile-selected.svg"
                      : "assets/images/plus-mobile.svg",
                  smBorder: 'top',
                ),
                onSelected: (result) {
                  if (result.toString() == LogoutPage.routeName) {
                    authProvider.logout().then((value) => {
                          context.push(LoginPage.routeName)
                          // Navigator.of(context).pushReplacementNamed(
                          //   LoginPage.routeName,
                          // )
                        });
                  } else {
                    _navigateTo(result.toString());
                  }
                },
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      child: ListTile(
                        leading: SvgPicture.asset(avatar, height: 25, width: 25),
                        title: Text("Mon profil"),
                      ),
                      value: ProfilePage.routeName,
                    ),
                    PopupMenuItem(
                      child: ListTile(
                        leading: SvgPicture.asset(settings, height: 25, width: 25),
                        title: Text("Paramétres"),
                      ),
                      value: '/settings',
                    ),
                    PopupMenuItem(
                      child: ListTile(
                        leading: SvgPicture.asset(assistance, height: 25, width: 25),
                        title: Text("Assistance"),
                      ),
                      value: '/assistance',
                    ),
                    PopupMenuItem(
                      child: ListTile(
                        leading: SvgPicture.asset(logout, height: 25, width: 25),
                        title: Text("Deconnexion"),
                      ),
                      value: LogoutPage.routeName,
                    )
                  ];
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
