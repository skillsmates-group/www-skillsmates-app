import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/messagerie/messagerie_page.dart';
import 'package:www_skillsmates_app/pages/network/network_page.dart';
import 'package:www_skillsmates_app/pages/notifications/notification_page.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '/pages/dashboard/dashboard_page.dart';
import '/pages/post_add/post_add_page.dart';
import '/pages/profile/profile_page.dart';
import '../../../enums/menu_enum.dart';
import '../../../models/account/account.dart';
import '../../../models/media.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/palette.dart';
import '../../../themes/theme.dart';
import '../../login/login_page.dart';
import '../../search/search_page.dart';
import '../../widgets/sm_circle_button.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/user_card.dart';

class MobileTopNavBar extends StatefulWidget {
  const MobileTopNavBar({Key? key}) : super(key: key);

  @override
  _MobileTopNavBarState createState() => _MobileTopNavBarState();
}

class _MobileTopNavBarState extends State<MobileTopNavBar> {
  Menu _selectedMenu = Menu.home;
  Menu _hoveredMenu = Menu.home;
  late Account loggedAccount;

  void _navigateTo(String routeName) {
    context.push(routeName);
  }

  void _selectMenuOnTap(Menu menu) {
    this._selectedMenu = menu;
    switch (menu) {
      case Menu.search:
        _navigateTo(SearchPage.routeName);
        break;
      case Menu.home:
        _navigateTo(DashboardPage.routeName);
        break;
      case Menu.profile:
        final authProvider = Provider.of<AuthProvider>(context, listen: false);
        authProvider.setProfileAccount(loggedAccount).then((value) => {_navigateTo(ProfilePage.routeName)});
        break;
      case Menu.messages:
        _navigateTo(MessageriePage.routeName);
        break;
      case Menu.notifications:
        _navigateTo(NotificationPage.routeName);
        break;
      case Menu.network:
        _navigateTo(NetworkPage.routeName);
        break;
      case Menu.posts:
        Provider.of<PostProvider>(context, listen: false).setSelectedMediaType(
            Media(code: 'MEDIA_TYPE_DOCUMENT', label: 'DOCUMENT', image: 'assets/images/document-bleu.svg'));
        _navigateTo(PostAddPage.routeName);
        // _navigateTo(PostAddPage.routeName);
        break;
      case Menu.plus:
        // TODO: Handle this case.
        break;
    }
  }

  void _onHoverMenu(Menu menu) {
    this._hoveredMenu = menu;
  }

  void _navigateToLogin() {
    context.push(LoginPage.routeName);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    return Container(
        decoration: const BoxDecoration(
          color: whitishColor,
        ),
        height: 70,
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        width: double.infinity,
        child: FutureBuilder<Account>(
            future: authProvider.getLoggedAccount(),
            builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
              if (snapshot.hasData) {
                loggedAccount = snapshot.data!;
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          _selectMenuOnTap(Menu.home);
                        });
                      },
                      child: Container(
                        child: Row(
                          children: [
                            SizedBox(
                              width: 40,
                              height: 40,
                              child: SvgPicture.asset('assets/images/symbole_skillsmates.svg'),
                            ),
                            const SizedBox(width: 8),
                            const SizedBox(
                              child: Text(
                                'Skills Mates',
                                style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        badges.Badge(
                          position: badges.BadgePosition.topEnd(top: 2, end: 2),
                          badgeAnimation: badges.BadgeAnimation.slide(
                            toAnimate: true,
                            animationDuration: Duration(seconds: 300),
                          ),
                          showBadge: false,
                          badgeContent: Text(
                            '4',
                            style: TextStyle(color: Colors.white),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: _selectedMenu == Menu.messages ? Palette.skillsmatesBlue : Colors.transparent,
                                  width: 3.0,
                                ),
                              ),
                            ),
                            child: SmCircleButton(
                              icon: skillschat_active,
                              iconSize: 30.0,
                              onPressed: () {
                                setState(() {
                                  _selectMenuOnTap(Menu.messages);
                                });
                              },
                            ),
                          ),
                        ),
                        badges.Badge(
                          position: badges.BadgePosition.topEnd(top: 2, end: 2),
                          badgeAnimation: badges.BadgeAnimation.slide(
                            toAnimate: true,
                            animationDuration: Duration(seconds: 300),
                          ),
                          showBadge: (loggedAccount.notifications ?? 0) > 0,
                          badgeContent: Text(
                            '${loggedAccount.notifications ?? 0}',
                            style: TextStyle(color: Colors.white),
                          ),
                          child: SmCircleButton(
                            icon: notification,
                            iconSize: 30.0,
                            onPressed: () {
                              setState(() {
                                _selectMenuOnTap(Menu.notifications);
                              });
                            },
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: _selectedMenu == Menu.profile ? Palette.skillsmatesBlue : Colors.transparent,
                                width: 3.0,
                              ),
                            ),
                          ),
                          child: UserCard(
                              account: loggedAccount,
                              handleOnTap: () {
                                setState(() {
                                  _selectMenuOnTap(Menu.profile);
                                });
                              }),
                        ),
                      ],
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          _selectMenuOnTap(Menu.home);
                        });
                      },
                      child: Container(
                        child: Row(
                          children: [
                            SizedBox(
                              width: 40,
                              height: 40,
                              child: SvgPicture.asset('assets/images/symbole_skillsmates.svg'),
                            ),
                            const SizedBox(width: 8),
                            const SizedBox(
                              child: Text(
                                'Skills Mates',
                                style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          SizedBox(
                            width: 160,
                            child: SmElevatedButton(
                              label: 'Connectez-vous',
                              color: primaryColor,
                              handleButton: () => _navigateToLogin(),
                            ),
                          ),
                          // const SizedBox(width: 8),
                          // SizedBox(
                          //   width: 160,
                          //   child: SmElevatedButton(
                          //     label: 'Inscrivez-vous',
                          //     color: greenColor,
                          //     handleButton: () => _navigateToLogin(),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ],
                );
              } else {
                return SmFutureBuilderWaiting();
              }
            }));
  }
}
