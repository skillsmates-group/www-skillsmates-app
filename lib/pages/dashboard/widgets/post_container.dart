import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:rounded_background_text/rounded_background_text.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/post_document.dart';
import 'package:www_skillsmates_app/pages/profile/profile_page.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '/models/models.dart';
import '/pages/widgets/profile_avatar.dart';
import '/pages/widgets/responsive.dart';
import '/themes/theme.dart';
import '../../../models/notification/interactionRequest.dart';
import '../../../models/notification/interaction_response.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/interaction_provider.dart';
import '../../../providers/post_provider.dart';
import '../../post_details/post_details_page.dart';
import '../../post_edit/post_edit_page.dart';
import '../../share_post/share_post_page.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../dashboard_page.dart';

class PostContainer extends StatefulWidget {
  final Post post;
  final bool share;

  const PostContainer({
    Key? key,
    required this.post,
    this.share = false,
  }) : super(key: key);

  @override
  State<PostContainer> createState() => _PostContainerState();
}

class _PostContainerState extends State<PostContainer> {
  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    return Card(
      margin: EdgeInsets.symmetric(
        vertical: 5.0,
        horizontal: isDesktop ? 5.0 : 0.0,
      ),
      elevation: isDesktop ? 1.0 : 0.0,
      shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _PostHeader(post: widget.post),
                  const SizedBox(height: 4.0),
                  //widget.post.document != null ? const SizedBox.shrink() : const SizedBox(height: 6.0),
                ],
              ),
            ),
            PostDocument(post: widget.post),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 20,
                      color: primaryColor,
                      fontFamily: 'Raleway',
                      fontWeight: FontWeight.bold,
                    ),
                    child: Text(
                      widget.post.title ?? '',
                    ),
                  ),
                  //widget.post.title != null ? const SizedBox(height: 3.0) : const SizedBox(height: 0),
                  DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 16,
                      color: primaryColor,
                      fontFamily: 'Raleway',
                      fontWeight: FontWeight.normal,
                      fontStyle: FontStyle.italic,
                    ),
                    child: Text(
                      widget.post.documentTitle ?? '',
                    ),
                  ),
                  //widget.post.documentTitle != null ? const SizedBox(height: 3.0,) : const SizedBox(height: 0),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    widget.post.description ?? '',
                    style: TextStyle(
                      color: primaryColor,
                      fontFamily: 'Raleway',
                    ),
                  ),
                  //widget.post.documentDescription != null ? const SizedBox(height: 3.0) : const SizedBox(height: 0),
                  Text(
                    widget.post.documentDescription ?? '',
                    style: TextStyle(
                      color: primaryColor,
                      fontFamily: 'Raleway',
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  //widget.post.documentDescription != null ? const SizedBox(height: 3.0) : const SizedBox(height: 0),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Wrap(
                spacing: 8,
                runSpacing: 8,
                children: [
                  widget.post.document != null ? const SizedBox.shrink() : const SizedBox(height: 6.0),
                  if (widget.post.keywords != null)
                    for (var keyword in widget.post.keywords?.split(',') ?? [])
                      Chip(
                        elevation: 1,
                        padding: EdgeInsets.all(8),
                        backgroundColor: Colors.grey.shade200,
                        label: Text(
                          keyword,
                          style: TextStyle(
                            color: primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                ],
              ),
            ),
            if (!widget.share)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: _PostStats(post: widget.post),
              ),
          ],
        ),
      ),
    );
  }
}

class _PostHeader extends StatelessWidget {
  final Post post;

  const _PostHeader({
    Key? key,
    required this.post,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void showSuccessToast(String title, String message) {
      MotionToast.success(
        title: Text(title),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromTop,
      ).show(context);
    }

    String computePostLink(Post post) {
      return Uri.base.origin + '/post/' + post.uuid!;
    }

    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);

    void _navigateToProfile(Account account) {
      authProvider.setProfileAccount(account).then((value) => {context.push(ProfilePage.routeName)});
    }

    void _navigateToPostDetails(Post post) {
      postProvider.setCurrentPost(post).then((value) => {context.push(PostDetailsPage.routeName)});
    }

    void _navigateToEditPost(Post post) {
      postProvider.setCurrentPost(post).then((value) => {context.push(PostEditPage.routeName)});
    }

    void _deletePost(Post post) {
      postProvider.deletePost(post).then((value) => {
            showSuccessToast(
              "Suppression d'une publication",
              "Votre publication " + post.title! + " a été supprimée avec succes",
            ),
            Future<void>.delayed(
              const Duration(seconds: 3),
              () => context.go(DashboardPage.routeName),
            ),
          });
    }

    return FutureBuilder<Account>(
      future: authProvider.getLoggedAccount(),
      builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
        if (snapshot.hasData) {
          late Account loggedAccount = snapshot.data!;
          return Row(
            children: [
              InkWell(
                onTap: () {
                  _navigateToProfile(post.account!);
                },
                child: ProfileAvatar(
                  imageUrl: post.account!.avatar?.toLowerCase() ?? '',
                  isActive: post.account!.active,
                ),
              ),
              const SizedBox(width: 8.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        text: post.account!.firstname,
                        style: const TextStyle(
                          fontSize: 16.0,
                          overflow: TextOverflow.ellipsis,
                        ),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                            text: post.account!.lastname,
                            style: const TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          '${post.account!.country ?? ''} • ${post.account!.city ?? ''} • ${post.durationText()} • ',
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 12.0,
                          ),
                        ),
                        Icon(
                          Icons.public,
                          color: Colors.grey[600],
                          size: 12.0,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              PopupMenuButton(
                child: const Icon(Icons.more_horiz),
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      child: ListTile(
                        leading: const Icon(Icons.remove_red_eye_outlined),
                        title: Text("Consulter"),
                      ),
                      value: post.uuid,
                      onTap: () {
                        _navigateToPostDetails(post);
                      },
                    ),
                    if (loggedAccount.uuid == post.account?.uuid)
                      PopupMenuItem(
                        child: ListTile(
                          leading: const Icon(Icons.edit),
                          title: Text("Modifier"),
                        ),
                        value: post.uuid,
                        onTap: () {
                          _navigateToEditPost(post);
                        },
                      ),
                    PopupMenuItem(
                      child: ListTile(
                        leading: const Icon(Icons.link),
                        title: Text("Copier le lien"),
                      ),
                      value: post.uuid,
                      onTap: () async {
                        await Clipboard.setData(ClipboardData(text: computePostLink(post)));
                        showSuccessToast('Copie du lien', 'lien de la publication copié avec succés');
                      },
                    ),
                    // PopupMenuItem(
                    //   child: ListTile(
                    //     leading: const Icon(Icons.doorbell),
                    //     title: Text("Activer les notifications"),
                    //   ),
                    //   value: '/about',
                    // ),
                    if (loggedAccount.uuid == post.account?.uuid)
                      PopupMenuItem(
                        child: ListTile(
                          leading: const Icon(Icons.delete),
                          title: Text("Supprimer"),
                        ),
                        value: post.uuid,
                        onTap: () {
                          Future<void>.delayed(
                            const Duration(), // OR const Duration(milliseconds: 500),
                            () => showDialog(
                              context: context,
                              barrierColor: Colors.black26,
                              builder: (context) => AlertDialog(
                                title: Text("Suppression"),
                                content: Text("Voulez-vous vraiment supprimer cette publication ?"),
                                actions: [
                                  TextButton(
                                    child: Text("NON"),
                                    onPressed: () {
                                      Navigator.pop(context, false);
                                    },
                                  ),
                                  TextButton(
                                    child: Text("OUI"),
                                    onPressed: () {
                                      _deletePost(post);
                                      Navigator.pop(context, false);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                  ];
                },
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Row(
            children: [
              InkWell(
                onTap: () {
                  _navigateToProfile(post.account!);
                },
                child: ProfileAvatar(
                  imageUrl: post.account!.avatar?.toLowerCase() ?? '',
                  isActive: post.account!.active,
                ),
              ),
              const SizedBox(width: 8.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        text: post.account!.firstname,
                        style: const TextStyle(
                          fontSize: 16.0,
                          overflow: TextOverflow.ellipsis,
                        ),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                            text: post.account!.lastname,
                            style: const TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          '${post.account!.country ?? ''} • ${post.account!.city ?? ''} • ${post.durationText()} • ',
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 12.0,
                          ),
                        ),
                        Icon(
                          Icons.public,
                          color: Colors.grey[600],
                          size: 12.0,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              PopupMenuButton(
                child: const Icon(Icons.more_horiz),
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      child: ListTile(
                        leading: const Icon(Icons.remove_red_eye_outlined),
                        title: Text("Consulter"),
                      ),
                      value: post.uuid,
                      onTap: () {
                        _navigateToPostDetails(post);
                      },
                    ),
                    PopupMenuItem(
                      child: ListTile(
                        leading: const Icon(Icons.link),
                        title: Text("Copier le lien"),
                      ),
                      value: post.uuid,
                      onTap: () async {
                        await Clipboard.setData(ClipboardData(text: computePostLink(post)));
                        showSuccessToast('Copie du lien', 'lien de la publication copié avec succés');
                      },
                    ),
                    // PopupMenuItem(
                    //   child: ListTile(
                    //     leading: const Icon(Icons.doorbell),
                    //     title: Text("Activer les notifications"),
                    //   ),
                    //   value: '/about',
                    // ),
                  ];
                },
              ),
            ],
          );
        } else {
          return SmFutureBuilderWaiting();
        }
      },
    );
  }
}

class _PostStats extends StatefulWidget {
  final Post post;

  const _PostStats({
    Key? key,
    required this.post,
  }) : super(key: key);

  @override
  State<_PostStats> createState() => _PostStatsState();
}

class _PostStatsState extends State<_PostStats> {
  final commentController = TextEditingController();
  bool showComment = false;

  void _deleteMyComment(String commentUuid) {
    InteractionProvider interactionProvider = Provider.of<InteractionProvider>(context, listen: false);
    interactionProvider.deleteInteraction(commentUuid);
    setState(() {
      showComment = false;
    });
  }

  void showSuccessToast(String title, String message) {
    MotionToast.success(
      title: Text(title),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void showInfoToast(String title, String message) {
    MotionToast.info(
      title: Text(title),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  void showErrorToast(String title, String message) {
    MotionToast.error(
      title: Text(title),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final interactionProvider = Provider.of<InteractionProvider>(context, listen: false);
    InteractionRequest interaction;

    onTapShare() {
      context.push(SharePostPage.routeName, extra: widget.post);
    }

    onToggleComments() {
      setState(() {
        showComment = !showComment;
      });
    }

    onTapComment() {
      if (commentController.text.isNotEmpty) {
        authProvider.getLoggedAccount().then((account) => {
              interaction = InteractionRequest(
                entity: widget.post.uuid!,
                interactionType: 'INTERACTION_TYPE_COMMENT',
                emitter: account.uuid,
                content: commentController.text,
                receiver: widget.post.account?.uuid ?? '',
              ),
              interactionProvider.saveInteraction(interaction).then((value) => {
                    setState(() {
                      widget.post.comments = widget.post.comments++;
                    }),
                    commentController.text = "",
                    showSuccessToast("Commentaire", "Vous avez commenté la publication : " + widget.post.title!),
                  })
            });
      } else {
        showInfoToast("Commentaire", "Veuillez entrer un commentaire");
      }
    }

    onTapLike() {
      setState(() {
        if (widget.post.liked) {
          widget.post.likes--;
        } else {
          widget.post.likes++;
        }
        widget.post.liked = !widget.post.liked;
      });
      authProvider.getLoggedAccount().then((account) => {
            interaction = InteractionRequest(
              entity: widget.post.uuid!,
              interactionType: 'INTERACTION_TYPE_LIKE',
              emitter: account.uuid,
              receiver: widget.post.account?.uuid ?? '',
            ),
            interactionProvider.saveInteraction(interaction).then((value) => {
                  showSuccessToast("J'aime", "Vous avez aimé la publication : " + widget.post.title!),
                })
          });
    }

    bool isToggleableInteraction(String interactionType) {
      return 'INTERACTION_TYPE_LIKE' == interactionType ||
          'INTERACTION_TYPE_FAVORITE' == interactionType ||
          'INTERACTION_TYPE_FOLLOWER' == interactionType;
    }

    return Column(
      children: [
        Row(
          children: [
            Container(
              padding: const EdgeInsets.all(4.0),
              child: SvgPicture.asset(
                widget.post.liked ? liked : like,
                width: 20,
                height: 20,
              ),
            ),
            const SizedBox(width: 4.0),
            Expanded(
              child: InkWell(
                onTap: () {},
                child: Text(
                  '${widget.post.likes}',
                  style: TextStyle(
                    color: Colors.grey[600],
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () => onToggleComments(),
              child: Text(
                '${widget.post.comments} Commentaires',
                style: TextStyle(
                  color: Colors.grey[600],
                ),
              ),
            ),
            const SizedBox(width: 8.0),
            InkWell(
              onTap: () {},
              child: Text(
                '${widget.post.shares} Partages',
                style: TextStyle(
                  color: Colors.grey[600],
                ),
              ),
            )
          ],
        ),
        const Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _PostButton(
              image: SvgPicture.asset(
                like,
                width: 20,
                height: 20,
              ),
              label: 'J\'aime',
              onTap: () => onTapLike(),
            ),
            _PostButton(
              image: SvgPicture.asset(
                comment_inactive,
                width: 20,
                height: 20,
              ),
              label: 'Commenter',
              onTap: () => onToggleComments(),
            ),
            _PostButton(
              image: SvgPicture.asset(
                share_inactive,
                width: 20,
                height: 20,
              ),
              label: 'Partager',
              onTap: () => onTapShare(),
            ),
          ],
        ),
        const Divider(),
        Center(
          child: ListTile(
            title: TextField(
              maxLines: 3,
              minLines: 1,
              maxLength: 255,
              controller: commentController,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.shade200,
                border: InputBorder.none,
                hintText: 'Ajouter un commentaire',
              ),
            ),
            trailing: InkWell(
              onTap: onTapComment,
              child: SvgPicture.asset(send_message, height: 25, width: 25),
            ),
          ),
        ),
        if (showComment)
          FutureBuilder<InteractionResponse>(
              future: interactionProvider.fetchInteractionsByTypeAndEntity(
                  'INTERACTION_TYPE_COMMENT', widget.post.uuid!, 0, 5),
              builder: (BuildContext context, AsyncSnapshot<InteractionResponse> snapshot) {
                if (snapshot.hasData) {
                  InteractionResponse interactionResponse = snapshot.data!;
                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: interactionResponse.resources?.length,
                      itemBuilder: (ctx, i) => ListTile(
                          leading: ProfileAvatar(
                            imageUrl: interactionResponse.resources![i].emitter.avatar?.toLowerCase() ?? '',
                            isActive: interactionResponse.resources![i].emitter.active,
                          ),
                          title: RoundedBackgroundText(
                            interactionResponse.resources![i].content!,
                            style: const TextStyle(fontWeight: FontWeight.normal),
                            backgroundColor: Colors.grey.shade200,
                          ),
                          trailing: interactionResponse.resources![i].emitter.uuid == authProvider.loggedAccount.uuid
                              ? PopupMenuButton(itemBuilder: (context) {
                                  return [
                                    PopupMenuItem(
                                        child: ListTile(title: Text('Supprimer')),
                                        onTap: () {
                                          _deleteMyComment(interactionResponse.resources![i].uuid!);
                                        }),
                                    //PopupMenuItem(child: ListTile(title: Text('Modifier')))
                                  ];
                                })
                              : Spacer()));
                } else if (snapshot.hasError) {
                  return SmFutureBuilderError();
                } else {
                  return SmFutureBuilderWaiting();
                }
              })
      ],
    );
  }
}

class _PostButton extends StatelessWidget {
  final Widget image;
  final String label;
  final Function() onTap;

  const _PostButton({
    Key? key,
    required this.image,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        color: Colors.white,
        child: InkWell(
          onTap: onTap,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 4.0),
            height: 25.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                image,
                const SizedBox(width: 4.0),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(label),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
