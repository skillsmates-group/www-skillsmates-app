import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mime/mime.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';
import 'package:www_skillsmates_app/themes/theme.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import '../../../models/posts/post.dart';
import '../../../services/download_service.dart';

class PostDocument extends StatefulWidget {
  final Post post;

  PostDocument({
    Key? key,
    required this.post,
  }) : super(key: key);

  @override
  State<PostDocument> createState() => PostDocumentState();
}

class PostDocumentState extends State<PostDocument> {
  String? getMediaType(String path) {
    if (isYouTubeUrl(path)) {
      return 'YOUTUBE';
    }

    final mimeType = lookupMimeType(path);

    if (mimeType != null) {
      if (mimeType.startsWith('image/')) {
        return 'IMAGE';
      } else if (mimeType.startsWith('application/msword')) {
        return 'DOCUMENT';
      } else if (mimeType.startsWith('application/pdf')) {
        return 'PDF';
      } else if (mimeType.startsWith('application/audio')) {
        return 'AUDIO';
      } else if (mimeType.startsWith('application/video')) {
        return 'VIDEO';
      }
    }
    return 'LINK';
  }

  Future<void> _downloadFile(String url) async {
    DownloadService downloadService = kIsWeb ? WebDownloadService() : MobileDownloadService();
    await downloadService.download(url: url);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.post.thumbnail == null) return const SizedBox.shrink();
    if (getMediaType(widget.post.link ?? '') == 'YOUTUBE')
      return Center(
        child: YoutubePlayerControllerProvider(
          controller: YoutubePlayerController(
            initialVideoId: getIdFromUrl(widget.post.link ?? '') ?? 'nPt8bK2gbaU',
            params: YoutubePlayerParams(
              // playlist: ['nPt8bK2gbaU', 'gQDByCdjUXw'], // Defining custom playlist
              startAt: Duration(seconds: 10),
              showControls: true,
              showFullscreenButton: true,
              autoPlay: false,
              mute: true,
            ),
          ),
          child: YoutubePlayerIFrame(
            aspectRatio: 16 / 9,
          ),
        ),
      );
    if (getMediaType(widget.post.link ?? '') == 'IMAGE')
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Center(
          child: CachedNetworkImage(
            imageUrl: widget.post.thumbnail!,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => SvgPicture.asset(
              video_rouge,
              height: 400,
            ),
          ),
        ),
      );
    if (getMediaType(widget.post.link ?? '') == 'PDF')
      return Center(
        child: Column(
          children: [
            Container(
              color: primaryColor,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    onPressed: () {
                      _downloadFile(widget.post.link ?? '');
                    },
                    icon: Icon(
                      Icons.download,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 600,
              height: 550,
              child: SfPdfViewer.network(
                widget.post.link ?? '',
                canShowPaginationDialog: true,
                scrollDirection: PdfScrollDirection.horizontal,
                pageLayoutMode: PdfPageLayoutMode.single,
              ),
            ),
          ],
        ),
      );
    if (getMediaType(widget.post.link ?? '') == 'VIDEO')
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Center(
          child: CachedNetworkImage(
            imageUrl: widget.post.thumbnail!,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => SvgPicture.asset(
              video_rouge,
              height: 200,
            ),
          ),
        ),
      );
    if (getMediaType(widget.post.link ?? '') == 'AUDIO')
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Center(
          child: CachedNetworkImage(
            imageUrl: widget.post.thumbnail!,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => SvgPicture.asset(
              audio_rose,
              height: 200,
            ),
          ),
        ),
      );
    if (getMediaType(widget.post.link ?? '') == 'DOCUMENT')
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Center(
          child: CachedNetworkImage(
            imageUrl: widget.post.thumbnail!,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => SvgPicture.asset(
              document_bleu,
              height: 200,
            ),
          ),
        ),
      );
    if (getMediaType(widget.post.link ?? '') == 'LINK') {
      return Container(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: InkWell(
          onTap: () {
            launchUrlString(widget.post.link!);
          },
          child: Center(
            child: CachedNetworkImage(
              imageUrl: widget.post.thumbnail!,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => SvgPicture.asset(
                lien_jaune,
                height: 200,
              ),
            ),
          ),
        ),
      );
    }
    return Text('');
  }

  bool isYouTubeUrl(String content) {
    RegExp regExp = RegExp(
        r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?');
    String? matches = regExp.stringMatch(content);
    if (matches == null) {
      return false;
    }
    return true;
  }
}
