import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/account/account.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../providers/account_provider.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/responsive.dart';

class ProfilesOnline extends StatefulWidget {
  const ProfilesOnline({Key? key}) : super(key: key);

  @override
  State<ProfilesOnline> createState() => _ProfilesOnlineState();
}

class _ProfilesOnlineState extends State<ProfilesOnline> {
  bool _customTileExpanded = false;

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);
    return Card(
      margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
      elevation: isDesktop ? 1.0 : 0.0,
      shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
      child: Container(
        width: 250,
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 8.0,
        ),
        color: Colors.white,
        child: FutureBuilder<List<Account>>(
          future: accountProvider.findAccountOnline(),
          builder: (BuildContext context, AsyncSnapshot<List<Account>> snapshot) {
            if (snapshot.hasData) {
              List<Account>? onlineNow = snapshot.data;
              return Column(
                children: [
                  ExpansionTile(
                    leading: SvgPicture.asset(online, height: 25, width: 25),
                    title: Text(
                      "En ligne (${onlineNow?.length})",
                      style: const TextStyle(
                        color: primaryColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    trailing: Icon(
                      _customTileExpanded ? Icons.arrow_drop_down_circle : Icons.arrow_drop_down,
                    ),
                    children: List.generate(10, (index) {
                      return ListTile(
                          leading: ProfileAvatar(
                              imageUrl: onlineNow?[index].avatar?.toLowerCase() ?? "",
                              isActive: onlineNow?[index].active ?? false,
                              radius: 20.0),
                          title: Text('${onlineNow?[index].firstname} ${onlineNow?[index].lastname}',
                              style: TextStyle(fontSize: 13)));
                    }),
                    onExpansionChanged: (bool expanded) {
                      setState(() => _customTileExpanded = expanded);
                    },
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          },
        ),
      ),
    );
  }
}
