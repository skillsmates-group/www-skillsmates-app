import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:www_skillsmates_app/data/skillbox_data.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/sm_skillbox.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';

class SkillsboxCard extends StatefulWidget {
  const SkillsboxCard({Key? key}) : super(key: key);

  @override
  State<SkillsboxCard> createState() => _SkillsboxCardState();
}

class _SkillsboxCardState extends State<SkillsboxCard> {
  bool _customTileExpanded = false;

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    return Card(
      margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
      elevation: isDesktop ? 1.0 : 0.0,
      shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
      child: Container(
        width: 250,
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 8.0,
        ),
        color: Colors.white,
        child: Column(
          children: [
            ExpansionTile(
              leading: SvgPicture.asset(library, height: 40, width: 40),
              title: Text(
                "SkillsBox (3)",
                style: const TextStyle(
                  color: primaryColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              trailing: Icon(
                _customTileExpanded ? Icons.arrow_drop_down_circle : Icons.arrow_drop_down,
              ),
              children: List.generate(3, (index) {
                return SmSkillbox(skillbox: skillboxes[index]);
              }),
              onExpansionChanged: (bool expanded) {
                setState(() => _customTileExpanded = expanded);
              },
            ),
          ],
        ),
      ),
    );
  }
}
