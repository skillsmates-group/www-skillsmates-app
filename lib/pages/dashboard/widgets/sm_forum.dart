import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:www_skillsmates_app/models/forum.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../widgets/profile_avatar.dart';

class SmForum extends StatelessWidget {
  final Forum forum;
  const SmForum({
    Key? key,
    required this.forum,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {},
                child: ProfileAvatar(
                  imageUrl: forum.account.avatar ?? '',
                  isActive: forum.account.active,
                ),
              ),
              const SizedBox(width: 8.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        text: forum.account.firstname,
                        style: const TextStyle(
                          fontSize: 16.0,
                          overflow: TextOverflow.ellipsis,
                        ),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                            text: forum.account.lastname,
                            style: const TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          forum.duration(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 12.0,
                          ),
                        ),
                        Icon(
                          Icons.public,
                          color: Colors.grey[600],
                          size: 12.0,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(forum.content),
              ],
            ),
          ),
          ListTile(
            title: Text.rich(
              TextSpan(
                text: 'Discipline: ',
                style: const TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: forum.discipline,
                    style: const TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            trailing: Text.rich(
              TextSpan(
                children: [
                  WidgetSpan(
                    child: SvgPicture.asset(
                      comment_active,
                      height: 16,
                      width: 16,
                    ),
                  ),
                  TextSpan(
                    text: ' ${forum.comments}',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
          ),
          // SizedBox(
          //   height: 10,
          // ),
          Divider(
            height: 1,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
