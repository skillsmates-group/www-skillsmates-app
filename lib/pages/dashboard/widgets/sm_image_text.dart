import 'package:flutter/material.dart';

import '../../../../../themes/theme.dart';
import '../../../themes/palette.dart';

Widget SmImageText({
  required String iconImage,
  required String name,
  required bool isSelectedButton,
  String smBorder = '',
}) {
  return Container(
    height: 70,
    width: 80,
    decoration: BoxDecoration(
      border: Border(
        bottom: smBorder == 'bottom'
            ? BorderSide(
                color: isSelectedButton == true ? Palette.skillsmatesBlue : Colors.transparent,
                width: 3.0,
              )
            : BorderSide.none,
        top: smBorder == 'top'
            ? BorderSide(
                color: isSelectedButton == true ? Palette.skillsmatesBlue : Colors.transparent,
                width: 3.0,
              )
            : BorderSide.none,
      ),
    ),
    margin: const EdgeInsets.symmetric(horizontal: 5),
    child: Column(
      children: [
        SizedBox(
          width: 40,
          height: 50,
          child: Image(image: ExactAssetImage(iconImage)),
        ),
        Text(
          name,
          style: const TextStyle(fontSize: 12, color: primaryColor),
        )
      ],
    ),
  );
}
