import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:www_skillsmates_app/models/skillbox.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../widgets/profile_avatar.dart';

class SmSkillbox extends StatelessWidget {
  final Skillbox skillbox;
  const SmSkillbox({
    Key? key,
    required this.skillbox,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            leading: SvgPicture.asset(
              doc,
              height: 50,
              width: 50,
            ),
            title: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(skillbox.post.description ?? ''),
                ],
              ),
            ),
          ),
          Row(
            children: [
              InkWell(
                onTap: () {},
                child: ProfileAvatar(
                  imageUrl: skillbox.account.avatar ?? '',
                  isActive: skillbox.account.active,
                ),
              ),
              const SizedBox(width: 8.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        text: skillbox.account.firstname,
                        style: const TextStyle(
                          fontSize: 16.0,
                          overflow: TextOverflow.ellipsis,
                        ),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                            text: skillbox.account.lastname,
                            style: const TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          skillbox.post.durationText(),
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 12.0,
                          ),
                        ),
                        Icon(
                          Icons.public,
                          color: Colors.grey[600],
                          size: 12.0,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 8.0),
          Divider(
            height: 1,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
