import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '/themes/theme.dart';
import '../../../themes/palette.dart';

Widget SmSvgImageText({
  required String iconImage,
  String name = '',
  required bool isSelectedButton,
  String smBorder = '',
  double height = 50,
  int badge = 0,
}) {
  return Container(
    // height: height,
    // width: width,
    decoration: BoxDecoration(
      border: Border(
        bottom: smBorder == 'bottom'
            ? BorderSide(
                color: isSelectedButton == true ? Palette.skillsmatesBlue : Colors.transparent,
                width: 3.0,
              )
            : BorderSide.none,
        top: smBorder == 'top'
            ? BorderSide(
                color: isSelectedButton == true ? Palette.skillsmatesBlue : Colors.transparent,
                width: 3.0,
              )
            : BorderSide.none,
      ),
    ),
    margin: const EdgeInsets.symmetric(horizontal: 5),
    child: Column(
      children: [
        SizedBox(
          width: 40,
          height: height,
          child: badges.Badge(
            position: badges.BadgePosition.topEnd(top: -1, end: -2),
            badgeAnimation: badges.BadgeAnimation.slide(
              toAnimate: true,
              animationDuration: Duration(seconds: 300),
            ),
            showBadge: badge > 0,
            badgeContent: Text(
              badge.toString(),
              style: TextStyle(color: Colors.white),
            ),
            child: SvgPicture.asset(
              iconImage,
              color: primaryColor,
            ),
          ),
        ),
        if (name != '')
          Text(
            name,
            style: const TextStyle(
              fontSize: 12,
              color: primaryColor,
            ),
          ),
      ],
    ),
  );
}
