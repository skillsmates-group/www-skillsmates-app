import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../models/account/account.dart';
import '../../../models/story_model.dart';
import '../../../themes/palette.dart';
import '../../widgets/responsive.dart';

class Suggestion extends StatelessWidget {
  final bool isAddStory;
  final Account? currentUser;
  final Story? story;

  const Suggestion({
    Key? key,
    this.isAddStory = false,
    this.currentUser,
    this.story,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(12.0),
          child: CachedNetworkImage(
            imageUrl: currentUser?.avatar?.toLowerCase() ?? '',
            height: double.infinity,
            width: 110.0,
            fit: BoxFit.cover,
          ),
        ),
        Container(
          height: double.infinity,
          width: 110.0,
          decoration: BoxDecoration(
            gradient: Palette.storyGradient,
            borderRadius: BorderRadius.circular(12.0),
            boxShadow: Responsive.isDesktop(context)
                ? const [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0, 2),
                      blurRadius: 4.0,
                    ),
                  ]
                : null,
          ),
        ),
        Positioned(
          top: 8.0,
          left: 8.0,
          child: Container(
            height: 20.0,
            width: 20.0,
            decoration: const BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
            ),
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: const Icon(Icons.class_),
              iconSize: 10.0,
              color: Palette.skillsmatesBlue,
              onPressed: () => print('Add to people'),
            ),
          ),
        ),
        Positioned(
          bottom: 8.0,
          left: 8.0,
          right: 8.0,
          child: Column(
            children: [
              Text.rich(
                TextSpan(
                  text: currentUser?.firstname,
                  style: const TextStyle(
                    fontSize: 16.0,
                    overflow: TextOverflow.ellipsis,
                  ),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                      text: currentUser?.lastname,
                      style: const TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w700,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                icon: const Icon(Icons.person_add),
                iconSize: 30.0,
                color: Palette.scaffold,
                onPressed: () => print('Add to people'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
