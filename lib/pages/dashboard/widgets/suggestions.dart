import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/pages/widgets/responsive.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../widgets/story_card.dart';

class Suggestions extends StatelessWidget {
  Suggestions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loggedAccount = Provider.of<AuthProvider>(context).loggedAccount;
    return Container(
      height: 260.0,
      color: Responsive.isDesktop(context) ? Colors.transparent : Colors.white,
      child: FutureBuilder(
        future: Provider.of<AccountProvider>(context, listen: false).fetchAccountSuggestions(loggedAccount.uuid),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (dataSnapshot.error != null) {
              return Center(
                child: Text('Une erreur est survenue lors de la recuperation des suggestions'),
              );
            } else {
              return Consumer<AccountProvider>(
                builder: (ctx, data, child) => ListView.builder(
                  padding: const EdgeInsets.symmetric(
                    // vertical: 10.0,
                    horizontal: 8.0,
                  ),
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: data.accountResponse.resources?.length,
                  itemBuilder: (ctx, i) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0),
                    child: StoryCard(
                      isAddStory: true,
                      account: data.accountResponse.resources![i],
                      bottomSpace: false,
                    ),
                  ),
                ),
              );
            }
          }
        },
      ),
    );
  }
}
