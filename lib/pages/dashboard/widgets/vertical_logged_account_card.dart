import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/documents_shelf_page.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_on_hover.dart';

import '/pages/profile/profile_page.dart';
import '../../../models/account/account.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/palette.dart';
import '../../../themes/picture_file.dart';
import '../../network/network_page.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_svg_icon_text.dart';

class VerticalLoggedAccountCard extends StatelessWidget {
  VerticalLoggedAccountCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);

    void _navigateToProfile(Account account) {
      authProvider.setProfileAccount(account).then((value) => {
            context.push(ProfilePage.routeName)
            // Navigator.of(context).pushReplacementNamed(
            //   ProfilePage.routeName,
            // )
          });
    }

    void _navigateToDocumentShelf(Account account) {
      authProvider.setProfileAccount(account).then((value) => {
            context.push(DocumentsShelfPage.routeName)
            // Navigator.of(context).pushReplacementNamed(
            //   DocumentsShelfPage.routeName,
            // )
          });
    }

    void _navigateToNetwork(Account account, {int selectedTab = 0}) {
      authProvider.setProfileAccount(account).then((value) => {
            context.push(NetworkPage.routeName, extra: selectedTab)
            /*Navigator.of(context).pushNamed(
              NetworkPage.routeName,
              arguments: selectedTab,
            )*/
          });
    }

    final bool isDesktop = Responsive.isDesktop(context);
    return Card(
      margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
      elevation: isDesktop ? 1.0 : 0.0,
      shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
      child: FutureBuilder<Account>(
        future: authProvider.getLoggedAccount(),
        builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
          if (snapshot.hasData) {
            late Account account = snapshot.data!;
            return Container(
              width: 250,
              padding: const EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 8.0,
              ),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SmSvgIconText(
                    image: account.statusIcon(),
                    text: Text(''),
                    color: Palette.skillsmatesBlue,
                  ),
                  InkWell(
                    onTap: () {
                      _navigateToProfile(account);
                    },
                    child: ProfileAvatar(
                      imageUrl: account.avatar?.toLowerCase() ?? "",
                      isActive: account.active,
                      radius: 70.0,
                    ),
                  ),
                  SmSvgIconText(
                    image: avatar,
                    text: Text.rich(
                      TextSpan(
                        text: account.firstname,
                        style: const TextStyle(
                          overflow: TextOverflow.ellipsis,
                        ),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                            text: account.lastname,
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    color: Palette.skillsmatesBlue,
                  ),
                  SmSvgIconText(
                    image: account.statusIcon(),
                    text: Text(account.currentJob ?? ''),
                    color: Palette.skillsmatesBlue,
                  ),
                  SmSvgIconText(
                    image: company,
                    text: Text(account.currentCompany ?? ''),
                    color: Palette.skillsmatesBlue,
                  ),
                  SmSvgIconText(
                    image: location,
                    text: Text((account.city ?? ' ') + (' - ') + (account.country ?? ' ')),
                    color: Palette.skillsmatesBlue,
                  ),
                  const Divider(height: 10.0, thickness: 0.5),
                  SmOnHover(
                    callbackOnTap: (s) {
                      _navigateToDocumentShelf(account);
                    },
                    child: SmSvgIconText(
                      image: publications,
                      text: Text('${account.posts} Publications'),
                      color: Palette.skillsmatesBlue,
                    ),
                  ),
                  SmOnHover(
                    callbackOnTap: (s) {
                      _navigateToNetwork(account, selectedTab: 1);
                    },
                    child: SmSvgIconText(
                      image: follower,
                      text: Text('${account.followers} Abonnés'),
                      color: Palette.skillsmatesBlue,
                    ),
                  ),
                  SmOnHover(
                    callbackOnTap: (s) {
                      _navigateToNetwork(account, selectedTab: 2);
                    },
                    child: SmSvgIconText(
                      image: followee,
                      text: Text('${account.followees} Abonnements'),
                      color: Palette.skillsmatesBlue,
                    ),
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        },
      ),
    );
  }
}
