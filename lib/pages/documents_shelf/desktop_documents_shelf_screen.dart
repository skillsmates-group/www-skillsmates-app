import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/widgets/desktop_documents_shelf_content.dart';

import '/pages/dashboard/widgets/desktop_nav_bar.dart';

class DesktopDocumentsShelfScreen extends StatefulWidget {
  const DesktopDocumentsShelfScreen({Key? key}) : super(key: key);

  @override
  State<DesktopDocumentsShelfScreen> createState() => _DesktopDocumentsShelfScreenState();
}

class _DesktopDocumentsShelfScreenState extends State<DesktopDocumentsShelfScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DesktopDocumentsShelfContent(),
          ],
        ),
      ),
    );
  }
}
