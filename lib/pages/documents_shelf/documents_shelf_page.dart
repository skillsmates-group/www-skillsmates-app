import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/desktop_documents_shelf_screen.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/mobile_documents_shelf_screen.dart';

class DocumentsShelfPage extends StatelessWidget {
  const DocumentsShelfPage({Key? key}) : super(key: key);
  static const routeName = '/documents-shelf';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const DesktopDocumentsShelfScreen();
      }
      if (sizingInformation.isTablet) {
        return const MobileDocumentsShelfScreen();
      }
      return const MobileDocumentsShelfScreen();
    });
  }
}
