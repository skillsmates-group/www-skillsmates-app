import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../data/media_data.dart';
import '../../../models/documents/document_type_metric.dart';

class ClasseurExpansionTile extends StatefulWidget {
  List<DocumentTypeMetric> documentTypes;
  List<DocumentTypeMetric>? selectedDocumentTypes;
  Function(DocumentTypeMetric)? callback;
  Function(bool?, List<DocumentTypeMetric>)? callbackSelectAll;

  ClasseurExpansionTile(
      {Key? key, required this.documentTypes, this.selectedDocumentTypes, this.callback, this.callbackSelectAll})
      : super(key: key);

  @override
  State<ClasseurExpansionTile> createState() => _ClasseurExpansionTileState();
}

class _ClasseurExpansionTileState extends State<ClasseurExpansionTile> {
  clickDocumentType(DocumentTypeMetric documentType) {
    widget.callback!(documentType);
  }

  clickSelectAllOption(bool? newValue) {
    widget.callbackSelectAll!(newValue, widget.documentTypes);
  }

  @override
  Widget build(BuildContext context) {
    int countTotalDocuments() {
      int total = 0;
      widget.documentTypes.forEach((element) {
        total += element.total;
      });
      return total;
    }

    bool isDocumentTypeSelected(DocumentTypeMetric documentType) {
      return widget.selectedDocumentTypes?.where((element) => element.code == documentType.code).isNotEmpty ?? false;
    }

    bool isAllSelected() {
      bool result = true;
      for (var type in widget.documentTypes) {
        result = result && isDocumentTypeSelected(type);
      }
      return result;
    }

    String getMediaTypeImage(DocumentTypeMetric documentTypeMetric) {
      return medias.where((element) => element.code == documentTypeMetric.mediaType.code).first.image;
    }

    return Column(children: [
      ExpansionTile(
          leading: SvgPicture.asset(getMediaTypeImage(widget.documentTypes.first), height: 25, width: 25),
          title: Text('${widget.documentTypes.first.mediaType.label} (${countTotalDocuments()})'),
          children: [
                ListTile(
                    leading: Checkbox(
                        value: isAllSelected(),
                        onChanged: (bool? value) {
                          clickSelectAllOption(value);
                        }),
                    title: Text("Tout", style: TextStyle(fontSize: 14)))
              ] +
              List.generate(widget.documentTypes.length, (index) {
                return ListTile(
                    leading: Checkbox(
                        value: isDocumentTypeSelected(widget.documentTypes[index]),
                        onChanged: (bool? value) {
                          clickDocumentType(widget.documentTypes[index]);
                        }),
                    title: Text('${widget.documentTypes[index].label} (${widget.documentTypes[index].total})',
                        style: TextStyle(fontSize: 14)));
              }),
          onExpansionChanged: (bool expanded) {
            // setState(() => _customTileExpanded = expanded);
          })
    ]);
  }
}
