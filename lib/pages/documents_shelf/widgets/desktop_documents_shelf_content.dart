import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/account_attribute_enum.dart';
import 'package:www_skillsmates_app/models/documents/document_type_metric.dart';
import 'package:www_skillsmates_app/models/media.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/widgets/document_shelf_tile.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';

import '../../../data/media_data.dart';
import '../../../models/account/account.dart';
import '../../../models/documents/document_type_metric_response.dart';
import '../../../models/search_param.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/theme.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_footer_row.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import 'classeur_expansion_tile.dart';

class DesktopDocumentsShelfContent extends StatefulWidget {
  const DesktopDocumentsShelfContent({Key? key}) : super(key: key);

  @override
  State<DesktopDocumentsShelfContent> createState() => _DesktopDocumentsShelfContentState();
}

class _DesktopDocumentsShelfContentState extends State<DesktopDocumentsShelfContent> {
  String tab = AccountAttribute.resume.name;
  List<DocumentTypeMetric> selectedDocumentTypes = [];
  SearchParam searchParam = SearchParam();
  int nbDocuments = 0;

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);

    clickMedia(Media media) {
      setState(() {
        tab = media.code;
      });
    }

    clickDocumentType(DocumentTypeMetric documentTypeMetric) {
      List<String> selected = [];
      setState(() {
        bool alreadySelected =
            selectedDocumentTypes.where((element) => element.code == documentTypeMetric.code).isNotEmpty;
        if (alreadySelected) {
          selectedDocumentTypes.removeWhere((element) => element.code == documentTypeMetric.code);
        } else {
          selectedDocumentTypes.add(documentTypeMetric);
        }
        selectedDocumentTypes.forEach((element) {
          selected.add(element.uuid);
        });
        searchParam.documentTypes = selected;
      });
    }

    clickSelectAll(bool? value, List<DocumentTypeMetric> all) {
      setState(() {
        selectedDocumentTypes.clear();
        if (value == true) {
          all.forEach((element) {
            selectedDocumentTypes.add(element);
          });
        }
      });
    }

    List<DocumentTypeMetric> filterDocumentTypesByMedia(Media media, List<DocumentTypeMetric> response) {
      return response.where((element) => element.mediaType.code == media.code).toList();
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: padding(),
        ),
        child: Column(
          children: [
            FutureBuilder<Account>(
                future: authProvider.getProfileAccount(),
                builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                  if (snapshot.hasData) {
                    Account profileAccount = snapshot.data!;
                    searchParam.account = profileAccount.uuid;
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Column(
                            children: [
                              FutureBuilder<DocumentTypeMetricResponse>(
                                future: postProvider.fetchProfileDocumentTypeMetric(profileAccount.uuid),
                                builder: (BuildContext context, AsyncSnapshot<DocumentTypeMetricResponse> snapshot) {
                                  if (snapshot.hasData) {
                                    return Card(
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            height: 50,
                                          ),
                                          for (var media in medias)
                                            Container(
                                              padding: const EdgeInsets.only(right: 20),
                                              decoration: BoxDecoration(
                                                  color: tab == media.code ? blueLightColor : Colors.white),
                                              child: InkWell(
                                                onTap: () => clickMedia(media),
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            padding:
                                                                const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                                                            child: ClasseurExpansionTile(
                                                                documentTypes: filterDocumentTypesByMedia(
                                                                    media, snapshot.data?.resources ?? []),
                                                                selectedDocumentTypes: selectedDocumentTypes,
                                                                callback: clickDocumentType,
                                                                callbackSelectAll: clickSelectAll),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          SizedBox(
                                            height: 50,
                                          ),
                                        ],
                                      ),
                                    );
                                  } else if (snapshot.hasError) {
                                    return SmFutureBuilderError();
                                  } else {
                                    return SmFutureBuilderWaiting();
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 9,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        '${nbDocuments} Documents partagés',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          InkWell(
                                            onTap: () => context.pop(),
                                            child: ProfileAvatar(
                                              imageUrl: profileAccount.avatar?.toLowerCase() ?? '',
                                              isActive: profileAccount.active,
                                            ),
                                          ),
                                          const SizedBox(width: 6.0),
                                          if (isDesktop)
                                            Flexible(
                                              child: InkWell(
                                                onTap: () => context.pop(),
                                                child: Text.rich(
                                                  TextSpan(
                                                    text: profileAccount.firstname,
                                                    style: const TextStyle(
                                                      fontSize: 16.0,
                                                      overflow: TextOverflow.ellipsis,
                                                    ),
                                                    children: [
                                                      TextSpan(text: ' '),
                                                      TextSpan(
                                                        text: profileAccount.lastname,
                                                        style: const TextStyle(
                                                          fontSize: 16.0,
                                                          fontWeight: FontWeight.w700,
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 0.0,
                                  horizontal: 30.0,
                                ),
                                child: Divider(color: Colors.white, thickness: 5.0),
                              ),
                              Container(
                                child: FutureBuilder(
                                  future: postProvider.searchPosts(searchParam),
                                  builder: (ctx, dataSnapshot) {
                                    if (dataSnapshot.connectionState == ConnectionState.waiting) {
                                      return SmFutureBuilderWaiting();
                                    } else {
                                      if (dataSnapshot.error != null) {
                                        return SmFutureBuilderError(
                                            error: 'Une erreur est survenue lors de la recuperation des publications');
                                      } else {
                                        return Consumer<PostProvider>(
                                          builder: (ctx, data, child) => ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: data.searchPostResponse!.resources?.length,
                                            itemBuilder: (context, index) {
                                              this.nbDocuments = data.searchPostResponse!.resources?.length ?? 0;
                                              return DocumentShelfTile(
                                                  post: data.searchPostResponse!.resources![index]);
                                            },
                                          ),
                                        );
                                      }
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return SmFutureBuilderError();
                  } else {
                    return SmFutureBuilderWaiting();
                  }
                }),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: const SmFooterRow(),
            ),
          ],
        ),
      ),
    );
  }
}
