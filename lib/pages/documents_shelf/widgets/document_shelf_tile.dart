import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/post_details/post_details_page.dart';

import '../../../providers/post_provider.dart';
import '../../../themes/picture_file.dart';

class DocumentShelfTile extends StatelessWidget {
  final Post post;

  const DocumentShelfTile({Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final postProvider = Provider.of<PostProvider>(context);

    void _navigateToPost() {
      postProvider.setCurrentPost(post).then((value) => {
            context.push(PostDetailsPage.routeName)
            // Navigator.of(context).pushReplacementNamed(
            //   PostDetailsPage.routeName,
            // )
          });
    }

    return InkWell(
      onTap: () {
        _navigateToPost();
      },
      child: Container(
        padding: EdgeInsets.all(10),
        child: ListTile(
          leading: thumbnail(post),
          title: Text(
            '${post.title}',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                post.description ?? '',
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                maxLines: 5,
              ),
              Text.rich(
                TextSpan(
                  text: 'Publiée : ',
                  style: const TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w700,
                    overflow: TextOverflow.ellipsis,
                  ),
                  children: [
                    TextSpan(
                      text: post.durationText(),
                      style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.normal,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          trailing: InkWell(
            child: const Icon(Icons.more_horiz),
            onTap: () {},
          ),
        ),
      ),
    );
  }

  Widget thumbnail(Post post) {
    if (getMediaType(post.link ?? '') == 'YOUTUBE')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: SvgPicture.asset(video, height: 50, width: 50),
      );
    if (getMediaType(post.link ?? '') == 'IMAGE')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: Image.network(post.thumbnail!, fit: BoxFit.fill),
      );
    if (getMediaType(post.link ?? '') == 'PDF')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: SvgPicture.asset(pdf, height: 50, width: 50),
      );
    if (getMediaType(post.link ?? '') == 'VIDEO')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: SvgPicture.asset(video_rouge, height: 50, width: 50),
      );
    if (getMediaType(post.link ?? '') == 'AUDIO')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: SvgPicture.asset(audio_rose, height: 50, width: 50),
      );
    if (getMediaType(post.link ?? '') == 'DOCUMENT')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: SvgPicture.asset(document_bleu, height: 50, width: 50),
      );
    if (getMediaType(post.link ?? '') == 'LINK')
      return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.only(right: 10),
        child: SvgPicture.asset(lien_jaune, height: 50, width: 50),
      );
    return const SizedBox.shrink();
  }

  String? getMediaType(String path) {
    if (isYouTubeUrl(path)) {
      return 'YOUTUBE';
    }

    final mimeType = lookupMimeType(path);

    if (mimeType != null) {
      if (mimeType.startsWith('image/')) {
        return 'IMAGE';
      } else if (mimeType.startsWith('application/msword')) {
        return 'DOCUMENT';
      } else if (mimeType.startsWith('application/pdf')) {
        return 'PDF';
      } else if (mimeType.startsWith('application/audio')) {
        return 'AUDIO';
      } else if (mimeType.startsWith('application/video')) {
        return 'VIDEO';
      }
    }
    return 'LINK';
  }

  bool isYouTubeUrl(String content) {
    RegExp regExp = RegExp(
        r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?');
    String? matches = regExp.stringMatch(content);
    if (matches == null) {
      return false;
    }
    return true;
  }
}
