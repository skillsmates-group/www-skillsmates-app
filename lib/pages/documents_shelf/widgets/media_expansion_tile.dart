import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/media.dart';
import 'package:www_skillsmates_app/pages/search/SearchProvider.dart';

import '../../../models/documents/document_type_metric.dart';

class MediaExpansionTile extends StatefulWidget {
  Media? media;
  Function(List<DocumentTypeMetric>, String)? callback;

  MediaExpansionTile({Key? key, this.media, this.callback}) : super(key: key);

  @override
  State<MediaExpansionTile> createState() => _MediaExpansionTileState();
}

class _MediaExpansionTileState extends State<MediaExpansionTile> {
  late SearchProvider _provider;
  List<DocumentTypeMetric> _all = List.empty(growable: true);
  List<DocumentTypeMetric> _selected = List.empty(growable: true);

  @override
  void initState() {
    super.initState();
    _provider = Provider.of<SearchProvider>(context, listen: false);
    _selected.clear();

    _provider.selectedDocumentTypes
        .where((element) => element.mediaType.code == widget.media!.code)
        .forEach((element1) {
      _selected.add(element1);
    });
    /*_provider.allDocumentType.where((element) => element.mediaType.code == widget.media!.code).forEach((element1) {
      _all.add(element1);
    });*/
    _all = _provider.getAllDocumentType(widget.media!);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _selected.clear();
    _provider.selectedDocumentTypes
        .where((element) => element.mediaType.code == widget.media!.code)
        .forEach((element1) {
      _selected.add(element1);
    });
    /*_provider.allDocumentType.where((element) => element.mediaType.code == widget.media!.code).forEach((element1) {
      _all.add(element1);
    });*/
    _all = _provider.getAllDocumentType(widget.media!);
  }

  toggleDocumentType(DocumentTypeMetric documentTypeMetric) {
    if (_selected.any((element) => element.uuid == documentTypeMetric.uuid)) {
      _selected.removeWhere((element) => element.uuid == documentTypeMetric.uuid);
    } else {
      _selected.add(documentTypeMetric);
    }
    widget.callback!(_selected, widget.media!.code);
  }

  toggleSelectAll(bool? value) {
    _selected.clear();
    if (value == true) {
      _all.forEach((element) {
        _selected.add(element);
      });
    }
    widget.callback!(_selected, widget.media!.code);
  }

  countTotalDocuments() {
    return _all.map((e) => e.total ?? 0).reduce((value, element) => value + element);
  }

  isAllSelected() {
    return _all.every((element1) => _provider.selectedDocumentTypes.any((element2) => element1.uuid == element2.uuid));
  }

  isDocumentTypeSelected(int index) {
    return _provider.selectedDocumentTypes.any((element) => element.uuid == _all[index].uuid);
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ExpansionTile(
          leading: SvgPicture.asset(widget.media!.image, height: 25, width: 25),
          title: Text('${widget.media?.label}'),
          children: [
                ListTile(
                    leading: Checkbox(value: isAllSelected(), onChanged: (value) => toggleSelectAll(value)),
                    title: Text("Tout", style: TextStyle(fontSize: 14)))
              ] +
              List.generate(context.watch<SearchProvider>().getAllDocumentType(widget.media!).length, (index) {
                return ListTile(
                    leading: Checkbox(
                        value: isDocumentTypeSelected(index), onChanged: (value) => toggleDocumentType(_all[index])),
                    title: Text('${_all[index].label}', style: TextStyle(fontSize: 14)));
              }),
          onExpansionChanged: (value) {})
    ]);
  }
}
