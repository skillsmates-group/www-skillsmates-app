import 'package:accordion/accordion.dart';
import 'package:accordion/controllers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../data/faq_data.dart';
import '../../models/faq.dart';
import '../../themes/picture_file.dart';
import '../widgets/responsive.dart';
import '../widgets/sm_button_back.dart';
import '../widgets/sm_footer_row.dart';

class DesktopFaqScreen extends StatelessWidget {
  const DesktopFaqScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _headerStyle = const TextStyle(color: Color(0xffffffff), fontSize: 15, fontWeight: FontWeight.bold);
    final _contentStyleHeader = const TextStyle(color: Color(0xff999999), fontSize: 14, fontWeight: FontWeight.w700);
    final _contentStyle = const TextStyle(color: Color(0xff999999), fontSize: 14, fontWeight: FontWeight.normal);

    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);
    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: padding()),
          child: Column(children: [
            Center(
              child: Text(
                "Besoin d'assistance ?".toUpperCase(),
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Row(children: [
              Expanded(
                child: Container(
                  width: 200,
                  child: Accordion(
                      maxOpenSections: 1,
                      headerBackgroundColorOpened: Colors.black54,
                      scaleWhenAnimating: true,
                      openAndCloseAnimation: true,
                      headerPadding: const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                      sectionOpeningHapticFeedback: SectionHapticFeedback.heavy,
                      sectionClosingHapticFeedback: SectionHapticFeedback.light,
                      children: [
                        for (Faq faq in faqs)
                          AccordionSection(
                            isOpen: true,
                            leftIcon: const Icon(Icons.question_answer_rounded, color: Colors.white),
                            headerBackgroundColor: Colors.grey,
                            headerBackgroundColorOpened: Colors.black,
                            header: Text(
                              faq.title,
                              style: _headerStyle,
                            ),
                            content: Text(
                              faq.content,
                              style: _contentStyle,
                            ),
                            contentHorizontalPadding: 20,
                            contentBorderWidth: 1,
                          ),
                      ]),
                ),
              ),
              Expanded(
                child: Column(children: [
                  SizedBox(height: 40),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(children: [
                          Text(
                            "les questions les plus récurrentes",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 25,
                            ),
                          ),
                          SvgPicture.asset(
                            assistanceQuestionMarkPeople,
                            height: 300,
                          ),
                        ]),
                      ]),
                ]),
              ),
            ]),
            SmButtonBack(),
            SmFooterRow()
          ]),
        ),
      ),
    );
  }
}
