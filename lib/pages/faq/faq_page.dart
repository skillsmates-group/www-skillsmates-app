import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/faq/mobile_faq_screen.dart';

import 'desktop_faq_screen.dart';

class FaqPage extends StatelessWidget {
  const FaqPage({Key? key}) : super(key: key);

  static const routeName = '/faq';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopFaqScreen();
      }
      if (sizingInformation.isTablet) {
        return MobileFaqScreen();
      }
      return MobileFaqScreen();
    });
  }
}
