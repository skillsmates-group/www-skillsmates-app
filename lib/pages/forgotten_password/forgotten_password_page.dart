import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '/pages/forgotten_password/screens/desktop_forgotten_password_screen.dart';
import '/pages/forgotten_password/screens/mobile_forgotten_password_screen.dart';

class ForgottenPasswordPage extends StatelessWidget {
  static const routeName = '/forgotten-password';
  const ForgottenPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
        builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const Padding(
          padding: EdgeInsets.all(10),
          child: DesktopForgottenPasswordScreen(),
        );
      }
      if (sizingInformation.isTablet) {
        return const DesktopForgottenPasswordScreen();
      }
      if (sizingInformation.isMobile) {
        return  MobileForgottenPasswordScreen();
      }
      return const DesktopForgottenPasswordScreen();
    });
  }
}
