import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/pages/forgotten_password/widgets/forgotten_password_card.dart';

import '../../widgets/slogan_text.dart';
import '../../widgets/sm_footer_row.dart';

class MobileForgottenPasswordScreen extends StatelessWidget {
  const MobileForgottenPasswordScreen({Key? key}) : super(key: key);
  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: SvgPicture.asset(
                logoSkillsmates,
                height: 100,
              ),
            ),
            const SloganText(),
            ForgottenPasswordCard(),
            // UpdatePasswordCard(),
            Center(
              child: SvgPicture.asset(
                landingPagePeople,
                // width: 50,
                height: 300,
              ),
            ),
            const SizedBox(height: 25),
            const SmFooterRow(),
          ],
        ),
      ),
    );
  }
}
