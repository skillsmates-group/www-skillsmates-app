import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/account_created/account_created_page.dart';

import '/pages/widgets/sm_elevated_button.dart';
import '/themes/theme.dart';
import '../../../providers/auth_provider.dart';
import '../../widgets/sm_text_form_field_email.dart';

class ForgottenPasswordCard extends StatefulWidget {
  const ForgottenPasswordCard({Key? key}) : super(key: key);

  @override
  State<ForgottenPasswordCard> createState() => _ForgottenPasswordCardState();
}

class _ForgottenPasswordCardState extends State<ForgottenPasswordCard> {
  final _formKey = GlobalKey<FormState>();
  final _email = TextEditingController();

  void _cancel() {
    context.pop();
    // Navigator.of(context).pop();
  }

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();
    if (isValid) {
      _formKey.currentState!.save();
      debugPrint('le pwd: ${_email.text},');
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      authProvider
          .initPasswordReset(_email.text)
          .then((value) => _showMessageSuccess(
                  "Visitez votre voite mail et cliquez sur le lien qui vous a été envoyé pour modifier votre mot de "
                  "passe")
              // Navigator.of(context).pushReplacementNamed(AccountCreatedPage.routeName)
              )
          .onError((error, stackTrace) => {_showMessageError("Erreur lors du traitement de la requette")});
    }
  }

  void _showMessageError(String message) {
    MotionToast.error(
      title: Text("Une erreur est survenue"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
      toastDuration: Duration(seconds: 10),
    ).show(context);
  }

  void _showMessageSuccess(String message) {
    _email.clear();
    MotionToast.success(
      title: Text("Felicitation"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
      toastDuration: Duration(seconds: 10),
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Text(
                  'Mot de passe oublié',
                  style: TextStyle(
                    fontSize: 20,
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                    'Saisissez votre adresse e-mail pour réinitialiser votre mot de passe. Vous devez peut-etre consulter votre dossier de spams ou ajouter l\'adresse no-reply@skills-mates.com à votre liste d\'expéditeurs autorisés.'),
                const SizedBox(
                  height: 20,
                ),
                SmTextFormFieldEmail(_email),
                SmElevatedButton(
                  label: 'Envoyer',
                  color: greenColor,
                  handleButton: _trySubmit,
                ),
                const SizedBox(
                  height: 8,
                ),
                const Divider(),
                Text('Retour à la page de connexion'),
                const SizedBox(
                  height: 10,
                ),
                SmElevatedButton(
                  label: 'Annuler',
                  color: primaryColor,
                  handleButton: _cancel,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
