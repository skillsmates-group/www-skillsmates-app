import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/login/tablet_login_screen.dart';

import '/pages/login/desktop_login_screen.dart';
import 'mobile_login_screen.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  static const routeName = '/login';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const Padding(
          padding: EdgeInsets.all(20),
          child: DesktopLoginScreen(),
        );
      }
      if (sizingInformation.isTablet) {
        return const TabletLoginScreen();
      }
      return const MobileLoginScreen();
    });
  }
}
