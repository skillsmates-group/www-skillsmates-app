import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/pages/login/widgets/connection_card.dart';

import '../widgets/slogan_text.dart';
import '../widgets/sm_footer_row.dart';

class MobileLoginScreen extends StatelessWidget {
  const MobileLoginScreen({Key? key}) : super(key: key);
  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: SvgPicture.asset(
                logoSkillsmates,
              ),
            ),
            Center(child: const SloganText()),
            const ConnectionCard(),
            SvgPicture.asset(
              landingPagePeople,
            ),
            const SmFooterRow(),
          ],
        ),
      ),
    );
  }
}
