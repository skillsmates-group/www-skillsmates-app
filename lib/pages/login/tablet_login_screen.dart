import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '/pages/login/widgets/connection_card.dart';
import '../widgets/slogan_text.dart';
import '../widgets/sm_footer_row.dart';

class TabletLoginScreen extends StatelessWidget {
  const TabletLoginScreen({Key? key}) : super(key: key);

  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: width * 0.01),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                  flex: 4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset(
                        logoSkillsmates,
                      ),
                      const SloganText(),
                      SvgPicture.asset(
                        landingPagePeople,
                      ),
                      const SmFooterRow(),
                    ],
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Column(
                    children: const [
                      ConnectionCard(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
