import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

class CheckboxGcu extends StatefulWidget {
  const CheckboxGcu({Key? key}) : super(key: key);

  @override
  State<CheckboxGcu> createState() => _CheckboxGcuState();
}

class _CheckboxGcuState extends State<CheckboxGcu> {
  var _isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Checkbox(
            value: _isChecked,
            onChanged: (bool? value) {
              setState(() {
                _isChecked = value!;
              });
            }),
        // const Text(""),
        TextButton(
          onPressed: () {
            launchUrlString('https://skills-mates.com/cgu');
          },
          child: const Text('J\'accepte les conditons de Skills-mates'),
        ),
      ],
    );
  }
}
