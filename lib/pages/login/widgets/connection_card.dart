import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:www_skillsmates_app/pages/account_created/account_created_page.dart';
import 'package:www_skillsmates_app/providers/auth_provider.dart';

import '/pages/dashboard/dashboard_page.dart';
import '/pages/login/widgets/checkbox_gcu.dart';
import '/pages/login/widgets/sm_checkbox_list_tile.dart';
import '../../../enums/status.dart';
import '../../../extensions/sm_http_exception.dart';
import '../../../providers/account_provider.dart';
import '../../../themes/theme.dart';
import '../../forgotten_password/forgotten_password_page.dart';
import '../../profile_edit/widgets/dropdown.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_text_button_field.dart';
import '../../widgets/sm_text_form_field.dart';
import '../../widgets/sm_text_form_field_email.dart';
import '../../widgets/sm_text_form_field_password.dart';

enum AuthMode { Signup, Login }

class ConnectionCard extends StatefulWidget {
  const ConnectionCard({Key? key}) : super(key: key);

  @override
  State<ConnectionCard> createState() => _ConnectionCardState();
}

class _ConnectionCardState extends State<ConnectionCard> with TickerProviderStateMixin {
  AuthMode _authMode = AuthMode.Login;
  final _formKey = GlobalKey<FormState>();
  final _prenom = TextEditingController();
  final _nom = TextEditingController();
  final _pwdConfirm = TextEditingController();
  final _status = TextEditingController();
  final _email = TextEditingController();
  final _pwd = TextEditingController();
  bool _isSubmitted = false;

  showSuccessToast() {
    MotionToast.success(
      title: Text("Success Motion Toast"),
      description: Text("Example of success motion toast"),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void _navigateToForgottenPasswordPage() {
    context.push(ForgottenPasswordPage.routeName);
    // Navigator.of(context).pushNamed(ForgottenPasswordPage.routeName);
  }

  void _showMessageError(String message) {
    MotionToast.error(
      title: Text("Une erreur est survenue"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  void login() {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    authProvider
        .signin(_email.text, _pwd.text)
        .then((value) => context.push(DashboardPage.routeName))
        .onError((error, stackTrace) => {
              setState(() {
                _isSubmitted = false;
              }),
              _showMessageError(error.toString()),
            });
  }

  void register() {
    if (validateInputOnRegister()) {
      final accountProvider = Provider.of<AccountProvider>(context, listen: false);
      accountProvider
          .register(_nom.text, _prenom.text, _status.text, _email.text, _pwd.text, _pwdConfirm.text)
          .then((value) => context.push(AccountCreatedPage.routeName))
          .onError((error, stackTrace) => {
                setState(() {
                  _isSubmitted = false;
                }),
                _showMessageError("Erreur lors de l\'enregistrement"),
              });
    } else {
      _showMessageError("Données incorrectes");
      setState(() {
        _isSubmitted = false;
      });
    }
  }

  bool validateInputOnRegister() {
    return _nom.text.isNotEmpty &&
        _prenom.text.isNotEmpty &&
        _email.text.isNotEmpty &&
        _pwd.text.isNotEmpty &&
        _pwd.text == _pwdConfirm.text;
  }

  Future<void> _trySubmit() async {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();
    if (isValid) {
      _formKey.currentState!.save();
      setState(() {
        _isSubmitted = true;
      });
      // Log user in
      try {
        if (_authMode == AuthMode.Login) {
          login();
        } else {
          register();
        }
      } on HttpException catch (e) {
        _showMessageError('Une erreur est survenue lors de la connexion, Veuillez reessayer');
      }
    }
  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(() {
        _authMode = AuthMode.Signup;
      });
    } else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    bool _isChecked = false;
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 10,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          // side: const BorderSide(color: Colors.red, width: 0),
        ),
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    _authMode == AuthMode.Login ? 'Connectez-vous' : 'Inscrivez-vous',
                    style: const TextStyle(
                      fontSize: 20,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  if (_authMode == AuthMode.Signup) ...[
                    SmTextFormField(_prenom, fieldName: 'Prénom'),
                    const SizedBox(
                      height: 20,
                    ),
                    SmTextFormField(_nom, fieldName: 'Nom'),
                    const SizedBox(
                      height: 20,
                    ),
                    Dropdown(
                      _status,
                      Status.values.map((e) => e.value).toList(),
                      "Statut",
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                  SmTextFormFieldEmail(_email),
                  SmTextFormFieldPassword(_pwd),
                  if (_authMode == AuthMode.Signup)
                    SmTextFormFieldPassword(
                      _pwdConfirm,
                      pwdForConfirm: _pwdConfirm.text,
                      fieldName: 'Confirmer Mot de passe',
                      helperText:
                          "Votre mot de passe doit contenir \n- entre 8 et 16 caractères\n- au moins 1 chiffre ou un caractère spécial : !&%\$ \n- au moins une lettre majuscule\n- au moins une lettre miniscule\n- ne doit pa contenir votre nom ou email",
                    ),
                  if (_authMode == AuthMode.Login)
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 1,
                            child: SmCheckboxListTile(
                              label: 'Memoriser',
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: FittedBox(
                              child: SmTextButton(
                                label: 'Mot de passe oublié',
                                color: primaryColor,
                                handleButton: _navigateToForgottenPasswordPage,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  const SizedBox(
                    height: 8,
                  ),
                  if (_authMode == AuthMode.Signup) const CheckboxGcu(),
                  if (_isSubmitted)
                    Container(
                      child: SizedBox(
                        width: 20,
                        height: 15,
                        child: CircularProgressIndicator(
                          color: greenColor,
                        ),
                      ),
                    ),
                  if (!_isSubmitted)
                    SmElevatedButton(
                      label: _authMode == AuthMode.Signup ? 'S\'inscrire' : 'Se connecter',
                      color: _authMode == AuthMode.Signup ? greenColor : primaryColor,
                      handleButton: _trySubmit,
                    ),
                  const SizedBox(
                    height: 8,
                  ),
                  const Divider(),
                  if (_authMode == AuthMode.Signup) ...[
                    const Text('Vous avez déjà un compte?'),
                    const SizedBox(
                      height: 8,
                    ),
                    SmElevatedButton(
                      label: 'Connectez-vous',
                      color: primaryColor,
                      handleButton: _switchAuthMode,
                    ),
                  ],
                  const SizedBox(
                    height: 8,
                  ),
                  if (_authMode == AuthMode.Login)
                    SmElevatedButton(
                      label: 'Inscrivez-vous',
                      color: greenColor,
                      handleButton: _switchAuthMode,
                    ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FittedBox(
                      child: SmTextButton(
                        label: "Télécharger l'application android",
                        color: blueSkyColor,
                        handleButton: () {
                          launchUrlString('https://skillsmatesresources.s3.us-east-2.amazonaws.com/skillsmates.apk');
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
