import 'package:flutter/material.dart';

import '../../../themes/theme.dart';

class RegistrationCard extends StatelessWidget {
  const RegistrationCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _login() {
      // Navigator.of(context).pushNamed(
      //   NavScreen.routeName,
      //   arguments: {},
      // );
    }

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
        side: const BorderSide(color: Colors.grey, width: 1),
      ),
      elevation: 0,
      child: Container(
        width: 430,
        // height: 360,
        padding: const EdgeInsets.all(30.0),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: const Text(
                'Inscrivez-vous',
                style: TextStyle(fontSize: 20, color: primaryColor, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(5),
              child: const TextField(
                autocorrect: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Email',
                    hintStyle: TextStyle(
                      fontSize: 16,
                      color: Colors.black45,
                    )),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(5),
              child: const TextField(
                obscureText: true,
                autocorrect: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Password',
                  hintStyle: TextStyle(fontSize: 16, color: Colors.black45, fontWeight: FontWeight.normal),
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ElevatedButton(
                      // shape: RoundedRectangleBorder(
                      //   borderRadius: BorderRadius.circular(7)
                      // ),
                      // textColor: Colors.white,
                      // color: primaryColor,
                      style: ElevatedButton.styleFrom(
                        primary: primaryColor,
                      ),
                      onPressed: _login,
                      child: const Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          'Se connecter',
                          style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            TextButton(
              onPressed: () {},
              // textColor: primaryColor,
              style: TextButton.styleFrom(
                primary: primaryColor,
              ),
              child: const Text(
                'Mot de passe oubié?',
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
            ),
            const Divider(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                // shape: RoundedRectangleBorder(
                //   borderRadius: BorderRadius.circular(7)
                // ),
                // textColor: Colors.white,
                // color: greenColor,
                style: ElevatedButton.styleFrom(
                  primary: greenColor,
                ),
                child: const Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    'Inscrivez-vous',
                    style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
                onPressed: () {},
              ),
            )
          ],
        ),
      ),
    );
  }
}
