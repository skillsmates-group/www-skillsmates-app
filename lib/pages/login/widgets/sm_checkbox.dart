import 'package:flutter/material.dart';

class SmCheckbox extends StatefulWidget {
  const SmCheckbox({Key? key}) : super(key: key);

  @override
  State<SmCheckbox> createState() => _SmCheckboxState();
}

class _SmCheckboxState extends State<SmCheckbox> {
  var _isChecked = false;
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("Memoriser"),
          Checkbox(
            value: _isChecked,
            onChanged: (bool? value) {
              setState(() {
                _isChecked = value!;
              });
            },
          ),
        ],
      ),
    );
  }
}
