import 'package:flutter/material.dart';

class SmCheckboxListTile extends StatefulWidget {
  final String label;
  const SmCheckboxListTile({Key? key, required this.label}) : super(key: key);

  @override
  _SmCheckboxListTileState createState() => _SmCheckboxListTileState();
}

class _SmCheckboxListTileState extends State<SmCheckboxListTile> {
  bool _isChecked = false;
  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: FittedBox(
        fit: BoxFit.scaleDown,
        child: Text(widget.label),
      ),
      value: _isChecked,
      onChanged: (bool? value) {
        setState(() {
          _isChecked = value!;
        });
      },
    );
  }
}
