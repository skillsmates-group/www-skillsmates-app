import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/login/login_page.dart';

import '../../providers/auth_provider.dart';

class LogoutPage extends StatelessWidget {
  const LogoutPage({Key? key}) : super(key: key);
  static const routeName = '/logout';

  @override
  Widget build(BuildContext context) {
    void _navigateTo(String routeName) {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      authProvider.logout().then((value) => {
            context.push(LoginPage.routeName)
            // Navigator.of(context).pushReplacementNamed(
            //   LoginPage.routeName,
            // )
          });
    }

    return Container();
  }
}
