import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/messagerie/widgets/messagerie_content.dart';

import '../dashboard/widgets/desktop_nav_bar.dart';

class DesktopMessagerieScreen extends StatelessWidget {
  const DesktopMessagerieScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: const MessagerieContent(),
      ),
    );
  }
}
