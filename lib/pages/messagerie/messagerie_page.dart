import 'package:flutter/material.dart';

import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/messagerie/desktop_messagerie_screen.dart';
import 'package:www_skillsmates_app/pages/messagerie/mobile_messagerie_screen.dart';

class MessageriePage extends StatelessWidget {
  const MessageriePage({Key? key}) : super(key: key);

  static const routeName = '/message';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
        builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopMessagerieScreen();
      }
      if (sizingInformation.isTablet) {
        return const DesktopMessagerieScreen();
      }
      return const MobileMessagerieScreen();
    });
  }
}
