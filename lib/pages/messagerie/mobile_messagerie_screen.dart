import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/messagerie/widgets/thread.dart';

import '../dashboard/widgets/mobile_nav_bar.dart';
import '../dashboard/widgets/mobile_top_nav_bar.dart';

class MobileMessagerieScreen extends StatelessWidget {
  const MobileMessagerieScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height - 100,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Thread(),
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
