import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/pages/messagerie/widgets/space_display_message.dart';
import 'package:www_skillsmates_app/pages/messagerie/widgets/thread.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_footer_row.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

class MessagerieContent extends StatelessWidget {
  const MessagerieContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 200.0,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "MESSAGES",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              const Icon(
                Icons.maps_ugc_outlined,
                size: 30.0,
              ),
              const SizedBox(
                width: 15,
              ),
              SvgPicture.asset(
                filter,
                width: 30,
                height: 30,
              ),
            ],
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 600,
            padding: EdgeInsets.all(10),
            child: Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                      height: double.maxFinite,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(
                            color: Color.fromARGB(115, 158, 158, 158),
                            width: 1.6,
                          ),
                        ),
                      ),
                      child: const Thread(),
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    child: Stack(
                      children: [
                        Positioned(
                          right: 0.0,
                          bottom: 0.0,
                          child: Container(
                            height: 200.0,
                            width: MediaQuery.of(context).size.width - 300,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(122, 158, 158, 158),
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(10),
                              ),
                            ),
                          ),
                        ),
                        const SpaceDisplayMessage(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          const SmFooterRow(),
        ],
      ),
    );
  }
}
