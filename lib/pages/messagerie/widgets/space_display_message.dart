import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

class SpaceDisplayMessage extends StatelessWidget {
  const SpaceDisplayMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                skillschat,
                width: 130,
                height: 130,
              ),
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: 300,
                child: const Text(
                  "Avec SkillsChat, communiquez et sollicitez l'aide des membres de la communauté.",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              const SizedBox(
                height: 70,
              ),
              SizedBox(
                  width: 300,
                  child: const Text(
                    "Collaborez et échangez tout type de contenus.",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w700,
                    ),
                  )),
              const SizedBox(
                height: 25,
              ),
              SizedBox(
                width: 300,
                child: Row(
                  children: [
                    SvgPicture.asset(
                      document_bleu,
                      width: 50,
                      height: 50,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SvgPicture.asset(
                      lien_jaune,
                      width: 50,
                      height: 50,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SvgPicture.asset(
                      video_rouge,
                      width: 50,
                      height: 50,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SvgPicture.asset(
                      photo_vert,
                      width: 50,
                      height: 50,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SvgPicture.asset(
                      audio_rose,
                      width: 50,
                      height: 50,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            width: 25,
          ),
          SvgPicture.asset(
            interactions,
            width: 300,
            height: 300,
          ),
        ],
      ),
    );
  }
}
