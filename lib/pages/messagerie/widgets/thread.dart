import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/themes/palette.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../widgets/profile_avatar.dart';

class Thread extends StatelessWidget {
  const Thread({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textEditing = TextEditingController();
    return Column(
      children: [
        const Text(
          "Discussion",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w600,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        TextField(
          controller: textEditing,
          decoration: const InputDecoration(
            hintText: "Rechercher dans les messages",
            border: OutlineInputBorder(),
            suffixIcon: Icon(
              Icons.search,
              size: 30,
              color: Palette.skillsmatesBlue,
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "Liste de Difussion",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const Text(
                "Groupes",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 400,
          child: ListView.builder(
            itemCount: 6,
            itemBuilder: (context, index) {
              return Container(
                margin: const EdgeInsets.symmetric(
                  vertical: 8.0,
                ),
                child: Column(
                  children: [
                    ListTile(
                      // isThreeLine: true,
                      leading: ProfileAvatar(
                        imageUrl: '',
                      ),
                      title: Text("Prenom NOM"),
                      subtitle: const Text(
                        "Message reçu/INformation reçue",
                        overflow: TextOverflow.ellipsis,
                      ),
                      trailing: SizedBox(
                        height: 48,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            SvgPicture.asset(
                              professional,
                              width: 20,
                              height: 20,
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            const Text("HH:MM:SS"),
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      indent: 40,
                      endIndent: 40,
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
