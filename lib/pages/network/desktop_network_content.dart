import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/network/widget/sm_tab_network.dart';
import 'package:www_skillsmates_app/pages/network/widget/sm_tabview_network.dart';

import '../../models/account/account.dart';
import '../../providers/auth_provider.dart';
import '../../themes/picture_file.dart';
import '../../themes/theme.dart';
import '../profile/widgets/horizontal_profile_card.dart';
import '../profile/widgets/mobile_profile_card.dart';
import '../widgets/responsive.dart';
import '../widgets/sm_footer_row.dart';
import '../widgets/sm_future_builder_error.dart';
import '../widgets/sm_future_builder_waiting.dart';

class DesktopNetworkContent extends StatefulWidget {
  const DesktopNetworkContent({Key? key}) : super(key: key);

  @override
  State<DesktopNetworkContent> createState() => _DesktopNetworkContentState();
}

class _DesktopNetworkContentState extends State<DesktopNetworkContent> with TickerProviderStateMixin {
  late TabController _tabController;
  int selectedTab = 0;

  void _selectedTab(int value) {
    Future.delayed(Duration.zero, () async {
      setState(() {
        this.selectedTab = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context);
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    // final args = ModalRoute.of(context)!.settings.arguments;
    // this.selectedTab = args != null ? args as int : 0;

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (isDesktop || isTablet) HorizontalProfileCard(),
          if (!isDesktop && !isTablet) MobileProfileCard(),
          FutureBuilder<Account>(
              future: authProvider.getLoggedAccount(),
              builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                if (snapshot.hasData) {
                  Account loggedAccount = snapshot.data!;
                  return Container(
                    padding: EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: padding(),
                    ),
                    child: Card(
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      elevation: 8,
                      child: Column(
                        children: [
                          TabBar(
                            onTap: (int value) => _selectedTab(value),
                            controller: _tabController,
                            labelStyle: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            // unselectedLabelColor: primaryColor,
                            indicator: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(10),
                              ),
                              color: primaryColor,
                            ),
                            padding: const EdgeInsets.all(5),
                            tabs: [
                              SmTabNetwork(
                                selected: this.selectedTab == 0,
                                picture: follow,
                                text: "Suggestions (${loggedAccount.suggestions})",
                              ),
                              SmTabNetwork(
                                selected: this.selectedTab == 1,
                                picture: follower,
                                text: "Abonnés (${loggedAccount.followers})",
                              ),
                              SmTabNetwork(
                                selected: this.selectedTab == 2,
                                picture: following,
                                text: "Abonnements (${loggedAccount.followees})",
                              ),
                              SmTabNetwork(
                                selected: this.selectedTab == 3,
                                picture: following,
                                text: "En ligne (${loggedAccount.suggestions})",
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 820,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: TabBarView(
                                controller: _tabController,
                                children: [
                                  SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "SUGGESTIONS"),
                                  SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "FOLLOWERS"),
                                  SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "FOLLOWEES"),
                                  SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "aroundyou"),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                } else if (snapshot.hasError) {
                  return SmFutureBuilderError();
                } else {
                  return SmFutureBuilderWaiting();
                }
              }),
          const SmFooterRow(),
        ],
      ),
    );
  }
}
