import 'package:flutter/material.dart';

import '/pages/dashboard/widgets/desktop_nav_bar.dart';
import 'desktop_network_content.dart';

class DesktopNetworkScreen extends StatefulWidget {
  const DesktopNetworkScreen({Key? key}) : super(key: key);

  @override
  State<DesktopNetworkScreen> createState() => _DesktopNetworkScreenState();
}

class _DesktopNetworkScreenState extends State<DesktopNetworkScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DesktopNetworkContent(),
          ],
        ),
      ),
    );
  }
}
