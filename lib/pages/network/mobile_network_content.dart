import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/network/widget/sm_tab_network.dart';
import 'package:www_skillsmates_app/pages/network/widget/sm_tabview_network.dart';

import '../../models/account/account.dart';
import '../../providers/auth_provider.dart';
import '../../themes/picture_file.dart';
import '../../themes/theme.dart';
import '../profile/widgets/horizontal_profile_card.dart';
import '../profile/widgets/mobile_profile_card.dart';
import '../widgets/responsive.dart';
import '../widgets/sm_footer_row.dart';
import '../widgets/sm_future_builder_error.dart';
import '../widgets/sm_future_builder_waiting.dart';

class MobileNetworkContent extends StatefulWidget {
  const MobileNetworkContent({Key? key}) : super(key: key);

  @override
  State<MobileNetworkContent> createState() => _MobileNetworkContentState();
}

class _MobileNetworkContentState extends State<MobileNetworkContent> with TickerProviderStateMixin {
  late TabController _tabController;
  int selectedTab = 0;

  void _selectedTab(int value) {
    Future.delayed(Duration.zero, () async {
      setState(() {
        this.selectedTab = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context);
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (isDesktop || isTablet) HorizontalProfileCard(),
          if (!isDesktop && !isTablet) MobileProfileCard(),
          FutureBuilder<Account>(
              future: authProvider.getLoggedAccount(),
              builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                if (snapshot.hasData) {
                  Account loggedAccount = snapshot.data!;
                  return Container(
                    padding: EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: padding(),
                    ),
                    child: Card(
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      elevation: 8,
                      child: Column(
                        children: [
                          TabBar(
                            isScrollable: true,
                            onTap: (value) {
                              setState(() {
                                this.selectedTab = value;
                              });
                            },
                            labelPadding: EdgeInsets.zero,
                            padding: EdgeInsets.zero,
                            controller: _tabController,
                            labelColor: primaryColor,
                            indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              // color: Colors.grey,
                              border: Border.all(
                                color: Colors.grey,
                              ),
                            ),
                            unselectedLabelColor: Colors.white,
                            labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                            indicatorWeight: 0.1,
                            indicatorColor: Colors.white,
                            indicatorSize: TabBarIndicatorSize.label,
                            tabs: [
                              SmTabNetwork(
                                selected: this.selectedTab == 0,
                                picture: follow,
                                text: "Suggestions (${loggedAccount.suggestions})",
                              ),
                              SmTabNetwork(
                                selected: this.selectedTab == 1,
                                picture: follower,
                                text: "Abonnés (${loggedAccount.followers})",
                              ),
                              SmTabNetwork(
                                selected: this.selectedTab == 2,
                                picture: following,
                                text: "Abonnements (${loggedAccount.followees})",
                              ),
                              SmTabNetwork(
                                selected: this.selectedTab == 3,
                                picture: following,
                                text: "En ligne (${loggedAccount.followees})",
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 800,
                            child: TabBarView(
                              controller: _tabController,
                              children: [
                                SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "SUGGESTIONS"),
                                SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "FOLLOWERS"),
                                SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "FOLLOWEES"),
                                SmTabviewNetwork(loggedAccount: loggedAccount, networkType: "aroundyou"),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                } else if (snapshot.hasError) {
                  return SmFutureBuilderError();
                } else {
                  return SmFutureBuilderWaiting();
                }
              }),
          const SmFooterRow(),
        ],
      ),
    );
  }
}
