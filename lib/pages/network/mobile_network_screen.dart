import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/mobile_nav_bar.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/mobile_top_nav_bar.dart';

import 'mobile_network_content.dart';

class MobileNetworkScreen extends StatefulWidget {
  const MobileNetworkScreen({Key? key}) : super(key: key);

  @override
  State<MobileNetworkScreen> createState() => _MobileNetworkScreenState();
}

class _MobileNetworkScreenState extends State<MobileNetworkScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MobileNetworkContent(),
          ],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
