import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/network/desktop_network_screen.dart';
import 'package:www_skillsmates_app/pages/network/mobile_network_screen.dart';

class NetworkPage extends StatelessWidget {
  const NetworkPage({Key? key}) : super(key: key);

  static const routeName = '/network';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopNetworkScreen();
      }
      if (sizingInformation.isTablet) {
        return DesktopNetworkScreen();
      }
      return const MobileNetworkScreen();
    });
  }
}
