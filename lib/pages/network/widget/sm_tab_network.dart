import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../themes/theme.dart';

class SmTabNetwork extends StatelessWidget {
  final String picture;
  final String text;
  final bool selected;
  const SmTabNetwork({
    Key? key,
    required this.picture,
    required this.text,
    this.selected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: selected ? primaryColor : Colors.grey.shade200,
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Tab(
        child: Container(
          child: Column(
            children: [
              SvgPicture.asset(
                picture,
                color: selected ? Colors.white : primaryColor,
                height: 30,
                width: 30,
              ),
              FittedBox(
                child: Text(
                  text,
                  style: TextStyle(
                    color: selected ? Colors.white : primaryColor,
                    fontWeight: selected ? FontWeight.bold : FontWeight.normal,
                    fontSize: selected ? 18 : 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
