import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/account/account.dart';
import 'package:www_skillsmates_app/models/account/account_response.dart';
import 'package:www_skillsmates_app/models/meta.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_dropdown_v2.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_profile_card.dart';
import 'package:www_skillsmates_app/providers/account_provider.dart';

import '../../../providers/auth_provider.dart';
import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_text_form_field.dart';

class SmTabviewNetwork extends StatefulWidget {
  final Account loggedAccount;
  final String networkType;

  const SmTabviewNetwork({Key? key, required this.loggedAccount, required this.networkType}) : super(key: key);

  @override
  State<SmTabviewNetwork> createState() => _SmTabviewNetworkState(loggedAccount, networkType);
}

class _SmTabviewNetworkState extends State<SmTabviewNetwork> {
  late AccountResponse network;
  late Account loggedAccount;
  late String networkType;

  _SmTabviewNetworkState(Account loggedAccount, String networkType) {
    this.loggedAccount = loggedAccount;
    this.networkType = networkType;
  }

  Future<AccountResponse> fetchNetwork() async {
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);
    return await accountProvider.findMyNetwork(this.loggedAccount.uuid, this.networkType, currentPage, itemsPerPage,
        selectedStatus, countryFieldController.text, nameFieldController.text);
  }

  @override
  void initState() {
    super.initState();
    fetchNetwork();
    nameFieldController.addListener(fetchNetwork);
    countryFieldController.addListener(fetchNetwork);
  }

  @override
  void didChangeDependencies() {
    loggedAccount = Provider.of<AuthProvider>(context).loggedAccount;
    super.didChangeDependencies();
  }

  final nameFieldController = TextEditingController();
  final countryFieldController = TextEditingController();
  var totalItems = 1;
  var currentPage = 0;
  var itemsPerPage = 12;
  String selectedStatus = "";
  var totalPage = 0;
  var bgColor = Colors.white.withOpacity(.7);

  void clickPrevious() {
    setState(() {
      if (currentPage > 0) {
        currentPage = currentPage - 1;
      }
      fetchNetwork();
    });
  }

  void clickNext() {
    setState(() {
      if (currentPage < totalPage - 1) {
        currentPage = currentPage + 1;
      }
      fetchNetwork();
    });
  }

  void clickFirst() {
    setState(() {
      currentPage = 0;
      fetchNetwork();
    });
  }

  void clickLast() {
    setState(() {
      currentPage = totalPage - 1;
      fetchNetwork();
    });
  }

  int firstPage() {
    int first = currentPage < 3 ? 0 : currentPage - 2;
    first = first + 5 >= totalPage ? totalPage - 5 : first;
    return first;
  }

  @override
  void dispose() {
    nameFieldController.dispose();
    countryFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    int crossAxisCount() {
      if (isDesktop) return 4;
      if (isTablet) return 3;
      return 2;
    }

    final Map<String, dynamic> statusTextValueMap = {
      "Tout": "",
      "Etudiant": "STUDENT",
      "Enseignant": "TEACHER",
      "Professionnel": "PROFESSIONAL"
    };
    final Map<String, dynamic> perPageTextValueMap = {"12": 12, "24": 24, "48": 48};
    loggedAccount = Provider.of<AuthProvider>(context).loggedAccount;
    network = Provider.of<AccountProvider>(context).network;
    List<Account> accounts = network.resources ?? List.empty();
    Meta meta = network.meta ?? new Meta();
    totalPage = meta.totalPages ?? 0;
    totalItems = meta.total ?? 0;
    return Column(
      children: [
        SizedBox(
          width: double.maxFinite,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: DropdownV2(
                    itemTextValue: statusTextValueMap,
                    hint: "Statut",
                    onChanged2: (value) {
                      setState(() {
                        selectedStatus = value;
                        fetchNetwork();
                      });
                    },
                  ),
                ),
              ),
              if (isDesktop || isTablet)
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: SmTextFormField(countryFieldController, fieldName: "Recherche par pays"),
                  ),
                ),
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: SmTextFormField(nameFieldController, fieldName: "Recherche par nom"),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        SizedBox(
            width: double.maxFinite,
            child: Row(mainAxisSize: MainAxisSize.min, children: [
              Text("${meta.total} Résultats Trouvés", style: TextStyle(fontSize: 13)),
              SizedBox(
                width: isDesktop ? 40 : 10,
              ),
              const Text("Afficher", style: TextStyle(fontSize: 13)),
              SizedBox(
                width: isDesktop ? 10 : 0,
              ),
              SizedBox(
                  width: isDesktop ? 87 : 80,
                  child: DropdownV2(
                      itemTextValue: perPageTextValueMap,
                      hint: itemsPerPage.toString(),
                      onChanged2: (value) {
                        setState(() {
                          itemsPerPage = value;
                          fetchNetwork();
                        });
                      })),
              SizedBox(
                width: isDesktop ? 10 : 0,
              ),
              if (isDesktop) Expanded(child: buildNavigation())
            ])),
        if (!isDesktop) SizedBox(child: buildNavigation()),
        const SizedBox(
          height: 10,
        ),
        Container(
          height: 600,
          color: Responsive.isDesktop(context) ? Colors.transparent : Colors.white,
          child: CustomScrollView(
            primary: false,
            slivers: <Widget>[
              SliverPadding(
                padding: const EdgeInsets.all(20),
                sliver: SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: crossAxisCount(),
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    childAspectRatio: 1.0,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return Container(
                        alignment: Alignment.center,
                        child: SmProfileCard(
                          isAddStory: true,
                          account: accounts[index],
                          isSuggestion: networkType == 'SUGGESTIONS',
                          bottomSpace: true,
                        ),
                      );
                    },
                    childCount: accounts.length,
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          width: double.maxFinite,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              /*Expanded(
                child: SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: clickPrevious,
                        child: const Text(
                          "précédent",
                          textAlign: TextAlign.left,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        width: 200,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: totalPage,
                          itemBuilder: (BuildContext _, int index) => index > firstPage() - 1 && index < firstPage() + 5
                              ? GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      // bgColor = primaryColor;
                                      currentPage = index;
                                      fetchNetwork();
                                    });
                                  },
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    padding: const EdgeInsets.all(5),
                                    margin: const EdgeInsets.only(
                                      right: 8,
                                    ),
                                    decoration: BoxDecoration(
                                      color: bgColor,
                                      border: Border.all(
                                        color: primaryColor,
                                      ),
                                    ),
                                    child: Text("${index + 1}",
                                        style: index == currentPage ? TextStyle(color: blueColor) : null),
                                  ),
                                )
                              : Container(),
                        ),
                      ),
                      TextButton(
                        onPressed: clickNext,
                        child: const Text(
                          "Suivant",
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ],
                  ),
                ),
              ),*/
              if (isDesktop) Expanded(child: buildNavigation())
            ],
          ),
        ),
        if (!isDesktop) SizedBox(child: buildNavigation()),
      ],
    );
  }

  Row buildNavigation() {
    return Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      TextButton(onPressed: currentPage < 1 ? null : clickFirst, child: Text("Début")),
      currentPage < 1
          ? TextButton(onPressed: null, child: Text("Prec."))
          : TextButton(onPressed: clickPrevious, child: Text("Prec.")),
      TextButton(onPressed: null, child: Text("${currentPage + 1}")),
      currentPage > totalPage - 2
          ? TextButton(onPressed: null, child: Text("Suiv."))
          : TextButton(onPressed: clickNext, child: Text("Suiv.")),
      TextButton(onPressed: currentPage > totalPage - 2 ? null : clickLast, child: Text("Fin"))
    ]);
  }
}
