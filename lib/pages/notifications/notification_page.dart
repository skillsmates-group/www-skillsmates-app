import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '/pages/notifications/desktop_notification.dart';
import '/pages/notifications/mobile_notification.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({Key? key}) : super(key: key);

  static const routeName = '/notifications';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopNotification();
      }
      if (sizingInformation.isTablet) {
        return const MobileNotification();
      }
      return const MobileNotification();
    });
  }
}
