import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:www_skillsmates_app/enums/interaction_type_enum.dart';
import 'package:www_skillsmates_app/models/notification/interaction_response.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/providers/interaction_provider.dart';

import '../../../models/account/account.dart';
import '../../../models/notification/interaction.dart';
import '../../../models/notification/interactionRequest.dart';
import '../../../providers/auth_provider.dart';
import '../../profile/profile_page.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/responsive.dart';

class NotificationContent extends StatefulWidget {
  const NotificationContent({Key? key}) : super(key: key);

  @override
  State<NotificationContent> createState() => _NotificationContentState();
}

class _NotificationContentState extends State<NotificationContent> {
  final LocalStorage storage = new LocalStorage('skills-mates_app');
  late InteractionResponse interactionResponse;

  Future<void> fetchNotifications() async {
    String uuid = storage.getItem("uuid");
    final interactionProvider = Provider.of<InteractionProvider>(context, listen: false);
    await interactionProvider.fetchNotifications(uuid, 0, 20);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final interactionProvider = Provider.of<InteractionProvider>(context, listen: false);

    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    void _navigateToProfile(Account? account) {
      authProvider.setProfileAccount(account!).then((value) => {context.push(ProfilePage.routeName)});
    }

    bool isProfileInteraction(Interaction? interaction) {
      return InteractionTypeEnum.INTERACTION_TYPE_FOLLOWER.name == interaction?.interactionType ||
          InteractionTypeEnum.INTERACTION_TYPE_FAVORITE.name == interaction?.interactionType;
    }

    bool isPostInteraction(Interaction? interaction) {
      return InteractionTypeEnum.INTERACTION_TYPE_LIKE.name == interaction?.interactionType ||
          InteractionTypeEnum.INTERACTION_TYPE_COMMENT.name == interaction?.interactionType ||
          InteractionTypeEnum.INTERACTION_TYPE_SHARE.name == interaction?.interactionType ||
          InteractionTypeEnum.INTERACTION_TYPE_POST.name == interaction?.interactionType;
    }

    String computePostLink(String postId) {
      return Uri.base.origin + '/post/' + postId;
    }

    void onClickNotification(Interaction? interaction) {
      InteractionRequest request = InteractionRequest(
        uuid: interaction?.uuid,
        active: false,
        entity: interaction?.entity ?? '',
        interactionType: interaction?.interactionType ?? '',
        emitter: interaction?.emitter.uuid ?? '',
        receiver: interaction?.receiver.uuid ?? '',
      );

      interactionProvider.updateInteraction(request).then((value) => {
            if (isProfileInteraction(interaction))
              {
                authProvider.getLoggedAccount().then((account) => {
                      authProvider.setProfileAccount(account),
                      _navigateToProfile(interaction?.emitter),
                    }),
              }
            else if (isPostInteraction(interaction))
              {
                launchUrlString(computePostLink(interaction?.entity ?? '')),
              }
          });
    }

    return FutureBuilder<Account>(
      future: Provider.of<AuthProvider>(context).getLoggedAccount(),
      builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
        if (snapshot.hasData) {
          return Container(
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: padding(),
            ),
            child: FutureBuilder(
              future: Provider.of<InteractionProvider>(context, listen: false)
                  .fetchNotifications(snapshot.data?.uuid ?? '', 0, 100),
              builder: (ctx, dataSnapshot) {
                if (dataSnapshot.connectionState == ConnectionState.waiting) {
                  return SmFutureBuilderWaiting();
                } else {
                  if (dataSnapshot.error != null) {
                    return SmFutureBuilderError(
                      error: 'Une erreur est survenue lors de la recuperation des notifications',
                    );
                  } else {
                    return Consumer<InteractionProvider>(
                      builder: (ctx, data, child) => ListView.builder(
                        shrinkWrap: true,
                        itemCount: data.interactionResponse.resources?.length,
                        itemBuilder: (ctx, index) => InkWell(
                          onTap: () {
                            onClickNotification(data.interactionResponse.resources?[index]);
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                            child: ListTile(
                              leading: ProfileAvatar(
                                imageUrl: data.interactionResponse.resources?[index].emitter.avatar ?? "",
                                isActive: data.interactionResponse.resources?[index].emitter.active ?? false,
                              ),
                              title: Column(
                                children: [
                                  Row(
                                    children: [
                                      FittedBox(
                                        fit: BoxFit.contain,
                                        child: Text(
                                          '${data.interactionResponse.resources?[index].emitter.firstname}  ${data.interactionResponse.resources?[index].emitter.lastname}',
                                          style: const TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w700,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      FittedBox(
                                        fit: BoxFit.fitWidth,
                                        child: Text(
                                          '${data.interactionResponse.resources?[index].displayedMessage()}',
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                          softWrap: true,
                                          style: const TextStyle(
                                            fontSize: 10,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              trailing: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Text('${data.interactionResponse.resources?[index].durationText()}')),
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                }
              },
            ),
          );
        } else if (snapshot.hasError) {
          return SmFutureBuilderError();
        } else {
          return SmFutureBuilderWaiting();
        }
      },
    );
  }
}
