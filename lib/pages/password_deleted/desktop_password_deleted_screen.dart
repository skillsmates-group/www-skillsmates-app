import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '/themes/theme.dart';
import '../widgets/sm_text_button_field.dart';

class DesktopPasswordDeletedScreen extends StatelessWidget {
  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';
  static const symboleSkillsmates = 'assets/images/symbole_skillsmates.svg';

  const DesktopPasswordDeletedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Column(children: [Center(child: SvgPicture.asset(logoSkillsmates, height: 100))]),
        Column(children: [Center(child: SvgPicture.asset(landingPagePeople, height: 300))])
      ]),
      Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30), side: const BorderSide(color: Colors.grey, width: 1)),
          elevation: 1,
          child: Container(
              width: 700,
              padding: const EdgeInsets.all(30),
              child: Column(children: [
                Row(children: const [
                  Icon(Icons.person, size: 30),
                  Text("REINITIALISATION DU MOT DE PASSE",
                      style: TextStyle(fontSize: 20, color: primaryColor, fontWeight: FontWeight.bold))
                ]),
                Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                    child: const Text("Votre mot de passe a été réinitialisé avec succès",
                        style: TextStyle(fontSize: 18, color: greenColor, fontWeight: FontWeight.bold))),
                Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                    child: const Text(
                        "Vous pouvez de nouveau accéser à la plateforme avec "
                        "votre nouveau mot de passe ",
                        style: TextStyle(fontSize: 18, color: primaryColor, fontWeight: FontWeight.normal))),
                Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                    child: Row(children: const [
                      SmTextButton(label: "Cliquez ici", color: primaryColor),
                      Text(
                          " pour accéder à la page de connexion de la plateforme."
                          ".",
                          style: TextStyle(fontSize: 18, color: primaryColor, fontWeight: FontWeight.normal))
                    ])),
                Container(
                    padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                    child: const Text(
                        " Si le bouton ci dessus ne fonctionne "
                        "pas, cliquez directement sur ce lien ou copiez et collez "
                        "le dans votre navigateur: https://skills-mates.com",
                        style: TextStyle(fontSize: 18, color: primaryColor, fontWeight: FontWeight.normal)))
              ]))),
      const SizedBox(height: 100),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        const Text(
            "A PROPOS - AIDE -POLITIQUE DE CONFIDENTIALITE - \nCONDITIONS "
            "D'UTILISATION - FAQ",
            style: TextStyle(fontSize: 18, color: primaryColor, fontWeight: FontWeight.normal)),
        const SizedBox(width: 20),
        Center(child: SvgPicture.asset(symboleSkillsmates, height: 100)),
        const Text("\u00a9 Skillsmates 2022",
            style: TextStyle(fontSize: 18, color: primaryColor, fontWeight: FontWeight.normal))
      ])
    ]));
  }
}
