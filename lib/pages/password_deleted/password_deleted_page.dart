import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '/pages/password_deleted/desktop_password_deleted_screen.dart';

class PasswordDeletedPage extends StatelessWidget {
  const PasswordDeletedPage({Key? key}) : super(key: key);

  static const routeName = '/password/deleted';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const Padding(
          padding: EdgeInsets.all(10),
          child: DesktopPasswordDeletedScreen(),
        );
      }
      if (sizingInformation.isTablet) {
        return const DesktopPasswordDeletedScreen();
      }
      return const DesktopPasswordDeletedScreen();
    });
  }
}
