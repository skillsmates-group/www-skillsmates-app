import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/post_add/widgets/desktop_post_add_content.dart';

import '/pages/dashboard/widgets/desktop_nav_bar.dart';

class DesktopPostAddScreen extends StatefulWidget {
  const DesktopPostAddScreen({Key? key}) : super(key: key);

  @override
  State<DesktopPostAddScreen> createState() => _DesktopPostAddScreenState();
}

class _DesktopPostAddScreenState extends State<DesktopPostAddScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DesktopPostAddContent(),
          ],
        ),
      ),
    );
  }
}
