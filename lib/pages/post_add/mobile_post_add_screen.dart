import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/mobile_nav_bar.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/mobile_top_nav_bar.dart';
import 'package:www_skillsmates_app/pages/post_add/widgets/mobile_post_add_content.dart';

class MobilePostAddScreen extends StatefulWidget {
  const MobilePostAddScreen({Key? key}) : super(key: key);

  @override
  State<MobilePostAddScreen> createState() => _MobilePostAddScreenState();
}

class _MobilePostAddScreenState extends State<MobilePostAddScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MobilePostAddContent(),
          ],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
