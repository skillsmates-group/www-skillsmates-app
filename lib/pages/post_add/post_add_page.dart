import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '/pages/post_add/desktop_post_add_screen.dart';
import '../widgets/responsive.dart';
import 'mobile_post_add_screen.dart';

class PostAddPage extends StatelessWidget {
  const PostAddPage({Key? key}) : super(key: key);

  static const routeName = '/post/add';

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const DesktopPostAddScreen();
      }
      if (sizingInformation.isTablet) {
        return const DesktopPostAddScreen();
      }
      return const MobilePostAddScreen();
    });
  }
}
