import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../widgets/sm_dropdown.dart';
import '/themes/theme.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_text_form_field.dart';

class EyeScreen extends StatefulWidget {
  const EyeScreen({Key? key}) : super(key: key);

  @override
  State<EyeScreen> createState() => _myEye();
}

class _myEye extends State<EyeScreen> {
  @override
  Widget build(BuildContext context) {
    const Color colorBlue = Color.fromRGBO(31, 172, 228, 1);
    const infoIcon = 'assets/images/info.svg';
    const documentIcon = 'assets/images/document-bleu.svg';
    const linkIcon = 'assets/images/lien-jaune.svg';
    const videoIcon = 'assets/images/video-rouge.svg';
    const imageIcon = 'assets/images/photo-vert.svg';
    const audioIcon = 'assets/images/audio-rose.svg';

    const List<String> categoryItems = [
      "Option 1",
      "Option 2",
      "Option 3",
      "Option 4",
      "Option 5"
    ];
    const infoText = "Nous vous prions de prendre le soin dépertorier et"
        " referencer votre contenu afin d'en faciliter l'accès à l'ensemble de "
        "la communauté. \nVeillez renseigner chacune des cases en vous laissant"
        " guider par les instructions. ces informations nous aiderons à "
        "optimiser les résultats de l'outil de recherche au sein de la "
        "bibliothèque partagée";

    final category = TextEditingController();
    final mediaUrl = TextEditingController();
    var upload = () async {
      var picked = await FilePicker.platform.pickFiles();
      if (picked != null) {
        print(picked.files.first.name);
      }
    };

    var documentTypeImage = SvgPicture.asset(documentIcon);

    clickDocument() async {
      setState(() {
        documentTypeImage = SvgPicture.asset(documentIcon);
      });
    }

    clickLink() async {
      setState(() {
        documentTypeImage = SvgPicture.asset(linkIcon);
      });
    }

    clickVideo() async {
      setState(() {
        documentTypeImage = SvgPicture.asset(videoIcon);
      });
    }

    clickImage() async {
      setState(() {
        documentTypeImage = SvgPicture.asset(imageIcon);
      });
    }

    void clickAudio() async {
      setState(() {
        documentTypeImage = SvgPicture.asset(audioIcon);
      });
    }

    return Container(
      child: SingleChildScrollView(
        child: Row(
          children: [
            Expanded(
              flex: 30,
              child: Column(
                children: [
                  Row(
                    children: const [
                      DefaultTextStyle(
                        style: TextStyle(
                          fontSize: 20,
                          color: colorBlue,
                          fontFamily: 'Raleway',
                          fontWeight: FontWeight.bold,
                        ),
                        child: Text("Nouveau contenu"),
                      )
                    ],
                  ),
                  const Divider(color: colorBlue, height: 20),
                  Container(
                    color: const Color.fromRGBO(31, 172, 228, 0.09),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 2,
                            child: SvgPicture.asset(infoIcon, height: 40)),
                        const Expanded(
                          flex: 10,
                          child: DefaultTextStyle(
                            style: TextStyle(
                                fontSize: 13,
                                color: colorBlue,
                                fontFamily: "Raleway",
                                fontWeight: FontWeight.normal),
                            child: Text(infoText),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Expanded(
                      child: DefaultTextStyle(
                        style: TextStyle(
                            fontSize: 18,
                            color: primaryColor,
                            fontFamily: "Raleway",
                            fontWeight: FontWeight.normal),
                        child: Text(
                            "Quel type de contenu souhaitez vous partager ?"),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: TextButton(
                              onPressed: clickDocument,
                              child:
                                  SvgPicture.asset(documentIcon, height: 40))),
                      Expanded(
                          child: TextButton(
                              onPressed: clickLink,
                              child: SvgPicture.asset(linkIcon, height: 40))),
                      Expanded(
                          child: TextButton(
                              onPressed: clickVideo,
                              child: SvgPicture.asset(videoIcon, height: 40))),
                      Expanded(
                          child: TextButton(
                              onPressed: clickImage,
                              child: SvgPicture.asset(imageIcon, height: 40))),
                      Expanded(
                          child: TextButton(
                              onPressed: clickAudio,
                              child: SvgPicture.asset(audioIcon, height: 40)))
                    ],
                  ),
                  const SizedBox(height: 20),
                  Container(
                      alignment: Alignment.centerLeft,
                      child: const Expanded(
                          child: DefaultTextStyle(
                              style: TextStyle(
                                  fontSize: 18,
                                  color: primaryColor,
                                  fontFamily: "Raleway",
                                  fontWeight: FontWeight.normal),
                              child: Text(
                                  "Quelle est la catégorie du contenu ?")))),
                  SizedBox(
                    width: 300,
                    child: DropdownStatut(
                      category,
                      hint: "Entrez la catégorie du contenu",
                      items: categoryItems,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Container(
                      alignment: Alignment.topLeft,
                      child: const Expanded(
                          child: DefaultTextStyle(
                              style: TextStyle(
                                  fontSize: 18,
                                  color: primaryColor,
                                  fontFamily: "Raleway",
                                  fontWeight: FontWeight.normal),
                              child: Text(
                                  "Collez l'url du contenu ou sélectionnez le sur votre appareil")))),
                  Material(
                      child: SmTextFormField(mediaUrl,
                          fieldName: "Entrez le lien du contenu")),
                  const SizedBox(height: 20),
                  SmElevatedButton(
                      label: "Choisir le fichier",
                      color: Colors.grey,
                      handleButton: upload)
                ],
              ),
            ),
            Expanded(flex: 5, child: Container()),
            Expanded(
              flex: 70,
              child: Container(
                padding: const EdgeInsets.all(35.0),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(60),
                  ),
                ),
                child: Column(
                  children: [
                    Row(
                      children: const [
                        DefaultTextStyle(
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontFamily: 'Raleway',
                                fontWeight: FontWeight.bold),
                            child: Text("Description du contenu"))
                      ],
                    ),
                    const Divider(color: Colors.black, height: 20),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          flex: 10,
                          child: Column(
                            children: [
                              Container(
                                  alignment: Alignment.centerLeft,
                                  child: const Expanded(
                                      child: DefaultTextStyle(
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: primaryColor,
                                              fontFamily: "Raleway",
                                              fontWeight: FontWeight.normal),
                                          child: Text(
                                              "Quel est le titre du contenu ?")))),
                              Material(
                                  child: SmTextFormField(mediaUrl,
                                      fieldName: "Entrez le titre du contenu")),
                              const SizedBox(height: 20),
                              Container(
                                  alignment: Alignment.centerLeft,
                                  child: const Expanded(
                                      child: DefaultTextStyle(
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: primaryColor,
                                              fontFamily: "Raleway",
                                              fontWeight: FontWeight.normal),
                                          child: Text(
                                              "Quels sont les mots clés auquels fait reférence le contenu ?")))),
                              const Material(
                                  child: TextField(
                                      textInputAction: TextInputAction.newline,
                                      keyboardType: TextInputType.multiline,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText:
                                              "Listez les disciplines, thématiques, concepts, théorèmes, auteur et niveau scolaire relatif au contenu",
                                          contentPadding:
                                              EdgeInsets.all(20.0)))),
                              const SizedBox(height: 20),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Expanded(
                                  child: DefaultTextStyle(
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: primaryColor,
                                        fontFamily: "Raleway",
                                        fontWeight: FontWeight.normal),
                                    child: Text("Décrivez votre contenu"),
                                  ),
                                ),
                              ),
                              const Material(
                                child: TextField(
                                  textInputAction: TextInputAction.newline,
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 5,
                                  decoration: InputDecoration(
                                    hintText: "Redigez un résumé du contenu "
                                        "en reprennant les principaux sujets "
                                        "traités.\nDécrivez égalemnt le "
                                        "contexte dans leqel vous avez été "
                                        "amené à le réaliser ou le consulter "
                                        "afin de mettre en lumière toute son "
                                        "utilité et sa pertinance",
                                    contentPadding: EdgeInsets.all(20.0),
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20),
                              Row(
                                children: [
                                  Expanded(flex: 2, child: Container()),
                                  Expanded(
                                      flex: 46,
                                      child: SmElevatedButton(
                                          label: "Publier",
                                          color: greenColor,
                                          handleButton: () {})),
                                  Expanded(flex: 4, child: Container()),
                                  Expanded(
                                      flex: 46,
                                      child: SmElevatedButton(
                                          label: "Annuler",
                                          color: Colors.grey,
                                          handleButton: () {})),
                                  Expanded(flex: 2, child: Container())
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 2,
                            child: Container(
                                alignment: Alignment.topCenter,
                                child: documentTypeImage))
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
