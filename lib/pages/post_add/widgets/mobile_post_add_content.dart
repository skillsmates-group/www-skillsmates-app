import 'dart:io';

import 'package:any_link_preview/any_link_preview.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/data/media_data.dart';
import 'package:youtube_metadata/youtube.dart';

import '../../../models/account/account.dart';
import '../../../models/documents/document_type.dart';
import '../../../models/media.dart';
import '../../../models/posts/post.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/theme.dart';
import '../../dashboard/dashboard_page.dart';
import '../../widgets/sm_document_types_dropdown.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/sm_info_text.dart';
import '../../widgets/sm_mandatory_field_error_message.dart';
import '../../widgets/sm_media_menu.dart';
import '../../widgets/sm_text_form_field.dart';

class MobilePostAddContent extends StatefulWidget {
  const MobilePostAddContent({Key? key}) : super(key: key);

  static const Color colorBlue = Color.fromRGBO(31, 172, 228, 1);
  static const infoIcon = 'assets/images/info.svg';
  static const documentIcon = 'assets/images/document-bleu.svg';
  static const linkIcon = 'assets/images/lien-jaune.svg';
  static const videoIcon = 'assets/images/video-rouge.svg';
  static const imageIcon = 'assets/images/photo-vert.svg';
  static const audioIcon = 'assets/images/audio-rose.svg';

  static const List<String> categoryItems = ["Option 1", "Option 2", "Option 3", "Option 4", "Option 5"];
  static const infoText = "Nous vous prions de prendre le soin dépertorier et"
      " referencer votre contenu afin d'en faciliter l'accès à l'ensemble de "
      "la communauté. \nVeillez renseigner chacune des cases en vous laissant"
      " guider par les instructions. ces informations nous aiderons à "
      "optimiser les résultats de l'outil de recherche au sein de la "
      "bibliothèque partagée";

  @override
  State<MobilePostAddContent> createState() => _MobilePostAddContentState();
}

class _MobilePostAddContentState extends State<MobilePostAddContent> {
  late Media selectedMediaType = medias[0];
  late DocumentType documentType;
  late File documentFile;
  late Post post;
  late File selectedFile;

  List<PlatformFile>? _paths;
  String? _extension;
  bool _multiPick = false;
  bool _isSubmitted = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = TextEditingController();
  late Account loggedAccount;

  final categoryController = TextEditingController();
  final mediaUrlController = TextEditingController();
  final titleController = TextEditingController();
  final keywordsController = TextEditingController();
  final descriptionController = TextEditingController();

  selectFile() async {
    _paths = (await FilePicker.platform.pickFiles(
      type: _pickingType,
      allowMultiple: _multiPick,
      onFileLoading: (FilePickerStatus status) => print(status),
      allowedExtensions: (_extension?.isNotEmpty ?? false) ? _extension?.replaceAll(' ', '').split(',') : null,
    ))
        ?.files;
    this.mediaUrlController.text = _paths?.first.name ?? '';
  }

  void _navigateTo(String routeName) {
    context.push(routeName);
  }

  Future<void> _cancelPost() async {
    _navigateTo(DashboardPage.routeName);
  }

  Future<void> _submitPost() async {
    setState(() {
      _isSubmitted = true;
    });
    post = Post(
      link: mediaUrlController.text.isNotEmpty ? mediaUrlController.text : null,
      keywords: keywordsController.text.isNotEmpty ? keywordsController.text : null,
      title: titleController.text.isNotEmpty ? titleController.text : null,
      description: descriptionController.text.isNotEmpty ? descriptionController.text : null,
      documentType: documentType.code,
      postType: 'POST_TYPE_ARTICLE',
      account: loggedAccount,
    );
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    postProvider.savePost(post, _paths?.first).whenComplete(() => {_navigateTo(DashboardPage.routeName)});
  }

  bool validatePost(Post post) {
    return post.title != null;
  }

  @override
  Widget build(BuildContext context) {
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final category = TextEditingController();
    final mediaUrl = TextEditingController();
    var upload = () async {
      var picked = await FilePicker.platform.pickFiles();
      if (picked != null) {
        print(picked.files.first.name);
      }
    };

    callbackMediaType(mediaType) {
      setState(() {
        selectedMediaType = mediaType;
        postProvider.setSelectedMediaType(selectedMediaType).then((value) => {});
      });
    }

    callback(newDocumentType) {
      setState(() {
        documentType = newDocumentType;
      });
    }

    Future<MetaDataModel?> getYoutubeMetadata(String url) async {
      return await YoutubeMetaData.getData(url);
    }

    callbackOnValueChanged(String link) async {
      bool _isValid = AnyLinkPreview.isValidLink(link);
      if (_isValid) {
        getYoutubeMetadata(link).then((value) => {print(value)});
      } else {
        debugPrint("URL is not valid");
      }
    }

    return Container(
      padding: const EdgeInsets.fromLTRB(25, 0, 25, 40),
      child: FutureBuilder<Media>(
        future: postProvider.getSelectedMediaType(),
        builder: (BuildContext context, AsyncSnapshot<Media> snapshot) {
          if (snapshot.hasData) {
            this.selectedMediaType = snapshot.data!;
            return Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 20,
                      color: blueSkyColor,
                      fontFamily: 'Raleway',
                      fontWeight: FontWeight.bold,
                    ),
                    child: Text("Nouveau contenu"),
                  ),
                ),
                const Divider(color: MobilePostAddContent.colorBlue, height: 20),
                SmInfoText(infoText: MobilePostAddContent.infoText),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.center,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal,
                    ),
                    child: FittedBox(
                      child: Text(
                        "Quel type de contenu souhaitez vous partager ?",
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SmMediaMenu(
                  callback: callbackMediaType,
                  selectedMediaType: selectedMediaType,
                ),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.center,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal,
                    ),
                    child: Text(
                      "Quelle est la catégorie du contenu ?",
                    ),
                  ),
                ),
                SizedBox(
                  child: Column(
                    children: [
                      SmDocumentTypesDropdown(
                        categoryController,
                        mediaCode: selectedMediaType.code,
                        callback: callback,
                      ),
                      if (categoryController.text.isEmpty && _isSubmitted)
                        SmMandatoryFieldErrorMessage(
                          message: "Veuillez selectionner une categorie",
                        ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.topLeft,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal,
                    ),
                    child: FittedBox(
                      child: Text(
                        "Collez l'url du contenu ou sélectionnez le sur votre appareil",
                      ),
                    ),
                  ),
                ),
                Material(
                  child: SmTextFormField(
                    mediaUrlController,
                    fieldName: "Entrez le lien du contenu",
                    onValueChanged: callbackOnValueChanged,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 3,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                SmElevatedButton(
                  label: "Choisir le fichier",
                  color: primaryColor,
                  handleButton: selectFile,
                ),
                const Divider(color: Colors.grey, height: 50),
                Row(
                  children: const [
                    DefaultTextStyle(
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontFamily: 'Raleway',
                        fontWeight: FontWeight.bold,
                      ),
                      child: Text(
                        "Description du contenu",
                      ),
                    )
                  ],
                ),
                const Divider(color: Colors.black, height: 20),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.center,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal,
                    ),
                    child: FittedBox(
                      child: Text(
                        "Quel est le titre du contenu ?",
                      ),
                    ),
                  ),
                ),
                Material(
                  child: SmTextFormField(
                    titleController,
                    fieldName: "Entrez le titre du contenu",
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.center,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal,
                    ),
                    child: FittedBox(
                      child: Text(
                        "Quels sont les mots clés auquels fait reférence le contenu ?",
                      ),
                    ),
                  ),
                ),
                Material(
                  child: TextField(
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    maxLines: 5,
                    controller: keywordsController,
                    decoration: InputDecoration(
                      hintText:
                          "Listez les disciplines, thématiques, concepts, théorèmes, auteur et niveau scolaire relatif au contenu",
                      contentPadding: EdgeInsets.all(20.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  alignment: Alignment.centerLeft,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 18,
                      color: primaryColor,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal,
                    ),
                    child: Text("Décrivez votre contenu"),
                  ),
                ),
                Material(
                  child: TextField(
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    maxLines: 5,
                    controller: descriptionController,
                    decoration: InputDecoration(
                      hintText: "Redigez un résumé du contenu "
                          "en reprennant les principaux sujets "
                          "traités.\nDécrivez égalemnt le "
                          "contexte dans leqel vous avez été "
                          "amené à le réaliser ou le consulter "
                          "afin de mettre en lumière toute son "
                          "utilité et sa pertinance",
                      contentPadding: EdgeInsets.all(20.0),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(flex: 2, child: Container()),
                    Expanded(
                      flex: 46,
                      child: Column(
                        children: [
                          if (_isSubmitted)
                            Container(
                              child: SizedBox(
                                width: 20,
                                height: 15,
                                child: CircularProgressIndicator(
                                  color: greenColor,
                                ),
                              ),
                            ),
                          if (!_isSubmitted)
                            SmElevatedButton(
                              label: "Publier",
                              color: greenColor,
                              handleButton: _submitPost,
                            ),
                        ],
                      ),
                    ),
                    Expanded(flex: 4, child: Container()),
                    Expanded(
                      flex: 46,
                      child: SmElevatedButton(
                        label: "Annuler",
                        color: Colors.grey,
                        handleButton: _cancelPost,
                      ),
                    ),
                    Expanded(flex: 2, child: Container())
                  ],
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        },
      ),
    );
  }
}
