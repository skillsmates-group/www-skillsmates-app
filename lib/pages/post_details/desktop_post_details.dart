import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/post_details/widgets/desktop_post_details_content.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_footer_row.dart';

import '../dashboard/widgets/desktop_nav_bar.dart';

class DesktopPostDetails extends StatelessWidget {
  const DesktopPostDetails({super.key});

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 90,
            vertical: 30,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              DesktopPostDetailsContent(),
              SmFooterRow(),
            ],
          ),
        ),
      ),
    );
  }
}
