import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/post_details/desktop_post_details.dart';
import 'package:www_skillsmates_app/pages/post_details/mobile_post_details.dart';

class PostDetailsPage extends StatelessWidget {
  const PostDetailsPage({super.key});
  static const routeName = '/post-details';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const DesktopPostDetails();
      }
      if (sizingInformation.isTablet) {
        return const MobilePostDetails();
      }
      return const MobilePostDetails();
    });
  }
}
