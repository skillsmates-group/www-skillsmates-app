import 'dart:convert';
import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:www_skillsmates_app/models/posts/post_response.dart';

import '../../models/account/account.dart';
import '../../models/posts/post.dart';
import '../../providers/properties.dart';

class PostDetailsProvider extends ChangeNotifier {
  String _currentPostId = "";
  PostResponse _postResponse = new PostResponse(meta: null, resource: null, resources: null);

  String get currentPostId => _currentPostId;

  PostResponse get postResponse => _postResponse;

  void initVariables() async {
    fetchPost();
    notifyListeners();
  }

  Future<PostResponse> fetchPost() async {
    String id = await getCurrentPostId();
    PostResponse response = await fetchPostById(id);
    return response;
  }

  Account getPostOwner() {
    return _postResponse.resource?.account ?? Account(uuid: '', active: false, firstname: '', lastname: '', email: '');
  }

  String getOwnerFirstname() {
    return getPostOwner().firstname;
  }

  String getOwnerLastname() {
    return getPostOwner().lastname;
  }

  Post getPost() {
    return _postResponse.resource!;
  }

  List<Post> getOtherPosts() {
    return _postResponse.resources ?? List.empty();
  }

  Future<String> getCurrentPostId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _currentPostId = prefs.getString('currentPostId') ?? '';
    notifyListeners();
    return _currentPostId;
  }

  Future<PostResponse> fetchPostById(String id) async {
    final url = Uri.parse(skillsmates_posts + '/posts/' + id);
    try {
      final response = await http.get(
        url,
        headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _postResponse = PostResponse.fromJson(body);
      notifyListeners();
      return _postResponse;
    } catch (e) {
      throw e;
    }
  }
}
