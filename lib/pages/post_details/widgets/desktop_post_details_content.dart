import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/post_details/post_details_provider.dart';
import 'package:www_skillsmates_app/pages/post_details/widgets/post_preview.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../../models/posts/post_response.dart';
import '../../dashboard/widgets/post_container.dart';
import '../../dashboard/widgets/vertical_profile_account_card.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_future_builder_error.dart';

class DesktopPostDetailsContent extends StatefulWidget {
  const DesktopPostDetailsContent({Key? key});

  @override
  State<DesktopPostDetailsContent> createState() => _DesktopPostDetailsContentState();
}

class _DesktopPostDetailsContentState extends State<DesktopPostDetailsContent> {
  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    final _postDetailsProvider = Provider.of<PostDetailsProvider>(context, listen: false);

    return FutureBuilder<PostResponse>(
        future: _postDetailsProvider.fetchPost(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            PostResponse postResponse = snapshot.data!;
            return Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: padding()),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                          flex: 3,
                          child: SingleChildScrollView(
                              child: Column(children: [
                            Align(
                                alignment: Alignment.topRight,
                                child: Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      InkWell(
                                          onTap: () => context.pop(),
                                          child: Row(children: [
                                            Flexible(
                                                flex: 5, child: SvgPicture.asset(arrow_left, width: 30, height: 30)),
                                            Flexible(flex: 7, child: Text("Retour"))
                                          ])),
                                      VerticalProfileAccountCard(account: postResponse.getPostOwner())
                                    ])))
                          ]))),
                      Flexible(flex: 6, child: Container(child: PostContainer(post: postResponse.getPost()))),
                      Flexible(
                          flex: 3,
                          child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                  padding: EdgeInsets.only(left: 8),
                                  child: Column(children: [
                                    RichText(
                                        text: TextSpan(text: "D'autres publications de ", children: [
                                      TextSpan(
                                          text:
                                              '${postResponse.getOwnerFirstname()} ${postResponse.getOwnerLastname()}',
                                          style: TextStyle(fontWeight: FontWeight.bold))
                                    ])),
                                    SizedBox(height: 5),
                                    if (postResponse.getOtherPosts().isNotEmpty)
                                      Container(
                                          padding: EdgeInsets.all(2.0),
                                          child: ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: postResponse.getOtherPosts().length,
                                              itemBuilder: (ctx, i) =>
                                                  PostPreview(post: postResponse.getOtherPosts()[i])))
                                  ]))))
                    ]));
          } else if (snapshot.hasError) {
            return SmFutureBuilderError(error: snapshot.error.toString());
          } else {
            return SmFutureBuilderWaiting();
          }
        });
  }
}
