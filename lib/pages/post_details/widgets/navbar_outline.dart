import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';

import '../../../themes/theme.dart';
import '../../login/login_page.dart';
import '../../widgets/sm_elevated_button.dart';

class NavbarOutline extends StatelessWidget {
  const NavbarOutline({super.key});

  @override
  Widget build(BuildContext context) {
    loadPage(String page) {
      context.push(page);
      // Navigator.of(context).pushReplacementNamed(page);
    }

    return Container(
      decoration: const BoxDecoration(
        color: whitishColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black45,
            blurRadius: 15.0,
            offset: Offset(
              0.0,
              0.75,
            ),
          ),
        ],
      ),
      height: 70,
      padding: const EdgeInsets.symmetric(
        horizontal: 50,
      ),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Row(
              children: [
                SizedBox(
                  width: 40,
                  height: 40,
                  child: SvgPicture.asset('assets/images/symbole_skillsmates.svg'),
                ),
                // const SizedBox(width: 8),
                // const SizedBox(
                //   child: Text(
                //     'Skills Mates',
                //     style: TextStyle(
                //       fontSize: 22,
                //       fontWeight: FontWeight.bold,
                //       color: primaryColor,
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                SizedBox(
                  width: 160,
                  child: SmElevatedButton(
                    label: 'Connectez-vous',
                    color: primaryColor,
                    handleButton: () => loadPage(LoginPage.routeName),
                  ),
                ),
                const SizedBox(width: 8),
                SizedBox(
                  width: 160,
                  child: SmElevatedButton(
                    label: 'Inscrivez-vous',
                    color: greenColor,
                    handleButton: () => loadPage(LoginPage.routeName),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
