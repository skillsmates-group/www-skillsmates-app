import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/providers/post_provider.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import '../../../themes/picture_file.dart';
import '../post_details_page.dart';

class PostPreview extends StatefulWidget {
  final Post post;

  const PostPreview({super.key, required this.post});

  @override
  State<PostPreview> createState() => _PostPreviewState();
}

class _PostPreviewState extends State<PostPreview> {
  void _navigateToPostDetails(Post post) {
    PostProvider postProvider = Provider.of<PostProvider>(context, listen: false);
    postProvider.setCurrentPost(post).then((value) => {context.push(PostDetailsPage.routeName)});
  }

  bool isYouTubeUrl(String content) {
    RegExp regExp = RegExp(
        r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?');
    String? matches = regExp.stringMatch(content);
    if (matches == null) {
      return false;
    }
    return true;
  }

  String? getMediaType(String path) {
    if (isYouTubeUrl(path)) {
      return 'YOUTUBE';
    }

    final mimeType = lookupMimeType(path);

    if (mimeType != null) {
      if (mimeType.startsWith('image/')) {
        return 'IMAGE';
      } else if (mimeType.startsWith('application/msword')) {
        return 'DOCUMENT';
      } else if (mimeType.startsWith('application/pdf')) {
        return 'PDF';
      } else if (mimeType.startsWith('application/audio')) {
        return 'AUDIO';
      } else if (mimeType.startsWith('application/video')) {
        return 'VIDEO';
      }
    }
    return 'LINK';
  }

  String getDefaultImage() {
    if ((getMediaType(widget.post.link ?? '') == 'LINK')) return lien_jaune;
    if ((getMediaType(widget.post.link ?? '') == 'DOCUMENT')) return document_bleu;
    if ((getMediaType(widget.post.link ?? '') == 'AUDIO')) return audio_rose;
    if ((getMediaType(widget.post.link ?? '') == 'VIDEO')) return video_rouge;
    if ((getMediaType(widget.post.link ?? '') == 'IMAGE'))
      return photo_vert;
    else
      return '';
  }

  bool isValidPostData() {
    return List.of(['LINK', 'DOCUMENT', 'AUDIO', 'VIDEO', 'IMAGE']).contains(getMediaType(widget.post.link ?? ''));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 300,
        padding: EdgeInsets.all(8),
        child: InkWell(
          onTap: () => _navigateToPostDetails(widget.post),
          child: Column(children: [
            if (widget.post.thumbnail == null) const SizedBox.shrink(),
            if (getMediaType(widget.post.link ?? '') == 'YOUTUBE')
              Row(children: [
                Expanded(
                    child: Container(
                        height: 170,
                        child: YoutubePlayerControllerProvider(
                            controller: YoutubePlayerController(
                                initialVideoId: getIdFromUrl(widget.post.link ?? '') ?? 'nPt8bK2gbaU',
                                params: YoutubePlayerParams(
                                    // playlist: ['nPt8bK2gbaU', 'gQDByCdjUXw'], // Defining custom playlist
                                    startAt: Duration(seconds: 10),
                                    showControls: false,
                                    showFullscreenButton: false,
                                    autoPlay: false,
                                    mute: true)),
                            child: YoutubePlayerIFrame(aspectRatio: 16 / 9))))
              ]),
            if (getMediaType(widget.post.link ?? '') == 'PDF')
              Row(children: [
                Expanded(
                    child: Container(
                        height: 170,
                        child: SfPdfViewer.network(widget.post.link ?? '',
                            canShowPaginationDialog: false,
                            scrollDirection: PdfScrollDirection.vertical,
                            pageLayoutMode: PdfPageLayoutMode.continuous)))
              ]),
            if (isValidPostData())
              Row(children: [
                Expanded(
                    child: Container(
                        height: 170,
                        child: CachedNetworkImage(
                            imageUrl: widget.post.thumbnail!,
                            placeholder: (context, url) => CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                SvgPicture.asset(getDefaultImage(), fit: BoxFit.contain))))
              ]),
            const SizedBox(height: 5),
            Text('${widget.post.title} \n ${widget.post.documentTitle ?? ''}',
                maxLines: 3, overflow: TextOverflow.ellipsis),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              Container(
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Icon(Icons.thumb_up, size: 14),
                const SizedBox(width: 4.0),
                FittedBox(fit: BoxFit.scaleDown, child: Text(widget.post.likes.toString()))
              ])),
              Container(
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Icon(Icons.message_rounded, size: 14),
                const SizedBox(width: 4.0),
                FittedBox(fit: BoxFit.scaleDown, child: Text(widget.post.comments.toString()))
              ])),
              Container(
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Icon(Icons.share, size: 14),
                const SizedBox(width: 4.0),
                FittedBox(fit: BoxFit.scaleDown, child: Text(widget.post.shares.toString()))
              ]))
            ])
          ]),
        ));
  }
}
