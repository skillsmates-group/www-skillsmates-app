import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/post_display/widgets/desktop_post_display_content.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_footer_row.dart';

import '../dashboard/widgets/desktop_nav_bar.dart';

class DesktopPostDisplayScreen extends StatelessWidget {
  final String postId;
  DesktopPostDisplayScreen({super.key, required this.postId});

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            DesktopPostDisplayContent(postId: postId),
            SmFooterRow(),
          ],
        ),
      ),
    );
  }
}
