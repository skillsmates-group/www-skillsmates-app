import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/post_display/widgets/mobile_post_display_content.dart';

import '../dashboard/widgets/mobile_nav_bar.dart';
import '../dashboard/widgets/mobile_top_nav_bar.dart';

class MobilePostDisplayScreen extends StatelessWidget {
  final String postId;
  const MobilePostDisplayScreen({Key? key, required this.postId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MobilePostDisplayContent(
              postId: postId,
            ),
          ],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
