import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/post_display/desktop_post_display_screen.dart';

import 'mobile_post_display_screen.dart';

class PostDisplayPage extends StatelessWidget {
  final String postId;
  PostDisplayPage({super.key, required this.postId});
  static const routeName = '/post/:postId';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopPostDisplayScreen(
          postId: postId,
        );
      }
      if (sizingInformation.isTablet) {
        return DesktopPostDisplayScreen(
          postId: postId,
        );
      }

      return MobilePostDisplayScreen(
        postId: postId,
      );
    });
  }
}
