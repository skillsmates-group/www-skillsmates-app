import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../../../models/account/account.dart';
import '../../../models/posts/post_response.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/theme.dart';
import '../../dashboard/widgets/post_container.dart';
import '../../dashboard/widgets/vertical_profile_account_card.dart';
import '../../login/login_page.dart';
import '../../post_details/widgets/post_preview.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';

class DesktopPostDisplayContent extends StatefulWidget {
  final String postId;
  DesktopPostDisplayContent({super.key, required this.postId});

  @override
  State<DesktopPostDisplayContent> createState() => _DesktopPostDisplayContentState();
}

class _DesktopPostDisplayContentState extends State<DesktopPostDisplayContent> {
  Future<PostResponse> fetchData() async {
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    return postProvider.fetchPostById(widget.postId);
  }

  void _navigateToLogin() {
    context.push(LoginPage.routeName);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: padding(),
      ),
      child: FutureBuilder<PostResponse>(
          future: fetchData(),
          builder: (BuildContext context, AsyncSnapshot<PostResponse> snapshot) {
            if (snapshot.hasData) {
              PostResponse postResponse = snapshot.data!;
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 3,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                right: 8,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  VerticalProfileAccountCard(account: postResponse.resource!.account!),
                                  FutureBuilder<Account>(
                                    future: authProvider.getLoggedAccount(),
                                    builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                                      if (snapshot.hasData) {
                                        return const SizedBox(height: 8);
                                      } else if (snapshot.hasError) {
                                        return Column(
                                          children: [
                                            const Text(
                                              "Rejoignez la communauté SkillsMates et découvrez d'autres contenus enrichissants",
                                              style: TextStyle(
                                                color: primaryColor,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                            const SizedBox(height: 8),
                                            SizedBox(
                                              width: 160,
                                              child: SmElevatedButton(
                                                label: 'Inscrivez-vous',
                                                color: greenColor,
                                                handleButton: () => _navigateToLogin(),
                                              ),
                                            ),
                                          ],
                                        );
                                      } else {
                                        return SmFutureBuilderWaiting();
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 6,
                    child: Container(
                      child: PostContainer(post: postResponse.resource!),
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        padding: EdgeInsets.only(
                          left: 8,
                        ),
                        child: Column(
                          children: [
                            RichText(
                              text: TextSpan(
                                text: "D'autres publications de ",
                                children: [
                                  TextSpan(
                                    text:
                                        '${postResponse.resource?.account?.firstname} ${postResponse.resource?.account?.lastname}',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            if (postResponse.resources != null)
                              Container(
                                padding: EdgeInsets.all(2.0),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: postResponse.resources?.length,
                                  itemBuilder: (ctx, i) => PostPreview(post: postResponse.resources![i]),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          }),
    );
  }
}
