import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../../../models/account/account.dart';
import '../../../models/posts/post_response.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/theme.dart';
import '../../dashboard/widgets/post_container.dart';
import '../../dashboard/widgets/vertical_profile_account_card.dart';
import '../../login/login_page.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';

class MobilePostDisplayContent extends StatefulWidget {
  final String postId;
  const MobilePostDisplayContent({super.key, required this.postId});

  @override
  State<MobilePostDisplayContent> createState() => _MobilePostDisplayContentState();
}

class _MobilePostDisplayContentState extends State<MobilePostDisplayContent> {
  Future<PostResponse> fetchData() async {
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    return postProvider.fetchPostById(widget.postId);
  }

  void _navigateToLogin() {
    context.push(LoginPage.routeName);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 0,
      ),
      child: FutureBuilder<PostResponse>(
          future: fetchData(),
          builder: (BuildContext context, AsyncSnapshot<PostResponse> snapshot) {
            if (snapshot.hasData) {
              PostResponse postResponse = snapshot.data!;
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      child: PostContainer(post: postResponse.resource!),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    // Align(
                    //   alignment: Alignment.topLeft,
                    //   child: Container(
                    //     padding: EdgeInsets.only(
                    //       left: 8,
                    //     ),
                    //     child: Column(
                    //       children: [
                    //         RichText(
                    //           text: TextSpan(
                    //             text: "D'autres publications de ",
                    //             children: [
                    //               TextSpan(
                    //                 text:
                    //                     '${postResponse.resource?.account?.firstname} ${postResponse.resource?.account?.lastname}',
                    //                 style: TextStyle(
                    //                   fontWeight: FontWeight.bold,
                    //                 ),
                    //               ),
                    //             ],
                    //           ),
                    //         ),
                    //         SizedBox(
                    //           height: 5,
                    //         ),
                    //         if (postResponse.resources != null)
                    //           Container(
                    //             padding: EdgeInsets.all(2.0),
                    //             child: ListView.builder(
                    //               shrinkWrap: true,
                    //               itemCount: postResponse.resources?.length,
                    //               itemBuilder: (ctx, i) => PostPreview(post: postResponse.resources![i]),
                    //             ),
                    //           ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 8,
                    // ),
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 8,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          VerticalProfileAccountCard(account: postResponse.resource!.account!),
                          FutureBuilder<Account>(
                            future: authProvider.getLoggedAccount(),
                            builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                              if (snapshot.hasData) {
                                return const SizedBox(height: 8);
                              } else if (snapshot.hasError) {
                                return Column(
                                  children: [
                                    const Text(
                                      "Rejoignez la communauté SkillsMates et découvrez d'autres contenus enrichissants",
                                      style: TextStyle(
                                        color: primaryColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                    const SizedBox(height: 8),
                                    SizedBox(
                                      width: 160,
                                      child: SmElevatedButton(
                                        label: 'Inscrivez-vous',
                                        color: greenColor,
                                        handleButton: () => _navigateToLogin(),
                                      ),
                                    ),
                                  ],
                                );
                              } else {
                                return SmFutureBuilderWaiting();
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          }),
    );
  }
}
