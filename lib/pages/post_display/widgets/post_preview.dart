import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mime/mime.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import '../../../themes/picture_file.dart';

class PostPreview extends StatelessWidget {
  final Post post;
  const PostPreview({super.key, required this.post});

  bool isYouTubeUrl(String content) {
    RegExp regExp = RegExp(
        r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?');
    String? matches = regExp.stringMatch(content);
    if (matches == null) {
      return false;
    }
    return true;
  }

  String? getMediaType(String path) {
    if (isYouTubeUrl(path)) {
      return 'YOUTUBE';
    }

    final mimeType = lookupMimeType(path);

    if (mimeType != null) {
      if (mimeType.startsWith('image/')) {
        return 'IMAGE';
      } else if (mimeType.startsWith('application/msword')) {
        return 'DOCUMENT';
      } else if (mimeType.startsWith('application/pdf')) {
        return 'PDF';
      } else if (mimeType.startsWith('application/audio')) {
        return 'AUDIO';
      } else if (mimeType.startsWith('application/video')) {
        return 'VIDEO';
      }
    }
    return 'LINK';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      padding: EdgeInsets.all(8),
      child: Column(children: [
        if (post.thumbnail == null) const SizedBox.shrink(),
        if (getMediaType(post.link ?? '') == 'YOUTUBE')
          Center(
            child: YoutubePlayerControllerProvider(
              controller: YoutubePlayerController(
                initialVideoId: getIdFromUrl(post.link ?? '') ?? 'nPt8bK2gbaU',
                params: YoutubePlayerParams(
                  // playlist: ['nPt8bK2gbaU', 'gQDByCdjUXw'], // Defining custom playlist
                  startAt: Duration(seconds: 10),
                  showControls: true,
                  showFullscreenButton: true,
                  autoPlay: false,
                  mute: true,
                ),
              ),
              child: YoutubePlayerIFrame(
                aspectRatio: 16 / 9,
              ),
            ),
          ),
        if (getMediaType(post.link ?? '') == 'IMAGE')
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: CachedNetworkImage(
                imageUrl: post.thumbnail!,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => SvgPicture.asset(
                  video_rouge,
                  height: 200,
                ),
              ),
            ),
          ),
        if (getMediaType(post.link ?? '') == 'PDF')
          Center(
            child: Container(
              // height: 200,
              child: Center(
                child: SvgPicture.asset(document_bleu, height: 50),
              ),
              // child: SfPdfViewer.network(
              //   post.link ?? '',
              //   canShowPaginationDialog: false,
              // ),
            ),
          ),
        if (getMediaType(post.link ?? '') == 'VIDEO')
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: SvgPicture.asset(video_rouge, height: 50),
            ),
          ),
        if (getMediaType(post.link ?? '') == 'AUDIO')
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: SvgPicture.asset(audio_rose, height: 50),
            ),
          ),
        if (getMediaType(post.link ?? '') == 'DOCUMENT')
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: SvgPicture.asset(document_bleu, height: 50),
            ),
          ),
        if (getMediaType(post.link ?? '') == 'LINK')
          Container(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: InkWell(
              onTap: () {
                launchUrlString(post.link!);
              },
              child: Center(
                child: Center(
                  child: SvgPicture.asset(lien_jaune, height: 50),
                ),
                // child: CachedNetworkImage(
                //   height: 200,
                //   imageUrl: post.thumbnail!,
                //   placeholder: (context, url) => CircularProgressIndicator(),
                //   errorWidget: (context, url, error) => SvgPicture.asset(
                //     lien_jaune,
                //     height: 200,
                //   ),
                // ),
              ),
            ),
          ),
        const SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.thumb_up,
                  ),
                  const SizedBox(width: 4.0),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(post.likes.toString()),
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.message_rounded,
                  ),
                  const SizedBox(width: 4.0),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(post.comments.toString()),
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.share,
                  ),
                  const SizedBox(width: 4.0),
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(post.shares.toString()),
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Text(
          '${post.title} \n ${post.documentTitle ?? ''}',
          maxLines: 3,
          overflow: TextOverflow.ellipsis,
        ),
      ]),
    );
  }
}
