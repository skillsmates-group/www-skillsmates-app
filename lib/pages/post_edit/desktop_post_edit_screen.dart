import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/post_edit/widgets/desktop_post_edit_content.dart';

import '/pages/dashboard/widgets/desktop_nav_bar.dart';

class DesktopPostEditScreen extends StatefulWidget {
  const DesktopPostEditScreen({Key? key}) : super(key: key);

  @override
  State<DesktopPostEditScreen> createState() => _DesktopPostEditScreenState();
}

class _DesktopPostEditScreenState extends State<DesktopPostEditScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DesktopPostEditContent(),
          ],
        ),
      ),
    );
  }
}
