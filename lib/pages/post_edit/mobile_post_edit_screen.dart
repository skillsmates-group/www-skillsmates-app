import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/post_edit/widgets/mobile_post_edit_content.dart';

import '../dashboard/widgets/mobile_nav_bar.dart';
import '../dashboard/widgets/mobile_top_nav_bar.dart';

class MobilePostEditScreen extends StatefulWidget {
  const MobilePostEditScreen({Key? key}) : super(key: key);

  @override
  State<MobilePostEditScreen> createState() => _MobilePostEditScreenState();
}

class _MobilePostEditScreenState extends State<MobilePostEditScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MobilePostEditContent(),
          ],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
