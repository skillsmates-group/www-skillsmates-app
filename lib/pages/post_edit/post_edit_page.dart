import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../widgets/responsive.dart';
import 'desktop_post_edit_screen.dart';
import 'mobile_post_edit_screen.dart';

class PostEditPage extends StatelessWidget {
  const PostEditPage({Key? key}) : super(key: key);

  static const routeName = '/post/edit';

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const DesktopPostEditScreen();
      }
      if (sizingInformation.isTablet) {
        return const DesktopPostEditScreen();
      }
      return const MobilePostEditScreen();
    });
  }
}
