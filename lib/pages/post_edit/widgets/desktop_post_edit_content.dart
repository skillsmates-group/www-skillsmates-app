import 'dart:io';
import 'dart:typed_data';

import 'package:any_link_preview/any_link_preview.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:mime/mime.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/documents/document_type.dart';
import 'package:www_skillsmates_app/models/media.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/dashboard/dashboard_page.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_footer_row.dart';
import 'package:www_skillsmates_app/providers/post_provider.dart';

import '../../../data/media_data.dart';
import '../../../models/sm_error.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_document_types_dropdown.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/sm_info_text.dart';
import '../../widgets/sm_mandatory_field_error_message.dart';
import '../../widgets/sm_media_menu.dart';
import '../../widgets/sm_text_form_field.dart';

class DesktopPostEditContent extends StatefulWidget {
  const DesktopPostEditContent({Key? key}) : super(key: key);

  static const infoText = "Nous vous prions de prendre le soin répertorier et"
      " referencer votre contenu afin d'en faciliter l'accès à l'ensemble de "
      "la communauté. \nVeillez renseigner chacune des cases en vous laissant"
      " guider par les instructions. ces informations nous aiderons à "
      "optimiser les résultats de l'outil de recherche au sein de la "
      "bibliothèque partagée";

  static const errorText = "Certaines valeurs requises sont manquantes";

  @override
  State<DesktopPostEditContent> createState() => _DesktopPostEditContentState();
}

class _DesktopPostEditContentState extends State<DesktopPostEditContent> {
  late Media selectedMediaType;
  final categoryController = TextEditingController();
  final mediaUrlController = TextEditingController();
  final titleController = TextEditingController();
  final keywordsController = TextEditingController();
  final descriptionController = TextEditingController();

  late DocumentType documentType = DocumentType();
  late File documentFile;
  late Post post;
  late File selectedFile;
  late SmError smError = SmError();

  List<PlatformFile>? _paths;
  String? _extension;
  bool _multiPick = false;
  bool _isSubmitted = false;
  bool _isPostValidated = true;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = TextEditingController();
  late Account loggedAccount;
  Uint8List? _selectedFileBytes;
  late String selectedLink = "";

  selectFile() async {
    _paths = (await FilePicker.platform.pickFiles(
      type: _pickingType,
      allowMultiple: _multiPick,
      onFileLoading: (FilePickerStatus status) => print(status),
      allowedExtensions: (_extension?.isNotEmpty ?? false) ? _extension?.replaceAll(' ', '').split(',') : null,
    ))
        ?.files;
    setState(() {
      _paths = _paths;
      _selectedFileBytes = _paths?.first.bytes;
      this.mediaUrlController.text = _paths?.first.name ?? '';
    });
  }

  String? getMediaType(String path) {
    if (isYouTubeUrl(path)) {
      return 'YOUTUBE';
    }

    final mimeType = lookupMimeType(path);

    if (mimeType != null) {
      if (mimeType.startsWith('image/')) {
        return 'IMAGE';
      } else if (mimeType.startsWith('application/msword')) {
        return 'DOCUMENT';
      } else if (mimeType.startsWith('application/pdf')) {
        return 'PDF';
      } else if (mimeType.startsWith('application/audio')) {
        return 'AUDIO';
      } else if (mimeType.startsWith('application/video')) {
        return 'VIDEO';
      }
    }
    return 'LINK';
  }

  bool isYouTubeUrl(String content) {
    RegExp regExp = RegExp(
        r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?');
    String? matches = regExp.stringMatch(content);
    if (matches == null) {
      return false;
    }
    return true;
  }

  void _navigateTo(String routeName) {
    context.push(routeName);
  }

  void showSuccessToast(String message) {
    MotionToast.success(
      title: Text("Modification d'une publication"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void showErrorToast(String message) {
    MotionToast.error(
      title: Text("Une erreur est survenue"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  Future<void> _cancelPost() async {
    _navigateTo(DashboardPage.routeName);
  }

  Future<void> _submitPost() async {
    validatePost();
    if (_isPostValidated) {
      setState(() {
        _isSubmitted = true;
      });
      final postProvider = Provider.of<PostProvider>(context, listen: false);

      postProvider
          .updatePost(post, _paths?.first)
          .then((value) => {
                showSuccessToast('Votre publication a été mise à jour avec succes!'),
                _navigateTo(DashboardPage.routeName),
              })
          .onError((error, stackTrace) => {
                showErrorToast(error.toString()),
              })
          .whenComplete(() => setState(() {
                _isSubmitted = false;
              }));
    }
  }

  void validatePost() {
    SmError error = SmError();
    bool validated = true;

    this.post.title = titleController.text.isNotEmpty ? titleController.text : null;
    this.post.link = mediaUrlController.text.isNotEmpty ? mediaUrlController.text : null;
    this.post.keywords = keywordsController.text.isNotEmpty ? keywordsController.text : null;
    this.post.description = descriptionController.text.isNotEmpty ? descriptionController.text : null;
    this.post.documentType = documentType.code;

    if (this.post.title?.isEmpty ?? true) {
      validated = false;
      error.title = "Le titre est requis";
    }
    if (this.post.link?.isEmpty ?? true) {
      validated = false;
      error.link = "Le lien du contenu est requis";
    }

    if (this.post.documentType?.isEmpty ?? true) {
      validated = false;
      error.category = "La categorie est requise";
    }
    setState(() {
      this._isPostValidated = validated;
      this.smError = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    authProvider.getLoggedAccount().then((value) => loggedAccount = value);

    callback(newDocumentType) {
      setState(() {
        this.smError.category = "";
        documentType = newDocumentType;
      });
    }

    callbackMediaType(mediaType) {
      setState(() {
        selectedMediaType = mediaType;
        postProvider.setSelectedMediaType(selectedMediaType).then((value) => {});
      });
    }

    callbackOnValueChanged(String link) async {
      bool _isValid = AnyLinkPreview.isValidLink(link);
      if (_isValid) {
        setState(() {
          this.selectedLink = link;
        });
      } else {
        debugPrint("URL is not valid");
      }

      setState(() {
        this.smError.link = "";
      });
    }

    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: padding(),
          ),
          child: FutureBuilder<Post>(
            future: postProvider.getCurrentPost(),
            builder: (BuildContext context, AsyncSnapshot<Post> snapshot) {
              if (snapshot.hasData) {
                this.post = snapshot.data!;
                this.selectedMediaType = getMediaByCode(this.post.mediaType!);
                postProvider.setSelectedMediaType(this.selectedMediaType);

                titleController.text = post.title!;
                if (post.documentType != null) {
                  categoryController.text = post.documentType!;
                  documentType.code = post.documentType;
                }
                if (post.keywords != null) {
                  keywordsController.text = post.keywords!;
                }
                if (post.description != null) {
                  descriptionController.text = post.description!;
                }
                if (post.link != null) {
                  mediaUrlController.text = post.link!;
                }
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 3,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              child: DefaultTextStyle(
                                style: TextStyle(
                                  fontSize: 20,
                                  color: blueSkyColor,
                                  fontFamily: 'Raleway',
                                  fontWeight: FontWeight.bold,
                                ),
                                child: Text("Nouveau contenu"),
                              ),
                            ),
                            const Divider(color: blueSkyColor, height: 20),
                            SmInfoText(infoText: DesktopPostEditContent.infoText),
                            const SizedBox(height: 20),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: DefaultTextStyle(
                                style: TextStyle(
                                  fontSize: 16,
                                  color: primaryColor,
                                  fontFamily: "Raleway",
                                  fontWeight: FontWeight.normal,
                                ),
                                child: FittedBox(
                                  child: Text(
                                    "Quel type de contenu souhaitez vous partager ?",
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            SmMediaMenu(
                              enabled: false,
                              callback: callbackMediaType,
                              selectedMediaType: selectedMediaType,
                            ),
                            const SizedBox(height: 20),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: DefaultTextStyle(
                                style: TextStyle(
                                  fontSize: 18,
                                  color: primaryColor,
                                  fontFamily: "Raleway",
                                  fontWeight: FontWeight.normal,
                                ),
                                child: FittedBox(
                                  child: Text(
                                    "Quelle est la catégorie du contenu ?",
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              child: Column(
                                children: [
                                  SmDocumentTypesDropdown(
                                    categoryController,
                                    enabled: false,
                                    defaultValue: post.documentType,
                                    mediaCode: selectedMediaType.code,
                                    callback: callback,
                                  ),
                                  if (smError.category?.isNotEmpty ?? false)
                                    SmMandatoryFieldErrorMessage(
                                      message: this.smError.category!,
                                    ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 20),
                            Container(
                              alignment: Alignment.topLeft,
                              child: DefaultTextStyle(
                                style: TextStyle(
                                  fontSize: 18,
                                  color: primaryColor,
                                  fontFamily: "Raleway",
                                  fontWeight: FontWeight.normal,
                                ),
                                child: FittedBox(
                                  child: Text(
                                    "Collez l'url du contenu ou sélectionnez le sur votre appareil",
                                  ),
                                ),
                              ),
                            ),
                            Column(
                              children: [
                                Material(
                                  child: SmTextFormField(
                                    readOnly: true,
                                    mediaUrlController,
                                    fieldName: "Entrez le lien du contenu",
                                    onValueChanged: callbackOnValueChanged,
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                                if (smError.link?.isNotEmpty ?? false)
                                  SmMandatoryFieldErrorMessage(
                                    message: this.smError.link!,
                                  ),
                              ],
                            ),
                            const SizedBox(height: 20),
                            // SmElevatedButton(
                            //   label: "Choisir le fichier",
                            //   color: primaryColor,
                            //   handleButton: selectFile,
                            // ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      flex: 7,
                      child: Card(
                        margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
                        elevation: isDesktop ? 1.0 : 0.0,
                        shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
                        child: Container(
                          padding: EdgeInsets.all(30),
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: DefaultTextStyle(
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontFamily: 'Raleway',
                                    fontWeight: FontWeight.bold,
                                  ),
                                  child: Text(
                                    "Description du contenu",
                                  ),
                                ),
                              ),
                              const Divider(
                                color: Colors.black,
                                height: 20,
                              ),
                              if (!_isPostValidated)
                                SmInfoText(
                                  infoText: DesktopPostEditContent.errorText,
                                  backgroundColor: redColorBackground,
                                  color: redColor,
                                  image: error,
                                ),
                              const SizedBox(height: 20),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Flexible(
                                    flex: 9,
                                    child: Column(
                                      children: [
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          child: DefaultTextStyle(
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: primaryColor,
                                                fontFamily: "Raleway",
                                                fontWeight: FontWeight.normal),
                                            child: Text(
                                              "Quel est le titre du contenu ?",
                                            ),
                                          ),
                                        ),
                                        Column(
                                          children: [
                                            Material(
                                              child: SmTextFormField(
                                                titleController,
                                                fieldName: "Entrez le titre du contenu",
                                              ),
                                            ),
                                            if (smError.title?.isNotEmpty ?? false)
                                              SmMandatoryFieldErrorMessage(
                                                message: this.smError.title!,
                                              ),
                                          ],
                                        ),
                                        const SizedBox(height: 20),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          child: DefaultTextStyle(
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: primaryColor,
                                                fontFamily: "Raleway",
                                                fontWeight: FontWeight.normal),
                                            child: Text(
                                              "Quels sont les mots clés auquels fait reférence le contenu ?",
                                            ),
                                          ),
                                        ),
                                        Material(
                                          child: TextField(
                                            textInputAction: TextInputAction.newline,
                                            keyboardType: TextInputType.multiline,
                                            maxLines: 5,
                                            controller: keywordsController,
                                            decoration: InputDecoration(
                                              hintText:
                                                  "Listez les disciplines, thématiques, concepts, théorèmes, auteur et niveau scolaire relatif au contenu",
                                              contentPadding: EdgeInsets.all(20.0),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(height: 20),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          child: DefaultTextStyle(
                                            style: TextStyle(
                                              fontSize: 18,
                                              color: primaryColor,
                                              fontFamily: "Raleway",
                                              fontWeight: FontWeight.normal,
                                            ),
                                            child: Text(
                                              "Décrivez votre contenu",
                                            ),
                                          ),
                                        ),
                                        Material(
                                          child: TextField(
                                            textInputAction: TextInputAction.newline,
                                            keyboardType: TextInputType.multiline,
                                            maxLines: 5,
                                            controller: descriptionController,
                                            decoration: InputDecoration(
                                              hintText: "Redigez un résumé du contenu "
                                                  "en reprennant les principaux sujets "
                                                  "traités.\nDécrivez égalemnt le "
                                                  "contexte dans leqel vous avez été "
                                                  "amené à le réaliser ou le consulter "
                                                  "afin de mettre en lumière toute son "
                                                  "utilité et sa pertinance",
                                              contentPadding: EdgeInsets.all(20.0),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(height: 20),
                                        Row(
                                          children: [
                                            Expanded(flex: 2, child: Container()),
                                            Expanded(
                                              flex: 46,
                                              child: Column(
                                                children: [
                                                  if (_isSubmitted && _isPostValidated)
                                                    Container(
                                                      child: SizedBox(
                                                        width: 20,
                                                        height: 15,
                                                        child: CircularProgressIndicator(
                                                          color: greenColor,
                                                        ),
                                                      ),
                                                    ),
                                                  if (!_isSubmitted || !_isPostValidated)
                                                    SmElevatedButton(
                                                      label: "Publier",
                                                      color: greenColor,
                                                      handleButton: _submitPost,
                                                    ),
                                                ],
                                              ),
                                            ),
                                            Expanded(flex: 4, child: Container()),
                                            Expanded(
                                              flex: 46,
                                              child: SmElevatedButton(
                                                label: "Annuler",
                                                color: Colors.grey,
                                                handleButton: _cancelPost,
                                              ),
                                            ),
                                            Expanded(flex: 2, child: Container())
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Flexible(
                                    flex: 3,
                                    child: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.topCenter,
                                      child: Column(
                                        children: [
                                          CachedNetworkImage(
                                            imageUrl: post.thumbnail!,
                                            placeholder: (context, url) => CircularProgressIndicator(),
                                            errorWidget: (context, url, error) => SvgPicture.asset(
                                              this.selectedMediaType.image,
                                            ),
                                          ),
                                          Text(post.title!),
                                          // if (getMediaType(_paths?.first.name ?? '') == 'IMAGE')
                                          //   Center(
                                          //     child: _selectedFileBytes == null
                                          //         ? SmTextButtonImage(
                                          //             label: selectedMediaType.label,
                                          //             image: selectedMediaType.image,
                                          //           )
                                          //         : Image.memory(
                                          //             _selectedFileBytes!,
                                          //             width: 260,
                                          //             height: 260,
                                          //             fit: BoxFit.cover,
                                          //           ),
                                          //   ),
                                          // if (getMediaType(_paths?.first.name ?? '') == 'PDF')
                                          //   SizedBox(
                                          //     height: 200,
                                          //     width: 150,
                                          //     child: _selectedFileBytes == null
                                          //         ? SmTextButtonImage(
                                          //             label: selectedMediaType.label,
                                          //             image: selectedMediaType.image,
                                          //           )
                                          //         : SfPdfViewer.memory(
                                          //             _selectedFileBytes!,
                                          //             pageLayoutMode: PdfPageLayoutMode.single,
                                          //             canShowScrollHead: false,
                                          //           ),
                                          //   ),
                                          // if (getMediaType(selectedLink) == 'YOUTUBE')
                                          //   SizedBox(
                                          //     height: 200,
                                          //     width: 150,
                                          //     child: YoutubePlayerControllerProvider(
                                          //       // Provides controller to all the widget below it.
                                          //       controller: YoutubePlayerController(
                                          //         initialVideoId: getIdFromUrl(selectedLink) ?? 'nPt8bK2gbaU',
                                          //         params: YoutubePlayerParams(
                                          //           startAt: Duration(seconds: 10),
                                          //           showControls: false,
                                          //           showFullscreenButton: false,
                                          //           autoPlay: false,
                                          //           mute: true,
                                          //         ),
                                          //       ),
                                          //       child: YoutubePlayerIFrame(
                                          //         aspectRatio: 16 / 9,
                                          //       ),
                                          //     ),
                                          //   ),
                                          // Text(_paths?.first.name ?? ""),
                                          // if (this.mediaUrlController.text.isEmpty)
                                          //   SmTextButtonImage(
                                          //     label: selectedMediaType.label,
                                          //     image: selectedMediaType.image,
                                          //   ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                );
              } else if (snapshot.hasError) {
                return SmFutureBuilderError();
              } else {
                return SmFutureBuilderWaiting();
              }
            },
          ),
        ),
        SmFooterRow(),
      ],
    );
  }
}
