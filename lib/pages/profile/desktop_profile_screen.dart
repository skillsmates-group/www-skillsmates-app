import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/profile_content.dart';

import '/pages/dashboard/widgets/desktop_nav_bar.dart';

class DesktopProfileScreen extends StatefulWidget {
  const DesktopProfileScreen({Key? key}) : super(key: key);

  @override
  State<DesktopProfileScreen> createState() => _DesktopProfileScreenState();
}

class _DesktopProfileScreenState extends State<DesktopProfileScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ProfileContent(
              scrollController: _trackingScrollController,
            ),
          ],
        ),
      ),
    );
  }
}
