import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/mobile_profile_content.dart';

import '../dashboard/widgets/mobile_nav_bar.dart';
import '../dashboard/widgets/mobile_top_nav_bar.dart';

class MobileProfileScreen extends StatefulWidget {
  const MobileProfileScreen({Key? key}) : super(key: key);

  @override
  _MobileProfileScreenState createState() => _MobileProfileScreenState();
}

class _MobileProfileScreenState extends State<MobileProfileScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MobileProfileContent(
              scrollController: _trackingScrollController,
            ),
          ],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
