import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'desktop_profile_screen.dart';
import 'mobile_profile_screen.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  static const routeName = '/profile';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const DesktopProfileScreen();
      }
      if (sizingInformation.isTablet) {
        return const DesktopProfileScreen();
      }
      return const MobileProfileScreen();
    });
  }
}
