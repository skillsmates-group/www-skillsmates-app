import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../providers/auth_provider.dart';

class BioInfo extends StatelessWidget {
  const BioInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.start, children: const [
        Text('Biographie', textAlign: TextAlign.start, style: const TextStyle(fontWeight: FontWeight.bold))
      ]),
      const SizedBox(height: 15),
      Text(authProvider.profileAccount.biography ?? '', overflow: TextOverflow.ellipsis, softWrap: true, maxLines: 5)
    ]);
  }
}
