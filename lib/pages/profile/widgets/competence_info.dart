import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/profile_edit/profile_edit_page.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';

import '/themes/theme.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../providers/account_provider.dart';
import '../../widgets/sm_text_button_field.dart';

class CompetenceInfo extends StatelessWidget {
  const CompetenceInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future<AccountAttributesResponse> fetchAccountAttributes() async {
      late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
      return attributes;
    }

    void _more() {
      context.push(ProfileEditPage.routeName);
    }

    late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
    return FutureBuilder(
        future: fetchAccountAttributes(),
        builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
          if (snapshot.hasData) {
            return Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text('Compétences  (${attributes.totalSkills})', style: const TextStyle(fontWeight: FontWeight.bold)),
                SmTextButton(label: 'voir plus', color: primaryColor, handleButton: _more)
              ]),
              const SizedBox(height: 15),
              for (var i = 0; i < min(2, (attributes.skills ?? []).length); i++)
                Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                  SvgPicture.asset(attributes.skills![i].image(), height: 30, width: 30),
                  const SizedBox(width: 9),
                  Flexible(
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Text(attributes.skills![i].title ?? '',
                        overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: true),
                    Text(attributes.skills![i].discipline ?? '',
                        overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: true),
                    const SizedBox(height: 15)
                  ]))
                ])
            ]);
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        });
  }
}
