import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import '../../../../../themes/theme.dart';
import '../../../enums/media_type_enum.dart';
import '../../../models/posts/post.dart';
import '../../../themes/picture_file.dart';
import '../../documents_shelf/documents_shelf_page.dart';
import '../../widgets/sm_text_button_field.dart';

class DocFile extends StatefulWidget {
  final String label;
  final Color bgColor;
  final int? total;
  final List<Post>? documents;

  const DocFile({
    Key? key,
    required this.label,
    required this.bgColor,
    this.total,
    this.documents,
  }) : super(key: key);

  @override
  State<DocFile> createState() => _DocFileState();
}

class _DocFileState extends State<DocFile> {
  void _more() {
    context.push(DocumentsShelfPage.routeName);
    // Navigator.of(context).pushReplacementNamed(
    //   DocumentsShelfPage.routeName,
    // );
  }

  String computeImage(Post post) {
    if (post.mediaType!.contains(MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name)) {
      return pdf;
    } else if (post.mediaType!.contains(MediaTypeEnum.MEDIA_TYPE_IMAGE.name)) {
      return photo_vert;
    } else if (post.mediaType!.contains(MediaTypeEnum.MEDIA_TYPE_VIDEO.name)) {
      return video_rouge;
    } else if (post.mediaType!.contains(MediaTypeEnum.MEDIA_TYPE_LINK.name)) {
      return lien_jaune;
    } else if (post.mediaType!.contains(MediaTypeEnum.MEDIA_TYPE_AUDIO.name)) {
      return audio_rose;
    } else {
      return doc;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: BorderSide(
          color: widget.bgColor,
        ),
      ),
      child: SizedBox(
        // height: 120,
        // width: 265,
        child: Column(
          children: [
            Container(
              // width: 265,
              decoration: BoxDecoration(
                color: widget.bgColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      '${widget.label}  (${widget.total})',
                      style: TextStyle(
                        color: whitishColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SmTextButton(
                    label: 'voir plus',
                    color: whitishColor,
                    handleButton: _more,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  for (Post document in widget.documents ?? [])
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SvgPicture.asset(
                          computeImage(document),
                          height: 40,
                          width: 30,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        SizedBox(
                          width: 60,
                          child: Text(
                            document.title ?? '',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                              fontSize: 10,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        const VerticalDivider(
                          width: 20,
                          thickness: 2,
                          indent: 20,
                          endIndent: 0,
                          color: primaryColor,
                        ),
                      ],
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
