import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../../../../themes/picture_file.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../providers/account_provider.dart';

class FormationInfo extends StatelessWidget {
  const FormationInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        Text('Formations  (${attributes.totalTrainings})',
            textAlign: TextAlign.start, style: const TextStyle(fontWeight: FontWeight.bold))
      ]),
      const SizedBox(height: 15),
      for (var i = 0; i < min(2, (attributes.trainings ?? []).length); i++)
        Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          SvgPicture.asset(student, height: 30, width: 30),
          const SizedBox(width: 9),
          Flexible(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(attributes.trainings![i].title ?? '', overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: true),
            Text(attributes.trainings![i].schoolName ?? '',
                overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: true)
          ])),
          const SizedBox(height: 30)
        ]),
      const SizedBox(height: 15),
      for (var i = 0; i < min(2, (attributes.certifications ?? []).length); i++)
        Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          SvgPicture.asset(certification, height: 30, width: 30),
          const SizedBox(width: 9),
          Flexible(
              child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Text(attributes.certifications![i].title ?? '-',
                overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: false),
            /*
            Text(attributes.certifications![i].activityArea?.label ?? '-',
                overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: false),*/
            const SizedBox(height: 15)
          ]))
        ])
    ]);
  }
}
