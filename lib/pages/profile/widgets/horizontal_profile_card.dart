import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/documents_shelf/documents_shelf_page.dart';
import 'package:www_skillsmates_app/pages/network/network_page.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/sm_profil_progress_bar.dart';
import 'package:www_skillsmates_app/pages/widgets/profile_avatar.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_on_hover.dart';
import 'package:www_skillsmates_app/providers/interaction_provider.dart';
import 'package:www_skillsmates_app/providers/post_provider.dart';

import '../../../models/account/account.dart';
import '../../../models/notification/interactionRequest.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../profile_edit/profile_edit_page.dart';

class HorizontalProfileCard extends StatefulWidget {
  const HorizontalProfileCard({Key? key}) : super(key: key);

  @override
  State<HorizontalProfileCard> createState() => _HorizontalProfileCardState();
}

class _HorizontalProfileCardState extends State<HorizontalProfileCard> {
  String _hovered = "";

  void _onHover(String menu) {
    this._hovered = menu;
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final interactionProvider = Provider.of<InteractionProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

    clickEditProfile(Account account) {
      authProvider.setProfileAccount(account).then((value) => {context.push(ProfileEditPage.routeName)});
    }

    _navigateDocumentShelf(Account account) {
      authProvider.setProfileAccount(account).then((value) => {context.push(DocumentsShelfPage.routeName)});
    }

    _navigateToNetwork(Account account, {int selectedTab = 0}) {
      authProvider
          .setProfileAccount(account)
          .then((value) => {context.push(NetworkPage.routeName, extra: selectedTab)});
    }

    void _showMessageError(String message) {
      MotionToast.error(
        title: Text("Une erreur est survenue"),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromRight,
        toastDuration: Duration(seconds: 10),
      ).show(context);
    }

    void _showMessageSuccess(String message) {
      MotionToast.success(
        title: Text("Felicitation"),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromRight,
        toastDuration: Duration(seconds: 10),
      ).show(context);
    }

    void onClickFollow(Account account) {
      InteractionRequest interaction;
      authProvider.getLoggedAccount().then((loggedAccount) => {
            interaction = InteractionRequest(
              entity: account.uuid,
              interactionType: 'INTERACTION_TYPE_FOLLOWER',
              emitter: loggedAccount.uuid,
              receiver: account.uuid,
            ),
            interactionProvider
                .saveInteraction(interaction)
                .then((value) => {
                      _showMessageSuccess('Vous suivez ' + account.firstname),
                    })
                .onError((error, stackTrace) => {_showMessageError('')})
          });
    }

    return Container(
      child: FutureBuilder<Account>(
        future: authProvider.getProfileAccount(),
        builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
          if (snapshot.hasData) {
            Account? account = snapshot.data;
            return Column(
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 18.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          clickEditProfile(account!);
                        },
                        child: ProfileAvatar(
                          imageUrl: account?.avatar?.toLowerCase() ?? '',
                          isActive: account?.active ?? false,
                          isProfil: true,
                          radius: 70.0,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        width: 450,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text.rich(
                                  TextSpan(
                                    text: account?.firstname,
                                    style: const TextStyle(
                                      fontSize: 22.0,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    children: [
                                      TextSpan(text: ' '),
                                      TextSpan(
                                        text: account?.lastname,
                                        style: const TextStyle(
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.w700,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SmOnHover(
                                  callbackOnTap: (s) {
                                    _navigateDocumentShelf(account!);
                                  },
                                  child: Text.rich(
                                    TextSpan(
                                      text: '${account?.posts ?? 0} ',
                                      style: TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Publications',
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                SmOnHover(
                                  callbackOnTap: (s) {
                                    _navigateToNetwork(account!, selectedTab: 1);
                                  },
                                  child: Text.rich(
                                    TextSpan(
                                      text: '${account?.followers ?? 0} ',
                                      style: const TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Abonnés',
                                          style: const TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                SmOnHover(
                                  callbackOnTap: (s) {
                                    _navigateToNetwork(account!, selectedTab: 2);
                                  },
                                  child: Text.rich(
                                    TextSpan(
                                      text: '${account?.followees ?? 0} ',
                                      style: const TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Abonnements',
                                          style: const TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  account?.statusIcon() ?? '',
                                  height: 20,
                                  width: 20,
                                ),
                                const SizedBox(
                                  width: 3,
                                ),
                                Text(
                                  account?.statusLabel() ?? '',
                                  style: const TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  job,
                                  height: 20,
                                  width: 20,
                                ),
                                const SizedBox(
                                  width: 3,
                                ),
                                Text(
                                  account?.currentJob ?? '',
                                  style: const TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  company,
                                  height: 20,
                                  width: 20,
                                ),
                                const SizedBox(
                                  width: 3,
                                ),
                                Text(
                                  account?.currentCompany ?? '',
                                  style: const TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  location,
                                  height: 20,
                                  width: 20,
                                ),
                                const SizedBox(
                                  width: 3,
                                ),
                                Text(
                                  '${account?.city ?? ''}, ${account?.country ?? ''}',
                                  style: const TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.normal,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            if (authProvider.isSameProfile)
                              InkWell(
                                onTap: () {
                                  clickEditProfile(account!);
                                },
                                child: const SmProfilProgressBar(),
                              ),
                            if (!authProvider.isSameProfile)
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  FutureBuilder<bool>(
                                      future: postProvider.checkSocialInteraction(loggedAccountUuid, account!.uuid),
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          bool? follow = snapshot.data;
                                          return Container(
                                              decoration: BoxDecoration(
                                                  color: follow! ? greenColor : null,
                                                  borderRadius: BorderRadius.circular(10),
                                                  border: Border.all(
                                                      color: primaryColor, width: 1, style: BorderStyle.solid)),
                                              child: TextButton.icon(
                                                  onPressed: () {
                                                    onClickFollow(account);
                                                  },
                                                  icon: SvgPicture.asset(user, height: 20, width: 20),
                                                  label: Text(follow! ? 'Abonné(e)' : 'S\'abonner',
                                                      style: TextStyle(color: primaryColor)),
                                                  style: TextButton.styleFrom(fixedSize: Size(200, 40))));
                                        } else if (snapshot.hasError) {
                                          return SmFutureBuilderError();
                                        } else {
                                          return SmFutureBuilderWaiting();
                                        }
                                      }),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(color: primaryColor, width: 1, style: BorderStyle.solid),
                                    ),
                                    child: TextButton.icon(
                                      onPressed: () {},
                                      label: const Text(
                                        'Message',
                                        style: TextStyle(
                                          color: primaryColor,
                                        ),
                                      ),
                                      icon: SvgPicture.asset(
                                        message_selected,
                                        height: 20,
                                        width: 20,
                                      ),
                                      style: TextButton.styleFrom(
                                        fixedSize: const Size(200, 40),
                                        primary: primaryColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: primaryColor, width: 1, style: BorderStyle.solid),
                              ),
                              child: TextButton.icon(
                                onPressed: () {},
                                label: const Text(
                                  'Partager le profil',
                                  style: TextStyle(
                                    color: primaryColor,
                                  ),
                                ),
                                icon: SvgPicture.asset(
                                  share,
                                  height: 20,
                                  width: 20,
                                ),
                                style: TextButton.styleFrom(
                                  fixedSize: const Size(200, 40),
                                  primary: primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 0.0,
                    horizontal: 300.0,
                  ),
                  child: Divider(color: Colors.white, thickness: 5.0),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        },
      ),
    );
  }
}
