import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/themes/theme.dart';
import '../../../models/posts/profile_documents_response.dart';
import '../../../providers/post_provider.dart';

class LinkFile extends StatelessWidget {
  final String label;
  final Color bgColor;
  const LinkFile({
    Key? key,
    required this.label,
    required this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late ProfileDocumentsResponse profileDocuments = Provider.of<PostProvider>(context).profileDocumentsResponse;
    return Card(
      color: bgColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: BorderSide(
          color: bgColor,
        ),
      ),
      child: Container(
        height: 40,
        width: 265,
        padding: const EdgeInsets.all(10),
        child: Text(
          '$label  (${profileDocuments.totalLinks})',
          textAlign: TextAlign.start,
          style: const TextStyle(
            color: whitishColor,
          ),
        ),
      ),
    );
  }
}
