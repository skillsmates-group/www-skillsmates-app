import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/account/account.dart';
import '../../../models/posts/post.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../dashboard/widgets/post_container.dart';

class LoadPosts extends StatefulWidget {
  final ScrollController scrollController;

  const LoadPosts({Key? key, required this.scrollController}) : super(key: key);

  @override
  State<LoadPosts> createState() => _LoadPostsState();
}

class _LoadPostsState extends State<LoadPosts> {
  List<Post> _data = List<Post>.empty(growable: true);
  int _page = 0;
  int _limit = 5;
  bool _isLoading = false;

   ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController = this.widget.scrollController;
    print("#########################\n#\n#\t\t1\n#\n#########################");
    _scrollController.addListener(_onScroll);
    _getData();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    print("#########################\n#\n#\t\t2\n#\n#########################");
    if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent ) {
      print("#########################\n#\n#\t\t3\n#\n#########################");
      _getData();
    }
  }

  Future<void> _getData() async {
    setState(() {
      _isLoading = true;
    });
    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      Account account = await authProvider.getProfileAccount();
      var apiResponse = await Provider.of<PostProvider>(context, listen: false)
          .fetchProfilePostsWithFilters(account.uuid ?? '', _page, _limit);
      List<Post> responses = apiResponse.resources!;
      setState(() {
        _data.addAll(responses);
        _page++;
      });
    } catch (e) {
      print('Error fetching data: $e');
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        controller: _scrollController,
        itemCount: _data.length,
        itemBuilder: (context, index) {
          final data = _data[index];
          return PostContainer(post: data);
        },
        shrinkWrap: true);
  }
}
