import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/media_type_enum.dart';

import '/themes/theme.dart';
import '../../../data/media_data.dart';
import '../../../data/profile_atributes_data.dart';
import '../../../enums/account_attribute_enum.dart';
import '../../../models/account/account.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../models/documents/document_type_metric.dart';
import '../../../models/documents/document_type_metric_response.dart';
import '../../../models/media.dart';
import '../../../models/search_param.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../profile_edit/widgets/biography.dart';
import '../../profile_edit/widgets/job_edit.dart';
import '../../profile_edit/widgets/profile_edit.dart';
import '../../profile_edit/widgets/profile_summary.dart';
import '../../profile_edit/widgets/skills_edit.dart';
import '../../profile_edit/widgets/training_edit.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';

class MobileCursus extends StatefulWidget {
  const MobileCursus({Key? key}) : super(key: key);

  @override
  State<MobileCursus> createState() => _MobileCursusState();
}

class _MobileCursusState extends State<MobileCursus> with TickerProviderStateMixin {
  var _color = blueColor;
  var _currentIndex = 0;
  late TabController _tabController;
  String tab = AccountAttribute.resume.name;
  List<DocumentTypeMetric> selectedDocumentTypes = [];
  SearchParam searchParam = SearchParam();
  int nbDocuments = 0;

  Color getColorSelectedTab(Media media) {
    if (media.code == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name) {
      return blueColor;
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_IMAGE.name) {
      return greenColor.withOpacity(.4);
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_VIDEO.name) {
      return redBoldColor;
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_AUDIO.name) {
      return pinkColor;
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_LINK.name) {
      return yellowColor;
    }

    return blueColor;
  }

  void _selectedTab(int i) {
    Future.delayed(Duration.zero, () async {
      setState(() {
        _color = getColorSelectedTab(medias[i]);
        _currentIndex = i;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: profileAttributes.length, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    _tabController.animateTo(_currentIndex);

    clickDocumentType(DocumentTypeMetric documentTypeMetric) {
      List<String> selected = [];
      setState(() {
        bool alreadySelected =
            selectedDocumentTypes.where((element) => element.code == documentTypeMetric.code).isNotEmpty;
        if (alreadySelected) {
          selectedDocumentTypes.removeWhere((element) => element.code == documentTypeMetric.code);
        } else {
          selectedDocumentTypes.add(documentTypeMetric);
        }
        selectedDocumentTypes.forEach((element) {
          selected.add(element.uuid);
        });
        searchParam.documentTypes = selected;
      });
    }

    List<DocumentTypeMetric> filterDocumentTypesByMedia(Media media, List<DocumentTypeMetric> response) {
      return response.where((element) => element.mediaType.code == media.code).toList();
    }

    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.topCenter,
      // width: 400,
      child: FutureBuilder<Account>(
          future: authProvider.getProfileAccount(),
          builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
            if (snapshot.hasData) {
              Account profileAccount = snapshot.data!;
              searchParam.account = profileAccount.uuid;
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FutureBuilder<DocumentTypeMetricResponse>(
                    future: postProvider.fetchProfileDocumentTypeMetric(profileAccount.uuid),
                    builder: (BuildContext context, AsyncSnapshot<DocumentTypeMetricResponse> snapshot) {
                      if (snapshot.hasData) {
                        return TabBar(
                          isScrollable: true,
                          controller: _tabController,
                          unselectedLabelColor: primaryColor,
                          labelColor: primaryColor,
                          indicatorColor: primaryColor,
                          indicatorWeight: 2,
                          labelStyle: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                          indicatorSize: TabBarIndicatorSize.label,
                          onTap: (int x) => _selectedTab(x),
                          tabs: [
                            for (var profileAttribute in profileAttributes)
                              Tab(
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    // color: Colors.grey,
                                    border: Border.all(
                                      color: Colors.grey,
                                    ),
                                  ),
                                  child: Text(
                                    '${profileAttribute.label}',
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )
                          ],
                        );
                      } else if (snapshot.hasError) {
                        return SmFutureBuilderError();
                      } else {
                        return SmFutureBuilderWaiting();
                      }
                    },
                  ),
                  FutureBuilder<AccountAttributesResponse>(
                    future: Provider.of<AccountProvider>(context, listen: false)
                        .fetchAccountAttributes(profileAccount.uuid),
                    builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
                      if (snapshot.hasData) {
                        return SingleChildScrollView(
                          child: SizedBox(
                            height: 400,
                            child: TabBarView(
                              controller: _tabController,
                              children: [
                                ProfileSummary(
                                  account: profileAccount,
                                  data: snapshot.data!,
                                ),
                                ProfileEdit(),
                                TrainingEdit(),
                                JobEdit(),
                                SkillsEdit(),
                                Biography(),
                              ],
                            ),
                          ),
                        );
                      } else if (snapshot.hasError) {
                        return SmFutureBuilderError(error: snapshot.error.toString());
                      } else {
                        return SmFutureBuilderWaiting();
                      }
                    },
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          }),
    );
  }
}
