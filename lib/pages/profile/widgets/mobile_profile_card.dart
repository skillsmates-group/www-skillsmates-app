import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/sm_profil_progress_bar.dart';

import '/themes/theme.dart';
import '../../../models/account/account.dart';
import '../../../models/notification/interactionRequest.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/interaction_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/picture_file.dart';
import '../../profile_edit/profile_edit_page.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';

class MobileProfileCard extends StatelessWidget {
  const MobileProfileCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final interactionProvider = Provider.of<InteractionProvider>(context, listen: false);
    final String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

    void _showMessageSuccess(String message) {
      MotionToast.success(
        title: Text("Felicitation"),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromRight,
        toastDuration: Duration(seconds: 10),
      ).show(context);
    }

    void _showMessageError(String message) {
      MotionToast.error(
        title: Text("Une erreur est survenue"),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromRight,
        toastDuration: Duration(seconds: 10),
      ).show(context);
    }

    void onClickFollow(Account account) {
      InteractionRequest interaction;
      authProvider.getLoggedAccount().then((loggedAccount) => {
            interaction = InteractionRequest(
              entity: account.uuid,
              interactionType: 'INTERACTION_TYPE_FOLLOWER',
              emitter: loggedAccount.uuid,
              receiver: account.uuid,
            ),
            interactionProvider
                .saveInteraction(interaction)
                .then((value) => {
                      _showMessageSuccess('Vous suivez ' + account.firstname),
                    })
                .onError((error, stackTrace) => {_showMessageError('')})
          });
    }

    void _navigateTo(String routeName) {
      context.push(routeName);
    }

    clickEditProfile(Account account) {
      authProvider.setProfileAccount(account).then((value) => {context.push(ProfileEditPage.routeName)});
    }

    return Container(
      child: FutureBuilder<Account>(
        future: authProvider.getProfileAccount(),
        builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
          if (snapshot.hasData) {
            Account? account = snapshot.data;
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            clickEditProfile(account!);
                          },
                          child: ProfileAvatar(
                            imageUrl: account?.avatar?.toLowerCase() ?? '',
                            isActive: account?.active ?? false,
                            isProfil: true,
                            radius: 70.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          width: 450,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  FittedBox(
                                    child: Text(
                                      account?.firstname ?? '',
                                      style: const TextStyle(
                                        fontSize: 18.0,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                  FittedBox(
                                    child: Text(
                                      account?.lastname ?? '',
                                      style: const TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: '${account?.posts ?? 0} ',
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Publications',
                                          style: const TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      text: '${account?.followers ?? 0} ',
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Abonnés',
                                          style: const TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      text: '${account?.followees ?? 0} ',
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w700,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Abonnements',
                                          style: const TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Wrap(
                    spacing: 8,
                    runSpacing: 8,
                    children: [
                      Chip(
                        avatar: SvgPicture.asset(
                          account?.statusIcon() ?? '',
                          height: 16,
                          width: 16,
                        ),
                        elevation: 1,
                        padding: EdgeInsets.all(8),
                        backgroundColor: whitishColor,
                        label: Text(
                          account?.statusLabel() ?? '',
                          style: const TextStyle(
                            color: primaryColor,
                            fontSize: 14.0,
                            fontWeight: FontWeight.normal,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Chip(
                        avatar: SvgPicture.asset(
                          job,
                          height: 16,
                          width: 16,
                        ),
                        elevation: 1,
                        padding: EdgeInsets.all(8),
                        backgroundColor: whitishColor,
                        label: Text(
                          account?.currentJob ?? '',
                          style: const TextStyle(
                            color: primaryColor,
                            fontSize: 14.0,
                            fontWeight: FontWeight.normal,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Chip(
                        avatar: SvgPicture.asset(
                          company,
                          height: 16,
                          width: 16,
                        ),
                        elevation: 1,
                        padding: EdgeInsets.all(8),
                        backgroundColor: whitishColor,
                        label: Text(
                          account?.currentCompany ?? '',
                          style: const TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.normal,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Chip(
                        avatar: SvgPicture.asset(
                          location,
                          height: 16,
                          width: 16,
                        ),
                        elevation: 1,
                        padding: EdgeInsets.all(8),
                        backgroundColor: whitishColor,
                        label: Text(
                          '${account?.city ?? ''}, ${account?.country ?? ''}',
                          style: const TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.normal,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: [
                      if (authProvider.isSameProfile)
                        Expanded(
                          flex: 1,
                          child: InkWell(
                            onTap: () {
                              clickEditProfile(account!);
                            },
                            child: const SmProfilProgressBar(),
                          ),
                        ),
                      if (!authProvider.isSameProfile)
                        Expanded(
                          flex: 1,
                          child: Container(
                              height: 40,
                              margin: EdgeInsets.all(2.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: primaryColor, width: 1, style: BorderStyle.solid),
                              ),
                              child: FutureBuilder<bool>(
                                future: postProvider.checkSocialInteraction(loggedAccountUuid, account!.uuid),
                                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                  if (snapshot.hasData) {
                                    bool? follow = snapshot.data;
                                    return TextButton.icon(
                                        onPressed: () {
                                          onClickFollow(account!);
                                        },
                                        label: Text(follow! ? 'Abonnée' : 'S\'abonner',
                                            style: TextStyle(color: primaryColor)),
                                        icon: SvgPicture.asset(user, height: 20, width: 20));
                                  } else if (snapshot.hasError) {
                                    return SmFutureBuilderError();
                                  } else {
                                    return SmFutureBuilderWaiting();
                                  }
                                },
                              )),
                        ),
                      if (!authProvider.isSameProfile)
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 40,
                            margin: EdgeInsets.all(2.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: primaryColor, width: 1, style: BorderStyle.solid),
                            ),
                            child: TextButton.icon(
                              onPressed: () {},
                              label: const Text(
                                'Message',
                                style: TextStyle(
                                  color: primaryColor,
                                ),
                              ),
                              icon: SvgPicture.asset(
                                message_selected,
                                height: 20,
                                width: 20,
                              ),
                            ),
                          ),
                        ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: 40,
                          margin: EdgeInsets.all(2.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: primaryColor, width: 1, style: BorderStyle.solid),
                          ),
                          child: TextButton.icon(
                            onPressed: () {},
                            label: const Text(
                              'Partager le profil',
                              style: TextStyle(
                                color: primaryColor,
                              ),
                            ),
                            icon: SvgPicture.asset(
                              share,
                              height: 20,
                              width: 20,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        },
      ),
    );
  }
}
