import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/account/account.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/mobile_cursus.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/sm_tab_profile_mobile.dart';

import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../dashboard/widgets/post_container.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import 'mobile_profile_card.dart';
import 'mobile_shelf.dart';

class MobileProfileContent extends StatefulWidget {
  final TrackingScrollController scrollController;

  const MobileProfileContent({
    Key? key,
    required this.scrollController,
  }) : super(key: key);

  @override
  State<MobileProfileContent> createState() => _MobileProfileContentState();
}

class _MobileProfileContentState extends State<MobileProfileContent> with TickerProviderStateMixin {
  late TabController _tabController;
  int selectedTab = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    return Column(
      children: [
        SizedBox(
          // height: 400,
          child: Column(
            children: [
              const MobileProfileCard(),
              TabBar(
                isScrollable: true,
                onTap: (value) {
                  setState(() {
                    this.selectedTab = value;
                  });
                },
                labelPadding: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                controller: _tabController,
                labelColor: primaryColor,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  // color: Colors.grey,
                  border: Border.all(
                    color: Colors.grey,
                  ),
                ),
                unselectedLabelColor: Colors.white,
                labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                indicatorWeight: 0.1,
                indicatorColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                tabs: [
                  SmTabProfileMobile(
                    selected: this.selectedTab == 0,
                    picture: publication,
                    text: "Mes Publications",
                  ),
                  SmTabProfileMobile(
                    selected: this.selectedTab == 1,
                    picture: shelf,
                    text: "Mon Classeur",
                  ),
                  SmTabProfileMobile(
                    selected: this.selectedTab == 2,
                    picture: cursus,
                    text: "Mon Cursus",
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        FutureBuilder<Account>(
            future: authProvider.getProfileAccount(),
            builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
              if (snapshot.hasData) {
                return SizedBox(
                  height: 500,
                  child: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: [
                      Container(
                        child: FutureBuilder(
                          future: Provider.of<PostProvider>(context, listen: false)
                              .fetchProfilePosts(snapshot.data?.uuid ?? ''),
                          builder: (ctx, dataSnapshot) {
                            if (dataSnapshot.connectionState == ConnectionState.waiting) {
                              return SmFutureBuilderWaiting();
                            } else {
                              if (dataSnapshot.error != null) {
                                return SmFutureBuilderError(
                                    error: 'Une erreur est survenue lors de la recuperation des publications');
                              } else {
                                return Consumer<PostProvider>(
                                  builder: (ctx, data, child) => ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: data.profilePostResponse.resources?.length,
                                    itemBuilder: (ctx, i) =>
                                        PostContainer(post: data.profilePostResponse.resources![i]),
                                  ),
                                );
                              }
                            }
                          },
                        ),
                      ),
                      const MobileShelf(),
                      const MobileCursus(),
                    ],
                  ),
                );
              } else if (snapshot.hasError) {
                return SmFutureBuilderError();
              } else {
                return SmFutureBuilderWaiting();
              }
            }),
      ],
    );
  }
}
