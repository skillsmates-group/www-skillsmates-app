import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/media_type_enum.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/mobile_shelf_tab.dart';

import '/themes/theme.dart';
import '../../../data/media_data.dart';
import '../../../enums/account_attribute_enum.dart';
import '../../../models/account/account.dart';
import '../../../models/documents/document_type_metric.dart';
import '../../../models/documents/document_type_metric_response.dart';
import '../../../models/media.dart';
import '../../../models/search_param.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../documents_shelf/widgets/document_shelf_tile.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';

class MobileShelf extends StatefulWidget {
  const MobileShelf({Key? key}) : super(key: key);

  @override
  State<MobileShelf> createState() => _MobileShelfState();
}

class _MobileShelfState extends State<MobileShelf> with TickerProviderStateMixin {
  var _color = blueColor;
  var _currentIndex = 0;
  late TabController _tabController;
  String tab = AccountAttribute.resume.name;
  List<DocumentTypeMetric> selectedDocumentTypes = [];
  SearchParam searchParam = SearchParam();
  int nbDocuments = 0;

  Color getColorSelectedTab(Media media) {
    if (media.code == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name) {
      return blueColor;
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_IMAGE.name) {
      return greenColor.withOpacity(.4);
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_VIDEO.name) {
      return redBoldColor;
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_AUDIO.name) {
      return pinkColor;
    }
    if (media.code == MediaTypeEnum.MEDIA_TYPE_LINK.name) {
      return yellowColor;
    }

    return blueColor;
  }

  void _selectedTab(int i) {
    Future.delayed(Duration.zero, () async {
      setState(() {
        _color = getColorSelectedTab(medias[i]);
        _currentIndex = i;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 5, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    // TabController tabController = TabController(length: 5, vsync: this);
    _tabController.animateTo(_currentIndex);

    clickDocumentType(DocumentTypeMetric documentTypeMetric) {
      List<String> selected = [];
      setState(() {
        bool alreadySelected =
            selectedDocumentTypes.where((element) => element.code == documentTypeMetric.code).isNotEmpty;
        if (alreadySelected) {
          selectedDocumentTypes.removeWhere((element) => element.code == documentTypeMetric.code);
        } else {
          selectedDocumentTypes.add(documentTypeMetric);
        }
        selectedDocumentTypes.forEach((element) {
          selected.add(element.uuid);
        });
        searchParam.documentTypes = selected;
      });
    }

    List<DocumentTypeMetric> filterDocumentTypesByMedia(Media media, List<DocumentTypeMetric> response) {
      return response.where((element) => element.mediaType.code == media.code).toList();
    }

    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.topCenter,
      // width: 400,
      child: FutureBuilder<Account>(
          future: authProvider.getProfileAccount(),
          builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
            if (snapshot.hasData) {
              Account profileAccount = snapshot.data!;
              searchParam.account = profileAccount.uuid;
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FutureBuilder<DocumentTypeMetricResponse>(
                    future: postProvider.fetchProfileDocumentTypeMetric(profileAccount.uuid),
                    builder: (BuildContext context, AsyncSnapshot<DocumentTypeMetricResponse> snapshot) {
                      if (snapshot.hasData) {
                        return TabBar(
                          isScrollable: true,
                          controller: _tabController,
                          unselectedLabelColor: primaryColor,
                          labelColor: _color,
                          indicatorColor: _color,
                          indicatorSize: TabBarIndicatorSize.label,
                          labelStyle: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal, fontSize: 12),
                          onTap: (int x) => _selectedTab(x),
                          tabs: [
                            for (var media in medias)
                              MobileShelfTab(
                                documentTypes: filterDocumentTypesByMedia(
                                  media,
                                  snapshot.data?.resources ?? [],
                                ),
                                selectedDocumentTypes: selectedDocumentTypes,
                                callback: clickDocumentType,
                              )
                          ],
                        );
                      } else if (snapshot.hasError) {
                        return SmFutureBuilderError();
                      } else {
                        return SmFutureBuilderWaiting();
                      }
                    },
                  ),
                  SingleChildScrollView(
                    child: SizedBox(
                      height: 400,
                      child: TabBarView(
                        controller: _tabController,
                        children: [
                          Container(
                            child: FutureBuilder(
                              future: postProvider.searchPosts(searchParam),
                              builder: (ctx, dataSnapshot) {
                                if (dataSnapshot.connectionState == ConnectionState.waiting) {
                                  return SmFutureBuilderWaiting();
                                } else {
                                  if (dataSnapshot.error != null) {
                                    return SmFutureBuilderError(
                                        error: 'Une erreur est survenue lors de la recuperation des publications');
                                  } else {
                                    return Consumer<PostProvider>(
                                      builder: (ctx, data, child) => ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: data.searchPostResponse!.resources?.length,
                                        itemBuilder: (context, index) {
                                          this.nbDocuments = data.searchPostResponse!.resources?.length ?? 0;
                                          return DocumentShelfTile(post: data.searchPostResponse!.resources![index]);
                                        },
                                      ),
                                    );
                                  }
                                }
                              },
                            ),
                          ),

                          // SmTabbarViewSelect(
                          //   firstText: "Sélectionner un type de document",
                          //   secondText: "Rechercher un document par titre",
                          // ),
                          // SmTabbarViewSelect(
                          //   firstText: "Sélectionner un type de photo",
                          //   secondText: "Rechercher un photo par titre",
                          // ),
                          // SmTabbarViewSelect(
                          //   firstText: "Sélectionner un type de vidéo",
                          //   secondText: "Rechercher une vidéo par titre",
                          // ),
                          // SmTabbarViewSelect(
                          //   firstText: "Sélectionner un type de audio",
                          //   secondText: "Rechercher un audio par titre",
                          // ),
                          // SmTabbarViewSelect(
                          //   firstText: "Sélectionner un type de lien",
                          //   secondText: "Rechercher un lien par titre",
                          // ),
                        ],
                      ),
                    ),
                  )
                ],
              );
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          }),
    );
  }
}
