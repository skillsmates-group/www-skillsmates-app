import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../data/media_data.dart';
import '../../../models/documents/document_type_metric.dart';

class MobileShelfTab extends StatefulWidget {
  List<DocumentTypeMetric> documentTypes;
  List<DocumentTypeMetric>? selectedDocumentTypes;
  Function(DocumentTypeMetric)? callback;

  MobileShelfTab({
    Key? key,
    required this.documentTypes,
    this.selectedDocumentTypes,
    this.callback,
  }) : super(key: key);

  @override
  _MobileShelfTabState createState() => _MobileShelfTabState();
}

class _MobileShelfTabState extends State<MobileShelfTab> {
  clickDocumentType(DocumentTypeMetric documentType) {
    widget.callback!(documentType);
  }

  @override
  Widget build(BuildContext context) {
    int countTotalDocuments() {
      int total = 0;
      widget.documentTypes.forEach((element) {
        total += element.total;
      });
      return total;
    }

    bool isDocumentTypeSelected(DocumentTypeMetric documentType) {
      return widget.selectedDocumentTypes?.where((element) => element.code == documentType.code).isNotEmpty ?? false;
    }

    String getMediaTypeImage(DocumentTypeMetric documentTypeMetric) {
      return medias.where((element) => element.code == documentTypeMetric.mediaType.code).first.image;
    }

    return Tab(
      icon: SvgPicture.asset(
        getMediaTypeImage(widget.documentTypes.first),
        height: 40,
        width: 40,
      ),
      // text: '05',
      child: FittedBox(
        child: Text(
          '${widget.documentTypes.first.mediaType.label} (${countTotalDocuments()})',
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
