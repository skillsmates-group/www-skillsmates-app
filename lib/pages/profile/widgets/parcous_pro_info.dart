import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../../models/account/account_attributes_response.dart';
import '../../../providers/account_provider.dart';

class ParcoursProInfo extends StatelessWidget {
  const ParcoursProInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        Text('Parcous Professionnel  (${attributes.totalJobs})',
            textAlign: TextAlign.start, style: const TextStyle(fontWeight: FontWeight.bold))
      ]),
      const SizedBox(height: 15),
      for (var i = 0; i < min(2, (attributes.jobs ?? []).length); i++)
        Row(children: [
          SvgPicture.asset(professional, height: 30, width: 30),
          const SizedBox(width: 9),
          Flexible(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(attributes.jobs![i].title!, overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: true),
            Text(attributes.jobs![i].company!, overflow: TextOverflow.ellipsis, maxLines: 1, softWrap: true),
            SizedBox(height: 10)
          ]))
        ])
    ]);
  }
}
