import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/side_file_upload.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/side_profil_info.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/themes/palette.dart';

import '../../../models/account/account.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../models/posts/post.dart';
import '../../../models/posts/profile_documents_response.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../dashboard/widgets/create_post_container.dart';
import '../../dashboard/widgets/post_container.dart';
import '../../widgets/responsive.dart';
import 'horizontal_profile_card.dart';

class ProfileContent extends StatefulWidget {
  final TrackingScrollController scrollController;

  ProfileContent({
    Key? key,
    required this.scrollController,
  }) : super(key: key);

  @override
  State<ProfileContent> createState() => _ProfileContentState();
}

class _ProfileContentState extends State<ProfileContent> {
  List<Post> _items = [];
  int _page = 0;
  int _limit = 5;
  bool _isFirstLoadRunning = false;
  bool _isLoadMoreRunning = false;
  bool _hasNextPage = true;
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _firstLoad();
    _scrollController = ScrollController()..addListener(_loadMore);
  }

  Future<List<Post>> _fetchPosts() async {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    Account account = await authProvider.getProfileAccount();
    var apiResponse = await Provider.of<PostProvider>(context, listen: false)
        .fetchProfilePostsWithFilters(account.uuid ?? '', _page, _limit);
    return apiResponse.resources!;
  }

  void _firstLoad() async {
    setState(() {
      _isFirstLoadRunning = true;
    });
    List<Post> posts = await _fetchPosts();
    setState(() {
      _items = posts;
    });
    setState(() {
      _isFirstLoadRunning = false;
    });
  }

  void _loadMore() async {
    if (_hasNextPage == true &&
            _isFirstLoadRunning == false &&
            _isLoadMoreRunning == false /*&&
        this.widget.scrollController.position.extentAfter < 300*/
        ) {
      setState(() {
        _isLoadMoreRunning = true;
      });
      _page += 1;
      List<Post> posts = await _fetchPosts();
      if (posts.isNotEmpty) {
        setState(() {
          _items.addAll(posts);
        });
      } else {
        setState(() {
          _hasNextPage = false;
        });
      }
      setState(() {
        _isLoadMoreRunning = false;
      });
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Column(
      children: [
        HorizontalProfileCard(),
        Container(
          padding: EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: padding(),
          ),
          child: FutureBuilder<Account>(
              future: authProvider.getProfileAccount(),
              builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                if (snapshot.hasData) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 3,
                        child: SingleChildScrollView(
                          child: FutureBuilder<AccountAttributesResponse>(
                            future: Provider.of<AccountProvider>(context, listen: false)
                                .fetchAccountAttributes(snapshot.data?.uuid ?? ''),
                            builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
                              if (snapshot.hasData) {
                                return Column(
                                  children: [
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: Column(
                                        children: [
                                          SideProfilInfo(),
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              } else if (snapshot.hasError) {
                                return SmFutureBuilderError();
                              } else {
                                return SmFutureBuilderWaiting();
                              }
                            },
                          ),
                        ),
                      ),
                      Flexible(
                          flex: 6,
                          child: Column(children: [
                            CreatePostContainer(),
                            SizedBox(height: 10),
                            Column(children: [
                              _isFirstLoadRunning
                                  ? CircularProgressIndicator()
                                  : ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: _items.length,
                                      controller: _scrollController,
                                      itemBuilder: (context, index) => PostContainer(post: _items[index])),
                              if (_isLoadMoreRunning)
                                Padding(
                                    padding: EdgeInsets.only(top: 10, bottom: 40),
                                    child: Center(child: CircularProgressIndicator())),
                              _hasNextPage
                                  ? ElevatedButton(onPressed: _loadMore, child: Text('Charger plus...'))
                                  : Container(
                                      padding: EdgeInsets.only(top: 20, bottom: 20),
                                      color: Palette.appGreen,
                                      child: Center(
                                          child: Text('Toutes les publications ont été affichées',
                                              style: TextStyle(fontWeight: FontWeight.bold))))
                            ])
                          ])),
                      Flexible(
                        flex: 3,
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: FutureBuilder<ProfileDocumentsResponse>(
                              future: Provider.of<PostProvider>(context, listen: false)
                                  .fetchProfileDocuments(snapshot.data?.uuid ?? ''),
                              builder: (BuildContext context, AsyncSnapshot<ProfileDocumentsResponse> snap) {
                                if (snap.hasData) {
                                  return Column(
                                    children: [
                                      SideFileUpload(),
                                    ],
                                  );
                                } else if (snapshot.hasError) {
                                  return SmFutureBuilderError();
                                } else {
                                  return SmFutureBuilderWaiting();
                                }
                              }),
                        ),
                      ),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return SmFutureBuilderError();
                } else {
                  return SmFutureBuilderWaiting();
                }
              }),
        ),
      ],
    );
  }
}
