import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/themes/theme.dart';
import '../../../models/posts/profile_documents_response.dart';
import '../../../providers/post_provider.dart';
import 'doc_file.dart';

class SideFileUpload extends StatelessWidget {
  const SideFileUpload({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late ProfileDocumentsResponse profileDocuments = Provider.of<PostProvider>(context).profileDocumentsResponse;
    return Column(
      children: [
        DocFile(
          label: 'Documents',
          bgColor: blueColor,
          total: profileDocuments.totalDocuments ?? 0,
          documents: profileDocuments.documents ?? [],
        ),
        SizedBox(
          height: 20,
        ),
        DocFile(
          label: 'Liens',
          bgColor: yellowColor,
          total: profileDocuments.totalLinks ?? 0,
          documents: profileDocuments.links ?? [],
        ),
        // LinkFile(label: 'Liens', bgColor: yellowColor),
        SizedBox(
          height: 20,
        ),
        DocFile(
          label: 'Vidéos',
          bgColor: Colors.red,
          total: profileDocuments.totalVideos ?? 0,
          documents: profileDocuments.videos ?? [],
        ),
        // VideoFile(),
        SizedBox(
          height: 20,
        ),
        DocFile(
          label: 'Images',
          bgColor: greenColor,
          total: profileDocuments.totalImages ?? 0,
          documents: profileDocuments.images ?? [],
        ),
        // LinkFile(label: 'Images', bgColor: greenColor),
        SizedBox(
          height: 20,
        ),
        DocFile(
          label: 'Audios',
          bgColor: pinkColor,
          total: profileDocuments.totalAudios ?? 0,
          documents: profileDocuments.audios ?? [],
        ),
        // LinkFile(label: 'Audios', bgColor: pinkColor),
      ],
    );
  }
}
