import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/profile/widgets/parcous_pro_info.dart';

import '../../widgets/responsive.dart';
import 'bio_info.dart';
import 'competence_info.dart';
import 'formation_info.dart';

class SideProfilInfo extends StatelessWidget {
  const SideProfilInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    return Container(
        alignment: Alignment.bottomLeft,
        width: 300,
        child: Card(
            margin: EdgeInsets.symmetric(horizontal: isDesktop ? 5.0 : 0.0),
            elevation: isDesktop ? 1.0 : 0.0,
            shape: isDesktop ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)) : null,
            child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(children: const [
                  CompetenceInfo(),
                  Divider(indent: 20, endIndent: 20),
                  FormationInfo(),
                  Divider(indent: 20, endIndent: 20),
                  ParcoursProInfo(),
                  Divider(indent: 20, endIndent: 20),
                  BioInfo()
                ]))));
  }
}
