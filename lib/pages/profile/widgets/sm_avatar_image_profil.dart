import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../themes/picture_file.dart';

class SmAvatarImageProfil extends StatelessWidget {
  const SmAvatarImageProfil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: 70,
          backgroundColor: Colors.white,
          child: SvgPicture.asset(
            avatar,
            height: 300,
            width: 300,
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          right: 4,
          bottom: 8,
          child: ElevatedButton(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
              primary: Colors.white,
              shape: const CircleBorder(),
            ),
            child: const Icon(
              Icons.add,
              color: Colors.yellow,
            ),
          ),
        ),
      ],
      //avatar image.
    );
  }
}
