import 'package:flutter/cupertino.dart';

import 'package:flutter_svg/svg.dart';


class SmProfilInfo extends StatelessWidget {
  final String picture;
  final String text;
  final bool isMobile;
  const SmProfilInfo({
    Key? key,
    required this.picture,
    required this.text,
    required this.isMobile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: isMobile ? 190 : 280,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            picture,
            height: 20,
            width: 20,
          ),
          const SizedBox(
            width: 3,
          ),
          Text(text),
        ],
      ),
    );
  }
}
