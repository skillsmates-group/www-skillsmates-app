import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '/themes/theme.dart';
import '../../profile_edit/profile_edit_page.dart';

class SmProfilProgressBar extends StatelessWidget {
  const SmProfilProgressBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _navigateTo(String routeName) {
      context.push(routeName);
      // Navigator.of(context).pushNamed(
      //   routeName,
      //   arguments: {},
      // );
    }

    clickEditProfile() {
      _navigateTo(ProfileEditPage.routeName);
    }

    var progress = 0.7;
    return Container(
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: progress == 0.7 ? greenColor : redBoldColor, width: 1, style: BorderStyle.solid),
      ),
      child: LinearPercentIndicator(
        animation: true,
        animationDuration: 1000,
        lineHeight: 40,
        percent: progress,
        width: 200,
        progressColor: progress == 0.7 ? greenOpaColor : redColor,
        padding: const EdgeInsets.all(0),
        backgroundColor: Colors.transparent,
        center: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Profil complété à ${progress * 100}%'),
            const Icon(
              Icons.edit_sharp,
              color: greenColor,
            ),
            // TextButton(
            //   onPressed: () {},
            //   child: const Icon(
            //     Icons.edit_sharp,
            //     color: greenColor,
            //   ),
            // ),
          ],
        ),
        barRadius: const Radius.circular(10),
      ),
    );
  }
}
