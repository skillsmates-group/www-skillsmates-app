import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../providers/auth_provider.dart';

class SmRowName extends StatelessWidget {
  const SmRowName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentUser = Provider.of<AuthProvider>(context).loggedAccount;
    return SizedBox(
      width: 100,
      child: Text.rich(
        TextSpan(
          text: currentUser.firstname,
          style: const TextStyle(
            fontSize: 16.0,
            overflow: TextOverflow.ellipsis,
          ),
          children: [
            TextSpan(text: ' '),
            TextSpan(
              text: currentUser.lastname,
              style: const TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w700,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
