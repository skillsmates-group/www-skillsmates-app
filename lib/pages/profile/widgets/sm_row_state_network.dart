import 'package:flutter/material.dart';

class SmRowStateNetwork extends StatelessWidget {
  final bool isMobile;
  const SmRowStateNetwork({Key? key, required this.isMobile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 380,
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (!isMobile) ...const [
            Text(
              '20',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              width: 3,
            ),
            Text('Publications'),
            SizedBox(
              width: 14,
            ),
            Text(
              '08',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              width: 3,
            ),
            Text('Followers'),
            SizedBox(
              width: 14,
            ),
            Text(
              '38',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              width: 3,
            ),
            Text('Following'),
          ],
          if (isMobile) ...[
            Column(
              children: const [
                Text(
                  '20',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text('Publications'),
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: const [
                Text(
                  '09',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text('Abonnés'),
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: const [
                Text(
                  '38',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text('Abonnements'),
              ],
            ),
          ],
        ],
      ),
    );
  }
}
