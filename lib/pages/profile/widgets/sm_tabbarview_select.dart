import 'package:flutter/cupertino.dart';

import '../../widgets/sm_dropdown.dart';
import '../../widgets/sm_text_form_field.dart';

class SmTabbarViewSelect extends StatelessWidget {
  final String firstText;
  final String secondText;

  const SmTabbarViewSelect({
    Key? key,
    required this.firstText,
    required this.secondText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textControl = TextEditingController();
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(firstText),
          const SizedBox(
            height: 8,
          ),
          DropdownStatut(textControl),
          const SizedBox(
            height: 10,
          ),
          SmTextFormField(
            textControl,
            fieldName: secondText,
            // isPrefixIcon: true,
          ),
          const SizedBox(
            height: 10,
          ),
          const Text(
            "Pas de résultats trouvés.",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
