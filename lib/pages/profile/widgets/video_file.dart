import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '/themes/picture_file.dart';
import '../../../../../themes/theme.dart';
import '../../widgets/sm_text_button_field.dart';

class VideoFile extends StatelessWidget {
  const VideoFile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: const BorderSide(
          color: redBoldColor,
        ),
      ),
      child: SizedBox(
        height: 120,
        width: 265,
        child: Column(
          children: [
            Container(
              width: 265,
              decoration: const BoxDecoration(
                color: redBoldColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    'Vidéos  (14)',
                    style: TextStyle(
                      color: whitishColor,
                    ),
                  ),
                  SmTextButton(
                    label: 'voir plus',
                    color: whitishColor,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      SvgPicture.asset(
                        video,
                        height: 40,
                        width: 20,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      const Text('titre_video'),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      SvgPicture.asset(
                        video,
                        height: 40,
                        width: 20,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      const Text('titre_video'),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      SvgPicture.asset(
                        video,
                        height: 40,
                        width: 20,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      const Text('titre_video'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
