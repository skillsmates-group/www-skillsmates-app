import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/account_attribute_enum.dart';
import 'package:www_skillsmates_app/models/profile_attribute.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/biography.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/job_edit.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/profile_edit.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/profile_summary.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/skills_edit.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/training_edit.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';

import '../../data/profile_atributes_data.dart';
import '../../models/account/account.dart';
import '../../models/account/account_attributes_response.dart';
import '../../providers/account_provider.dart';
import '../../providers/auth_provider.dart';
import '../../themes/picture_file.dart';
import '../../themes/theme.dart';
import '../profile/widgets/horizontal_profile_card.dart';
import '../widgets/responsive.dart';
import '../widgets/sm_footer_row.dart';
import '../widgets/sm_future_builder_waiting.dart';

class DesktopProfileEditContent extends StatefulWidget {
  const DesktopProfileEditContent({Key? key}) : super(key: key);

  @override
  State<DesktopProfileEditContent> createState() => _DesktopProfileEditContentState();
}

class _DesktopProfileEditContentState extends State<DesktopProfileEditContent> {
  String tab = AccountAttribute.resume.name;

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    clickProfileAttribute(ProfileAttribute profileAttribute) {
      setState(() {
        tab = profileAttribute.code.name;
      });
    }

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    String? getTotalElements(AccountAttributesResponse? data, String element) {
      if (element == 'trainings') {
        return '(${(data?.trainings?.length ?? 0) + (data?.diplomas?.length ?? 0) + (data?.certifications?.length ?? 0)})';
      } else if (element == 'jobs') {
        return '(${data?.jobs?.length ?? 0})';
      } else if (element == 'skills') {
        return '(${data?.totalSkills})';
      } else {
        return '';
      }
    }

    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return SingleChildScrollView(
      child: Column(
        children: [
          HorizontalProfileCard(),
          FutureBuilder<Account>(
              future: authProvider.getProfileAccount(),
              builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                if (snapshot.hasData) {
                  Account profileAccount = snapshot.data!;
                  return Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    margin: EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: padding(),
                    ),
                    child: FutureBuilder<AccountAttributesResponse>(
                      future: Provider.of<AccountProvider>(context, listen: false)
                          .fetchAccountAttributes(profileAccount.uuid),
                      builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
                        if (snapshot.hasData) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 50,
                                    ),
                                    for (var profileAttribute in profileAttributes)
                                      Container(
                                        padding: const EdgeInsets.only(right: 20),
                                        decoration: BoxDecoration(
                                            color: tab == profileAttribute.code.name ? blueLightColor : Colors.white),
                                        child: InkWell(
                                          onTap: () => clickProfileAttribute(profileAttribute),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                                                      child: Text(
                                                        '${profileAttribute.label} ${getTotalElements(snapshot.data, profileAttribute.code.name)}',
                                                        style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight: tab == profileAttribute.code.name
                                                              ? FontWeight.w700
                                                              : FontWeight.normal,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    SizedBox(
                                      height: 50,
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 9,
                                child: Container(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 20.0,
                                    horizontal: 20.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      if (tab == AccountAttribute.resume.name)
                                        ProfileSummary(
                                            account: profileAccount,
                                            data: snapshot.data!,
                                            handleVoirPlus: clickProfileAttribute),
                                      if (tab == AccountAttribute.general_infos.name) ProfileEdit(),
                                      if (tab == AccountAttribute.trainings.name) TrainingEdit(),
                                      if (tab == AccountAttribute.jobs.name) JobEdit(),
                                      if (tab == AccountAttribute.skills.name) SkillsEdit(),
                                      if (tab == AccountAttribute.biography.name) Biography()
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                        } else if (snapshot.hasError) {
                          return SmFutureBuilderError(error: snapshot.error.toString());
                        } else {
                          return SmFutureBuilderWaiting();
                        }
                      },
                    ),
                  );
                } else if (snapshot.hasError) {
                  return SmFutureBuilderError();
                } else {
                  return SmFutureBuilderWaiting();
                }
              }),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: primaryColor,
                  fixedSize: Size(150, 40),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
              onPressed: () => context.pop(),
              child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Expanded(flex: 4, child: SvgPicture.asset(arrow_left, width: 30, height: 30)),
                Expanded(flex: 8, child: Text('Retour', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700)))
              ])),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: const SmFooterRow(),
          ),
        ],
      ),
    );
  }
}
