import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/profile_edit/desktop_profile_edit_content.dart';

import '/pages/dashboard/widgets/desktop_nav_bar.dart';

class DesktopProfileEditScreen extends StatefulWidget {
  const DesktopProfileEditScreen({Key? key}) : super(key: key);

  @override
  State<DesktopProfileEditScreen> createState() => _DesktopProfileEditScreenState();
}

class _DesktopProfileEditScreenState extends State<DesktopProfileEditScreen> {
  final TrackingScrollController _trackingScrollController = TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DesktopProfileEditContent(),
          ],
        ),
      ),
    );
  }
}
