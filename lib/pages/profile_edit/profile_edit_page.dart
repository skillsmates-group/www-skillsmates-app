import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '/pages/profile_edit/mobile_profile_edit_screen.dart';
import 'desktop_profile_edit_screen.dart';

class ProfileEditPage extends StatelessWidget {
  const ProfileEditPage({Key? key}) : super(key: key);
  static const routeName = '/profile-edit';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return const DesktopProfileEditScreen();
      }
      if (sizingInformation.isTablet) {
        return const MobileProfileEditScreen();
      }
      return const MobileProfileEditScreen();
    });
  }
}
