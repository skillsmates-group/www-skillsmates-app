import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../models/account/job.dart';
import '../../../themes/picture_file.dart';

class JobTile extends StatelessWidget {
  final Job job;

  const JobTile({Key? key, required this.job}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {},
        child: ListTile(
            leading: SvgPicture.asset(company, height: 50, width: 50),
            title: Text('${job.title} - ${job.company}', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
            subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text.rich(TextSpan(
                  text: 'Secteur d\'activité: ',
                  style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(
                        text: '${job.activitySector?.label ?? ''} - ${job.activityArea?.label ?? ''}',
                        style: const TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                  ])),
              Text.rich(TextSpan(
                  text: 'Periode: ',
                  style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(
                        text: 'de ${job.convertStringToDate(job.startDate)} au ${job.convertStringToDate(job.endDate)}',
                        style: const TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                  ])),
              Text.rich(
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  maxLines: 2,
                  TextSpan(
                      text: 'Description: ',
                      style:
                          const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                      children: [
                        TextSpan(
                            text: job.description ?? '',
                            style: const TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                      ]))
            ]),
            trailing: InkWell(
              child: const Icon(Icons.more_horiz),
              onTap: () {},
            )));
  }
}
