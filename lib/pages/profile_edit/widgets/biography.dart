import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';

import '../../../models/account/account.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/picture_file.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_icon_text.dart';
import '../../widgets/sm_info_text.dart';
import '../../widgets/sm_svg_icon_text.dart';

class Biography extends StatefulWidget {
  const Biography({Key? key}) : super(key: key);

  @override
  State<Biography> createState() => _BiographyState();
}

class _BiographyState extends State<Biography> {
  bool isEditable = false;
  late Account loggedAccount;
  final textEditingController = TextEditingController();

  void showSuccessToast(String message) {
    MotionToast.success(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void showErrorToast(String message) {
    MotionToast.error(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    authProvider.getLoggedAccount().then((value) => {loggedAccount = value});
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);

    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 50;
      if (isTablet) return 10;
      return 0;
    }

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: padding(),
      ),
      child: FutureBuilder<Account>(
          future: authProvider.getProfileAccount(),
          builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
            if (snapshot.hasData) {
              if (isEditable) {
                return editBiography(snapshot.data!);
              } else {
                return displayBiography(snapshot.data!);
              }
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          }),
    );
  }

  Widget displayBiography(Account account) {
    String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");
    String loadedAccountUuid = Provider.of<AuthProvider>(context, listen: false).profileAccount.uuid;
    String infoText =
        'Redigez un résume de votre profil en mettant en avant les disciplines majeures relatives à votre parcours '
        'académique et professionnel. N\'hesitez pas aussi à partager vos centres d\'interets, exposer vos besoins et '
        'eventuellement à promouvoir vos activités';

    onClickEdit() {
      setState(() {
        this.isEditable = true;
      });
    }

    return SingleChildScrollView(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
      SmInfoText(infoText: infoText),
      SizedBox(height: 10),
      if (loadedAccountUuid == loggedAccountUuid)
        SmIconText(
            image: plus_colored,
            text: Text('Ajouter / Modifier la biographie',
                style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
            color: Colors.red,
            handleButton: onClickEdit),
      if (loadedAccountUuid != loggedAccountUuid)
        SmIconText(
            image: plus_colored,
            text: Text('Biographie de ${account.lastname} ${account.firstname}',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
            color: Colors.red),
      SizedBox(height: 10),
      Container(child: Text(account.biography ?? ''))
    ]));
  }

  Widget editBiography(Account account) {
    textEditingController.text = account.biography ?? '';
    onClickButton(button) {
      if (button == 'SAVE') {
        account.biography = textEditingController.text;
        final accountProvider = Provider.of<AccountProvider>(context, listen: false);
        accountProvider.updateBiography(account).then((value) => {
              showSuccessToast("Votre biographie a été mis à jour avec succés"),
              setState(
                () {
                  this.isEditable = false;
                },
              )
            });
      } else if (button == 'CANCEL') {
        setState(() {
          this.isEditable = false;
        });
      } else {
        setState(() {
          this.isEditable = false;
        });
      }
    }

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            SmSvgIconText(
              image: avatar,
              text: Text(
                'Biographie',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              color: Colors.red,
            ),
            const SizedBox(height: 20),
            Material(
              child: TextField(
                controller: textEditingController,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                maxLines: 10,
                decoration: InputDecoration(
                  hintText: "Saisissez votre biographie ici..Saisissez votre biographie ici..",
                  contentPadding: EdgeInsets.all(20.0),
                ),
              ),
            ),
            const SizedBox(height: 20),
            SmSaveCancelButton(
              handleButton: onClickButton,
            )
          ],
        ),
      ),
    );
  }
}
