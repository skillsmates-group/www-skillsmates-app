import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/models/account/certification.dart';

import '../../../themes/picture_file.dart';

class CertificationTile extends StatelessWidget {
  final Certification certificate;

  const CertificationTile({Key? key, required this.certificate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {},
        child: ListTile(
            leading: SvgPicture.asset(certification, height: 50, width: 50),
            title: Text(certificate.title!, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
            subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text.rich(TextSpan(
                  text: 'Année: ',
                  style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(
                        text: '${certificate.endDate ?? ''}',
                        style: const TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                  ])),
              Text.rich(TextSpan(
                  text: 'Compétence acquise: ',
                  style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(
                        text: '${certificate.activityArea ?? ''}',
                        style: const TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                  ]))
            ]),
            trailing: InkWell(child: const Icon(Icons.more_horiz), onTap: () {})));
  }
}
