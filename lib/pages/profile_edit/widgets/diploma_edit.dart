import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/dropdown.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_text_form_field.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class DiplomaEdit extends StatelessWidget {
  const DiplomaEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _diploma = TextEditingController();
    TextEditingController _school = TextEditingController();
    TextEditingController _domain = TextEditingController();
    TextEditingController _level = TextEditingController();
    TextEditingController _education = TextEditingController();

    List<String> items = ["Item 1", "Item 2", "Item 3"];

    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      SingleChildScrollView(
          child: Row(children: [
        Container(),
        Container(
            width: 550,
            padding: EdgeInsets.all(30),
            child: Column(children: [
              Row(children: [
                DefaultTextStyle(
                    style: TextStyle(
                        fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.bold),
                    child: Text("Diplome obtenu"))
              ]),
              const SizedBox(height: 20),
              Material(child: Dropdown(_education, items, "Enseignement secondaire")),
              const SizedBox(height: 20),
              Material(child: Dropdown(_level, items, "Niveau d'étude (BAC+1 à BAC+12)")),
              const SizedBox(height: 20),
              Material(child: SmTextFormField(_diploma, fieldName: "Intitulé du diplome")),
              const SizedBox(height: 20),
              Material(child: SmTextFormField(_school, fieldName: "Nom de l'établissement")),
              const SizedBox(height: 20),
              Material(child: SmTextFormField(_school, fieldName: "Ville")),
              const SizedBox(height: 20),
              Material(child: SmTextFormField(_domain, fieldName: "Domaien d'étude ou spécialité (facultatif)")),
              const SizedBox(height: 20),
              DateTimeFormField(
                  decoration: const InputDecoration(
                      hintStyle: TextStyle(color: primaryColor),
                      errorStyle: TextStyle(color: primaryColor),
                      border: OutlineInputBorder(),
                      suffixIcon: Icon(Icons.event_note),
                      labelText: "Date d'obtention"),
                  mode: DateTimeFieldPickerMode.date,
                  autovalidateMode: AutovalidateMode.always,
                  dateTextStyle: TextStyle(
                      fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.normal)),
              const SizedBox(height: 20),
              Material(
                  child: TextField(
                      textInputAction: TextInputAction.newline,
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      decoration: InputDecoration(
                          hintText: "Description de la formation (facultatif)", contentPadding: EdgeInsets.all(20.0)))),
              const SizedBox(height: 20),
              SmSaveCancelButton()
            ]))
      ]))
    ]);
  }
}
