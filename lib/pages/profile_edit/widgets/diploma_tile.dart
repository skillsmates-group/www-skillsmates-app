import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/models/account/diploma.dart';

import '../../../themes/picture_file.dart';

class DiplomaTile extends StatelessWidget {
  final Diploma diploma;

  const DiplomaTile({Key? key, required this.diploma}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {},
        child: ListTile(
            leading: SvgPicture.asset(student, height: 50, width: 50),
            title: Text(diploma.title!, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
            subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text.rich(TextSpan(
                  text: 'Ville: ',
                  style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(
                        text:
                            '${diploma.city ?? ''} - ${diploma.convertStringToDate(diploma.startDate) ?? ''} - ${diploma.convertStringToDate(diploma.endDate) ?? ''}',
                        style: const TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                  ])),
              Text.rich(TextSpan(
                  text: 'Compétence acquise: ',
                  style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(
                        text: '${diploma.activityArea ?? ''} - ${diploma.level ?? ''}',
                        style: const TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                  ])),
              Text.rich(
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  maxLines: 2,
                  TextSpan(
                      text: 'Description: ',
                      style:
                          const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis),
                      children: [
                        TextSpan(
                            text: diploma.description ?? '',
                            style: const TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.normal, overflow: TextOverflow.ellipsis))
                      ]))
            ]),
            trailing: InkWell(child: const Icon(Icons.more_horiz), onTap: () {})));
  }
}
