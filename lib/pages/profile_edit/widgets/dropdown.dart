import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Dropdown extends StatefulWidget {
  TextEditingController formValue;
  List<String> items;
  String label;
  String? previousValue;

  Dropdown(this.formValue, this.items, this.label, {Key? key, this.previousValue}) : super(key: key);

  @override
  State<Dropdown> createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  String? dropdownvalue;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      onSaved: (String? newValue) => widget.formValue.text = newValue!,
      validator: (String? value) => value == null ? "Sélectionner un statut" : null,
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      value: widget.previousValue,
      hint: Text(widget.label, style: TextStyle(color: Colors.black)),
      items: widget.items.map((String item) {
        return DropdownMenuItem<String>(
          value: item,
          child: Text(
            item,
            style: const TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
      onChanged: (String? newValue) {
        setState(() {
          dropdownvalue = newValue!;
          widget.formValue.text = newValue;
        });
      },
      icon: const Icon(Icons.keyboard_arrow_down),
    );
  }
}
