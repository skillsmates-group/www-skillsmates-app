import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/account/job.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_switch.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_parameters_dropdown.dart';

import '../../../models/account/account.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../models/parameter_response.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/attribute_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_icon_text.dart';
import '../../widgets/sm_info_text.dart';
import '../../widgets/sm_svg_icon_text.dart';
import '../../widgets/sm_text_form_field.dart';
import 'Job_tile.dart';

class JobEdit extends StatefulWidget {
  const JobEdit({Key? key}) : super(key: key);

  @override
  State<JobEdit> createState() => _JobEditState();
}

class _JobEditState extends State<JobEdit> {
  bool isEditable = false;
  late ParameterResponse _activityArea;
  late ParameterResponse _activitySector;
  late bool _currentJob = false;
  late String _startDate;
  late String _endDate;

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    Future<AccountAttributesResponse> fetchAccountAttributes() async {
      late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
      return attributes;
    }

    double padding() {
      if (isDesktop) return 50;
      if (isTablet) return 10;
      return 0;
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: padding(),
        ),
        child: FutureBuilder<AccountAttributesResponse>(
            future: fetchAccountAttributes(),
            builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
              if (snapshot.hasData) {
                if (isEditable) {
                  return editJob();
                } else {
                  return displayJob(snapshot.data!);
                }
              } else if (snapshot.hasError) {
                return SmFutureBuilderError();
              } else {
                return SmFutureBuilderWaiting();
              }
            }),
      ),
    );
  }

  Widget displayJob(AccountAttributesResponse data) {
    String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");
    String loadedAccountUuid = Provider.of<AuthProvider>(context, listen: false).profileAccount.uuid;
    String infoText = 'Veuillez lister chacunes de vos experiences professionnelles afin que la communauté puisse '
        'apprecier votre parcours et s\'en inspirer. Nous pourrons ainsi vous mettre en relation avec les confreres '
        'susceptibles de vous transmettre de nouvelles competences et opportunités';

    onClickEdit() {
      setState(() {
        this.isEditable = true;
      });
    }

    return Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
      SmInfoText(infoText: infoText),
      SizedBox(height: 10),
      if (loadedAccountUuid == loggedAccountUuid)
        SmIconText(
            image: plus_colored,
            text: Text('Ajouter un emploi',
                style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
            color: Colors.red,
            handleButton: onClickEdit),
      SizedBox(height: 10),
      for (Job job in data.jobs ?? []) JobTile(job: job),
      SizedBox(height: 10)
    ]);
  }

  Widget editJob() {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    TextEditingController _company = TextEditingController();
    TextEditingController _title = TextEditingController();
    TextEditingController _city = TextEditingController();
    TextEditingController _description = TextEditingController();

    callbackActivityArea(ParameterResponse activityArea) {
      setState(() {
        _activityArea = activityArea;
      });
    }

    callbackActivitySector(ParameterResponse activitySector) {
      setState(() {
        _activitySector = activitySector;
      });
    }

    callbackCurrentJob(bool isCurrentJob) {
      setState(() {
        _currentJob = isCurrentJob;
      });
    }

    onStartDateSelected(startDate) {
      setState(() {
        _startDate = formatter.format(startDate);
      });
    }

    onEndDateSelected(endDate) {
      setState(() {
        _endDate = formatter.format(endDate);
      });
    }

    onClickButton(button) {
      if (button == 'SAVE') {
        final authProvider = Provider.of<AuthProvider>(context, listen: false);
        Account loggedAccount = authProvider.profileAccount;
        Job job = new Job();
        job.title = _title.text;
        job.company = _company.text;
        job.description = _description.text;
        job.city = _city.text;
        job.startDate = _startDate;
        job.endDate = _endDate;
        job.currentJob = _currentJob;
        job.activityArea = _activityArea;
        job.activitySector = _activitySector;
        job.account = loggedAccount.uuid;

        final attributeProvider = Provider.of<AttributeProvider>(context, listen: false);

        attributeProvider.saveJob(job).then((value) => {
              setState(() {
                this.isEditable = false;
              })
            });
      } else if (button == 'CANCEL') {
        setState(() {
          this.isEditable = false;
        });
      } else {
        setState(() {
          this.isEditable = false;
        });
      }
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          SmSvgIconText(
            image: avatar,
            text: Text(
              'Nouvel emploi',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            color: Colors.red,
          ),
          const SizedBox(height: 20),
          Material(
            child: SmTextFormField(_company, fieldName: "Nom de l'entreprise ou de l'établissement"),
          ),
          const SizedBox(height: 20),
          Material(
            child: SmParametersDropdown(
              label: "Secteur d'activité",
              parameterType: 'activityareas',
              callback: callbackActivityArea,
            ),
          ),
          const SizedBox(height: 20),
          Material(
            child: SmTextFormField(_title, fieldName: "Intitulé du poste"),
          ),
          const SizedBox(height: 20),
          // Material(child: SmTextFormField(_domain, fieldName: "Domaine d'activité du poste")),
          Material(
            child: SmParametersDropdown(
              label: "Domaine d'activité du poste",
              parameterType: 'activitysectors',
              callback: callbackActivitySector,
            ),
          ),
          const SizedBox(height: 20),
          Row(children: [
            Expanded(
              flex: 30,
              child: DateTimeFormField(
                onDateSelected: onStartDateSelected,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: primaryColor),
                    errorStyle: TextStyle(color: primaryColor),
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(Icons.event_note),
                    labelText: 'Début'),
                mode: DateTimeFieldPickerMode.date,
                autovalidateMode: AutovalidateMode.always,
                dateTextStyle: TextStyle(
                  fontSize: 16,
                  color: primaryColor,
                  fontFamily: "Raleway",
                  fontWeight: FontWeight.normal,
                ),
                // initialValue: DateTime.tryParse(job.startDate!),
                // initialDate: DateTime.tryParse(account.birthdate!),
              ),
            ),
            Expanded(flex: 5, child: Container()),
            Expanded(
              flex: 30,
              child: DateTimeFormField(
                onDateSelected: onEndDateSelected,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: primaryColor),
                    errorStyle: TextStyle(color: primaryColor),
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(Icons.event_note),
                    labelText: 'Fin'),
                mode: DateTimeFieldPickerMode.date,
                autovalidateMode: AutovalidateMode.always,
                dateTextStyle:
                    TextStyle(fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.normal),
              ),
            ),
            Expanded(flex: 5, child: Container()),
            Expanded(
              flex: 30,
              child: Material(
                child: SmSwitch("En cours", onSwitch: callbackCurrentJob),
              ),
            ),
          ]),
          const SizedBox(height: 20),
          Material(child: SmTextFormField(_city, fieldName: "Lieu")),
          const SizedBox(height: 20),
          Material(
            child: TextField(
              controller: _description,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
              maxLines: 5,
              decoration: InputDecoration(
                hintText: "Description des taches effectuées (facultatif)",
                contentPadding: EdgeInsets.all(20.0),
              ),
            ),
          ),
          const SizedBox(height: 20),
          SmSaveCancelButton(
            handleButton: onClickButton,
          )
        ],
      ),
    );
  }
}
