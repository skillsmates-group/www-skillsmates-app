import 'dart:io';
import 'dart:typed_data';

import 'package:date_field/date_field.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/gender.dart';
import 'package:www_skillsmates_app/enums/status.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_switch.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_elevated_button.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../models/account/account.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/picture_file.dart';
import '../../widgets/profile_avatar.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_icon_text.dart';
import '../../widgets/sm_info_text.dart';
import '../../widgets/sm_text_form_field.dart';
import 'dropdown.dart';

class ProfileEdit extends StatefulWidget {
  const ProfileEdit({Key? key}) : super(key: key);

  @override
  State<ProfileEdit> createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  bool isEditable = false;
  bool isAvatarEditable = false;
  Uint8List? _selectedFileBytes;
  Widget? initial;

  ImagePicker imagePicker = ImagePicker();
  File? uploadedImage;
  File? croppedImage;
  bool uploaded = false;
  bool cropped = false;
  Uint8List? uploadedImageAsByte;
  Uint8List? croppedImageAsByte;
  String? filename;
  String? fileType;

  pickImage() async {
    uploadedImage = null;
    croppedImage = null;
    uploadedImageAsByte = null;
    croppedImageAsByte = null;
    uploaded = false;
    cropped = false;
    var imgData = await imagePicker.pickImage(source: ImageSource.gallery);
    filename = imgData!.name;
    fileType = imgData.mimeType!;
    uploadedImage = File(imgData!.path);
    uploadedImageAsByte = await imgData.readAsBytes();
    uploaded = true;
    setState(() {});
  }

  cropImage() async {
    croppedImage = null;
    croppedImageAsByte = null;
    uploaded = true;
    cropped = false;
    var imgData = await ImageCropper().cropImage(sourcePath: uploadedImage!.path, uiSettings: [
      WebUiSettings(
          context: context,
          presentStyle: CropperPresentStyle.dialog,
          boundary: CroppieBoundary(width: 350, height: 350),
          viewPort: CroppieViewPort(width: 300, height: 300, type: 'circle'),
          enableExif: true,
          enableZoom: true,
          showZoomer: true)
    ]);
    croppedImage = File(imgData!.path);
    croppedImageAsByte = await imgData.readAsBytes();
    cropped = true;
    setState(() {});
  }

  changeStateOnAvatarProcess() {
    setState(() {
      isEditable = false;
      isAvatarEditable = false;
      croppedImage = null;
      uploadedImage = null;
      uploaded = false;
      cropped = false;
    });
  }

  processAvatar(button) {
    if (button == 'SAVE') {
      if (uploaded) {
        final accountProvider = Provider.of<AccountProvider>(context, listen: false);
        accountProvider
            .updateAvatar(new LocalStorage('skills-mates_app').getItem("uuid"),
                cropped ? croppedImageAsByte! : uploadedImageAsByte!, filename!, fileType!)
            .whenComplete(() => showSuccessToast('Votre photo a été mise à jour'))
            .then((value) => changeStateOnAvatarProcess())
            .onError((error, stackTrace) => (error, stackTrace) {
                  showErrorToast('Erreur lors de la mise à jour de votre photo de profile');
                  changeStateOnAvatarProcess();
                });
      }
    } else if (button == 'CANCEL') {
      changeStateOnAvatarProcess();
    } else {
      changeStateOnAvatarProcess();
    }
  }

  void showSuccessToast(String message) {
    MotionToast.success(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void showErrorToast(String message) {
    MotionToast.error(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  Gender getGenderByValue(String value) {
    return Gender.values.where((element) => element.value == value).first;
  }

  Status getStatusByValue(String value) {
    return Status.values.where((element) => element.value == value).first;
  }

  Gender getGenderByName(String name) {
    return Gender.values.where((element) => element.name == name).first;
  }

  Status getStatusByName(String name) {
    return Status.values.where((element) => element.name == name).first;
  }

  updateAvatar() {
    setState(() {
      this.isAvatarEditable = false;
      this.isEditable = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);
    String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

    double padding() {
      if (isDesktop) return 50;
      if (isTablet) return 10;
      return 0;
    }

    return SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: padding()),
            child: FutureBuilder<Account>(
                future: authProvider.getProfileAccount(),
                builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
                  if (snapshot.hasData) {
                    if (isEditable) {
                      if (isAvatarEditable) {
                        return avatarEdition(snapshot.data!);
                      }
                      return editProfile(snapshot.data!);
                    } else {
                      return displayProfile(snapshot.data!, loggedAccountUuid);
                    }
                  } else if (snapshot.hasError) {
                    return SmFutureBuilderError();
                  } else {
                    return SmFutureBuilderWaiting();
                  }
                })));
  }

  Widget displayProfile(Account account, String loggedAccountUuid) {
    String infoText = 'Afin que votre profil soit plus attractif, nous vous invitons à renseigner le maximum '
        'd\'informations possibles. C\'est grâce à l\'implication ce chaque utilisateur que nous serons à mesure '
        's\'optimiser les resultats proposés lorsque vous effectueriez des recherches de profils bien specifiques';

    onClickEdit() {
      setState(() {
        this.isEditable = true;
      });
    }

    onClickAvatarEdit() {
      setState(() {
        this.isEditable = true;
        this.isAvatarEditable = true;
      });
    }

    return Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
      SmInfoText(infoText: infoText),
      SizedBox(height: 20),
      if (loggedAccountUuid == account.uuid)
        SmIconText(
            image: plus_colored,
            text: Text('Mettre à jour son profil',
                style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
            color: Colors.blue,
            handleButton: onClickEdit),
      if (loggedAccountUuid == account.uuid) SizedBox(height: 20),
      Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Expanded(
            flex: 8,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(account.firstname, style: TextStyle(fontSize: 22)),
              Text(account.lastname, style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
              SizedBox(height: 30),
              Text.rich(TextSpan(
                  text: 'Sexe : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.gender != null && account.gender != ''
                            ? getGenderByName(account.gender ?? '').value
                            : '',
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ])),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                  text: 'Statut : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.status != null && account.status != ''
                            ? getStatusByName(account.status ?? '').value
                            : '',
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ])),
              if (!(account.hideBirthdate ?? false)) SizedBox(height: 10),
              if (!(account.hideBirthdate ?? false))
                Text.rich(TextSpan(
                    text: 'Date de naissance : ',
                    style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                    children: [
                      TextSpan(text: ' '),
                      TextSpan(
                          text: account.birthDate(),
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                    ])),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                  text: 'Pays de residence : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.country,
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ])),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                  text: 'Ville de residence : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.city,
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ]))
            ])),
        Flexible(
            flex: 3,
            child: InkWell(
                child: Container(
                    child: ProfileAvatar(
                        imageUrl: account.avatar?.toLowerCase() ?? '',
                        isActive: account.active,
                        isProfil: true,
                        radius: 70.0)),
                onTap: () => loggedAccountUuid == account.uuid ? onClickAvatarEdit() : ()))
      ]),
      SizedBox(height: 10),
    ]);
  }

  Widget editProfile(Account account) {
    TextEditingController _firstname = TextEditingController();
    TextEditingController _lastname = TextEditingController();
    TextEditingController _birthdate = TextEditingController();
    TextEditingController _city = TextEditingController();
    TextEditingController _country = TextEditingController();
    TextEditingController _gender = TextEditingController();
    TextEditingController _status = TextEditingController();

    _firstname.text = account.firstname;
    _lastname.text = account.lastname;
    _birthdate.text = account.birthdate ?? '';
    _city.text = account.city ?? '';
    _country.text = account.country ?? '';
    _gender.text = account.gender != null && account.gender != '' ? getGenderByName(account.gender ?? '').value : '';
    _status.text = account.status != null && account.status != '' ? getStatusByName(account.status ?? '').value : '';

    onClickButton(button) {
      if (button == 'SAVE') {
        account.firstname = _firstname.text;
        account.lastname = _lastname.text;
        account.birthdate = _birthdate.text;
        account.gender = getGenderByValue(_gender.text).name;
        account.status = getStatusByValue(_status.text).name;
        account.city = _city.text;
        account.country = _country.text;

        final accountProvider = Provider.of<AccountProvider>(context, listen: false);
        accountProvider.updateAccount(account).then((value) => {
              showSuccessToast("Votre profil a été modifié avec succés"),
              setState(
                () {
                  this.isEditable = false;
                },
              )
            });
      } else if (button == 'CANCEL') {
        setState(() {
          this.isEditable = false;
        });
      } else {
        setState(() {
          this.isEditable = false;
        });
      }
    }

    var upload = () async {
      var picked = await FilePicker.platform.pickFiles();
      if (picked != null) {
        print(picked.files.first.name);
      }
    };

    String? getPreviousStatus() {
      if (account.status == null) return null;
      return Status.values.where((element) => element.name == account.status).first.value;
    }

    String? getPreviousGender() {
      if (account.gender == null) return null;
      return Gender.values.where((element) => element.name == account.gender).first.value;
    }

    onSwitch(bool value) {
      account.hideBirthdate = value;
    }

    return SingleChildScrollView(
      child: Column(
        children: [
          // Row(children: [
          //   Material(
          //     child: IconButton(
          //       onPressed: upload,
          //       icon: SvgPicture.asset("assets/images/user.svg", height: 100),
          //     ),
          //   ),
          //   DefaultTextStyle(
          //       style:
          //           TextStyle(fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.normal),
          //       child: Text("Changer la photo de profil"))
          // ]),
          // const SizedBox(height: 20),
          Material(child: SmTextFormField(_firstname, fieldName: "Prénom")),
          const SizedBox(height: 20),
          Material(child: SmTextFormField(_lastname, fieldName: "Nom")),
          const SizedBox(height: 20),
          Row(children: [
            Expanded(
              flex: 48,
              child: DateTimeFormField(
                decoration: const InputDecoration(
                    hintStyle: TextStyle(color: primaryColor),
                    errorStyle: TextStyle(color: primaryColor),
                    border: OutlineInputBorder(),
                    suffixIcon: Icon(Icons.event_note),
                    labelText: 'Date de naissance'),
                mode: DateTimeFieldPickerMode.date,
                autovalidateMode: AutovalidateMode.always,
                dateTextStyle: TextStyle(
                  fontSize: 16,
                  color: primaryColor,
                  fontFamily: "Raleway",
                  fontWeight: FontWeight.normal,
                ),
                initialDate: DateTime.tryParse(account.birthdate ?? ''),
                initialValue: DateTime.tryParse(account.birthdate ?? ''),
                onDateSelected: (value) {
                  account.birthdate = value.toString();
                  _birthdate.text = value.toString();
                },
              ),
            ),
            Expanded(flex: 4, child: Container()),
            Expanded(
              flex: 48,
              child: Material(
                child: SmSwitch("Information masquée", isSwitched: account.hideBirthdate, onSwitch: onSwitch),
              ),
            )
          ]),
          const SizedBox(height: 20),
          Material(
            child: Dropdown(
              _status,
              Status.values.map((e) => e.value).toList(),
              "Statut",
              previousValue: getPreviousStatus(),
            ),
          ),
          const SizedBox(height: 20),
          Material(
            child: Dropdown(
              _gender,
              Gender.values.map((e) => e.value).toList(),
              "Genre",
              previousValue: getPreviousGender(),
            ),
          ),
          const SizedBox(height: 20),
          Material(child: SmTextFormField(_country, fieldName: "Pays de résidence")),
          const SizedBox(height: 20),
          Material(child: SmTextFormField(_city, fieldName: "Ville de résidence")),
          const SizedBox(height: 20),
          SmSaveCancelButton(handleButton: onClickButton)
        ],
      ),
    );
  }

  Widget avatarEdition(Account account) {
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Flexible(
            child: SmElevatedButton(
                label: uploaded ? "Choisir une autre photo" : "Choisir une photo",
                color: primaryColor,
                handleButton: pickImage)),
        (uploaded ? Flexible(child: SizedBox(width: 0)) : SizedBox(width: 0)),
        (uploaded
            ? Flexible(child: SmElevatedButton(label: "Rogner la photo", color: primaryColor, handleButton: cropImage))
            : SizedBox(width: 0))
      ]),
      SizedBox(height: 20),
      Center(
          child: cropped
              ? Image.network(croppedImage!.path)
              : uploaded
                  ? Image.network(uploadedImage!.path, width: 300, height: 300)
                  : account.avatar != null
                      ? Image.network(account.avatar!, width: 300, height: 300)
                      : Image.asset(user, width: 300, height: 300)),
      uploaded ? SizedBox(height: 20) : SizedBox(height: 0),
      SizedBox(height: 20),
      SmSaveCancelButton(handleButton: processAvatar)
    ]);
  }
}
