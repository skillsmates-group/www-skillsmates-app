import 'dart:math';

import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/enums/account_attribute_enum.dart';
import 'package:www_skillsmates_app/models/profile_attribute.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/training_tile.dart';

import '../../../models/account/account.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../themes/theme.dart';
import '../../widgets/profile_avatar.dart';
import 'Job_tile.dart';

class ProfileSummary extends StatelessWidget {
  ProfileSummary({Key? key, required this.account, required this.data, this.handleVoirPlus}) : super(key: key);

  Account account;
  AccountAttributesResponse data;
  Function(ProfileAttribute)? handleVoirPlus;

  @override
  Widget build(BuildContext context) {
    clickVoirPlus(AccountAttribute attribute) {
      handleVoirPlus!(new ProfileAttribute(code: attribute, label: '', image: ''));
    }

    return SingleChildScrollView(
        child: Container(
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(height: 20),
      Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Expanded(
            flex: 4,
            child: Container(child: ProfileAvatar(imageUrl: account.avatar?.toLowerCase() ?? '', radius: 100.0))),
        Expanded(
            flex: 8,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(account.firstname, style: TextStyle(fontSize: 22)),
              Text(account.lastname, style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
              SizedBox(height: 30),
              Text.rich(TextSpan(
                  text: 'Sexe : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.gender,
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ])),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                  text: 'Statut : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.status,
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ])),
              if (!(account.hideBirthdate ?? false)) SizedBox(height: 10),
              if (!(account.hideBirthdate ?? false))
                Text.rich(TextSpan(
                    text: 'Date de naissance : ',
                    style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                    children: [
                      TextSpan(text: ' '),
                      TextSpan(
                          text: account.birthDate(),
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                    ])),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                  text: 'Pays de residence : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.country,
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ])),
              SizedBox(height: 10),
              Text.rich(TextSpan(
                  text: 'Ville de residence : ',
                  style: const TextStyle(fontSize: 16, overflow: TextOverflow.ellipsis),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                        text: account.city,
                        style:
                            const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, overflow: TextOverflow.ellipsis))
                  ]))
            ]))
      ]),
      SizedBox(height: 10),
      Text('A propos de moi', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
      SizedBox(height: 10),
      Text(account.biography ?? '', overflow: TextOverflow.ellipsis, softWrap: true, maxLines: 10),
      SizedBox(height: 20),
      Container(
          padding: EdgeInsets.only(left: 12.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), border: Border.all(width: 1.0, color: Colors.grey)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                ListTile(
                    // leading: SvgPicture.asset(green_arrow_plus),
                    title: Text('Expériences', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700)),
                    trailing: InkWell(child: Text('Voir plus'), onTap: () => clickVoirPlus(AccountAttribute.jobs))),
                SizedBox(height: 10),
                for (var i = 0; i < min(3, (data.jobs ?? []).length); i++) JobTile(job: data.jobs![i]),
                SizedBox(height: 10)
              ])),
      SizedBox(height: 20),
      Container(
          padding: EdgeInsets.only(left: 12.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), border: Border.all(width: 1.0, color: Colors.grey)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                ListTile(
                    // leading: SvgPicture.asset(school),
                    title: Text('Formations', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700)),
                    trailing:
                        InkWell(child: Text('Voir plus'), onTap: () => clickVoirPlus(AccountAttribute.trainings))),
                SizedBox(height: 10),
                for (var i = 0; i < min(2, (data.trainings ?? []).length); i++)
                  TrainingTile(training: data.trainings![i]),
                SizedBox(height: 10)
              ])),
      SizedBox(height: 20),
      Container(
          padding: EdgeInsets.only(left: 12.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), border: Border.all(width: 1.0, color: Colors.grey)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                ListTile(
                    title: Text('Compétences', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700)),
                    trailing: InkWell(child: Text('Voir plus'), onTap: () => clickVoirPlus(AccountAttribute.skills))),
                SizedBox(height: 10),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                    child: Wrap(
                        spacing: 8,
                        runSpacing: 8,
                        children: ((data.skills ?? []).sublist(0, min(3, (data.skills ?? []).length)))
                            .map((e) => Chip(
                                padding: EdgeInsets.all(8),
                                backgroundColor: Colors.grey,
                                label: Text(e.title!,
                                    style: TextStyle(color: primaryColor, fontSize: 20, fontWeight: FontWeight.w600))))
                            .toList())),
                SizedBox(height: 10)
              ]))
    ])));
  }
}
