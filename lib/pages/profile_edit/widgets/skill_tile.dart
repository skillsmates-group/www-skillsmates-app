import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../models/account/skill.dart';

class SkillTile extends StatelessWidget {
  final Skill skill;
  const SkillTile({Key? key, required this.skill}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: ListTile(
        leading: SvgPicture.asset(skill.image(), height: 50, width: 50),
        title: Text(
          skill.title!,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text.rich(
              TextSpan(
                text: 'Domaine: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: skill.discipline,
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                text: 'Niveau: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: skill.nameLevel(),
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            Text.rich(
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              maxLines: 2,
              TextSpan(
                text: 'Description: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: skill.description ?? '',
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        trailing: InkWell(
          child: const Icon(Icons.more_horiz),
          onTap: () {},
        ),
      ),
    );
  }
}
