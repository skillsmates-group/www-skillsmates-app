import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/skill_category.dart';
import 'package:www_skillsmates_app/models/account/skill.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/skill_tile.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_chip.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_text_form_field.dart';
import 'package:www_skillsmates_app/providers/auth_provider.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../enums/skill_level.dart';
import '../../../models/account/account.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/attribute_provider.dart';
import '../../../themes/picture_file.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_icon_text.dart';
import '../../widgets/sm_info_text.dart';
import '../../widgets/sm_svg_icon_text.dart';

class SkillsEdit extends StatefulWidget {
  const SkillsEdit({Key? key}) : super(key: key);

  @override
  State<SkillsEdit> createState() => _SkillsEditState();
}

class _SkillsEditState extends State<SkillsEdit> {
  bool isEditable = false;
  late String selectedSkillLevel = '';
  late String selectedSkillCategory = '';

  TextEditingController _title = TextEditingController();
  TextEditingController _discipline = TextEditingController();
  TextEditingController _description = TextEditingController();

  void showSuccessToast(String message) {
    MotionToast.success(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void showErrorToast(String message) {
    MotionToast.error(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    Future<AccountAttributesResponse> fetchAccountAttributes() async {
      late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
      return attributes;
    }

    double padding() {
      if (isDesktop) return 50;
      if (isTablet) return 10;
      return 0;
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: padding(),
        ),
        child: FutureBuilder<AccountAttributesResponse>(
            future: fetchAccountAttributes(),
            builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
              if (snapshot.hasData) {
                if (isEditable) {
                  return editSkills();
                } else {
                  return displaySkills(snapshot.data!);
                }
              } else if (snapshot.hasError) {
                return SmFutureBuilderError();
              } else {
                return SmFutureBuilderWaiting();
              }
            }),
      ),
    );
  }

  Widget editSkills() {
    onClickButton(button) {
      if (button == 'SAVE') {
        final authProvider = Provider.of<AuthProvider>(context, listen: false);
        Account loggedAccount = authProvider.profileAccount;
        Skill skill = new Skill();
        skill.title = _title.text;
        skill.discipline = _discipline.text;
        skill.description = _description.text;
        skill.level = selectedSkillLevel;
        skill.category = selectedSkillCategory;
        skill.account = loggedAccount.uuid;

        final attributeProvider = Provider.of<AttributeProvider>(context, listen: false);

        attributeProvider.saveSkill(skill).then((value) => {
              showSuccessToast("Votre competence a été mis à jour avec succes"),
              setState(() {
                this.isEditable = false;
              })
            });
      } else if (button == 'CANCEL') {
        setState(() {
          this.isEditable = false;
        });
      } else {
        setState(() {
          this.isEditable = false;
        });
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SingleChildScrollView(
          child: Row(
            children: [
              IntrinsicWidth(
                child: Column(
                  children: [
                    SmSvgIconText(
                      image: avatar,
                      text: Text(
                        'Nouvelle compétence',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      color: Colors.red,
                    ),
                    const SizedBox(height: 20),
                    Material(child: SmTextFormField(_title, fieldName: "Intitulé de la compétence")),
                    const SizedBox(height: 20),
                    Material(child: SmTextFormField(_discipline, fieldName: "Discipline ou domaine de la compétence")),
                    const SizedBox(height: 20),
                    Material(
                      child: TextField(
                        controller: _description,
                        textInputAction: TextInputAction.newline,
                        keyboardType: TextInputType.multiline,
                        maxLines: 5,
                        decoration: InputDecoration(
                          hintText: "Description et mots clés",
                          contentPadding: EdgeInsets.all(20.0),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Row(children: [
                      DefaultTextStyle(
                          style: TextStyle(
                              fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.normal),
                          child: Text("Niveau de maitrise actuel"))
                    ]),
                    const SizedBox(height: 20),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 25,
                          child: InkWell(
                            child: SmChip(
                              label: SkillLevel.LEVEL_1.value,
                              image: SkillLevel.LEVEL_1.image,
                              color: selectedSkillLevel == SkillLevel.LEVEL_1.name ? Colors.green : Colors.grey,
                            ),
                            onTap: () {
                              setState(() {
                                selectedSkillLevel = SkillLevel.LEVEL_1.name;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          flex: 25,
                          child: InkWell(
                            child: SmChip(
                              label: SkillLevel.LEVEL_2.value,
                              image: SkillLevel.LEVEL_2.image,
                              color: selectedSkillLevel == SkillLevel.LEVEL_2.name ? Colors.green : Colors.grey,
                            ),
                            onTap: () {
                              setState(() {
                                selectedSkillLevel = SkillLevel.LEVEL_2.name;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          flex: 25,
                          child: InkWell(
                            child: SmChip(
                              label: SkillLevel.LEVEL_3.value,
                              image: SkillLevel.LEVEL_3.image,
                              color: selectedSkillLevel == SkillLevel.LEVEL_3.name ? Colors.green : Colors.grey,
                            ),
                            onTap: () {
                              setState(() {
                                selectedSkillLevel = SkillLevel.LEVEL_3.name;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          flex: 25,
                          child: InkWell(
                            child: SmChip(
                              label: SkillLevel.LEVEL_4.value,
                              image: SkillLevel.LEVEL_4.image,
                              color: selectedSkillLevel == SkillLevel.LEVEL_4.name ? Colors.green : Colors.grey,
                            ),
                            onTap: () {
                              setState(() {
                                selectedSkillLevel = SkillLevel.LEVEL_4.name;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    SmSaveCancelButton(handleButton: onClickButton),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget displaySkills(AccountAttributesResponse data) {
    String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");
    String loadedAccountUuid = Provider.of<AuthProvider>(context, listen: false).profileAccount.uuid;
    String infoText =
        'Veuillez lister les competences pour lesquelles la communauté pourrait vous solliciter ainsi que celles que '
        'vous aimeriez ameliorer afin que nous puissons vous proposer des profils utils';

    onClickEdit() {
      setState(() {
        this.isEditable = true;
      });
    }

    List<Skill>? filterSkillsByCategory(List<Skill>? skills, String category) {
      return skills?.where((i) => i.category == category).toList();
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SmInfoText(infoText: infoText),
        SizedBox(height: 10),
        Text(
            'Competences maîtrisées (${(filterSkillsByCategory(data.skills ?? [], SkillCategory.CATEGORY_2.name) ?? []).length})',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
        SizedBox(height: 10),
        if (loggedAccountUuid == loadedAccountUuid)
          MouseRegion(
              child: SmIconText(
                  image: plus_colored,
                  text: Text('Ajouter une competence',
                      style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
                  color: Colors.red,
                  handleButton: onClickEdit),
              onHover: (event) {
                this.selectedSkillCategory = SkillCategory.CATEGORY_2.name;
              }),
        SizedBox(height: 10),
        for (Skill skill in filterSkillsByCategory(data.skills ?? [], SkillCategory.CATEGORY_2.name) ?? [])
          SkillTile(skill: skill),
        SizedBox(height: 10),
        Divider(height: 1, color: Colors.grey),
        SizedBox(height: 10),
        Text(
            'Competences à developper (${(filterSkillsByCategory(data.skills ?? [], SkillCategory.CATEGORY_1.name) ?? []).length})',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
        SizedBox(height: 10),
        if (loggedAccountUuid == loadedAccountUuid)
          MouseRegion(
            cursor: SystemMouseCursors.grab,
            child: SmIconText(
              image: plus_colored,
              text: Text(
                'Ajouter une competence',
                style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              color: Colors.red,
              handleButton: onClickEdit,
            ),
            onHover: (event) {
              this.selectedSkillCategory = SkillCategory.CATEGORY_1.name;
            },
          ),
        SizedBox(height: 10),
        for (Skill skill in filterSkillsByCategory(data.skills ?? [], SkillCategory.CATEGORY_1.name) ?? [])
          SkillTile(skill: skill),
      ],
    );
  }
}
