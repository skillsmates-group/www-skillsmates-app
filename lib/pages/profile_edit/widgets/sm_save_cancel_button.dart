import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_elevated_button.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class SmSaveCancelButton extends StatefulWidget {
  SmSaveCancelButton({
    Key? key,
    this.handleButton,
    this.saveButtonText,
    this.cancelButtonText,
  }) : super(key: key);
  Function(String)? handleButton;
  String? saveButtonText;
  String? cancelButtonText;

  @override
  State<SmSaveCancelButton> createState() => _SmSaveCancelButtonState();
}

class _SmSaveCancelButtonState extends State<SmSaveCancelButton> {
  onClickButton(String button) {
    widget.handleButton!(button);
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(flex: 2, child: Container()),
      Expanded(
        flex: 46,
        child: SmElevatedButton(
          label: "${widget.saveButtonText??'Enregistrer'}",
          color: greenColor,
          handleButton: () => onClickButton('SAVE'),
        ),
      ),
      Expanded(flex: 4, child: Container()),
      Expanded(
        flex: 46,
        child: SmElevatedButton(
          label: "${widget.cancelButtonText??'Annuler'}",
          color: Colors.grey,
          handleButton: () => onClickButton('CANCEL'),
        ),
      ),
      Expanded(flex: 2, child: Container())
    ]);
  }
}
