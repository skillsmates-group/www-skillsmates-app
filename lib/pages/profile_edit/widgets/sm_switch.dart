import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class SmSwitch extends StatefulWidget {
  final String label;
  final bool? isSwitched;
  Function(bool)? onSwitch;

  SmSwitch(
    this.label, {
    Key? key,
    this.isSwitched,
    this.onSwitch,
  }) : super(key: key);

  @override
  State<SmSwitch> createState() => _SmSwitchState();
}

class _SmSwitchState extends State<SmSwitch> {
  late bool isSwitched = false;
  late bool isInit = true;

  @override
  initState() {
    if (isInit) {
      isSwitched = widget.isSwitched ?? false;
      isInit = false;
    }
    super.initState();
  }

  onSwitchChanged(bool value) {
    widget.onSwitch!(value);
    setState(() {
      isSwitched = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      DefaultTextStyle(
          style: TextStyle(fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.normal),
          child: Text(widget.label)),
      Switch(
        value: isSwitched,
        activeColor: greenColor,
        onChanged: (value) => onSwitchChanged(value),
      )
    ]);
  }
}
