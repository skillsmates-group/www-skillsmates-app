import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/account/certification.dart';
import 'package:www_skillsmates_app/models/account/diploma.dart';
import 'package:www_skillsmates_app/models/account/training.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/certification_tile.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/diploma_tile.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/dropdown.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/training_tile.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_text_form_field.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../enums/education.dart';
import '../../../models/account/account.dart';
import '../../../models/account/account_attributes_response.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/attribute_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../themes/picture_file.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_icon_text.dart';
import '../../widgets/sm_info_text.dart';

class TrainingEdit extends StatefulWidget {
  const TrainingEdit({Key? key}) : super(key: key);

  @override
  State<TrainingEdit> createState() => _TrainingEditState();
}

class _TrainingEditState extends State<TrainingEdit> {
  bool isEditable = false;

  Education getEducationByValue(String value) {
    return Education.values.where((element) => element.value == value).first;
  }

  void showSuccessToast(String message) {
    MotionToast.success(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromTop,
    ).show(context);
  }

  void showErrorToast(String message) {
    MotionToast.error(
      title: Text("Modification du profil"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 50;
      if (isTablet) return 10;
      return 0;
    }

    Future<AccountAttributesResponse> fetchAccountAttributes() async {
      late AccountAttributesResponse attributes = Provider.of<AccountProvider>(context).accountAttributesResponse;
      return attributes;
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: padding(),
        ),
        child: FutureBuilder<AccountAttributesResponse>(
            future: fetchAccountAttributes(),
            builder: (BuildContext context, AsyncSnapshot<AccountAttributesResponse> snapshot) {
              if (snapshot.hasData) {
                if (isEditable) {
                  return editTraining();
                } else {
                  return displayTraining(snapshot.data!);
                }
              } else if (snapshot.hasError) {
                return SmFutureBuilderError();
              } else {
                return SmFutureBuilderWaiting();
              }
            }),
      ),
    );
  }

  Widget displayTraining(AccountAttributesResponse data) {
    String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");
    String loadedAccountUuid = Provider.of<AuthProvider>(context, listen: false).profileAccount.uuid;
    String infoText = '';

    onClickEdit() {
      setState(() {
        this.isEditable = true;
      });
    }

    return Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
      SmInfoText(infoText: infoText),
      SizedBox(height: 10),
      Text('Formation en cours (${data.trainings?.length})',
          style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w600)),
      SizedBox(height: 10),
      if (loggedAccountUuid == loadedAccountUuid)
        SmIconText(
            image: plus_colored,
            text: Text('Ajouter une formation',
                style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
            color: Colors.red,
            handleButton: onClickEdit),
      SizedBox(height: 10),
      for (Training training in data.trainings ?? []) TrainingTile(training: training),
      Divider(height: 4),
      SizedBox(height: 20),
      Text('Diplômes Obtenus (${data.diplomas?.length})',
          style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w600)),
      SizedBox(height: 10),
      if (loggedAccountUuid == loadedAccountUuid)
        SmIconText(
            image: plus_colored,
            text: Text('Ajouter un diplôme',
                style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
            color: Colors.red),
      SizedBox(height: 10),
      for (Diploma diploma in data.diplomas ?? []) DiplomaTile(diploma: diploma),
      Divider(height: 4),
      SizedBox(height: 20),
      Text('Certifications (${data.certifications?.length})',
          style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w600)),
      SizedBox(height: 10),
      if (loggedAccountUuid == loadedAccountUuid)
        SmIconText(
            image: plus_colored,
            text: Text('Ajouter une certification',
                style: TextStyle(color: Colors.lightBlue, fontSize: 20, fontWeight: FontWeight.w600)),
            color: Colors.red),
      SizedBox(height: 10),
      for (Certification certification in data.certifications ?? []) CertificationTile(certificate: certification),
    ]);
  }

  Widget editTraining() {
    TextEditingController _schoolClass = TextEditingController();
    TextEditingController _schoolName = TextEditingController();
    TextEditingController _education = TextEditingController();
    TextEditingController _title = TextEditingController();
    TextEditingController _city = TextEditingController();
    TextEditingController _description = TextEditingController();
    TextEditingController _teachingName = TextEditingController();

    onClickButton(button) {
      if (button == 'SAVE') {
        final authProvider = Provider.of<AuthProvider>(context, listen: false);
        Account loggedAccount = authProvider.profileAccount;
        Training training = new Training();
        training.title = _title.text;
        training.description = _description.text;
        training.account = loggedAccount.uuid;
        training.education = getEducationByValue(_education.text).name;
        // training.schoolType = '';
        training.schoolName = _schoolName.text;
        training.city = _city.text;
        training.schoolClass?.label = _schoolClass.text;
        // training.level = '';
        training.teachingName?.label = _teachingName.text;

        print(training);

        final attributeProvider = Provider.of<AttributeProvider>(context, listen: false);

        attributeProvider.saveTraining(training).then((value) => {
              showSuccessToast("Votre formation a été mise à jour avec succes"),
              setState(() {
                this.isEditable = false;
              })
            });
      } else if (button == 'CANCEL') {
        setState(() {
          this.isEditable = false;
        });
      } else {
        setState(() {
          this.isEditable = false;
        });
      }
    }

    return SingleChildScrollView(
        child: Column(children: [
      Row(children: [
        DefaultTextStyle(
            style: TextStyle(fontSize: 16, color: primaryColor, fontFamily: "Raleway", fontWeight: FontWeight.bold),
            child: Text("Formation en cours"))
      ]),
      const SizedBox(height: 20),
      Material(child: Dropdown(_education, Education.values.map((e) => e.value).toList(), "Education")),
      const SizedBox(height: 20),
      Material(child: SmTextFormField(_title, fieldName: "Titre de la formation")),
      const SizedBox(height: 20),
      Material(child: SmTextFormField(_schoolClass, fieldName: "Classe fréquentée")),
      const SizedBox(height: 20),
      Material(child: SmTextFormField(_teachingName, fieldName: "Domaine d'étude ou spécialité (facultatif)")),
      const SizedBox(height: 20),
      Material(child: SmTextFormField(_schoolName, fieldName: "Nom de l'établissement")),
      const SizedBox(height: 20),
      Material(child: SmTextFormField(_city, fieldName: "ville")),
      const SizedBox(height: 20),
      Material(
          child: TextField(
              controller: _description,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
              maxLines: 5,
              decoration: InputDecoration(
                  hintText: "Description de la formation (facultatif)", contentPadding: EdgeInsets.all(20.0)))),
      const SizedBox(height: 20),
      SmSaveCancelButton(handleButton: onClickButton)
    ]));
  }
}
