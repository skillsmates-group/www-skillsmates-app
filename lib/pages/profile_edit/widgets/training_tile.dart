import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/models/account/training.dart';

import '../../../themes/picture_file.dart';

class TrainingTile extends StatelessWidget {
  final Training training;

  const TrainingTile({Key? key, required this.training}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: ListTile(
        leading: SvgPicture.asset(school, height: 50, width: 50),
        title: Text(
          training.title!,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text.rich(
              TextSpan(
                text: 'Etablissement: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: '${training.schoolClass ?? ''} - ${training.schoolName ?? ''} - ${training.city ?? ''}',
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            Text.rich(
              TextSpan(
                text: 'Etude: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: '${training.teachingName ?? ''} - ${training.level ?? ''}',
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            Text.rich(
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              maxLines: 2,
              TextSpan(
                text: 'Description: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w700,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: training.description ?? '',
                    style: const TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        trailing: InkWell(
          child: const Icon(Icons.more_horiz),
          onTap: () {},
        ),
      ),
    );
  }
}
