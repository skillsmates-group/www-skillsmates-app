import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../widgets/slogan_text.dart';
import '../widgets/sm_footer_row.dart';
import '../widgets/update_password_card.dart';

class MobileResetPassword extends StatelessWidget {
  String token;

  MobileResetPassword({Key? key, required this.token}) : super(key: key);
  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: SvgPicture.asset(
                logoSkillsmates,
                height: 100,
              ),
            ),
            const SloganText(),
            UpdatePasswordCard(token: this.token),
            Center(
              child: SvgPicture.asset(
                landingPagePeople,
                // width: 50,
                height: 300,
              ),
            ),
            const SizedBox(height: 25),
            const SmFooterRow(),
          ],
        ),
      ),
    );
  }
}
