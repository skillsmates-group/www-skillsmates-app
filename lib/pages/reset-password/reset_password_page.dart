import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/reset-password/desktop_reset_password.dart';
import 'package:www_skillsmates_app/pages/reset-password/mobile_reset_password.dart';

class ResetPasswordPage extends StatelessWidget {
  String token;
  static const routeName = '/reset-password/:token';

  ResetPasswordPage({Key? key, required this.token}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return Padding(padding: EdgeInsets.all(10), child: DesktopResetPassword(token: this.token));
      }
      if (sizingInformation.isTablet) {
        return DesktopResetPassword(token: this.token);
      }
      if (sizingInformation.isMobile) {
        return MobileResetPassword(token: this.token);
      }
      return DesktopResetPassword(token: this.token);
    });
  }
}
