import 'package:flutter/material.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';

import '../../../providers/auth_provider.dart';
import '../../../themes/theme.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_text_form_field_password.dart';

class ResetPassword extends StatefulWidget {
  String token;

  ResetPassword({super.key, required this.token});

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final _formKey = GlobalKey<FormState>();
  final _pwd = TextEditingController();
  final _pwd2 = TextEditingController();

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();
    if (isValid) {
      _formKey.currentState!.save();
      if (_pwd.text != _pwd2.text) {
        _showMessageError("Les mots de passe de correspondent pas");
      } else {
        final authProvider = Provider.of<AuthProvider>(context, listen: false);
        authProvider
            .resetPassword(this.widget.token, _pwd.text)
            .then((value) => _showMessageSuccess(
                    "Votre mot de passe a été mis à jour. Retournez à la page d'accueil pour vous connecter")
                // Navigator.of(context).pushReplacementNamed(AccountCreatedPage.routeName)
                )
            .onError((error, stackTrace) => {
                  _showMessageError("Erreur lors du traitement de la "
                      "requette")
                });
      }
    }
  }

  void _showMessageError(String message) {
    MotionToast.error(
      title: Text("Une erreur est survenue"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
      toastDuration: Duration(seconds: 10),
    ).show(context);
  }

  void _showMessageSuccess(String message) {
    _pwd.clear();
    _pwd2.clear();
    MotionToast.success(
      title: Text("Felicitation"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
      toastDuration: Duration(seconds: 10),
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(children: [
              const Text(
                'Entrer le nouveau mot de passe',
                style: TextStyle(
                  fontSize: 20,
                  color: primaryColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              SmTextFormFieldPassword(
                _pwd,
                fieldName: "Nouveau mot de passe",
              ),
              SizedBox(height: 15),
              SmTextFormFieldPassword(
                _pwd2,
                fieldName: "Confirmation du nouveau mot de passe",
              ),
              SizedBox(height: 25),
              SmElevatedButton(
                label: 'Envoyer',
                color: greenColor,
                handleButton: _trySubmit,
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
