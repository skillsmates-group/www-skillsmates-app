import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:www_skillsmates_app/models/media.dart';

import '../../enums/direction.dart';
import '../../enums/media_type_enum.dart';
import '../../models/account/account_response.dart';
import '../../models/account/account_status_metric.dart';
import '../../models/account/account_status_metric_response.dart';
import '../../models/documents/document_type_metric.dart';
import '../../models/documents/document_type_metric_response.dart';
import '../../models/posts/profile_documents_response.dart';
import '../../models/search_param.dart';
import '../../providers/properties.dart';

class SearchProvider extends ChangeNotifier {
  final String _headerNameProfile = "Profils";
  final String _headerNameDocument = "Documents";
  final String _headerNameVideo = "Vidéos";
  final String _headerNameLink = "Liens";
  final String _headerNameImage = "Photos";
  final String _headerNameAudio = "Audios";

  bool _shouldDisplayProfile = true;
  bool _shouldDisplayDocuments = true;
  bool _shouldDisplayVideos = true;
  bool _shouldDisplayLinks = true;
  bool _shouldDisplayImages = true;
  bool _shouldDisplayAudios = true;

  bool _showProfiles = false;
  bool _showDocuments = false;
  bool _showVideos = false;
  bool _showLinks = false;
  bool _showImages = false;
  bool _showAudios = false;

  List<AccountStatusMetric> _allStatuses = [];
  List<AccountStatusMetric> _selectedProfileStatus = List.empty(growable: true);
  List<DocumentTypeMetric> _allDocumentType = [];
  List<DocumentTypeMetric> _selectedDocumentTypes = List.empty(growable: true);

  List<DocumentTypeMetric> _documentMetric = [];
  List<DocumentTypeMetric> _videoMetric = [];
  List<DocumentTypeMetric> _linkMetric = [];
  List<DocumentTypeMetric> _imageMetric = [];
  List<DocumentTypeMetric> _audioMetric = [];

  int _currentPage = 0;
  bool _shouldCountResult = false;

  SearchParam _searchParam =
      SearchParam(direction: "DESC", country: "", city: "", statuses: [], documentTypes: [], content: "");
  ProfileDocumentsResponse? _searchPostResponse;
  AccountResponse? _accountResponse;

  bool get shouldDisplayProfile => _shouldDisplayProfile;

  bool get shouldDisplayDocuments => _shouldDisplayDocuments;

  bool get shouldDisplayVideos => _shouldDisplayVideos;

  bool get shouldDisplayLinks => _shouldDisplayLinks;

  bool get shouldDisplayImages => _shouldDisplayImages;

  bool get shouldDisplayAudios => _shouldDisplayAudios;

  bool get showProfiles => _showProfiles;

  bool get showDocuments => _showDocuments;

  bool get showVideos => _showVideos;

  bool get showLinks => _showLinks;

  bool get showImages => _showImages;

  bool get showAudios => _showAudios;

  List<AccountStatusMetric> get allStatuses => _allStatuses;

  List<AccountStatusMetric> get selectedProfileStatus => _selectedProfileStatus;

  List<DocumentTypeMetric> get allDocumentType => _allDocumentType;

  List<DocumentTypeMetric> get selectedDocumentTypes => _selectedDocumentTypes;

  SearchParam get searchParam => _searchParam;

  ProfileDocumentsResponse? get searchPostResponse => _searchPostResponse;

  AccountResponse? get accountResponse => _accountResponse;

  int get currentPage => _currentPage;

  bool get shouldCountResult => _shouldCountResult;

  String get headerNameProfile => _headerNameProfile;

  String get headerNameDocument => _headerNameDocument;

  String get headerNameVideo => _headerNameVideo;

  String get headerNameLink => _headerNameLink;

  String get headerNameImage => _headerNameImage;

  String get headerNameAudio => _headerNameAudio;

  List<DocumentTypeMetric> get documentMetric => _documentMetric = [];

  List<DocumentTypeMetric> get videoMetric => _videoMetric = [];

  List<DocumentTypeMetric> get linkMetric => _linkMetric = [];

  List<DocumentTypeMetric> get imageMetric => _imageMetric = [];

  List<DocumentTypeMetric> get audioMetric => _audioMetric = [];

  updateSelectedAccount(List<AccountStatusMetric> list) {
    _selectedProfileStatus.clear();
    _selectedDocumentTypes.clear();
    list.forEach((element) {
      _selectedProfileStatus.add(element);
    });
    _searchParam.documentTypes = _selectedDocumentTypes.map((e) => e.uuid).toList();
    _searchParam.statuses = _selectedProfileStatus.map((e) => e.code).toList();

    _currentPage = 0;
    //research(_searchParam, _currentPage);
    search(_searchParam, _currentPage);

    _shouldDisplayProfile = true;
    _shouldDisplayDocuments = false;
    _shouldDisplayVideos = false;
    _shouldDisplayLinks = false;
    _shouldDisplayImages = false;
    _shouldDisplayAudios = false;

    _showProfiles = _showProfiles;
    _showDocuments = false;
    _showVideos = false;
    _showLinks = false;
    _showImages = false;
    _showAudios = false;

    _shouldCountResult = true;

    notifyListeners();
  }

  updateSelectedDocument(List<DocumentTypeMetric> list, String mediaType) {
    _selectedProfileStatus.clear();
    _selectedDocumentTypes.clear();
    list.forEach((element) {
      _selectedDocumentTypes.add(element);
    });
    _searchParam.documentTypes = _selectedDocumentTypes.map((e) => e.uuid).toList();
    _searchParam.statuses = _selectedProfileStatus.map((e) => e.code).toList();

    _currentPage = 0;
    research(_searchParam, _currentPage);
    //search(_searchParam, _currentPage);

    _shouldDisplayProfile = false;
    _shouldDisplayDocuments = mediaType == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name;
    _shouldDisplayVideos = mediaType == MediaTypeEnum.MEDIA_TYPE_VIDEO.name;
    _shouldDisplayLinks = mediaType == MediaTypeEnum.MEDIA_TYPE_LINK.name;
    _shouldDisplayImages = mediaType == MediaTypeEnum.MEDIA_TYPE_IMAGE.name;
    _shouldDisplayAudios = mediaType == MediaTypeEnum.MEDIA_TYPE_AUDIO.name;

    _showProfiles = false;
    _showDocuments = mediaType == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name && _showDocuments;
    _showVideos = mediaType == MediaTypeEnum.MEDIA_TYPE_VIDEO.name && _showVideos;
    _showLinks = mediaType == MediaTypeEnum.MEDIA_TYPE_LINK.name && _showLinks;
    _showImages = mediaType == MediaTypeEnum.MEDIA_TYPE_IMAGE.name && _showImages;
    _showAudios = mediaType == MediaTypeEnum.MEDIA_TYPE_AUDIO.name && _showAudios;

    _shouldCountResult = true;

    notifyListeners();
  }

  clickSearchButton(String country, String city, String content, String direction) {
    _searchParam.country = country==''? null : country;
    _searchParam.city = city ==''? null : city;
    _searchParam.content = content ==''? null : content;
    String value = direction.isNotEmpty
        ? Direction.values.where((element) => element.value == direction).first.name
        : Direction.DESC.name;
    _searchParam.direction = value;

    _selectedDocumentTypes.clear();
    _allDocumentType.forEach((element) {
      _selectedDocumentTypes.add(element);
    });

    _selectedProfileStatus.clear();
    _allStatuses.forEach((element) {
      _selectedProfileStatus.add(element);
    });

    _searchParam.documentTypes = _selectedDocumentTypes.map((e) => e.uuid).toList();
    _searchParam.statuses = _selectedProfileStatus.map((e) => e.code).toList();

    _shouldDisplayProfile = true;
    _shouldDisplayDocuments = true;
    _shouldDisplayVideos = true;
    _shouldDisplayLinks = true;
    _shouldDisplayImages = true;
    _shouldDisplayAudios = true;

    _showProfiles = false;
    _showDocuments = false;
    _showVideos = false;
    _showLinks = false;
    _showImages = false;
    _showAudios = false;

    _shouldCountResult = true;

    _currentPage = 0;
    searchProfileAndPost();

    notifyListeners();
  }

  updateShouldDisplayProfileResult(bool value) {
    _shouldDisplayProfile = true;
    _shouldDisplayDocuments = false;
    _shouldDisplayVideos = false;
    _shouldDisplayLinks = false;
    _shouldDisplayImages = false;
    _shouldDisplayAudios = false;

    _showProfiles = value;
    _showDocuments = false;
    _showVideos = false;
    _showLinks = false;
    _showImages = false;
    _showAudios = false;

    _shouldCountResult = true;

    _selectedDocumentTypes = [];
    _searchParam.documentTypes = _selectedDocumentTypes.map((e) => e.uuid).toList();
    notifyListeners();
  }

  updateShouldDisplayDocumentResult(MediaTypeEnum value) {
    _shouldDisplayProfile = false;
    _shouldDisplayDocuments = value == MediaTypeEnum.MEDIA_TYPE_DOCUMENT;
    _shouldDisplayVideos = value == MediaTypeEnum.MEDIA_TYPE_VIDEO;
    _shouldDisplayLinks = value == MediaTypeEnum.MEDIA_TYPE_LINK;
    _shouldDisplayImages = value == MediaTypeEnum.MEDIA_TYPE_IMAGE;
    _shouldDisplayAudios = value == MediaTypeEnum.MEDIA_TYPE_AUDIO;

    _showProfiles = false;
    _showDocuments = value == MediaTypeEnum.MEDIA_TYPE_DOCUMENT;
    _showVideos = value == MediaTypeEnum.MEDIA_TYPE_VIDEO;
    _showLinks = value == MediaTypeEnum.MEDIA_TYPE_LINK;
    _showImages = value == MediaTypeEnum.MEDIA_TYPE_IMAGE;
    _showAudios = value == MediaTypeEnum.MEDIA_TYPE_AUDIO;

    _shouldCountResult = true;

    _selectedDocumentTypes.removeWhere((element) => element.mediaType.code != value.name);
    _searchParam.documentTypes = _selectedDocumentTypes.map((e) => e.uuid).toList();
    _selectedProfileStatus = [];
    _searchParam.statuses = _selectedProfileStatus.map((e) => e.code).toList();

    notifyListeners();
  }

  bool hasNoResultFound() {
    int totalProfile = _accountResponse?.meta?.total ?? 0;
    int totalDocument = _searchPostResponse?.totalDocuments ?? 0;
    int totalLinks = _searchPostResponse?.totalLinks ?? 0;
    int totalAudio = _searchPostResponse?.totalAudios ?? 0;
    int totalImages = _searchPostResponse?.totalImages ?? 0;
    int totalVideos = _searchPostResponse?.totalVideos ?? 0;
    return totalProfile + totalDocument + totalLinks + totalAudio + totalImages + totalVideos == 0;
  }

  bool shouldDisplayNoResult() {
    return (_shouldCountResult == true) && (hasNoResultFound() == true);
  }

  goToProfilePage(int page) {
    _currentPage = page;
    search(_searchParam, _currentPage);
    notifyListeners();
  }

  goToDocumentPage(int page) {
    _currentPage = page;
    research(_searchParam, _currentPage);
    notifyListeners();
  }

  shouldDisplayResults(MediaTypeEnum mediaTypeEnum) {
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_DOCUMENT) return _showDocuments;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_VIDEO) return _showVideos;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_LINK) return _showLinks;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_IMAGE) return _showImages;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_AUDIO) return _showAudios;
  }

  getHeaderName(MediaTypeEnum mediaTypeEnum) {
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_DOCUMENT) return _headerNameDocument;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_VIDEO) return _headerNameVideo;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_LINK) return _headerNameLink;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_IMAGE) return _headerNameImage;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_AUDIO) return _headerNameAudio;
  }

  getTotal(MediaTypeEnum mediaTypeEnum) {
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_DOCUMENT) return _searchPostResponse?.totalDocuments ?? 0;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_VIDEO) return _searchPostResponse?.totalVideos ?? 0;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_LINK) return _searchPostResponse?.totalLinks ?? 0;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_IMAGE) return _searchPostResponse?.totalImages ?? 0;
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_AUDIO) return _searchPostResponse?.totalAudios ?? 0;
  }

  getTotalProfile() {
    return _accountResponse?.meta?.total ?? 0;
  }

  getPost(MediaTypeEnum mediaTypeEnum) {
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_DOCUMENT) return _searchPostResponse?.documents ?? [];
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_VIDEO) return _searchPostResponse?.videos ?? [];
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_LINK) return _searchPostResponse?.links ?? [];
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_IMAGE) return _searchPostResponse?.images ?? [];
    if (mediaTypeEnum == MediaTypeEnum.MEDIA_TYPE_AUDIO) return _searchPostResponse?.audios ?? [];
  }

  getProfiles() {
    AccountResponse ar = _accountResponse ?? new AccountResponse();
    return ar.resources ?? [];
  }

  int totalPage(MediaTypeEnum mediaType) {
    int reste = getTotal(mediaType) % 9;
    int page = getTotal(mediaType) ~/ 9;
    return reste == 0 ? page : page + 1;
  }

  profileTotalPage() {
    return _accountResponse?.meta!.totalPages ?? 1;
  }

  void initVariables() {
    fetchDataFromAPI();
    _currentPage = 0;
    _searchParam = SearchParam(direction: "DESC", country: "", city: "", statuses: [], documentTypes: [], content: "");

    _selectedDocumentTypes = [];
    _allDocumentType.forEach((element) {
      _selectedDocumentTypes.add(element);
    });
    _searchParam.documentTypes = _selectedDocumentTypes.map((e) => e.uuid).toList();

    _selectedProfileStatus = [];
    _allStatuses.forEach((element) {
      _selectedProfileStatus.add(element);
    });
    _searchParam.statuses = _selectedProfileStatus.map((e) => e.code).toList();
    _shouldDisplayProfile = true;
    _shouldDisplayDocuments = true;
    _shouldDisplayVideos = true;
    _shouldDisplayLinks = true;
    _shouldDisplayImages = true;
    _shouldDisplayAudios = true;

    _showProfiles = false;
    _showDocuments = false;
    _showVideos = false;
    _showLinks = false;
    _showImages = false;
    _showAudios = false;

    _shouldCountResult = false;

    notifyListeners();
  }

  Future<void> fetchDataFromAPI() async {
    _allDocumentType = await fetchDocumentTypeMetric();
    _allStatuses = await fetchAccountStatusMetric();

    _documentMetric =
        _allDocumentType.where((element) => element.mediaType.code == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name).toList();
    _videoMetric =
        _allDocumentType.where((element) => element.mediaType.code == MediaTypeEnum.MEDIA_TYPE_VIDEO.name).toList();
    _linkMetric =
        _allDocumentType.where((element) => element.mediaType.code == MediaTypeEnum.MEDIA_TYPE_LINK.name).toList();
    _imageMetric =
        _allDocumentType.where((element) => element.mediaType.code == MediaTypeEnum.MEDIA_TYPE_IMAGE.name).toList();
    _audioMetric =
        _allDocumentType.where((element) => element.mediaType.code == MediaTypeEnum.MEDIA_TYPE_AUDIO.name).toList();

    notifyListeners();
  }

  getAllDocumentType(Media media) {
    if (media.code == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name) return _documentMetric;
    if (media.code == MediaTypeEnum.MEDIA_TYPE_VIDEO.name) return _videoMetric;
    if (media.code == MediaTypeEnum.MEDIA_TYPE_LINK.name) return _linkMetric;
    if (media.code == MediaTypeEnum.MEDIA_TYPE_IMAGE.name) return _imageMetric;
    if (media.code == MediaTypeEnum.MEDIA_TYPE_AUDIO.name) return _audioMetric;
  }

  void searchProfileAndPost() async {
    List<Future<void>> futures = [
      research(_searchParam, _currentPage),
      search(_searchParam, _currentPage),
    ];
  }

  Future<ProfileDocumentsResponse?> research(SearchParam searchParam, int page) async {
    final url = Uri.parse(skillsmates_posts + '/posts/research?offset=$page&limit=9');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'account': searchParam.account,
          'content': searchParam.content,
          'country': searchParam.country,
          'city': searchParam.city,
          'documentTypes': searchParam.documentTypes,
          'statutes': searchParam.statuses,
          'direction': searchParam.direction,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _searchPostResponse = ProfileDocumentsResponse.fromJson(body);
      notifyListeners();
      return _searchPostResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<AccountResponse?> search(SearchParam searchParam, int page) async {
    final url = Uri.parse(skillsmates_accounts + '/search?offset=$page&limit=9');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'account': searchParam.account,
          'content': searchParam.content,
          'country': searchParam.country,
          'city': searchParam.city,
          'documentTypes': searchParam.documentTypes,
          'statuses': searchParam.statuses,
          'direction': searchParam.direction,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _accountResponse = AccountResponse.fromJson(body);
      notifyListeners();
      return _accountResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<List<DocumentTypeMetric>> fetchDocumentTypeMetric() async {
    final url = Uri.parse(skillsmates_posts + '/document-types/metric');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _allDocumentType = DocumentTypeMetricResponse.fromJson(body).resources!;
      notifyListeners();
      return _allDocumentType;
    } catch (e) {
      throw e;
    }
  }

  Future<List<AccountStatusMetric>> fetchAccountStatusMetric() async {
    final url = Uri.parse(skillsmates_accounts + '/status/metric');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _allStatuses = AccountStatusMetricResponse.fromJson(body).resources!;
      notifyListeners();
      return _allStatuses;
    } catch (e) {
      throw e;
    }
  }
}
