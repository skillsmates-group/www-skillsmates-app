import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/search/widgets/desktop_search_content.dart';

import '../dashboard/widgets/desktop_nav_bar.dart';

class DesktopSearchScreen extends StatefulWidget {
  const DesktopSearchScreen({Key? key}) : super(key: key);

  @override
  _DesktopSearchScreenState createState() => _DesktopSearchScreenState();
}

class _DesktopSearchScreenState extends State<DesktopSearchScreen> {
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: DesktopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DesktopSearchContent(),
          ],
        ),
      ),
    );
  }
}
