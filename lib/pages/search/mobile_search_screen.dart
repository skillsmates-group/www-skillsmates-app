import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/pages/search/widgets/mobile_search_content.dart';

import '../dashboard/widgets/mobile_nav_bar.dart';
import '../dashboard/widgets/mobile_top_nav_bar.dart';

class MobileSearchScreen extends StatefulWidget {
  const MobileSearchScreen({Key? key}) : super(key: key);

  @override
  _MobileSearchScreenState createState() => _MobileSearchScreenState();
}

class _MobileSearchScreenState extends State<MobileSearchScreen> {
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 100.0),
        child: MobileTopNavBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [MobileSearchContent()],
        ),
      ),
      bottomNavigationBar: MobileNavBar(),
    );
  }
}
