import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'desktop_search_screen.dart';
import 'mobile_search_screen.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  static const routeName = '/search';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
        builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopSearchScreen();
      }
      if (sizingInformation.isTablet) {
        return const MobileSearchScreen();
      }
      return const MobileSearchScreen();
    });
  }
}
