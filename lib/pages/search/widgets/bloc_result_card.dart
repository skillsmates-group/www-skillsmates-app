import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/search/widgets/result_card_item.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../enums/media_type_enum.dart';
import '../SearchProvider.dart';

class BlocResultCard extends StatefulWidget {
  final String headerPicture;
  final Color headerColor;
  final MediaTypeEnum mediaType;
  Function(MediaTypeEnum)? callbackShowResults;

  BlocResultCard(
      {Key? key,
      required this.mediaType,
      this.callbackShowResults,
      required this.headerPicture,
      required this.headerColor})
      : super(key: key);

  @override
  State<BlocResultCard> createState() => _BlocResultCardState();
}

class _BlocResultCardState extends State<BlocResultCard> {
  late SearchProvider _provider;

  getImage(Post post) {
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name) return doc;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_AUDIO.name) return audio_rose;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_VIDEO.name) return video_rouge;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_IMAGE.name) return photo_vert;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_LINK.name) return lien_jaune;
  }

  clickNavigation(int page) {
    _provider.goToDocumentPage(page);
  }

  @override
  void initState() {
    super.initState();
    _provider = Provider.of<SearchProvider>(context, listen: false);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _provider = Provider.of<SearchProvider>(context, listen: false);
  }

  showResults() {
    widget.callbackShowResults!(widget.mediaType);
  }

  @override
  Widget build(BuildContext context) {
    var withScreen = MediaQuery.of(context).size.width;
    return Card(
        elevation: smElevationCard,
        child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            width: withScreen,
            child: Column(children: [
              Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: widget.headerColor),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  child: Row(children: [
                    SvgPicture.asset(widget.headerPicture, height: 40, width: 40),
                    const SizedBox(width: 10),
                    Text.rich(TextSpan(
                        text: _provider.getHeaderName(widget.mediaType),
                        style: const TextStyle(fontWeight: FontWeight.w700, color: whitishColor),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                              text: "(${_provider.getTotal(widget.mediaType)})",
                              style: const TextStyle(fontWeight: FontWeight.w500, color: whitishColor))
                        ])),
                    Spacer(),
                    if (!_provider.shouldDisplayResults(widget.mediaType) &&
                        _provider.getPost(widget.mediaType).length > 3)
                      ElevatedButton.icon(
                          onPressed: () => showResults(),
                          icon: const Icon(Icons.arrow_drop_down_circle_outlined),
                          label: const Text("Voir plus de résultats"),
                          style: ElevatedButton.styleFrom(primary: Colors.transparent))
                  ])),
              if (_provider.getPost(widget.mediaType).length > 0)
                GridView.builder(
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3, mainAxisSpacing: 2.0, crossAxisSpacing: 2.0),
                    itemCount: _provider.shouldDisplayResults(widget.mediaType)
                        ? _provider.getPost(widget.mediaType).length
                        : min(3, _provider.getPost(widget.mediaType).length),
                    itemBuilder: (context, index) {
                      return GridTile(
                          child: ResultCardItem(
                              post: _provider.getPost(widget.mediaType)[index],
                              svgPicture: getImage(_provider.getPost(widget.mediaType)[index])));
                    }),
              if (_provider.shouldDisplayResults(widget.mediaType) && _provider.totalPage(widget.mediaType) > 1)
                Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                  TextButton(
                      onPressed: _provider.currentPage > 0 ? () => clickNavigation(0) : null, child: Text('Début')),
                  TextButton(
                      onPressed: _provider.currentPage > 0 ? () => clickNavigation(_provider.currentPage - 1) : null,
                      child: Text('Prec.')),
                  TextButton(
                      onPressed: null,
                      child: Text(' ${_provider.currentPage + 1} / ${_provider.totalPage(widget.mediaType)}')),
                  TextButton(
                      onPressed: _provider.currentPage < _provider.totalPage(widget.mediaType) - 1
                          ? () => clickNavigation(_provider.currentPage + 1)
                          : null,
                      child: Text('Suiv.')),
                  TextButton(
                      onPressed: _provider.currentPage < _provider.totalPage(widget.mediaType) - 1
                          ? () => clickNavigation(_provider.totalPage(widget.mediaType) - 1)
                          : null,
                      child: Text('Fin'))
                ])
            ])));
  }
}
