import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

import '../../../providers/post_provider.dart';
import '../../../themes/theme.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/story_card.dart';
import '../SearchProvider.dart';

class BlocResultCardProfile extends StatefulWidget {
  final String headerPicture;
  final Color headerColor;
  Function(bool)? callbackShowResults;

  BlocResultCardProfile({Key? key, required this.headerPicture, required this.headerColor, this.callbackShowResults})
      : super(key: key);

  @override
  State<BlocResultCardProfile> createState() => _BlocResultCardProfileState();
}

class _BlocResultCardProfileState extends State<BlocResultCardProfile> {
  late SearchProvider _provider;

  //bool _showResults = false;

  clickNavigation(int page) {
    _provider.goToProfilePage(page);
  }

  @override
  void initState() {
    super.initState();
    _provider = Provider.of<SearchProvider>(context, listen: false);
    //_showResults = _provider.showProfiles;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _provider = Provider.of<SearchProvider>(context, listen: false);
    //_showResults = _provider.showProfiles;
  }

  showResults() {
    /*setState(() {
      _showResults = true;
    });*/
    widget.callbackShowResults!(true);
  }

  @override
  Widget build(BuildContext context) {
    var withScreen = MediaQuery.of(context).size.width;
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

    return Card(
        elevation: smElevationCard,
        child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            width: withScreen,
            child: Column(children: [
              Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: widget.headerColor),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  child: Row(children: [
                    SvgPicture.asset(widget.headerPicture, height: 40, width: 40),
                    const SizedBox(width: 10),
                    Text.rich(TextSpan(
                        text: _provider.headerNameProfile,
                        style: const TextStyle(fontWeight: FontWeight.w700, color: whitishColor),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                              text: "(${_provider.getTotalProfile()})",
                              style: const TextStyle(fontWeight: FontWeight.w500, color: whitishColor))
                        ])),
                    Spacer(),
                    if (!_provider.showProfiles && _provider.getProfiles().length > 3)
                      ElevatedButton.icon(
                          onPressed: () => showResults(),
                          icon: const Icon(Icons.arrow_drop_down_circle_outlined),
                          label: const Text("Voir plus de résultats"),
                          style: ElevatedButton.styleFrom(primary: Colors.transparent))
                  ])),
              if (_provider.getProfiles().length > 0)
                GridView.builder(
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3, mainAxisSpacing: 2.0, crossAxisSpacing: 2.0),
                    itemCount: _provider.showProfiles
                        ? _provider.getProfiles().length
                        : min(3, _provider.getProfiles().length),
                    itemBuilder: (context, index) {
                      return GridTile(
                          child: FutureBuilder<bool>(
                              future: postProvider.checkSocialInteraction(
                                  loggedAccountUuid, _provider.getProfiles()[index].uuid),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  return StoryCard(
                                      bottomSpace: true,
                                      isAddStory: true,
                                      account: _provider.getProfiles()[index],
                                      isSuggestion: !snapshot.data!);
                                } else if (snapshot.hasError) {
                                  return SmFutureBuilderError();
                                } else {
                                  return SmFutureBuilderWaiting();
                                }
                              }));
                    }),
              if (_provider.showProfiles && _provider.profileTotalPage() > 1)
                Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                  TextButton(
                      onPressed: _provider.currentPage > 0 ? () => clickNavigation(0) : null, child: Text('Début')),
                  TextButton(
                      onPressed: _provider.currentPage > 0 ? () => clickNavigation(_provider.currentPage - 1) : null,
                      child: Text('Prec.')),
                  TextButton(
                      onPressed: null, child: Text(' ${_provider.currentPage + 1} / ${_provider.profileTotalPage()}')),
                  TextButton(
                      onPressed: _provider.currentPage < _provider.profileTotalPage() - 1
                          ? () => clickNavigation(_provider.currentPage + 1)
                          : null,
                      child: Text('Suiv.')),
                  TextButton(
                      onPressed: _provider.currentPage < _provider.profileTotalPage() - 1
                          ? () => clickNavigation(_provider.profileTotalPage() - 1)
                          : null,
                      child: Text('Fin'))
                ])
            ])));
  }
}
