import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:www_skillsmates_app/pages/login/widgets/sm_checkbox_list_tile.dart';
import 'package:www_skillsmates_app/providers/help_provider.dart';

import '../../../../../themes/theme.dart';

class CategorieSearch extends StatefulWidget {
  final String label;
  final Color bgColor;
  final String pictureCateg;

  const CategorieSearch({
    Key? key,
    required this.label,
    required this.bgColor,
    required this.pictureCateg,
  }) : super(key: key);

  @override
  State<CategorieSearch> createState() => _CategorieSearchState();
}

class _CategorieSearchState extends State<CategorieSearch> {
  bool _isSelect = false;
  void _isSelected(BuildContext ctx) {
    var helpProvider = Provider.of<HelpProvider>(ctx, listen: false);
    setState(() {
      if (_isSelect == false) {
        _isSelect = true;
      } else {
        _isSelect = false;
      }
    });
    if (_isSelect) {
      helpProvider.selectValue(widget.label);
    } else {
      helpProvider.selectValue("Tout");
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      onPressed: () => _isSelected(context),
      icon: SvgPicture.asset(
        widget.pictureCateg,
        height: 30,
        width: 30,
      ),
      label: _isSelect && widget.label != "Tout"
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${widget.label} (0)'),
                const SizedBox(
                  height: 3,
                ),
                SmCheckboxListTile(label: "All"),
                const SizedBox(
                  height: 3,
                ),
                SmCheckboxListTile(label: "Résumé"),
                const SizedBox(
                  height: 3,
                ),
                SmCheckboxListTile(label: "Autres"),
              ],
            )
          : Text('${widget.label} (0)'),
      style: TextButton.styleFrom(
        primary: _isSelect ? whitishColor : Colors.black,
        backgroundColor:
            _isSelect ? widget.bgColor.withOpacity(.6) : whitishColor,
        alignment: Alignment.centerLeft,
        // onPrimary: isSelected ? whitishColor : Colors.black,
        padding: const EdgeInsets.all(8),
        fixedSize: _isSelect && widget.label != "Tout"
            ? const Size(260, 200)
            : const Size(260, 50),
        shape: _isSelect ? const RoundedRectangleBorder() : null,
      ),
    );
  }
}
