import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/media_type_enum.dart';
import 'package:www_skillsmates_app/pages/search/SearchProvider.dart';
import 'package:www_skillsmates_app/pages/search/widgets/profile_expansion_tile.dart';
import 'package:www_skillsmates_app/pages/search/widgets/search_card.dart';

import '../../../data/media_data.dart';
import '../../../enums/account_attribute_enum.dart';
import '../../../models/account/account_status_metric.dart';
import '../../../models/documents/document_type_metric.dart';
import '../../../models/media.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../documents_shelf/widgets/media_expansion_tile.dart';
import '../../widgets/responsive.dart';
import 'bloc_result_card.dart';
import 'bloc_result_card_profile.dart';

class DesktopSearchContent extends StatefulWidget {
  const DesktopSearchContent({Key? key}) : super(key: key);

  @override
  State<DesktopSearchContent> createState() => _DesktopSearchContentState();
}

class _DesktopSearchContentState extends State<DesktopSearchContent> {
  final _keywordController = TextEditingController();
  final _cityController = TextEditingController();
  final _countryController = TextEditingController();
  final _directionController = TextEditingController();

  String _tab = AccountAttribute.resume.name;
  late SearchProvider _searchProvider;

  @override
  void dispose() {
    _keywordController.dispose();
    _cityController.dispose();
    _countryController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _searchProvider = Provider.of<SearchProvider>(context, listen: false);
    _searchProvider.initVariables();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _searchProvider = Provider.of<SearchProvider>(context, listen: false);
  }

  clickMedia(Media media) {
    setState(() {
      _tab = media.code;
    });
  }

  callbackToggleAccountStatus(List<AccountStatusMetric> list) {
    setState(() {
      _searchProvider.updateSelectedAccount(list);
    });
  }

  callbackToggleDocumentType(List<DocumentTypeMetric> list, String mediaTypeCode) {
    setState(() {
      _searchProvider.updateSelectedDocument(list, mediaTypeCode);
    });
  }

  callbackShowProfileResults(bool value) {
    setState(() {
      _searchProvider.updateShouldDisplayProfileResult(value);
    });
  }

  callbackShowDocumentsResults(MediaTypeEnum value) {
    setState(() {
      _searchProvider.updateShouldDisplayDocumentResult(value);
    });
  }

  handleSearchButton() {
    setState(() {
      _searchProvider.clickSearchButton(
          _countryController.text, _cityController.text, _keywordController.text, _directionController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: padding()),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
              flex: 3,
              child: Column(children: [
                Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              padding: const EdgeInsets.only(left: 20, top: 20),
                              child: Text('Catégories',
                                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700, color: primaryColor))),
                          Container(
                              padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
                              child: Divider(color: primaryColor, thickness: 2.0)),
                          Container(
                              padding: const EdgeInsets.only(right: 20),
                              decoration: BoxDecoration(color: _tab == 'PROFILE' ? blueLightColor : Colors.white),
                              child: InkWell(
                                  onTap: () => clickMedia(medias[1]),
                                  child: Row(children: [
                                    Expanded(
                                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Container(
                                          padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                                          child: ProfileExpansionTile(callback: callbackToggleAccountStatus))
                                    ]))
                                  ]))),
                          Column(children: [
                            for (var media in medias)
                              Container(
                                  padding: const EdgeInsets.only(right: 20),
                                  decoration: BoxDecoration(color: _tab == media.code ? blueLightColor : Colors.white),
                                  child: InkWell(
                                      onTap: () => clickMedia(media),
                                      child: Row(children: [
                                        Expanded(
                                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                          Container(
                                              padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                                              child: MediaExpansionTile(
                                                  callback: callbackToggleDocumentType, media: media))
                                        ]))
                                      ])))
                          ])
                        ]))
              ])),
          Expanded(
              flex: 9,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                        onTap: handleSearchButton,
                        child: SearchCard(
                            textEditing: _keywordController,
                            city: _cityController,
                            country: _countryController,
                            direction: _directionController,
                            hasNoResult: false,
                            handleButton: handleSearchButton)),
                    SizedBox(height: 20),
                    Consumer<SearchProvider>(builder: (context, searchProvider, child) {
                      return Column(children: [
                        if (searchProvider.shouldDisplayProfile && !searchProvider.shouldDisplayNoResult())
                          BlocResultCardProfile(
                              headerColor: Colors.grey,
                              headerPicture: user,
                              callbackShowResults: callbackShowProfileResults)
                      ]);
                    }),
                    SizedBox(height: 15),
                    Consumer<SearchProvider>(builder: (context, searchProvider, child) {
                      return Column(children: [
                        if (searchProvider.shouldDisplayDocuments && !searchProvider.shouldDisplayNoResult())
                          BlocResultCard(
                              mediaType: MediaTypeEnum.MEDIA_TYPE_DOCUMENT,
                              callbackShowResults: callbackShowDocumentsResults,
                              headerPicture: document,
                              headerColor: blueColor),
                        if (searchProvider.shouldDisplayDocuments && !searchProvider.shouldDisplayNoResult())
                          SizedBox(height: 15),
                        if (searchProvider.shouldDisplayVideos && !searchProvider.shouldDisplayNoResult())
                          BlocResultCard(
                              mediaType: MediaTypeEnum.MEDIA_TYPE_VIDEO,
                              callbackShowResults: callbackShowDocumentsResults,
                              headerPicture: video_rouge,
                              headerColor: redColor),
                        if (searchProvider.shouldDisplayVideos && !searchProvider.shouldDisplayNoResult())
                          SizedBox(height: 15),
                        if (searchProvider.shouldDisplayLinks && !searchProvider.shouldDisplayNoResult())
                          BlocResultCard(
                              mediaType: MediaTypeEnum.MEDIA_TYPE_LINK,
                              callbackShowResults: callbackShowDocumentsResults,
                              headerPicture: lien_jaune,
                              headerColor: yellowColor),
                        if (searchProvider.shouldDisplayLinks && !searchProvider.shouldDisplayNoResult())
                          SizedBox(height: 15),
                        if (searchProvider.shouldDisplayImages && !searchProvider.shouldDisplayNoResult())
                          BlocResultCard(
                              mediaType: MediaTypeEnum.MEDIA_TYPE_IMAGE,
                              callbackShowResults: callbackShowDocumentsResults,
                              headerPicture: photo_vert,
                              headerColor: greenColor),
                        if (searchProvider.shouldDisplayImages && !searchProvider.shouldDisplayNoResult())
                          SizedBox(height: 15),
                        if (searchProvider.shouldDisplayAudios && !searchProvider.shouldDisplayNoResult())
                          BlocResultCard(
                              mediaType: MediaTypeEnum.MEDIA_TYPE_AUDIO,
                              callbackShowResults: callbackShowDocumentsResults,
                              headerPicture: audio_rose,
                              headerColor: pinkColor),
                        if (searchProvider.shouldDisplayAudios && !searchProvider.shouldDisplayNoResult())
                          SizedBox(height: 15),
                        if (_searchProvider.shouldDisplayNoResult())
                          Container(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Text("Aucun résultat trouvé",
                                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
                                    const SizedBox(height: 10),
                                    SvgPicture.asset(no_result),
                                    const SizedBox(height: 10),
                                    const Text(
                                        "Supprimer des filtres ou reformulez votre recherches pour obtenir des résultats.",
                                        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w300),
                                        textAlign: TextAlign.center)
                                  ]))
                      ]);
                    })
                  ]))
        ]));
  }
}
