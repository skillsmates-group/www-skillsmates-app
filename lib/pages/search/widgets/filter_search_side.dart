import 'package:flutter/material.dart';

import '/pages/widgets/sm_footer_column.dart';
import '../../../../../themes/picture_file.dart';
import '../../../../../themes/theme.dart';
import 'categorie_search.dart';

class FilterSearchSide extends StatefulWidget {
  final String typeFilter;
  const FilterSearchSide({
    Key? key,
    this.typeFilter = " ",
  }) : super(key: key);

  @override
  State<FilterSearchSide> createState() => _FilterSearchSideState();
}

class _FilterSearchSideState extends State<FilterSearchSide> {
  var scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: 8),
      height: MediaQuery.of(context).size.height - 100,
      padding: EdgeInsets.only(
        top: 25,
        left: 10,
        right: 1,
        bottom: 3,
      ),
      decoration: BoxDecoration(
        border: Border.all(),
        borderRadius: BorderRadius.circular(10),
      ),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              widget.typeFilter == "classeur" ? 'Filtrer' : 'Catégories',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Divider(
              thickness: 2,
              height: 8,
              color: Colors.black,
              endIndent: 30,
            ),
            SizedBox(
              height: 10,
            ),
            if (widget.typeFilter != "classeur")
              CategorieSearch(
                label: 'Tout',
                bgColor: primaryColor,
                pictureCateg: filter,
              ),
            if (widget.typeFilter != "classeur")
              CategorieSearch(
                label: 'Personnes',
                bgColor: primaryColor,
                pictureCateg: profile,
              ),
            if (widget.typeFilter != "classeur")
              SizedBox(
                height: 8,
              ),
            CategorieSearch(
              label: 'Documents',
              bgColor: blueColor,
              pictureCateg: document_bleu,
            ),
            SizedBox(
              height: 8,
            ),
            CategorieSearch(
              label: 'Liens',
              bgColor: yellowColor,
              pictureCateg: lien_jaune,
            ),
            SizedBox(
              height: 8,
            ),
            CategorieSearch(
              label: 'Videos',
              bgColor: redBoldColor,
              pictureCateg: video_rouge,
            ),
            SizedBox(
              height: 8,
            ),
            CategorieSearch(
              label: 'Photos',
              bgColor: greenColor,
              pictureCateg: photo_vert,
            ),
            SizedBox(
              height: 8,
            ),
            CategorieSearch(
              label: 'Audios',
              bgColor: pinkColor,
              pictureCateg: audio_rose,
            ),
            SizedBox(
              height: 30,
            ),
            SmFooterColumn(),
          ],
        ),
      ),
    );
  }
}
