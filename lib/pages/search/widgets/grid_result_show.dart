import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/models/posts/post.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '/pages/search/widgets/result_card_item.dart';

class GridResultShow extends StatelessWidget {
  final String categoryLabel;
  const GridResultShow({
    Key? key,
    required this.categoryLabel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var items = [1, 2, 3, 4];
    var picturesvg = '';
    var title = '';
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 1.35,
      ),
      shrinkWrap: true,
      itemCount: items.length,
      itemBuilder: (context, index) {
        if (categoryLabel == "Photos") {
          picturesvg = photo_vert;
          title = "Titre de la photo";
        } else if (categoryLabel == "Videos") {
          picturesvg = video_rouge;
          title = "Titre de la video";
        } else if (categoryLabel == "Liens") {
          picturesvg = lien_jaune;
          title = "Titre du lien";
        } else if (categoryLabel == "Audios") {
          picturesvg = audio_rose;
          title = "Titre de l'audio";
        } else {
          picturesvg = doc;
          title = 'Titre du document';
        }
        return ResultCardItem(
          post: Post(),
          svgPicture: picturesvg,
        );
      },
    );
  }
}
