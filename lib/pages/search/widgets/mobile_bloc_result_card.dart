import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/search/widgets/result_card_item.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '../../../enums/media_type_enum.dart';
import '../../widgets/responsive.dart';

class MobileBlocResultCard extends StatelessWidget {
  final String headerPicture;
  final String headerName;
  final Color headerColor;
  final List<Post> posts;
  final int numberPosts;
  final bool showAll;

  const MobileBlocResultCard(
      {Key? key,
      required this.headerName,
      required this.headerPicture,
      required this.headerColor,
      required this.posts,
      required this.numberPosts,
      required this.showAll})
      : super(key: key);

  getImage(Post post) {
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_DOCUMENT.name) return doc;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_AUDIO.name) return audio_rose;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_VIDEO.name) return video_rouge;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_IMAGE.name) return photo_vert;
    if (post.mediaType == MediaTypeEnum.MEDIA_TYPE_LINK.name) return lien_jaune;
  }

  @override
  Widget build(BuildContext context) {
    var withScreen = MediaQuery.of(context).size.width;
    return Card(
        elevation: smElevationCard,
        child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            width: withScreen,
            child: Column(children: [
              Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: headerColor),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  child: Row(children: [
                    SvgPicture.asset(headerPicture, height: 40, width: 40),
                    const SizedBox(width: 10),
                    Text.rich(TextSpan(
                        text: headerName,
                        style: const TextStyle(fontWeight: FontWeight.w700, color: whitishColor),
                        children: [
                          TextSpan(text: ' '),
                          TextSpan(
                              text: "(${numberPosts})",
                              style: const TextStyle(fontWeight: FontWeight.w500, color: whitishColor))
                        ]))
                  ])),
              const SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(0),
                padding: EdgeInsets.all(0),
                height: 260.0,
                color: Responsive.isDesktop(context) ? Colors.transparent : Colors.white,
                child: ListView.builder(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8.0,
                  ),
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: posts.length,
                  itemBuilder: (ctx, i) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0),
                    child: ResultCardItem(
                        post: posts[i],
                        svgPicture: getImage(
                          posts[i],
                        )),
                  ),
                ),
              ),
              showAll
                  ? SizedBox()
                  : Column(children: [
                      SizedBox(height: 10),
                      ElevatedButton.icon(
                          onPressed: () {},
                          icon: const Icon(Icons.arrow_drop_down_circle_outlined),
                          label: const Text("Voir tout"),
                          style: ElevatedButton.styleFrom(
                              fixedSize: Size((withScreen - 200), 20), primary: Colors.grey[400]))
                    ])
            ])));
  }
}
