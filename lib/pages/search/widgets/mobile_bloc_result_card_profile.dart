import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

import '../../../models/account/account.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/story_card.dart';

class MobileBlocResultCardProfile extends StatelessWidget {
  final String headerPicture;
  final String headerName;
  final Color headerColor;
  final List<Account> accounts;
  final bool showAll;

  const MobileBlocResultCardProfile(
      {Key? key,
      required this.headerName,
      required this.headerPicture,
      required this.headerColor,
      required this.accounts,
      required this.showAll})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var withScreen = MediaQuery.of(context).size.width;
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

    return Card(
      elevation: smElevationCard,
      child: Container(
        margin: EdgeInsets.all(
          10,
        ),
        padding: EdgeInsets.all(
          10,
        ),
        width: withScreen,
        child: Column(children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                10,
              ),
              color: headerColor,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: 5,
              vertical: 4,
            ),
            child: Row(children: [
              SvgPicture.asset(
                headerPicture,
                height: 40,
                width: 40,
              ),
              const SizedBox(
                width: 10,
              ),
              Text.rich(
                TextSpan(
                    text: headerName,
                    style: const TextStyle(
                      fontWeight: FontWeight.w700,
                      color: whitishColor,
                    ),
                    children: [
                      TextSpan(
                        text: ' ',
                      ),
                      TextSpan(
                        text: "(${accounts.length})",
                        style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          color: whitishColor,
                        ),
                      ),
                    ]),
              ),
            ]),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.all(0),
            padding: EdgeInsets.all(0),
            height: 264.0,
            color: Responsive.isDesktop(context) ? Colors.transparent : Colors.white,
            child: ListView.builder(
              padding: const EdgeInsets.symmetric(
                horizontal: 4.0,
              ),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: accounts.length,
              itemBuilder: (ctx, i) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: FutureBuilder<bool>(
                  future: postProvider.checkSocialInteraction(loggedAccountUuid, accounts[i].uuid),
                  builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                    if (snapshot.hasData) {
                      return StoryCard(
                          isAddStory: true, account: accounts[i], bottomSpace: true, isSuggestion: !snapshot.data!);
                    } else if (snapshot.hasError) {
                      return SmFutureBuilderError();
                    } else {
                      return SmFutureBuilderWaiting();
                    }
                  },
                ),
              ),
            ),
          ),
          showAll
              ? SizedBox()
              : Column(children: [
                  SizedBox(height: 10),
                  ElevatedButton.icon(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.arrow_drop_down_circle_outlined,
                    ),
                    label: const Text(
                      "Voir tout",
                    ),
                    style: ElevatedButton.styleFrom(fixedSize: Size((withScreen - 200), 20), primary: Colors.grey[400]),
                  ),
                ]),
        ]),
      ),
    );
  }
}
