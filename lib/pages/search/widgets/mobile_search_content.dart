import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/enums/direction.dart';
import 'package:www_skillsmates_app/pages/search/widgets/search_card.dart';

import '../../../enums/account_attribute_enum.dart';
import '../../../enums/media_type_enum.dart';
import '../../../models/account/account_response.dart';
import '../../../models/account/account_status_metric.dart';
import '../../../models/documents/document_type_metric.dart';
import '../../../models/media.dart';
import '../../../models/posts/post.dart';
import '../../../models/posts/profile_documents_response.dart';
import '../../../models/search_param.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../../providers/post_provider.dart';
import '../../../themes/picture_file.dart';
import '../../../themes/theme.dart';
import '../../widgets/responsive.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import 'mobile_bloc_result_card.dart';
import 'mobile_bloc_result_card_profile.dart';

class MobileSearchContent extends StatefulWidget {
  const MobileSearchContent({Key? key}) : super(key: key);

  @override
  State<MobileSearchContent> createState() => _MobileSearchContentState();
}

class _MobileSearchContentState extends State<MobileSearchContent> {
  String tab = AccountAttribute.resume.name;
  List<DocumentTypeMetric> selectedDocumentTypes = [];
  List<AccountStatusMetric> selectedProfileStatus = [];
  SearchParam searchParam =
      SearchParam(direction: "DESC", country: "", city: "", statuses: [], documentTypes: [], content: "");
  final _keywordController = TextEditingController();
  final _cityController = TextEditingController();
  final _countryController = TextEditingController();
  final _directionController = TextEditingController();
  List<Post> posts = [];
  bool isDocument = false;
  bool isImage = false;
  bool isLink = false;
  bool isVideo = false;
  bool isAudio = false;
  bool isNotFound = false;
  bool isAll = true;
  bool isProfile = false;

  updateSections() {
    disableAllSections();
    isProfile = selectedProfileStatus.length > 0;
    if (selectedDocumentTypes.isEmpty && !isProfile) {
      isNotFound = true;
    } else {
      if (hasPostOfType(MediaTypeEnum.MEDIA_TYPE_DOCUMENT)) isDocument = true;
      if (hasPostOfType(MediaTypeEnum.MEDIA_TYPE_IMAGE)) isImage = true;
      if (hasPostOfType(MediaTypeEnum.MEDIA_TYPE_LINK)) isLink = true;
      if (hasPostOfType(MediaTypeEnum.MEDIA_TYPE_AUDIO)) isAudio = true;
      if (hasPostOfType(MediaTypeEnum.MEDIA_TYPE_VIDEO)) isVideo = true;
    }
  }

  disableAllSections() {
    isDocument = false;
    isImage = false;
    isLink = false;
    isVideo = false;
    isAudio = false;
    isNotFound = false;
    isAll = false;
  }

  hasPostOfType(MediaTypeEnum mediaTypeEnum) {
    return selectedDocumentTypes.where((element) => element.mediaType.code == mediaTypeEnum.name).isNotEmpty;
  }

  findPostOfType(MediaTypeEnum mediaTypeEnum) {
    return posts.where((element) => element.mediaType == mediaTypeEnum.name).toList();
  }

  updateSearchParams() {
    searchParam.country = _countryController.text;
    searchParam.city = _cityController.text;
    searchParam.content = _keywordController.text;
    String value = _directionController.text.isNotEmpty
        ? Direction.values.where((element) => element.value == _directionController.text).first.name
        : Direction.DESC.name;
    searchParam.direction = value;
  }

  @override
  void dispose() {
    _keywordController.dispose();
    _cityController.dispose();
    _countryController.dispose();
    super.dispose();
  }

  clickMedia(Media media) {
    setState(() {
      tab = media.code;
    });
  }

  clickDocumentType(DocumentTypeMetric documentTypeMetric) {
    List<String> selected = [];
    setState(() {
      bool alreadySelected =
          selectedDocumentTypes.where((element) => element.code == documentTypeMetric.code).isNotEmpty;
      if (alreadySelected) {
        selectedDocumentTypes.removeWhere((element) => element.code == documentTypeMetric.code);
      } else {
        selectedDocumentTypes.add(documentTypeMetric);
      }
      selectedDocumentTypes.forEach((element) {
        selected.add(element.uuid);
      });
      searchParam.documentTypes = selected;
      updateSections();
      updateSearchParams();
    });
  }

  clickSearchButton() {
    setState(() {
      updateSearchParams();
    });
  }

  clickAccountStatus(AccountStatusMetric status) {
    List<String> selected = [];
    setState(() {
      bool alreadySelected = selectedProfileStatus.where((element) => element.code == status.code).isNotEmpty;
      if (alreadySelected) {
        selectedProfileStatus.removeWhere((element) => element.code == status.code);
      } else {
        selectedProfileStatus.add(status);
      }
      selectedProfileStatus.forEach((element) {
        selected.add(element.code);
      });
      searchParam.statuses = selected;
      updateSections();
      // isProfile = selectedProfileStatus.length > 0;
      updateSearchParams();
    });
  }

  List<DocumentTypeMetric> filterDocumentTypesByMedia(Media media, List<DocumentTypeMetric> response) {
    return response.where((element) => element.mediaType.code == media.code).toList();
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: padding(),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
        // Expanded(
        //   flex: 3,
        //   child: Column(children: [
        //     Card(
        //       elevation: 0,
        //       shape: RoundedRectangleBorder(
        //         borderRadius: BorderRadius.circular(20),
        //       ),
        //       child: Column(
        //           mainAxisAlignment: MainAxisAlignment.start,
        //           crossAxisAlignment: CrossAxisAlignment.start,
        //           children: [
        //             Container(
        //               padding: const EdgeInsets.only(
        //                 left: 20,
        //                 top: 20,
        //               ),
        //               child: Text(
        //                 'Catégories',
        //                 style: TextStyle(
        //                   fontSize: 22,
        //                   fontWeight: FontWeight.w700,
        //                   color: primaryColor,
        //                 ),
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(
        //                 left: 20,
        //                 top: 20,
        //                 right: 20,
        //               ),
        //               child: Divider(
        //                 color: primaryColor,
        //                 thickness: 2.0,
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, top: 20),
        //               child: ListTile(
        //                 leading: SvgPicture.asset(user, height: 25, width: 25),
        //                 title: Text(
        //                   'Tous (${0})',
        //                 ),
        //               ),
        //             ),
        //             FutureBuilder<AccountStatusMetricResponse>(
        //                 future: accountProvider.fetchAccountStatusMetric(),
        //                 builder: (BuildContext context, AsyncSnapshot<AccountStatusMetricResponse> snapshot) {
        //                   List<AccountStatusMetric> accountStatusMetricList = snapshot.data?.resources ?? [];
        //                   if (snapshot.hasData) {
        //                     return Container(
        //                       padding: const EdgeInsets.only(right: 20),
        //                       decoration: BoxDecoration(color: tab == 'PROFILE' ? blueLightColor : Colors.white),
        //                       child: InkWell(
        //                         onTap: () => clickMedia(medias[1]),
        //                         child: Row(children: [
        //                           Expanded(
        //                             child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        //                               Container(
        //                                 padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
        //                                 child: ProfileExpansionTile(
        //                                   accountStatus: accountStatusMetricList,
        //                                   selectedAccountStatus: selectedProfileStatus,
        //                                   callback: clickAccountStatus,
        //                                 ),
        //                               ),
        //                             ]),
        //                           ),
        //                         ]),
        //                       ),
        //                     );
        //                   } else if (snapshot.hasError) {
        //                     return SmFutureBuilderError();
        //                   } else {
        //                     return SmFutureBuilderWaiting();
        //                   }
        //                 }),
        //             FutureBuilder<DocumentTypeMetricResponse>(
        //                 future: postProvider.fetchDocumentTypeMetric(),
        //                 builder: (BuildContext context, AsyncSnapshot<DocumentTypeMetricResponse> snapshot) {
        //                   List<DocumentTypeMetric> list = snapshot.data?.resources ?? [];
        //                   if (snapshot.hasData) {
        //                     return Column(children: [
        //                       for (var media in medias)
        //                         Container(
        //                           padding: const EdgeInsets.only(right: 20),
        //                           decoration: BoxDecoration(color: tab == media.code ? blueLightColor : Colors.white),
        //                           child: InkWell(
        //                             onTap: () => clickMedia(media),
        //                             child: Row(children: [
        //                               Expanded(
        //                                 child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        //                                   Container(
        //                                     padding: const EdgeInsets.only(
        //                                       left: 20,
        //                                       top: 20,
        //                                       bottom: 20,
        //                                     ),
        //                                     child: MediaExpansionTile(
        //                                       documentTypes: filterDocumentTypesByMedia(media, list),
        //                                       selectedDocumentTypes: selectedDocumentTypes,
        //                                       callback: clickDocumentType,
        //                                     ),
        //                                   ),
        //                                 ]),
        //                               ),
        //                             ]),
        //                           ),
        //                         ),
        //                       SizedBox(height: 50)
        //                     ]);
        //                   } else if (snapshot.hasError) {
        //                     return SmFutureBuilderError();
        //                   } else {
        //                     return SmFutureBuilderWaiting();
        //                   }
        //                 }),
        //           ]),
        //     ),
        //   ]),
        // ),
        Expanded(
          flex: 9,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SearchCard(
                  city: _cityController,
                  country: _countryController,
                  textEditing: _keywordController,
                  direction: _directionController,
                  handleButton: clickSearchButton,
                  hasNoResult: isNotFound,
                ),
                SizedBox(
                  height: 20,
                ),
                FutureBuilder(
                    future: accountProvider.search(searchParam, 0),
                    builder: (BuildContext context, AsyncSnapshot<AccountResponse> snapshot) {
                      if (snapshot.hasData) {
                        return Column(children: [
                          isAll | isProfile
                              ? MobileBlocResultCardProfile(
                                  accounts: snapshot.data?.resources ?? [],
                                  showAll: isProfile,
                                  headerColor: Colors.grey,
                                  headerName: "Profile",
                                  headerPicture: user,
                                )
                              : SizedBox()
                        ]);
                      } else if (snapshot.hasError) {
                        return SmFutureBuilderError();
                      } else {
                        return SmFutureBuilderWaiting();
                      }
                    }),
                const SizedBox(
                  height: 15,
                ),
                FutureBuilder(
                    future: postProvider.research(searchParam, 0),
                    builder: (BuildContext context, AsyncSnapshot<ProfileDocumentsResponse?> snapshot) {
                      if (snapshot.hasData) {
                        return Column(children: [
                          isAll || isDocument
                              ? MobileBlocResultCard(
                                  posts: snapshot.data?.documents ?? [],
                                  numberPosts: snapshot.data?.totalDocuments ?? 0,
                                  showAll: isDocument,
                                  headerColor: blueColor,
                                  headerName: "Documents",
                                  headerPicture: document,
                                )
                              : SizedBox(),
                          const SizedBox(
                            height: 15,
                          ),
                          isAll || isVideo
                              ? MobileBlocResultCard(
                                  posts: snapshot.data?.videos ?? [],
                                  numberPosts: snapshot.data?.totalVideos ?? 0,
                                  showAll: isVideo,
                                  headerColor: redColor,
                                  headerName: "Videos",
                                  headerPicture: video_rouge,
                                )
                              : SizedBox(),
                          const SizedBox(
                            height: 15,
                          ),
                          isAll || isLink
                              ? MobileBlocResultCard(
                                  posts: snapshot.data?.links ?? [],
                                  numberPosts: snapshot.data?.totalLinks ?? 0,
                                  showAll: isLink,
                                  headerColor: yellowColor,
                                  headerName: "Liens",
                                  headerPicture: lien_jaune,
                                )
                              : SizedBox(),
                          const SizedBox(
                            height: 15,
                          ),
                          isAll || isImage
                              ? MobileBlocResultCard(
                                  posts: snapshot.data?.images ?? [],
                                  numberPosts: snapshot.data?.totalImages ?? 0,
                                  showAll: isImage,
                                  headerColor: greenColor,
                                  headerName: "Photos",
                                  headerPicture: photo_vert,
                                )
                              : SizedBox(),
                          const SizedBox(
                            height: 15,
                          ),
                          isAll || isAudio
                              ? MobileBlocResultCard(
                                  posts: snapshot.data?.audios ?? [],
                                  numberPosts: snapshot.data?.totalAudios ?? 0,
                                  showAll: isAudio,
                                  headerColor: pinkColor,
                                  headerName: "Audio",
                                  headerPicture: audio_rose,
                                )
                              : SizedBox(),
                          const SizedBox(
                            height: 15,
                          ),
                          isNotFound
                              ? Container(
                                  padding: const EdgeInsets.all(
                                    10,
                                  ),
                                  child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Aucun résultat trouvé",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        SvgPicture.asset(
                                          no_result,
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        const Text(
                                          "Supprimer des filtres ou reformulez votre recherches pour obtenir des résultats.",
                                          style: TextStyle(
                                            fontSize: 30,
                                            fontWeight: FontWeight.w300,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ]),
                                )
                              : SizedBox()
                        ]);
                      } else if (snapshot.hasError) {
                        return SmFutureBuilderError();
                      } else {
                        return SmFutureBuilderWaiting();
                      }
                    }),
              ]),
        ),
      ]),
    );
  }
}
