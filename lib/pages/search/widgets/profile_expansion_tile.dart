import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/search/SearchProvider.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../../models/account/account_status_metric.dart';

class ProfileExpansionTile extends StatefulWidget {
  Function(List<AccountStatusMetric>)? callback;

  ProfileExpansionTile({Key? key, this.callback}) : super(key: key);

  @override
  State<ProfileExpansionTile> createState() => _ProfileExpansionTileState();
}

class _ProfileExpansionTileState extends State<ProfileExpansionTile> {
  List<AccountStatusMetric> _selected = List.empty(growable: true);
  late SearchProvider _provider;

  @override
  void initState() {
    _provider = Provider.of<SearchProvider>(context, listen: false);
    _provider.selectedProfileStatus.forEach((element) {
      _selected.add(element);
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _selected.clear();
    _provider.selectedProfileStatus.forEach((element) {
      _selected.add(element);
    });
  }

  void toggleAccountStatus(AccountStatusMetric status) {
    if (_selected.any((element) => element.code == status.code)) {
      _selected.removeWhere((element) => element.code == status.code);
    } else {
      _selected.add(status);
    }
    widget.callback!(_selected);
  }

  void toggleSelectAll(bool? value) {
    _selected.clear();
    if (value == true) {
      _provider.allStatuses.forEach((element) {
        _selected.add(element);
      });
    }
    widget.callback!(_selected);
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ExpansionTile(
          leading: SvgPicture.asset(user, height: 25, width: 25),
          title: Text('Personnes'),
          children: [
                ListTile(
                    leading: Checkbox(
                        value: _provider.allStatuses
                            .every((element1) => _selected.any((element2) => element1.code == element2.code)),
                        onChanged: (value) => toggleSelectAll(value)),
                    title: Text('Tout', style: TextStyle(fontSize: 14)))
              ] +
              List.generate(context.watch<SearchProvider>().allStatuses.length, (index) {
                return ListTile(
                    leading: Checkbox(
                        value: _selected.any((element) => element.code == _provider.allStatuses[index].code),
                        onChanged: (value) => toggleAccountStatus(_provider.allStatuses[index])),
                    title: Text('${_provider.allStatuses[index].label} (${_provider.allStatuses[index].total})',
                        style: TextStyle(fontSize: 14)));
              }),
          onExpansionChanged: (value) {})
    ]);
  }
}
