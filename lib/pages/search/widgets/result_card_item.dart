import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';
import 'package:www_skillsmates_app/themes/theme.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import '../../../models/posts/post.dart';
import '../../../providers/post_provider.dart';
import '../../post_details/post_details_page.dart';

class ResultCardItem extends StatelessWidget {
  final String svgPicture;
  final Post post;

  const ResultCardItem({Key? key, this.svgPicture = doc, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final postProvider = Provider.of<PostProvider>(context);

    void _navigateToPostDetails(Post post) {
      postProvider.setCurrentPost(post).then((value) => {
            context.push(PostDetailsPage.routeName)
            // Navigator.of(context).pushReplacementNamed(
            //   PostDetailsPage.routeName,
            // )
          });
    }

    bool isYouTubeUrl(String content) {
      RegExp regExp = RegExp(
          r'^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?');
      String? matches = regExp.stringMatch(content);
      if (matches == null) {
        return false;
      }
      return true;
    }

    String? getMediaType(String path) {
      if (isYouTubeUrl(path)) {
        return 'YOUTUBE';
      }

      final mimeType = lookupMimeType(path);

      if (mimeType != null) {
        if (mimeType.startsWith('image/')) {
          return 'IMAGE';
        } else if (mimeType.startsWith('application/msword')) {
          return 'DOCUMENT';
        } else if (mimeType.startsWith('application/pdf')) {
          return 'PDF';
        } else if (mimeType.startsWith('application/audio')) {
          return 'AUDIO';
        } else if (mimeType.startsWith('application/video')) {
          return 'VIDEO';
        }
      }
      return 'LINK';
    }

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      color: Colors.grey.shade200,
      elevation: 2,
      child: InkWell(
        onTap: () {
          _navigateToPostDetails(post);
        },
        child: Container(
          width: 220,
          padding: const EdgeInsets.all(8.0),
          child: ClipRRect(
            child: Column(
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: () {},
                      child: SvgPicture.asset(
                        svgPicture,
                        height: 40,
                        width: 40,
                      ),
                    ),
                    const SizedBox(width: 2.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text.rich(
                            softWrap: true,
                            maxLines: 2,
                            TextSpan(
                              text: post.title ?? '',
                              style: const TextStyle(
                                fontSize: 11.0,
                                overflow: TextOverflow.ellipsis,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          FittedBox(
                            fit: BoxFit.contain,
                            child: Row(
                              children: [
                                Text(
                                  'Discipline: ${post.discipline ?? ''}',
                                  softWrap: true,
                                  maxLines: 1,
                                  style: TextStyle(
                                    color: Colors.grey[600],
                                    overflow: TextOverflow.ellipsis,
                                    fontSize: 11.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          FittedBox(
                            fit: BoxFit.contain,
                            child: Row(
                              children: [
                                Text(
                                  'Publié ${post.durationText()}',
                                  softWrap: true,
                                  style: TextStyle(
                                    color: Colors.grey[600],
                                    fontSize: 9.0,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Icon(
                                  Icons.public,
                                  color: Colors.grey[600],
                                  size: 9.0,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      post.description ?? '',
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 2,
                      style: TextStyle(fontSize: 11),
                    ),
                  ],
                ),
                if (getMediaType(post.link ?? '') == 'YOUTUBE')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: YoutubePlayerControllerProvider(
                              controller: YoutubePlayerController(
                                  initialVideoId: getIdFromUrl(post.link ?? '') ?? 'nPt8bK2gbaU',
                                  params: YoutubePlayerParams(
                                      // playlist: ['nPt8bK2gbaU', 'gQDByCdjUXw'], // Defining custom playlist
                                      startAt: Duration(seconds: 10),
                                      showControls: false,
                                      showFullscreenButton: false,
                                      autoPlay: false,
                                      mute: true)),
                              child: YoutubePlayerIFrame(aspectRatio: 16 / 9)))),
                if (getMediaType(post.link ?? '') == 'IMAGE')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: CachedNetworkImage(
                              imageUrl: post.thumbnail!,
                              placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  SvgPicture.asset(video_rouge, fit: BoxFit.contain)))),
                if (getMediaType(post.link ?? '') == 'PDF')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: Container(
                              height: 130,
                              child: SfPdfViewer.network(post.link ?? '',
                                  canShowPaginationDialog: false,
                                  scrollDirection: PdfScrollDirection.vertical,
                                  pageLayoutMode: PdfPageLayoutMode.continuous)))),
                if (getMediaType(post.link ?? '') == 'VIDEO')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: CachedNetworkImage(
                              imageUrl: post.thumbnail!,
                              placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  SvgPicture.asset(video_rouge, fit: BoxFit.contain)))),
                if (getMediaType(post.link ?? '') == 'AUDIO')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: CachedNetworkImage(
                              imageUrl: post.thumbnail!,
                              placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  SvgPicture.asset(audio_rose, fit: BoxFit.contain)))),
                if (getMediaType(post.link ?? '') == 'DOCUMENT')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: CachedNetworkImage(
                              imageUrl: post.thumbnail!,
                              placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  SvgPicture.asset(document_bleu, fit: BoxFit.contain)))),
                if (getMediaType(post.link ?? '') == 'LINK')
                  SizedBox(
                      height: 130,
                      child: SingleChildScrollView(
                          child: CachedNetworkImage(
                              imageUrl: post.thumbnail!,
                              placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  SvgPicture.asset(lien_jaune, fit: BoxFit.contain)))),
                Spacer(),
                const Divider(thickness: 1, color: primaryColor, indent: 20, endIndent: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SvgPicture.asset(heart, height: 15, width: 15),
                    const SizedBox(width: 7),
                    Text('(${post.likes})'),
                    const SizedBox(width: 10),
                    SvgPicture.asset(share, height: 15, width: 15),
                    const SizedBox(width: 7),
                    Text('(${post.shares})'),
                    const SizedBox(width: 10),
                    SvgPicture.asset(skillschat_active, height: 15, width: 15),
                    const SizedBox(width: 7),
                    Text('(${post.comments})'),
                    const SizedBox(width: 10),
                    SvgPicture.asset(download, height: 15, width: 15),
                    const SizedBox(width: 7),
                    const Text('(0)')
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
