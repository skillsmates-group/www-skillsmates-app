import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:www_skillsmates_app/pages/search/widgets/result_show_interface.dart';

import '../../../themes/picture_file.dart';

class ResultSearchSide extends StatelessWidget {
  const ResultSearchSide({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var resultFound = true;
    var textDropdown = TextEditingController();
    const itemsCount = ['10', '20', '50', '100'];
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.start,
        //   crossAxisAlignment: CrossAxisAlignment.center,
        //   children: [
        //     const Text("Nombres d'éléments à afficher par page"),
        //     SizedBox(
        //       width: 200,
        //       height: 40,
        //       child: DropdownStatut(
        //         textDropdown,
        //         hint: '--',
        //         items: itemsCount,
        //       ),
        //     ),
        //   ],
        // ),
        const SizedBox(
          height: 15,
        ),
        if (resultFound) ResultShowInterface(),
        if (!resultFound)
          // ignore: dead_code
          Container(
            /* No result found */
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text(
                  "Aucun Résultat trouvé",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                SvgPicture.asset(no_result),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "Supprimer des filtres ou reformulez votre recherches pour obtenir des résultats.",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w300,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
      ],
    );
  }
}
