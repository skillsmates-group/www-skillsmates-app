import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/search/widgets/grid_result_show.dart';
import 'package:www_skillsmates_app/pages/search/widgets/show_all_result_bloc.dart';
import 'package:www_skillsmates_app/providers/help_provider.dart';

class ResultShowInterface extends StatefulWidget {
  const ResultShowInterface({Key? key}) : super(key: key);

  @override
  State<ResultShowInterface> createState() => _ResultShowInterfaceState();
}

class _ResultShowInterfaceState extends State<ResultShowInterface> {
  // var _gridViewShow = false;
  @override
  Widget build(BuildContext context) {
    final helpProvider = Provider.of<HelpProvider>(context);
    return (helpProvider.SelectName != "Tout")
        ? GridResultShow(
            categoryLabel: helpProvider.SelectName,
          )
        : ShowAllResultBloc();
  }
}
