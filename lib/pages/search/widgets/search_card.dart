import 'package:flutter/material.dart';

import '/themes/theme.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_text_form_field.dart';

class SearchCard extends StatefulWidget {
  final TextEditingController textEditing;
  final TextEditingController city;
  final TextEditingController country;
  final TextEditingController direction;
  final void Function()? handleButton;
  final bool hasNoResult;

  const SearchCard({
    Key? key,
    required this.textEditing,
    required this.city,
    required this.country,
    required this.direction,
    this.handleButton,
    required this.hasNoResult,
  }) : super(key: key);

  @override
  State<SearchCard> createState() => _SearchCardState();
}

class _SearchCardState extends State<SearchCard> {
  final _stackKey = GlobalKey();

  double get positionLeft {
    final keyObject = _stackKey.currentContext;
    var positionSearchButton = 400.0;
    if (keyObject != null) {
      final box = keyObject.findRenderObject() as RenderBox;
      positionSearchButton = (box.size.width - 146) / 2;
    }
    return positionSearchButton;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      margin: const EdgeInsets.all(15),
      child: Stack(key: _stackKey, alignment: Alignment.center, clipBehavior: Clip.none, children: [
        Card(
          elevation: smElevationCard,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      color: Colors.white60,
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          20,
                        ),
                      ),
                    ),
                    child: TextField(
                      controller: widget.textEditing,
                      autofocus: true,
                      decoration: const InputDecoration(
                        hintText: "Rechercher",
                        border: OutlineInputBorder(),
                        suffixIcon: Icon(
                          Icons.search,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                    Expanded(
                      flex: 38,
                      child: SmTextFormField(
                        widget.city,
                        fieldName: 'Ville',
                      ),
                    ),
                    Expanded(
                      flex: 38,
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 10,
                        ),
                        child: SmTextFormField(
                          widget.country,
                          fieldName: 'Pays',
                        ),
                      ),
                    ),
                    // Expanded(
                    //   flex: 24,
                    //   child: Padding(
                    //     padding: const EdgeInsets.only(
                    //       left: 10,
                    //     ),
                    //     child: Material(
                    //       child: Dropdown(
                    //         widget.direction,
                    //         Direction.values.map((e) => e.value).toList(),
                    //         "Filter par",
                    //         previousValue: Direction.DESC.value,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ]),
                ]),
          ),
        ),
        Positioned(
          left: positionLeft,
          bottom: -25,
          child: Column(children: [
            SizedBox(
              width: 120,
              child: SmElevatedButton(
                label: 'Rechercher',
                color: primaryColor,
                handleButton: widget.handleButton,
              ),
            ),
            widget.hasNoResult
                ? const Text(
                    'Aucun résultat trouvé',
                  )
                : Container(),
          ]),
        ),
      ]),
    );
  }
}
