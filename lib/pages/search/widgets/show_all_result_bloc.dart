import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/models/search_param.dart';
import 'package:www_skillsmates_app/pages/search/widgets/bloc_result_card.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class ShowAllResultBloc extends StatelessWidget {
  const ShowAllResultBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [/*
        BlocResultCard(
          cardResult: StoryCard(
            isAddStory: true,
            account:
                Account(uuid: '', email: '', active: true, avatar: '', firstname: 'firstname', lastname: 'lastname'),
          ),
          headerColor: primaryColor,
          headerName: "Personnes",
          headerPicture: profile,
        ),
        const SizedBox(
          height: 15,
        ),
        BlocResultCard(
          currentPage: 0,
          searchParam: new SearchParam(),
          posts: [],
          showAll: false,
          // cardResult: ResultCardItem(post: Post()),
          headerColor: blueColor,
          headerName: "Documents",
          headerPicture: document,
          numberPosts: 0,
          showResult: false,
        ),
        const SizedBox(
          height: 15,
        ),
        BlocResultCard(
          currentPage: 0,
          searchParam: new SearchParam(),
          numberPosts: 0,
          posts: [],
          showAll: false,
          // cardResult: ResultCardItem(post: Post(),
          //   svgPicture: photo_vert,
          // ),
          headerColor: greenColor,
          headerName: "Photos",
          headerPicture: photo_vert,
          showResult: false,
        ),
        const SizedBox(
          height: 15,
        ),
        BlocResultCard(
          currentPage: 0,
          searchParam: new SearchParam(),
          numberPosts: 0,
          posts: [],
          showAll: false,
          // cardResult: ResultCardItem( post: Post(),
          //   svgPicture: video_rouge,
          // ),
          headerColor: redColor,
          headerName: "Videos",
          headerPicture: video_rouge,
          showResult: false,
        ),*/
      ],
    );
  }
}
