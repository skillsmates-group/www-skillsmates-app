import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class SmCheckboxSearch extends StatefulWidget {
  final String label;
  const SmCheckboxSearch({Key? key, required this.label}) : super(key: key);

  @override
  State<SmCheckboxSearch> createState() => _SmCheckboxSearchState();
}

class _SmCheckboxSearchState extends State<SmCheckboxSearch> {
  var _isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Checkbox(
            value: _isChecked,
            checkColor: whitishColor,
            activeColor: primaryColor,
            onChanged: (bool? value) {
              setState(() {
                _isChecked = value!;
              });
            }),
        Text("${widget.label} (0)"),
      ],
    );
  }
}
