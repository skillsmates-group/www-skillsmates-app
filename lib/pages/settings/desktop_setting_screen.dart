import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:www_skillsmates_app/data/setting_attribute_data.dart';
import 'package:www_skillsmates_app/enums/setting_attribute.dart';
import 'package:www_skillsmates_app/enums/setting_code.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/desktop_nav_bar.dart';
import 'package:www_skillsmates_app/pages/settings/widgets/setting_account_edit.dart';
import 'package:www_skillsmates_app/pages/settings/widgets/setting_security_edit.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_footer_row.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class DesktopSettingScreen extends StatefulWidget {
  const DesktopSettingScreen({Key? key}) : super(key: key);

  @override
  State<DesktopSettingScreen> createState() => _DesktopSettingScreenState();
}

class _DesktopSettingScreenState extends State<DesktopSettingScreen> {
  String tab = SettingCode.account.name;

  @override
  Widget build(BuildContext context) {
    clickTab(SettingAttribute settingAttribute) {
      setState(() {
        tab = settingAttribute.code.name;
      });
    }

    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: PreferredSize(preferredSize: Size(screenSize.width, 100), child: DesktopNavBar()),
        body: SingleChildScrollView(
            child: Column(children: [
          SizedBox(height: 35),
          Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
            Icon(Icons.settings, color: blueColor),
            Text("Paramètres".toUpperCase(), style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold))
          ]),
          Card(
              elevation: 5,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              margin: EdgeInsets.symmetric(horizontal: 200, vertical: 10),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 4,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 50),
                              for (var currentTab in settingAttributes)
                                Container(
                                    padding: EdgeInsets.only(right: 20),
                                    decoration: BoxDecoration(
                                        color: tab == currentTab.code.name ? blueLightColor : Colors.white),
                                    child: InkWell(
                                        onTap: () => clickTab(currentTab),
                                        child: Row(children: [
                                          Expanded(
                                              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                            Container(
                                                padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                                                child: Row(children: [
                                                  SvgPicture.asset(currentTab.image, height: 20, width: 20),
                                                  SizedBox(width: 5),
                                                  Text(currentTab.label,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight: tab == currentTab.code.name
                                                              ? FontWeight.w700
                                                              : FontWeight.normal))
                                                ]))
                                          ]))
                                        ]))),
                              SizedBox(height: 50)
                            ])),
                    Expanded(
                        flex: 8,
                        child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  if (tab == SettingCode.account.name) SettingAccountEdit(),
                                  if (tab == SettingCode.security.name) SettingSecurityEdit(),
                                  if (tab == SettingCode.confidentiality.name) Container(),
                                  if (tab == SettingCode.location.name) Container()
                                ])))
                  ])),
          SmFooterRow(),
        ])));
  }
}
