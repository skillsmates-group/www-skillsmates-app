import 'package:flutter/material.dart';

import '../widgets/responsive.dart';
import '../widgets/sm_button_back.dart';
import '../widgets/sm_footer_row.dart';

class MobileSettingsScreen extends StatelessWidget {
  const MobileSettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    final bool isTablet = Responsive.isTablet(context);

    double padding() {
      if (isDesktop) return 200;
      if (isTablet) return 50;
      return 0;
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: padding(),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  textAlign: TextAlign.center,
                  "Paramétres".toUpperCase(),
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 30),
              SmButtonBack(),
              SizedBox(height: 50),
              SmFooterRow()
            ],
          ),
        ),
      ),
    );
  }
}
