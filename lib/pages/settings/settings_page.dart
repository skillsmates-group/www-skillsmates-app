import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/settings/desktop_setting_screen.dart';
import 'package:www_skillsmates_app/pages/settings/mobile_settings_screen.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  static const routeName = '/settings';

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return DesktopSettingScreen();
      }
      if (sizingInformation.isTablet) {
        return MobileSettingsScreen();
      }
      return MobileSettingsScreen();
    });
  }
}
