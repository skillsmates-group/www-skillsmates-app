import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';

import '../../../models/account/account.dart';
import '../../../providers/account_provider.dart';
import '../../../providers/auth_provider.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/sm_text_form_field.dart';
import '../../widgets/sm_text_form_field_password.dart';

class SettingAccountEdit extends StatefulWidget {
  const SettingAccountEdit({Key? key}) : super(key: key);

  @override
  State<SettingAccountEdit> createState() => _SettingAccountEditState();
}

class _SettingAccountEditState extends State<SettingAccountEdit> {
  bool isNameEditable = false;
  bool isEmailEditable = false;
  bool isPhoneEditable = false;
  late Account loggedAccount;
  final _passwordForName = TextEditingController();
  final _passwordForEmail = TextEditingController();
  final _passwordForPhone = TextEditingController();
  final _firstname = TextEditingController();
  final _lastname = TextEditingController();
  final _email = TextEditingController();
  final _phone = TextEditingController();

  setEditableState(bool name, bool email, bool phone) {
    setState(() {
      this.isNameEditable = name;
      this.isEmailEditable = email;
      this.isPhoneEditable = phone;
    });
  }

  cancelEdition() {
    setEditableState(false, false, false);
  }

  dynamic _showMessageError(String message) {
    return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(title: Text('An error Occurred!'), content: Text(message), actions: [
              TextButton(
                  onPressed: () {
                    context.pop();
                    // Navigator.of(context).pop();
                  },
                  child: Text('Okay'))
            ]));
  }

  void authenticateAndUpdate(String password) {
    Provider.of<AuthProvider>(context, listen: false)
        .signin(loggedAccount.email, password)
        .then((value) => Provider.of<AccountProvider>(context, listen: false)
            .updateAccount(loggedAccount)
            .then((value) => {cancelEdition()})
            .onError((error, stackTrace) => _showMessageError("update failed")))
        .onError((error, stackTrace) => _showMessageError("mot de passe incorrect"));
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return FutureBuilder<Account>(
        future: authProvider.getLoggedAccount(),
        builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
          if (snapshot.hasData) {
            loggedAccount = snapshot.data!;
            return Container(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      isNameEditable ? nameEditable() : nameToEdit(),
                      const Divider(height: 10.0, thickness: 0.5),
                      isEmailEditable ? emailEditable() : emailToEdit(),
                      const Divider(height: 10.0, thickness: 0.5),
                      isPhoneEditable ? phoneEditable() : phoneToEdit(),
                      const Divider(height: 10.0, thickness: 0.5)
                    ]));
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        });
  }

  Widget nameToEdit() {
    return Row(children: [
      SizedBox(width: 100, child: Text("Nom:")),
      SizedBox(
          width: 300,
          child: Text("${loggedAccount.lastname} ${loggedAccount.firstname}",
              style: TextStyle(fontWeight: FontWeight.bold))),
      SizedBox(
          width: 100,
          child: SmElevatedButton(
              label: "Modifier", color: Colors.grey, handleButton: () => setEditableState(true, false, false)))
    ]);
  }

  Widget emailToEdit() {
    return Row(children: [
      SizedBox(width: 100, child: Text("Email:")),
      SizedBox(width: 300, child: Text("${loggedAccount.email}", style: TextStyle(fontWeight: FontWeight.bold))),
      SizedBox(
          width: 100,
          child: SmElevatedButton(
              label: "Modifier", color: Colors.grey, handleButton: () => setEditableState(false, true, false)))
    ]);
  }

  Widget phoneToEdit() {
    return Row(children: [
      SizedBox(width: 100, child: Text("Téléphone:")),
      SizedBox(
          width: 300, child: Text("${loggedAccount.phoneNumber ?? ''}", style: TextStyle(fontWeight: FontWeight.bold))),
      SizedBox(
          width: 100,
          child: SmElevatedButton(
              label: "Modifier", color: Colors.grey, handleButton: () => setEditableState(false, false, true)))
    ]);
  }

  Widget nameEditable() {
    final _formKey = GlobalKey<FormState>();
    _firstname.text = loggedAccount.firstname;
    _lastname.text = loggedAccount.lastname;
    return Form(
      key: _formKey,
      child: Column(
        children: commonForm([
          Row(children: [
            Expanded(child: SmTextFormField(_firstname, fieldName: "Prénom(s)")),
            SizedBox(width: 30),
            Expanded(child: SmTextFormField(_lastname, fieldName: "Nom(s)"))
          ])
        ], _formKey, _passwordForName, () {
          loggedAccount.firstname = _firstname.text.trim();
          loggedAccount.lastname = _lastname.text.trim();
        }),
      ),
    );
  }

  Widget emailEditable() {
    final _formKey = GlobalKey<FormState>();
    _email.text = loggedAccount.email;
    return Form(
      key: _formKey,
      child: Column(
        children: commonForm([SmTextFormField(_email, fieldName: "Email")], _formKey, _passwordForEmail, () {
          loggedAccount.email = _email.text.trim();
        }),
      ),
    );
  }

  Widget phoneEditable() {
    final _formKey = GlobalKey<FormState>();
    _phone.text = loggedAccount.phoneNumber ?? ''.toString();
    return Form(
      key: _formKey,
      child: Column(
        children: commonForm([SmTextFormField(_phone, fieldName: "Téléphone")], _formKey, _passwordForPhone, () {
          loggedAccount.phoneNumber = _phone.text.trim();
          print("#${loggedAccount.phoneNumber}#");
          print("#${_phone.text.trim()}#");
        }),
      ),
    );
  }

  List<Widget> commonForm(
      List<Widget> widgets, GlobalKey<FormState> key, TextEditingController _password, Function handleButton) {
    return widgets +
        [
          SizedBox(height: 10),
          Text("Saisissez votre mot de passe pour valider les modifications"),
          SizedBox(height: 10),
          SmTextFormFieldPassword(_password),
          SmSaveCancelButton(
            saveButtonText: "Enregistrer les informations",
            cancelButtonText: "Annuler",
            handleButton: (button) {
              if (button == 'SAVE') {
                key.currentState!.save();
                if (_password.text.trim().isEmpty) {
                  _showMessageError("veillez spécifier votre mot de passe");
                } else {
                  handleButton();
                  authenticateAndUpdate(_password.text.trim());
                }
              } else if (button == 'CANCEL') {
                cancelEdition();
              }
            },
          ),
        ];
  }
}
