import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/profile_edit/widgets/sm_save_cancel_button.dart';

import '../../../models/account/account.dart';
import '../../../providers/auth_provider.dart';
import '../../widgets/sm_elevated_button.dart';
import '../../widgets/sm_future_builder_error.dart';
import '../../widgets/sm_future_builder_waiting.dart';
import '../../widgets/sm_text_form_field_password.dart';

class SettingSecurityEdit extends StatefulWidget {
  const SettingSecurityEdit({Key? key}) : super(key: key);

  @override
  State<SettingSecurityEdit> createState() => _SettingSecurityEditState();
}

class _SettingSecurityEditState extends State<SettingSecurityEdit> {
  bool isPasswordEditable = false;
  bool isDeleteEditable = false;
  String? _action = 'DISABLE';
  late Account loggedAccount;

  setEditableState(bool password, bool delete) {
    setState(() {
      this.isPasswordEditable = password;
      this.isDeleteEditable = delete;
    });
  }

  dynamic _showMessageError(String message) {
    return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(title: Text('An error Occurred!'), content: Text(message), actions: [
              TextButton(
                  onPressed: () {
                    context.pop();
                    // Navigator.of(context).pop();
                  },
                  child: Text('Okay'))
            ]));
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    return FutureBuilder<Account>(
        future: authProvider.getLoggedAccount(),
        builder: (BuildContext context, AsyncSnapshot<Account> snapshot) {
          if (snapshot.hasData) {
            loggedAccount = snapshot.data!;
            return Container(
              padding: EdgeInsets.symmetric(
                horizontal: 50,
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    isPasswordEditable ? passwordEditable() : passwordToEdit(),
                    const Divider(
                      height: 10.0,
                      thickness: 0.5,
                    ),
                    isDeleteEditable ? deleteEditable() : deleteToEdit(),
                    const Divider(
                      height: 10.0,
                      thickness: 0.5,
                    )
                  ]),
            );
          } else if (snapshot.hasError) {
            return SmFutureBuilderError();
          } else {
            return SmFutureBuilderWaiting();
          }
        });
  }

  Widget passwordToEdit() {
    return Column(children: [
      Row(children: [
        SizedBox(width: 400, child: Text("Changer le mot de passe:")),
        SizedBox(
            width: 100,
            child: SmElevatedButton(
                label: "Modifier", color: Colors.grey, handleButton: () => setEditableState(true, false)))
      ]),
      Text(
          "Maximisez la sécurité de votre compte en enregistrant un mot de passe que vous n'utilisez nulle part ailleurs")
    ]);
  }

  Widget deleteToEdit() {
    return Row(children: [
      SizedBox(width: 400, child: Text("Désactiver temporairement ou supprimer définitivement votre compte")),
      SizedBox(
          width: 100,
          child: SmElevatedButton(
              label: "Modifier", color: Colors.grey, handleButton: () => setEditableState(false, true)))
    ]);
  }

  Widget passwordEditable() {
    final _formKey = GlobalKey<FormState>();
    final _oldPassword = TextEditingController();
    final _newPassword = TextEditingController();
    final _confirmPassword = TextEditingController();
    return Form(
        key: _formKey,
        child: Column(children: [
          Text("Changer le mot de passe", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(height: 10),
          SmTextFormFieldPassword(_oldPassword, fieldName: 'Ancien mot de passe'),
          Row(children: [
            Expanded(child: SmTextFormFieldPassword(_newPassword, fieldName: 'nouveau mot de passe')),
            SizedBox(width: 30),
            Expanded(child: SmTextFormFieldPassword(_confirmPassword, fieldName: 'Confirmation du mot de passe'))
          ]),
          SmSaveCancelButton(
              saveButtonText: "Enregistrer les modifications",
              cancelButtonText: "Annuler",
              handleButton: (button) {
                if (button == 'SAVE') {
                  _formKey.currentState!.save();
                  if (_oldPassword.text.isEmpty) {
                    _showMessageError("veillez spécifier votre mot de passe actuel");
                  } else {
                    _showMessageError("PASSWORD UPDATE NOT YET IMPLEMENTED");
                  }
                } else if (button == 'CANCEL') {
                  setEditableState(false, false);
                }
              })
        ]));
  }

  Widget deleteEditable() {
    final _formKey = GlobalKey<FormState>();
    final _password = TextEditingController();

    return Form(
        key: _formKey,
        child: Column(children: [
          Text("Désactiver temporairement ou supprimer définitivement votre compte",
              style: TextStyle(fontWeight: FontWeight.bold)),
          Row(children: [
            Expanded(
                child: ListTile(
                    title: const Text('Désactiver'),
                    leading: Radio<String>(
                        value: 'DISABLE',
                        groupValue: _action,
                        onChanged: (String? value) {
                          setState(() {
                            _action = value;
                          });
                        }))),
            SizedBox(width: 40),
            Expanded(
                child: ListTile(
                    title: const Text('Supprimer'),
                    leading: Radio<String>(
                        value: 'DELETE',
                        groupValue: _action,
                        onChanged: (String? value) {
                          setState(() {
                            _action = value;
                          });
                        })))
          ]),
          SizedBox(height: 10),
          Text("Saisissez votre mot de passe pour valider les modifications"),
          SizedBox(height: 10),
          SmTextFormFieldPassword(_password),
          SmSaveCancelButton(
              saveButtonText: "Enregistrer les modifications",
              cancelButtonText: "Annuler",
              handleButton: (button) {
                if (button == 'SAVE') {
                  _formKey.currentState!.save();
                  if (_password.text.trim().isEmpty) {
                    _showMessageError("veillez spécifier votre mot de passe");
                  } else {
                    if (_action == 'DISABLE') {
                      Provider.of<AuthProvider>(context, listen: false)
                          .signin(loggedAccount.email, _password.text.trim())
                          .then((value) => Provider.of<AuthProvider>(context, listen: false)
                              .deactivateAccount(loggedAccount.uuid)
                              .then((value) => {setEditableState(false, false)})
                              .onError((error, stackTrace) => _showMessageError("disable failed")))
                          .onError((error, stackTrace) => _showMessageError("mot de passe incorrect"));
                    } else if (_action == 'DELETE') {
                      Provider.of<AuthProvider>(context, listen: false)
                          .signin(loggedAccount.email, _password.text.trim())
                          .then((value) => Provider.of<AuthProvider>(context, listen: false)
                              .deleteAccount(loggedAccount.uuid)
                              .then((value) => {setEditableState(false, false)})
                              .onError((error, stackTrace) => _showMessageError("delete failed")))
                          .onError((error, stackTrace) => _showMessageError("mot de passe incorrect"));
                    }
                  }
                } else if (button == 'CANCEL') {
                  setEditableState(false, false);
                }
              })
        ]));
  }
}
