import 'package:flutter/material.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/share_post/widgets/desktop_share_content.dart';

class DesktopSharePost extends StatelessWidget {
  final Post param;
  const DesktopSharePost({required this.param, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 65,
            vertical: 50,
          ),
          child: Center(
            child: DesktopShareContent(
              postData: param,
            ),
          ),
        ),
      ),
    );
  }
}
