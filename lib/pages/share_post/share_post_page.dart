import 'package:flutter/material.dart';

import 'package:responsive_builder/responsive_builder.dart';
import 'package:www_skillsmates_app/pages/share_post/desktop_share_post.dart';
import 'package:www_skillsmates_app/pages/share_post/mobile_share_post.dart';

import '../../models/posts/post.dart';

class SharePostPage extends StatelessWidget {
  const SharePostPage({super.key});

  static const routeName = '/share';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as Post;
    return ResponsiveBuilder(
        builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return Padding(
          padding: EdgeInsets.all(10),
          child: DesktopSharePost(
            param: args,
          ),
        );
      }
      if (sizingInformation.isTablet) {
        return const MobileSharePost();
      }
      if (sizingInformation.isMobile) {
        return const MobileSharePost();
      }
      return const MobileSharePost();
    });
  }
}
