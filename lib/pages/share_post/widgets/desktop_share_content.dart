import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/dashboard/dashboard_page.dart';
import 'package:www_skillsmates_app/pages/dashboard/widgets/post_container.dart';
import 'package:www_skillsmates_app/providers/auth_provider.dart';
import 'package:www_skillsmates_app/providers/interaction_provider.dart';

import '../../../models/notification/interactionRequest.dart';
import '../../../models/posts/post.dart';

class DesktopShareContent extends StatefulWidget {
  final Post postData;
  const DesktopShareContent({
    required this.postData,
    super.key,
  });

  @override
  State<DesktopShareContent> createState() => _DesktopShareContentState();
}

class _DesktopShareContentState extends State<DesktopShareContent> {
  final _msgCtrl = TextEditingController();
  late InteractionRequest _interactionRequest;
  _submitShare(
    String emitter,
    InteractionProvider interactionProvider,
  ) {
    _interactionRequest = InteractionRequest(
      entity: widget.postData.uuid!,
      interactionType: "SHARE",
      emitter: emitter,
      receiver: widget.postData.account!.uuid,
      content: _msgCtrl.text,
    );
    interactionProvider.saveInteraction(
      _interactionRequest,
    );
    debugPrint(
      _interactionRequest.content! +
          ' - ' +
          _interactionRequest.emitter +
          ' - ' +
          _interactionRequest.entity +
          ' - ' +
          _interactionRequest.interactionType +
          ' - ' +
          _interactionRequest.receiver,
    );
  }

  @override
  Widget build(BuildContext context) {
    String emitterUuid = Provider.of<AuthProvider>(context).userId;
    final interactionProvider = Provider.of<InteractionProvider>(
      context,
      listen: false,
    );
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(),
        borderRadius: BorderRadius.all(
          Radius.circular(
            10,
          ),
        ),
      ),
      elevation: 15,
      child: Container(
        height: 1000,
        width: 600,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 6,
              ),
              decoration: BoxDecoration(
                color: Colors.grey,
                /* border: Border(
                      bottom: BorderSide(),
                    ), */
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Partager une Publication',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      context.push(DashboardPage.routeName);
                      // Navigator.of(context).pushReplacementNamed(
                      //   DashboardPage.routeName,
                      // );
                    },
                    icon: Icon(Icons.close),
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 2,
              height: 8,
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  const Text(
                    'Message de partage',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    controller: _msgCtrl,
                    maxLines: 5,
                    keyboardType: TextInputType.multiline,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  PostContainer(
                    post: widget.postData,
                    share: true,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    thickness: 1.2,
                    height: 5,
                    color: Colors.black,
                    endIndent: 30,
                    indent: 30,
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  ElevatedButton(
                    onPressed: () => _submitShare(
                      emitterUuid,
                      interactionProvider,
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.grey,
                      foregroundColor: Colors.black,
                      fixedSize: Size(300, 45),
                    ),
                    child: Text("Partager"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
