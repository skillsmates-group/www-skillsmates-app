import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../widgets/slogan_text.dart';
import '../../widgets/sm_footer_row.dart';
import '../../widgets/update_password_card.dart';

class DesktopUpdatePasswordScreen extends StatelessWidget {
  String token;

  DesktopUpdatePasswordScreen({Key? key, required this.token}) : super(key: key);

  static const logoSkillsmates = 'assets/images/logo-skillsmates.svg';
  static const landingPagePeople = 'assets/images/landing-page-people.svg';

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: width * 0.1),
          child: Center(
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Flexible(
                flex: 4,
                child: Column(
                  children: [
                    Center(
                      child: SvgPicture.asset(
                        logoSkillsmates,
                      ),
                    ),
                    const SloganText(),
                    Center(
                      child: SvgPicture.asset(
                        landingPagePeople,
                      ),
                    ),
                    const SmFooterRow(),
                  ],
                ),
              ),
              Flexible(
                flex: 2,
                child: Column(
                  children: [UpdatePasswordCard(token: this.token)],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
