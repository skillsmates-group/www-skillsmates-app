import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '/pages/update_password/screens/desktop_update_password_screen.dart';
import '/pages/update_password/screens/mobile_update_password_screen.dart';

class UpdatePasswordPage extends StatelessWidget {
  String token;
  static const routeName = 'forgotten-password/:token';

  UpdatePasswordPage({Key? key, required this.token}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (BuildContext context, SizingInformation sizingInformation) {
      if (sizingInformation.isDesktop) {
        return Padding(padding: EdgeInsets.all(10), child: DesktopUpdatePasswordScreen(token: this.token));
      }
      if (sizingInformation.isTablet) {
        return MobileUpdatePasswordScreen(token: this.token);
      }
      if (sizingInformation.isMobile) {
        return MobileUpdatePasswordScreen(token: this.token);
      }
      return MobileUpdatePasswordScreen(token: this.token);
    });
  }
}
