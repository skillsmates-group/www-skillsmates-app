import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../themes/palette.dart';
import '../../themes/picture_file.dart';

class ProfileAvatar extends StatelessWidget {
  final String imageUrl;
  final bool isActive;
  final bool isProfil;
  final bool hasBorder;
  final double radius;

  const ProfileAvatar({
    Key? key,
    required this.imageUrl,
    this.isActive = false,
    this.isProfil = false,
    this.hasBorder = false,
    this.radius = 20.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: radius,
          backgroundColor: Palette.skillsmatesBlue,
          backgroundImage: imageUrl.isEmpty ? null : CachedNetworkImageProvider(imageUrl),
          child: imageUrl.isEmpty
              ? SvgPicture.asset(
                  avatar,
                  height: 300,
                  width: 300,
                  fit: BoxFit.cover,
                )
              : null,
        ),
        if (isProfil || isActive)
          isProfil
              ? Positioned(
                  right: 4,
                  bottom: 8,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      shape: const CircleBorder(),
                    ),
                    child: const Icon(
                      Icons.add,
                      color: Colors.yellow,
                    ),
                  ),
                )
              : isActive
                  ? Positioned(
                      bottom: radius == 20.0 ? 0.0 : 8.0,
                      right: radius == 20.0 ? 0.0 : 4.0,
                      child: Container(
                        height: radius == 20.0 ? 10.0 : 15.0,
                        width: radius == 20.0 ? 10.0 : 15.0,
                        decoration: BoxDecoration(
                          color: Palette.online,
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 2.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  : Positioned(
                      bottom: radius == 20.0 ? 0.0 : 8.0,
                      right: radius == 20.0 ? 0.0 : 4.0,
                      child: Container(
                        height: radius == 20.0 ? 10.0 : 15.0,
                        width: radius == 20.0 ? 10.0 : 15.0,
                        decoration: BoxDecoration(
                          color: Palette.offline,
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 2.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
      ],
    );
  }
}
