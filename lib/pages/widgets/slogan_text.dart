import 'package:flutter/material.dart';

class SloganText extends StatelessWidget {
  const SloganText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top: 20),
      child: Text(
        'AMÉLIORE ET PARTAGE \nTES CONNAISSANCES AVEC \nLA COMMUNAUTÉ',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 22,
        ),
      ),
    );
  }
}
