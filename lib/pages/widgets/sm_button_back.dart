import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:www_skillsmates_app/pages/dashboard/dashboard_page.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

class SmButtonBack extends StatelessWidget {
  const SmButtonBack({super.key});

  @override
  Widget build(BuildContext context) {
    // final String imageUrl = Provider.of<AuthProvider>(context).imageUser;
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: primaryColor,
        fixedSize: Size(
          220,
          40,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      onPressed: () => context.push(DashboardPage.routeName)
      //     Navigator.of(context).pushReplacementNamed(
      //   DashboardPage.routeName,
      // )
      ,
      // icon: Image.asset(
      //   user,
      //   width: 30,
      //   height: 30,
      // ),
      child: Text(
        "Retourner à l'accueil",
      ),
    );
  }
}
