import 'package:flutter/material.dart';

import '../../themes/theme.dart';

class SmCheckboxListTile extends StatefulWidget {
  final String label;
  const SmCheckboxListTile({
    Key? key,
    required this.label,
  }) : super(key: key);

  @override
  State<SmCheckboxListTile> createState() => _SmCheckboxListTileState();
}

class _SmCheckboxListTileState extends State<SmCheckboxListTile> {
  var _checkedValue = false;
  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        widget.label,
        style: TextStyle(
          color: whitishColor,
        ),
      ),
      value: _checkedValue,
      onChanged: (newValue) {
        setState(() {
          _checkedValue = newValue!;
        });
      },
      controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
    );
  }
}
