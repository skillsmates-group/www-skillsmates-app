import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../themes/theme.dart';

class SmChip extends StatelessWidget {
  String label;
  String image;
  Color? color;

  SmChip({Key? key, required this.label, required this.image, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Chip(
        backgroundColor: color,
        padding: EdgeInsets.all(8),
        avatar: SvgPicture.asset(image, height: 20),
        label: DefaultTextStyle(
          style: TextStyle(
            fontSize: 14,
            color: primaryColor,
            fontFamily: "Raleway",
            fontWeight: FontWeight.normal,
          ),
          child: Text(label),
        ),
      ),
    );
  }
}
