import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_waiting.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_text_form_field.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../models/documents/document_type.dart';
import '../../models/documents/document_types_response.dart';
import '../../models/media.dart';
import '../../providers/post_provider.dart';
import '../../providers/properties.dart';
import '../../themes/theme.dart';

class SmDocumentTypesDropdown extends StatefulWidget {
  late final TextEditingController formValue;
  final bool? enabled;
  final String? defaultValue;
  final String mediaCode;
  Function(DocumentType) callback;
  List<DocumentType> items = List.empty();
  SmDocumentTypesDropdown(
    this.formValue, {
    this.enabled = true,
    this.defaultValue = 'Catégorie du contenu',
    required this.callback,
    Key? key,
    this.mediaCode = 'MEDIA_TYPE_DOCUMENT',
  }) : super(key: key);

  @override
  State<SmDocumentTypesDropdown> createState() => _SmDocumentTypesDropdownState();
}

class _SmDocumentTypesDropdownState extends State<SmDocumentTypesDropdown> {
  @override
  Widget build(BuildContext context) {
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    if (widget.enabled ?? true) {
      return FutureBuilder<Media>(
          future: postProvider.getSelectedMediaType(),
          builder: (BuildContext context, AsyncSnapshot<Media> snapshot) {
            if (snapshot.hasData) {
              return Container(
                child: DropdownSearch<DocumentType>(
                  asyncItems: (String? filter) => getDocumentTypes(filter, snapshot.data?.code ?? ''),
                  popupProps: PopupPropsMultiSelection.menu(
                    showSelectedItems: true,
                    itemBuilder: _customPopupItemBuilder,
                    showSearchBox: true,
                  ),
                  compareFn: (item, sItem) => item.uuid == sItem.uuid || item.code == widget.formValue.text,
                  dropdownDecoratorProps: DropDownDecoratorProps(
                    dropdownSearchDecoration: InputDecoration(
                      labelText: widget.defaultValue,
                      filled: true,
                      fillColor: Theme.of(context).inputDecorationTheme.fillColor,
                    ),
                  ),
                  onChanged: (data) {
                    widget.callback(data!);
                  },
                ),
              );
            } else if (snapshot.hasError) {
              return SmFutureBuilderError();
            } else {
              return SmFutureBuilderWaiting();
            }
          });
    } else {
      final mediaUrlController = TextEditingController();
      mediaUrlController.text = widget.defaultValue!;
      return Container(
        alignment: Alignment.centerLeft,
        child: Material(
          child: SmTextFormField(
            readOnly: true,
            mediaUrlController,
            fieldName: 'Catégorie du contenu',
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                color: Colors.grey,
              ),
            ),
          ),
        ),
      );
    }
  }

  Widget _customPopupItemBuilder(BuildContext context, DocumentType? item, bool isSelected) {
    Color getMediaTypeColor(String mediaTypeCode) {
      if (mediaTypeCode.contains('DOCUMENT')) {
        return blueColor;
      } else if (mediaTypeCode.contains('VIDEO')) {
        return redColor;
      } else if (mediaTypeCode.contains('LINK')) {
        return yellowColor;
      } else if (mediaTypeCode.contains('IMAGE')) {
        return greenColor;
      } else if (mediaTypeCode.contains('AUDIO')) {
        return pinkColor;
      } else {
        return Colors.white;
      }
    }

    String getMediaTypeImage(String mediaTypeCode) {
      if (mediaTypeCode.contains('DOCUMENT')) {
        return document_bleu;
      } else if (mediaTypeCode.contains('VIDEO')) {
        return video_rouge;
      } else if (mediaTypeCode.contains('LINK')) {
        return lien_jaune;
      } else if (mediaTypeCode.contains('IMAGE')) {
        return photo_vert;
      } else if (mediaTypeCode.contains('AUDIO')) {
        return audio_rose;
      } else {
        return user;
      }
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        selected: isSelected,
        title: Text(item?.label ?? ''),
        subtitle: Text(item?.mediaType?.label ?? ''),
        leading: CircleAvatar(
          child: SvgPicture.asset(getMediaTypeImage(item?.code ?? '')),
          backgroundColor: Colors.white,
        ),
      ),
    );
  }

  Future<List<DocumentType>> getDocumentTypes(filter, String mediaCode) async {
    var response = await Dio().get(
      skillsmates_posts + '/document-types/' + mediaCode,
      queryParameters: {"filter": filter},
    );

    final data = response.data;
    if (data != null) {
      return DocumentTypesResponse.fromJsonList(data['resources']);
    }

    return [];
  }
}
