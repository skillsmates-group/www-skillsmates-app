import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class DropdownV2 extends StatefulWidget {
  final String hint;
  final Function(dynamic val) onChanged2;
  final Map<String, dynamic> itemTextValue;

  DropdownV2({Key? key, required this.hint, required this.onChanged2, required this.itemTextValue}) : super(key: key);

  @override
  State<DropdownV2> createState() => _DropdownV2State();
}

class _DropdownV2State extends State<DropdownV2> {
  dynamic? dropdownvalue;
  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
        child: DropdownButton2(
            isExpanded: true,
            hint: Row(
              children: [
                const Icon(Icons.list, size: 16, color: Colors.black),
                const SizedBox(width: 4),
                Expanded(
                    child: Text(widget.hint,
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
                        overflow: TextOverflow.ellipsis))
              ],
            ),
            items: widget.itemTextValue.entries
                .map(
                  (e) => DropdownMenuItem(
                    value: e.value,
                    child: Text(
                      e.key,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                )
                .toList(),
            value: dropdownvalue,
            onChanged: (value) {
              setState(() {
                dropdownvalue = value;
                widget.onChanged2(value);
              });
            },
            icon: const Icon(Icons.arrow_forward_ios_outlined),
            iconSize: 14,
            // iconEnabledColor: Colors.yellow,
            iconDisabledColor: Colors.black,
            buttonHeight: 50,
            buttonWidth: 160,
            buttonPadding: const EdgeInsets.only(left: 14, right: 14),
            buttonDecoration:
                BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: Colors.black26)),
            // buttonElevation: 2,
            itemHeight: 40,
            itemPadding: const EdgeInsets.only(left: 14, right: 14),
            dropdownMaxHeight: 200,
            dropdownWidth: 200,
            dropdownPadding: null,
            dropdownDecoration: BoxDecoration(borderRadius: BorderRadius.circular(14)),
            dropdownElevation: 3,
            scrollbarRadius: const Radius.circular(40),
            scrollbarThickness: 6,
            scrollbarAlwaysShow: true,
            offset: const Offset(0, 0)));
  }
}
