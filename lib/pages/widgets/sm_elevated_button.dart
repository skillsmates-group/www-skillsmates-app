import 'package:flutter/material.dart';

class SmElevatedButton extends StatelessWidget {
  final String label;
  final Color color;
  final void Function()? handleButton;
  const SmElevatedButton(
      {Key? key, required this.label, required this.color, this.handleButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 2,
        primary: color,
        minimumSize: const Size.fromHeight(40),
      ),
      onPressed: handleButton,
      child: Text(label),
    );
  }
}
