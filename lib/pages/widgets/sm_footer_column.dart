import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../providers/properties.dart';
import '../../themes/theme.dart';

class SmFooterColumn extends StatelessWidget {
  const SmFooterColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const symboleSkillsmates = 'assets/images/symbole_skillsmates.svg';
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 0.0),
      elevation: 0.0,
      shape: null,
      child: Container(
        width: 250,
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 8.0,
        ),
        color: Color.fromARGB(6, 255, 255, 255),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'A PROPOS - AIDE -\nPOLITIQUE DE CONFIDENTIALITE - \nCONDITIONS D\'UTILISATION - FAQ',
              style: TextStyle(
                color: primaryColor,
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.left,
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  symboleSkillsmates,
                  height: 20,
                ),
                const Text(
                  ' Skillsmates 2022',
                  style: TextStyle(
                    color: primaryColor,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.right,
                ),
              ],
            ),
            Text.rich(
              TextSpan(
                text: 'Version: ',
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.normal,
                  overflow: TextOverflow.ellipsis,
                ),
                children: [
                  TextSpan(
                    text: skillsmates_version,
                    style: const TextStyle(
                      fontSize: 14.0,
                      color: Colors.red,
                      fontWeight: FontWeight.w700,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
