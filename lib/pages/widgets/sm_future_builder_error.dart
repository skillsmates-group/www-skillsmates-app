import 'package:flutter/material.dart';

class SmFutureBuilderError extends StatelessWidget {
  SmFutureBuilderError({Key? key, this.error = 'Error occurred'}) : super(key: key);
  String error;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Icon(
          Icons.error_outline,
          color: Colors.red,
          size: 60,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16),
          child: Text(error),
        ),
      ],
    );
  }
}
