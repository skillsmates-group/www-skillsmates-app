import 'package:flutter/material.dart';

class SmFutureBuilderWaiting extends StatelessWidget {
  SmFutureBuilderWaiting({Key? key, this.message = 'Chargement en cours...'}) : super(key: key);
  String message;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SizedBox(
        child: CircularProgressIndicator(),
      ),
      Padding(
        padding: EdgeInsets.only(
          top: 16,
        ),
        child: Text(
          message,
        ),
      ),
    ]);
  }
}
