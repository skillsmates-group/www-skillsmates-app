import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../themes/picture_file.dart';
import '../../themes/theme.dart';

class SmInfoText extends StatelessWidget {
  const SmInfoText({
    Key? key,
    required this.infoText,
    this.color = blueSkyColor,
    this.backgroundColor = blueLightColor,
    this.image = info,
  }) : super(key: key);

  final String infoText;
  final Color color;
  final Color backgroundColor;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      color: backgroundColor,
      child: Row(
        children: [
          Expanded(flex: 2, child: SvgPicture.asset(image, height: 40)),
          Expanded(
            flex: 10,
            child: DefaultTextStyle(
              style: TextStyle(
                fontSize: 13,
                color: color,
                fontFamily: "Raleway",
                fontWeight: FontWeight.normal,
              ),
              child: Text(infoText),
            ),
          ),
        ],
      ),
    );
  }
}
