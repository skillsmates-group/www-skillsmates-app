import 'package:flutter/material.dart';

class SmMandatoryFieldErrorMessage extends StatelessWidget {
  const SmMandatoryFieldErrorMessage({Key? key, required this.message}) : super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Text(
        message,
        style: const TextStyle(
          color: Colors.red,
          fontSize: 12,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
