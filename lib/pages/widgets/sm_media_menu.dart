import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:www_skillsmates_app/themes/theme.dart';

import '/models/media.dart';
import '../../data/media_data.dart';

class SmMediaMenu extends StatefulWidget {
  late final Media? selectedMediaType;
  Function(Media) callback;
  final bool? enabled;

  SmMediaMenu({
    Key? key,
    this.enabled = true,
    this.selectedMediaType,
    required this.callback,
  }) : super(key: key);

  @override
  _SmMediaMenuState createState() => _SmMediaMenuState();
}

class _SmMediaMenuState extends State<SmMediaMenu> {
  late Media? selectedMediaType = widget.selectedMediaType;

  clickMedia(Media media) {
    setState(() {
      selectedMediaType = media;
    });
    widget.callback(media);
  }

  @override
  Widget build(BuildContext context) {
    Color getMediaTypeColor(Media media) {
      if (media.code.contains('DOCUMENT')) {
        return blueColorBackground;
      } else if (media.code.contains('VIDEO')) {
        return redColorBackground;
      } else if (media.code.contains('LINK')) {
        return yellowColorBackground;
      } else if (media.code.contains('IMAGE')) {
        return greenColorBackground;
      } else if (media.code.contains('AUDIO')) {
        return pinkColorBackground;
      } else {
        return Colors.white;
      }
    }

    return Row(
      children: [
        for (var media in medias)
          Expanded(
            child: InkWell(
              onTap: widget.enabled! ? () => clickMedia(media) : null,
              child: Container(
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  color: media.code == selectedMediaType?.code ? getMediaTypeColor(media) : Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  children: [
                    SvgPicture.asset(media.image, height: 40),
                    FittedBox(
                      child: Text(
                        media.label,
                        style: TextStyle(
                          color: media.code == selectedMediaType?.code ? Colors.white : primaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
      ],
    );
  }
}
