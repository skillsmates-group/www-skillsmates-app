import 'package:flutter/material.dart';

class SmOnHover extends StatefulWidget {
  final Widget child;
  Function(String)? callbackOnTap;

  SmOnHover({
    Key? key,
    required this.child,
    this.callbackOnTap,
  }) : super(key: key);

  @override
  _SmOnHoverState createState() => _SmOnHoverState();
}

class _SmOnHoverState extends State<SmOnHover> {
  bool isHovered = false;

  callbackOnTap() {
    widget.callbackOnTap!('ontap');
  }

  @override
  Widget build(BuildContext context) {
    final hoveredTransform = Matrix4.identity()
      ..scale(1.2)
      ..translate(0, -2, 0);
    final transform = isHovered ? hoveredTransform : Matrix4.identity();

    return MouseRegion(
      onEnter: (event) => onEntered(true),
      onExit: (event) => onEntered(false),
      cursor: SystemMouseCursors.click,
      child: AnimatedContainer(
        duration: Duration(microseconds: 200),
        transform: transform,
        child: InkWell(
          onTap: callbackOnTap,
          child: widget.child,
        ),
      ),
    );
  }

  onEntered(bool isHovered) => setState(() {
        this.isHovered = isHovered;
      });
}
