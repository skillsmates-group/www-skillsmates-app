import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

import '../../models/parameter_response.dart';
import '../../providers/properties.dart';
import '../../themes/theme.dart';

class SmParametersDropdown extends StatefulWidget {
  final String? label;
  final String? code;
  final String parameterType;
  Function(ParameterResponse) callback;
  List<ParameterResponse> items = List.empty();
  SmParametersDropdown({
    required this.callback,
    Key? key,
    this.label,
    this.code,
    required this.parameterType,
  }) : super(key: key);

  @override
  State<SmParametersDropdown> createState() => _SmParametersDropdownState();
}

class _SmParametersDropdownState extends State<SmParametersDropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DropdownSearch<ParameterResponse>(
        asyncItems: (String? filter) => getParameters(filter),
        popupProps: PopupPropsMultiSelection.menu(
          showSelectedItems: true,
          itemBuilder: _customPopupItemBuilder,
          showSearchBox: true,
        ),
        compareFn: (item, sItem) => item.uuid == sItem.uuid,
        dropdownDecoratorProps: DropDownDecoratorProps(
          dropdownSearchDecoration: InputDecoration(
            labelText: widget.label,
            filled: true,
            fillColor: Theme.of(context).inputDecorationTheme.fillColor,
          ),
        ),
        onChanged: (data) {
          widget.callback(data!);
        },
      ),
    );
  }

  Widget _customPopupItemBuilder(BuildContext context, ParameterResponse? item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        selected: isSelected,
        title: Text(
          item?.label ?? '',
        ),
        leading: CircleAvatar(
          backgroundColor: primaryColor,
        ),
      ),
    );
  }

  Future<List<ParameterResponse>> getParameters(filter) async {
    var response = await Dio().get(
      skillsmates_parameters + '/' + widget.parameterType,
      queryParameters: {"filter": filter},
    );

    final data = response.data;
    if (data != null) {
      return ParameterResponse.fromJsonList(data['resources']);
    }

    return [];
  }
}
