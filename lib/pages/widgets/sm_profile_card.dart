import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';
import 'package:www_skillsmates_app/models/models.dart';
import 'package:www_skillsmates_app/pages/widgets/responsive.dart';
import 'package:www_skillsmates_app/pages/widgets/sm_future_builder_error.dart';
import 'package:www_skillsmates_app/themes/picture_file.dart';

import '../../models/notification/interactionRequest.dart';
import '../../providers/auth_provider.dart';
import '../../providers/interaction_provider.dart';
import '../../providers/post_provider.dart';
import '../../themes/palette.dart';
import '../../themes/theme.dart';
import '../profile/profile_page.dart';

class SmProfileCard extends StatefulWidget {
  final bool isAddStory;
  final Account? account;
  final Story? story;
  final bool isSuggestion;
  final bool bottomSpace;

  const SmProfileCard({
    Key? key,
    this.isAddStory = false,
    this.account,
    this.story,
    this.isSuggestion = true,
    required this.bottomSpace,
  }) : super(key: key);

  @override
  State<SmProfileCard> createState() => _SmProfileCardState(isSuggestion);
}

class _SmProfileCardState extends State<SmProfileCard> {
  bool _isTap = false;
  late bool isSuggestion;

  _SmProfileCardState(bool isSuggestion) {
    this.isSuggestion = isSuggestion;
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final interactionProvider = Provider.of<InteractionProvider>(context, listen: false);
    final postProvider = Provider.of<PostProvider>(context, listen: false);
    final String loggedAccountUuid = new LocalStorage('skills-mates_app').getItem("uuid");

    void _showMessageError(String message) {
      MotionToast.error(
        title: Text("Une erreur est survenue"),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromRight,
        toastDuration: Duration(seconds: 10),
      ).show(context);
    }

    void _showMessageSuccess(String message) {
      MotionToast.success(
        title: Text("Felicitation"),
        description: Text(message),
        position: MotionToastPosition.top,
        animationType: AnimationType.fromRight,
        toastDuration: Duration(seconds: 10),
      ).show(context);
    }

    void onClickFollow(Account account) {
      InteractionRequest interaction;
      authProvider.getLoggedAccount().then((loggedAccount) => {
            interaction = InteractionRequest(
              entity: account.uuid,
              interactionType: 'INTERACTION_TYPE_FOLLOWER',
              emitter: loggedAccount.uuid,
              receiver: account.uuid,
            ),
            interactionProvider
                .saveInteraction(interaction)
                .then((value) => {
                      _showMessageSuccess('Vous suivez ' + account.firstname),
                    })
                .onError((error, stackTrace) => {_showMessageError('')})
          });
    }

    return InkWell(
      onTap: () {
        authProvider.setProfileAccount(widget.account!).then((value) => {context.push(ProfilePage.routeName)});
        if (Responsive.isMobile(context)) {
          setState(() {
            _isTap = true;
          });
        }
      },
      onHover: (ishovering) {
        if (Responsive.isDesktop(context)) {
          if (ishovering) {
            setState(() {
              _isTap = ishovering;
            });
          } else {
            setState(() {
              _isTap = ishovering;
            });
          }
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        child: Stack(children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(
              10.0,
            ),
            child: CachedNetworkImage(
              imageUrl: widget.account!.avatar?.toLowerCase() ?? '',
              height: 300,
              width: 220.0,
              fit: BoxFit.cover,
              errorWidget: (context, url, error) => SvgPicture.asset(
                cursus,
                height: 300,
                width: 220.0,
                fit: BoxFit.cover,
                color: Colors.black45,
              ),
            ),
          ),
          Positioned(
            top: 8.0,
            left: 8.0,
            child: Container(
              height: 20.0,
              width: 20.0,
              decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
              child: IconButton(
                padding: EdgeInsets.zero,
                icon: const Icon(
                  Icons.class_,
                ),
                iconSize: 10.0,
                color: Palette.skillsmatesBlue,
                onPressed: () => print(
                  'Add to people',
                ),
              ),
            ),
          ),
          if (!_isTap)
            Positioned(
              bottom: 8.0,
              left: 8.0,
              right: 8.0,
              child: ClipRRect(
                child: SizedBox(
                  width: 220.0,
                  child: Column(children: [
                    Text.rich(
                      TextSpan(
                          text: widget.account?.firstname,
                          style: const TextStyle(
                            fontSize: 16.0,
                            color: whitishColor,
                            overflow: TextOverflow.ellipsis,
                          ),
                          children: [
                            TextSpan(
                              text: ' ',
                            ),
                            TextSpan(
                              text: widget.account?.lastname,
                              style: const TextStyle(
                                fontSize: 16.0,
                                color: whitishColor,
                                fontWeight: FontWeight.w700,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ]),
                    ),
                    IconButton(
                      padding: EdgeInsets.zero,
                      icon: const Icon(
                        Icons.person_add,
                      ),
                      iconSize: 30.0,
                      color: Palette.scaffold,
                      onPressed: () => print(
                        'Add to people',
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          if (_isTap)
            Positioned(
              bottom: 0,
              child: ClipPath(
                clipper: OvalTopBorderClipper(),
                child: Container(
                  width: 220.0,
                  height: 200,
                  padding: const EdgeInsets.all(
                    8,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(
                      .7,
                    ),
                    borderRadius: const BorderRadius.only(),
                  ),
                  child: Center(
                    child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                      Text.rich(
                        TextSpan(
                            text: widget.account?.firstname,
                            style: const TextStyle(
                              fontSize: 16.0,
                              overflow: TextOverflow.ellipsis,
                            ),
                            children: [
                              TextSpan(
                                text: widget.account?.lastname,
                                style: const TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w700,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ]),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        width: 180,
                        child: Row(mainAxisSize: MainAxisSize.min, children: [
                          SvgPicture.asset(
                            student,
                            height: 20,
                            width: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            width: 120,
                            child: Text(
                              widget.account?.currentJob ?? '',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ]),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        width: 180,
                        child: Row(mainAxisSize: MainAxisSize.min, children: [
                          SvgPicture.asset(
                            company,
                            height: 20,
                            width: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            width: 120,
                            child: Text(
                              widget.account?.currentCompany ?? '',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ]),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        width: 180,
                        child: Row(mainAxisSize: MainAxisSize.min, children: [
                          SvgPicture.asset(
                            location,
                            height: 20,
                            width: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            width: 120,
                            child: Text(
                              "${widget.account?.city ?? ''} - ${widget.account?.country ?? ''}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ]),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        width: 150,
                        child: FutureBuilder<bool>(
                          future: postProvider.checkSocialInteraction(loggedAccountUuid, widget.account!.uuid),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              bool? follow = snapshot.data;
                              return ElevatedButton(
                                  onPressed: () {
                                    onClickFollow(widget.account!);
                                  },
                                  style: ElevatedButton.styleFrom(
                                      primary: follow! ? greenColor : Colors.grey,
                                      shape: RoundedRectangleBorder(
                                          side: BorderSide(color: Colors.black, width: 1.3, style: BorderStyle.solid),
                                          borderRadius: BorderRadius.circular(50))),
                                  child: Text(follow! ? "Abonné(e)" : "S'abonner"));
                            } else if (snapshot.hasError) {
                              return SmFutureBuilderError();
                            } else {
                              return ElevatedButton(
                                  onPressed: () {
                                    onClickFollow(widget.account!);
                                  },
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.grey,
                                      shape: RoundedRectangleBorder(
                                          side: BorderSide(color: Colors.black, width: 1.3, style: BorderStyle.solid),
                                          borderRadius: BorderRadius.circular(50))),
                                  child: Text("..."));
                            }
                          },
                        ),
                      ),
                    ]),
                  ),
                ),
              ),
            ),
        ]),
      ),
    );
  }
}
