import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SmSvgIconText extends StatelessWidget {
  final String image;
  final Text text;
  final Color color;
  Function? handleButton;
  SmSvgIconText({
    Key? key,
    required this.image,
    required this.text,
    this.color = Colors.white,
    this.handleButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
          icon: SvgPicture.asset(image),
          color: color,
          onPressed: () => handleButton!(),
        ),
        const SizedBox(width: 8.0),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              text,
            ],
          ),
        ),
      ],
    );
  }
}
