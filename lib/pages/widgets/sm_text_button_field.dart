import 'package:flutter/material.dart';

class SmTextButton extends StatelessWidget {
  final String label;
  final Color color;
  final void Function()? handleButton;
  const SmTextButton({Key? key, required this.label, required this.color, this.handleButton}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        primary: color,
      ),
      onPressed: handleButton,
      child: Text(label),
    );
  }
}
