import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '/themes/palette.dart';

class SmTextButtonImage extends StatelessWidget {
  final String label;
  final String image;
  SmTextButtonImage({Key? key, required this.label, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextButton(
          onPressed: () {},
          child: SvgPicture.asset(image, height: 40),
        ),
        Text(
          label,
          style: const TextStyle(
            color: Palette.skillsmatesBlue,
          ),
        ),
      ],
    );
  }
}
