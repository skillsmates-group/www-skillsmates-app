import 'package:flutter/material.dart';

class SmTextFormField extends StatelessWidget {
  bool? readOnly;
  final String fieldName;
  final TextEditingController textValue;
  final TextInputType textInputType;
  InputBorder? enabledBorder;
  Function(String)? onValueChanged;
  SmTextFormField(
    this.textValue, {
    Key? key,
    this.readOnly,
    required this.fieldName,
    this.textInputType = TextInputType.text,
    this.enabledBorder = null,
    this.onValueChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: readOnly ?? false,
      controller: textValue,
      onSaved: (newValue) => textValue.text = newValue!,
      onChanged: (value) => onValueChanged!(value),
      keyboardType: textInputType,
      validator: (s) {
        if (s!.isEmpty) {
          return 'Entrer un $fieldName';
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: fieldName,
        enabledBorder: enabledBorder,
      ),
    );
  }
}
