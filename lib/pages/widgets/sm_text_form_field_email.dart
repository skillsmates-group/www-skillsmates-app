import 'package:flutter/material.dart';

import '/extensions/string_extensions.dart';

class SmTextFormFieldEmail extends StatelessWidget {
  final String error;
  final TextEditingController formValue;
  const SmTextFormFieldEmail(this.formValue, {Key? key, this.error = 'Entrer un email valide'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: formValue,
      onSaved: (newValue) => formValue.text = newValue!,
      keyboardType: TextInputType.emailAddress,
      validator: (s) {
        if (!s!.isValidEmail()) {
          return error;
        }
        return null;
      },
      decoration: const InputDecoration(
        labelText: "Email",
        helperText: "",
        hintText: "email@test.com",
      ),
    );
  }
}
