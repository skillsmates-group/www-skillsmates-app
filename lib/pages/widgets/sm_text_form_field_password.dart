import 'package:flutter/material.dart';

import '/extensions/string_extensions.dart';
import '/themes/theme.dart';

class SmTextFormFieldPassword extends StatefulWidget {
  final String fieldName;
  final String helperText;
  final String pwdForConfirm;
  final TextEditingController formValue;
  const SmTextFormFieldPassword(
    this.formValue, {
    Key? key,
    this.fieldName = 'Mot de passe',
    this.helperText = '',
    this.pwdForConfirm = '',
  }) : super(key: key);

  @override
  State<SmTextFormFieldPassword> createState() => _SmTextFormFieldPasswordState();
}

class _SmTextFormFieldPasswordState extends State<SmTextFormFieldPassword> {
  bool _obscurePassword = true;
  bool _helperText = true;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: () => setState(() {
        _helperText = true;
      }),
      onSaved: (newValue) => widget.formValue.text = newValue!,
      validator: (s) {
        if (s!.isWhiteSpace()) {
          return "Le mot de passe est requis";
        }
        if (widget.pwdForConfirm.isNotEmpty) {
          if (s != widget.pwdForConfirm) {
            return "Les mots de passes ne correspondent pas";
          }
        }
        return null;
      },
      obscureText: _obscurePassword,
      keyboardType: TextInputType.visiblePassword,
      decoration: InputDecoration(
        labelText: widget.fieldName,
        helperText: _helperText ? widget.helperText : "",
        helperStyle: const TextStyle(color: blueColor),
        hintText: widget.fieldName,
        suffixIcon: IconButton(
          onPressed: () => setState(() => _obscurePassword = !_obscurePassword),
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}
