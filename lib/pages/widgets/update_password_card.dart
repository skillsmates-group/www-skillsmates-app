import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:motion_toast/resources/arrays.dart';
import 'package:provider/provider.dart';

import '/pages/widgets/sm_elevated_button.dart';
import '/pages/widgets/sm_text_form_field_password.dart';
import '/themes/theme.dart';
import '../../providers/auth_provider.dart';

class UpdatePasswordCard extends StatefulWidget {
  String token;

  UpdatePasswordCard({Key? key, required this.token}) : super(key: key);

  @override
  State<UpdatePasswordCard> createState() => _UpdatePasswordCardState();
}

class _UpdatePasswordCardState extends State<UpdatePasswordCard> {
  final _formKey = GlobalKey<FormState>();
  final _pwd = TextEditingController();
  final _pwd2 = TextEditingController();

  void _cancel() {
    context.pop();
    // Navigator.of(context).pop();
  }

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();
    if (isValid) {
      _formKey.currentState!.save();
      debugPrint('le pwd: ${_pwd.text},');
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      authProvider
          .resetPassword(this.widget.token, _pwd.text)
          .then((value) => _showMessageSuccess(
                  "Votre mot de passe a été mis à jour. Retournez à la page d'accueil pour vous connecter")
              // Navigator.of(context).pushReplacementNamed(AccountCreatedPage.routeName)
              )
          .onError((error, stackTrace) => {_showMessageError("Erreur lors du traitement de la requette")});
    }
  }

  void _showMessageError(String message) {
    MotionToast.error(
      title: Text("Une erreur est survenue"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
      toastDuration: Duration(seconds: 10),
    ).show(context);
  }

  void _showMessageSuccess(String message) {
    _pwd.clear();
    _pwd2.clear();
    MotionToast.success(
      title: Text("Felicitation"),
      description: Text(message),
      position: MotionToastPosition.top,
      animationType: AnimationType.fromRight,
      toastDuration: Duration(seconds: 10),
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
        side: const BorderSide(color: Colors.grey, width: 1),
      ),
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Text(
                  'Réinitialisation du mot de passe',
                  style: TextStyle(
                    fontSize: 20,
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SmTextFormFieldPassword(
                  _pwd,
                  fieldName: 'Nouveau mot de passe',
                ),
                SmTextFormFieldPassword(
                  _pwd2,
                  pwdForConfirm: _pwd2.text,
                  fieldName: 'Confirmer le nouveau Mot de passe',
                  helperText:
                      "Votre mot de passe doit contenir \n- entre 8 et 16 caractères\n- au moins 1 chiffre ou un caractère spécial : !&%\$ \n- au moins une lettre majuscule\n- au moins une lettre miniscule\n- ne doit pa contenir votre nom ou email",
                ),
                const SizedBox(
                  height: 8,
                ),
                SmElevatedButton(
                  label: 'Envoyer',
                  color: greenColor,
                  handleButton: _trySubmit,
                ),
                const Divider(),
                SmElevatedButton(
                  label: 'Annuler',
                  color: Colors.grey,
                  handleButton: _cancel,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
