import 'package:flutter/material.dart';

import '/pages/widgets/responsive.dart';
import '../../models/account/account.dart';
import 'profile_avatar.dart';

class UserCard extends StatelessWidget {
  final Account account;
  final void Function()? handleOnTap;

  const UserCard({
    Key? key,
    required this.account,
    required this.handleOnTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isDesktop = Responsive.isDesktop(context);
    return InkWell(
      onTap: handleOnTap,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ProfileAvatar(
            imageUrl: account.avatar?.toLowerCase() ?? '',
            isActive: account.active,
          ),
          const SizedBox(width: 6.0),
          if (isDesktop)
            Flexible(
              child: Text.rich(
                TextSpan(
                  text: account.firstname,
                  style: const TextStyle(
                    fontSize: 16.0,
                    overflow: TextOverflow.ellipsis,
                  ),
                  children: [
                    TextSpan(text: ' '),
                    TextSpan(
                      text: account.lastname,
                      style: const TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w700,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
