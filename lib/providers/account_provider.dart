import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:www_skillsmates_app/models/account/account.dart';
import 'package:www_skillsmates_app/providers/properties.dart';

import '../models/account/account_attributes_response.dart';
import '../models/account/account_response.dart';
import '../models/account/account_status_metric_response.dart';
import '../models/documents/document_type_metric_response.dart';
import '../models/search_param.dart';

class AccountProvider with ChangeNotifier {
  late Account loggedAccount;
  late List<Account> _online;
  late AccountResponse _network;
  late AccountResponse _accountResponse;
  late AccountAttributesResponse _accountAttributesResponse;
  late DocumentTypeMetricResponse _documentTypeMetricResponse;
  late AccountStatusMetricResponse _accountStatusMetricResponse;

  Account get account => loggedAccount;

  List<Account> get online => _online;

  AccountResponse get network => _network;

  AccountResponse get accountResponse => _accountResponse;

  AccountAttributesResponse get accountAttributesResponse => _accountAttributesResponse;

  DocumentTypeMetricResponse get documentTypeMetricResponse => _documentTypeMetricResponse;

  AccountStatusMetricResponse get accountStatusMetricResponse => _accountStatusMetricResponse;

  Future<Account> findAccount(String id) async {
    final url = Uri.parse(skillsmates_accounts + '/' + id);
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      loggedAccount = Account.fromJson(responseData['resource']);
      return loggedAccount;
    } catch (e) {
      throw e;
    }
  }

  Future<void> fetchAccountSuggestions(String accountId) async {
    final url = Uri.parse(skillsmates_accounts + '/suggestions/' + accountId);
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _accountResponse = AccountResponse.fromJson(body);
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }

  Future<AccountAttributesResponse> fetchAccountAttributes(String accountId) async {
    final url = Uri.parse(skillsmates_accounts + '/attributes/' + accountId);
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      _accountAttributesResponse = AccountAttributesResponse.fromJson(responseData as Map<String, dynamic>);
      notifyListeners();
      return _accountAttributesResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<List<Account>> findAccountOnline() async {
    final url = Uri.parse(skillsmates_accounts + '/online');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      _online = List<Account>.from(responseData['resources'].map((model) => Account.fromJson(model)));
      notifyListeners();
      return _online;
    } catch (e) {
      throw e;
    }
  }

  Future<AccountResponse> findMyNetwork(
      String accountId, String networkType, int offset, int limit, String status, String country, String name) async {
    final url = Uri.parse(
        '${skillsmates_accounts}/network/${networkType}/${accountId}?offset=${offset}&limit=${limit}&status=${status}&country=${country}&name=${name}');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      _network = AccountResponse.fromJson(responseData);
      notifyListeners();
      return _network;
    } catch (e) {
      throw e;
    }
  }

  Future<Account> updateBiography(Account account) async {
    final url = Uri.parse(skillsmates_accounts + '/biography/' + account.uuid);
    try {
      final response = await http.put(
        url,
        body: jsonEncode({
          'uuid': account.uuid,
          'biography': account.biography!,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }

      account = Account.fromJson(data['resource']);

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('loggedAccount', jsonEncode(account));
      notifyListeners();
      return account;
    } catch (e) {
      throw e;
    }
  }

  Future<Account> updateAccount(Account account) async {
    print(account);
    final url = Uri.parse(skillsmates_accounts + '/' + account.uuid);
    try {
      final response = await http.put(
        url,
        body: jsonEncode({
          'uuid': account.uuid,
          'firstname': account.firstname,
          'lastname': account.lastname,
          'gender': account.gender,
          'status': account.status,
          'birthdate': account.birthdate,
          'hideBirthdate': account.hideBirthdate,
          'city': account.city,
          'country': account.country,
          'address': account.address,
          'phoneNumber': account.phoneNumber,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }

      account = Account.fromJson(data['resource']);

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('loggedAccount', jsonEncode(account));
      notifyListeners();
      return account;
    } catch (e) {
      throw e;
    }
  }

  Future<void> register(
      String lastname, String firstname, String status, String email, String password, String confirmPassword) async {
    final url = Uri.parse(skillsmates_accounts);
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          "firstname": firstname,
          "lastname": lastname,
          "email": email,
          "password": password,
          "confirmPassword": confirmPassword,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors']['message']);
      }
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }

  Future<AccountStatusMetricResponse> fetchAccountStatusMetric() async {
    final url = Uri.parse(skillsmates_accounts + '/status/metric');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _accountStatusMetricResponse = AccountStatusMetricResponse.fromJson(body);
      notifyListeners();
      return _accountStatusMetricResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<AccountResponse> search(SearchParam searchParam, int page) async {
    final url = Uri.parse(skillsmates_accounts + '/search?offset=$page&limit=9');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'account': searchParam.account,
          'content': searchParam.content,
          'country': searchParam.country,
          'city': searchParam.city,
          'documentTypes': searchParam.documentTypes,
          'statuses': searchParam.statuses,
          'direction': searchParam.direction,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _accountResponse = AccountResponse.fromJson(body);
      notifyListeners();
      return _accountResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<String> updateAvatar(String accountUuid, Uint8List file, String name, String mimeType) async {
    var request = http.MultipartRequest('POST', Uri.parse(skillsmates_accounts + '/avatar/' + accountUuid));
    request.headers.addAll({'Content-Type': 'multipart/form-data', 'Accept': 'application/json;charset=utf-8'});
    var data = mimeType.split('/');
    request.files.add(
        await http.MultipartFile.fromBytes('file', file, filename: name, contentType: MediaType(data[0], data[1])));
    var response = await request.send();
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('loggedAccount', jsonEncode(await findAccount(accountUuid)));
    return response.toString();
  }
}
