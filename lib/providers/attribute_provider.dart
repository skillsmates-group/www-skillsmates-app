import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:www_skillsmates_app/models/account/training.dart';

import '/models/account/skill.dart';
import '/providers/properties.dart';
import '../models/account/job.dart';

class AttributeProvider with ChangeNotifier {
  Future<Skill> saveSkill(Skill skill) async {
    final url = Uri.parse(skillsmates_accounts + '/skills');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'title': skill.title,
          'discipline': skill.discipline,
          'description': skill.description,
          'account': skill.account,
          'category': skill.category,
          'level': skill.level,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }

      notifyListeners();
      return skill;
    } catch (e) {
      throw e;
    }
  }

  Future<Training> saveTraining(Training training) async {
    final url = Uri.parse(skillsmates_accounts + '/trainings');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'title': training.title,
          'education': training.education,
          'description': training.description,
          'account': training.account,
          'schoolName': training.schoolName,
          'level': training.level,
          'city': training.city,
          'schoolType': training.schoolType,
          'schoolClass': training.schoolClass,
          'teachingName': training.teachingName,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }

      notifyListeners();
      return training;
    } catch (e) {
      throw e;
    }
  }

  Future<Job> saveJob(Job job) async {
    final url = Uri.parse(skillsmates_accounts + '/jobs');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'title': job.title,
          'company': job.company,
          'description': job.description,
          'account': job.account,
          'startDate': job.startDate,
          'endDate': job.endDate,
          'city': job.city,
          'currentJob': job.currentJob,
          'activityArea': job.activityArea?.code,
          'activitySector': job.activitySector?.code,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }

      notifyListeners();
      return job;
    } catch (e) {
      throw e;
    }
  }
}
