import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:www_skillsmates_app/providers/properties.dart';

import '../extensions/sm_http_exception.dart';
import '../models/account/account.dart';
import '../models/account/account_auth_response.dart';

class AuthProvider with ChangeNotifier {
  final LocalStorage storage = new LocalStorage('skills-mates_app');
  String _userId = '';
  String _imageUser = '';
  bool _isAuth = false;
  late Account _loggedAccount;
  late Account _profileAccount;
  late AccountAuthResponseResource _accountAuthResponseResource;
  late bool _isSameProfile = true;

  bool get isAuth => _isAuth;

  bool get isSameProfile => _isSameProfile;

  Account get loggedAccount => _loggedAccount;

  String get imageUser => _imageUser;

  Account get profileAccount => _profileAccount;

  String get userId => _userId;

  AccountAuthResponseResource get accountAuthResponseResource => _accountAuthResponseResource;

  Future<Account> getLoggedAccount() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> json = jsonDecode(prefs.getString('loggedAccount') ?? '');
    _loggedAccount = Account.fromJson(json);
    // _imageUser = _loggedAccount.avatar!.toLowerCase();
    return _loggedAccount;
  }

  Future<Account> getProfileAccount() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> json = jsonDecode(prefs.getString('profileAccount') ?? '');
    _profileAccount = Account.fromJson(json);
    getLoggedAccount().then((value) => {
          _isSameProfile = _loggedAccount.uuid == _profileAccount.uuid,
        });

    return _profileAccount;
  }

  Future<Account> setProfileAccount(Account account) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('profileAccount', jsonEncode(account));
    _profileAccount = account;
    _isSameProfile = _loggedAccount.uuid == _profileAccount.uuid;
    return _profileAccount;
  }

  Future<void> signin(String email, String password) async {
    final url = Uri.parse(skillsmates_authenticate);
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'email': email,
          'password': password,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }

      _loggedAccount = Account.fromJson(data['resource']);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('loggedAccount', jsonEncode(_loggedAccount));
      prefs.setString('profileAccount', jsonEncode(_loggedAccount));
      _isSameProfile = true;
      storage.setItem("uuid", _loggedAccount.uuid);

      notifyListeners();
    } catch (e) {
      throw e;
    }
  }

  Future<bool> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }

  Future<Account> deactivateAccount(String accountId) async {
    final url = Uri.parse(skillsmates_accounts + '/deactivate/' + accountId);
    try {
      final response = await http.put(
        url,
        body: jsonEncode({}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }
      _profileAccount = Account.fromJson(data['resource']);
      notifyListeners();
      return _profileAccount;
    } catch (e) {
      throw e;
    }
  }

  Future<AccountAuthResponseResource> deleteAccount(String accountId) async {
    final url = Uri.parse(skillsmates_accounts + '/' + accountId);
    try {
      final response = await http.delete(
        url,
        body: jsonEncode({}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null) {
        throw HttpException(data['errors'][0]['detail']);
      }
      _accountAuthResponseResource = AccountAuthResponseResource.fromJson(data['resource']);
      notifyListeners();
      return _accountAuthResponseResource;
    } catch (e) {
      throw e;
    }
  }

  Future<void> initPasswordReset(String email) async {
    final url = Uri.parse(skillsmates_password + '/init-reset');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({"email": email}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null || response.statusCode != 200) {
        if (data['errors'] != null) {
          throw HttpException(data['errors'][0]['detail']);
        } else {
          throw HttpException('Erreur lors du traitement de la requette');
        }
      }
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }

  Future<void> resetPassword(String token, String password) async {
    final url = Uri.parse(skillsmates_password + '/reset');
    try {
      final response = await http.patch(
        url,
        body: jsonEncode({"token": token, "password": password}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final data = jsonDecode(response.body);
      if (data['errors'] != null || response.statusCode != 200) {
        if (data['errors'] != null) {
          throw HttpException(data['errors'][0]['detail']);
        } else {
          throw HttpException('Erreur lors du traitement de la requette');
        }
      }
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }
}
