import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HelpProvider with ChangeNotifier {
  String _selectName = "Tout";
  String get SelectName {
    return _selectName;
  }

  void selectValue(String val) {
    _selectName = val;
    notifyListeners();
  }

  Future<void> putKeyValue(String key, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  Future<String?> getValue(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}
