import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '/models/notification/interaction_response.dart';
import '/providers/properties.dart';
import '../models/notification/interactionRequest.dart';

class InteractionProvider with ChangeNotifier {
  late InteractionResponse _interactionResponse;

  InteractionResponse get interactionResponse => _interactionResponse;

  Future<InteractionResponse> fetchNotifications(String accountId, int offset, int limit) async {
    final url = Uri.parse(skillsmates_interactions + '/account/' + accountId + '?offset=${offset}&limit=${limit}');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      _interactionResponse = InteractionResponse.fromJson(responseData);
      notifyListeners();
      return _interactionResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<InteractionResponse> saveInteraction(InteractionRequest interaction) async {
    final url = Uri.parse(skillsmates_interactions);
    try {
      final response = await http.post(
        url,
        body: jsonEncode(interaction.toJson()),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _interactionResponse = InteractionResponse.fromJson(body);
      notifyListeners();
      return _interactionResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<InteractionResponse> updateInteraction(InteractionRequest interaction) async {
    final url = Uri.parse(skillsmates_interactions + "/" + interaction.uuid!);
    try {
      final response = await http.put(
        url,
        body: jsonEncode(interaction.toJson()),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _interactionResponse = InteractionResponse.fromJson(body);
      notifyListeners();
      return _interactionResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<InteractionResponse> fetchInteractionsByTypeAndEntity(
      String type, String entity, int offset, int limit) async {
    final url = Uri.parse(skillsmates_interactions +
        '/type/' +
        type +
        '/entity/' +
        entity +
        '?offset=' +
        offset.toString() +
        '&limit=' +
        limit.toString());
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _interactionResponse = InteractionResponse.fromJson(body);
      notifyListeners();
      return _interactionResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<InteractionResponse> deleteInteraction(String interactionUuid) async {
    final url = Uri.parse(skillsmates_interactions + "/" + interactionUuid);
    try {
      final response = await http.delete(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      print('########## before checking');
      if (responseData['errors'] != null) {
        print('########## in error');
        print(responseData);
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _interactionResponse = InteractionResponse.fromJson(body);
      notifyListeners();
      return _interactionResponse;
    } catch (e) {
      throw e;
    }
  }
}
