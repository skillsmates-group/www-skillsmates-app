import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:www_skillsmates_app/models/search_param.dart';

import '/models/models.dart';
import '/models/posts/profile_documents_response.dart';
import '/providers/properties.dart';
import '../models/documents/document_type_metric_response.dart';
import '../models/documents/document_types_response.dart';
import '../models/media.dart';
import '../models/posts/post_response.dart';

class PostProvider with ChangeNotifier {
  late List<Post> _posts = List.empty();
  late Post _post;
  late PostResponse _postResponse;
  late PostResponse _profilePostResponse;
  late ProfileDocumentsResponse _profileDocumentsResponse;
  late DocumentTypesResponse _documentTypesResponse;
  late DocumentTypeMetricResponse _documentTypeMetricResponse;
  late Media _selectedMediaType;
  late PostResponse? _searchPostResponse;

  List<Post> get posts => _posts;

  Post get post => _post;

  PostResponse get postResponse => _postResponse;

  PostResponse get profilePostResponse => _profilePostResponse;

  ProfileDocumentsResponse get profileDocumentsResponse => _profileDocumentsResponse;

  DocumentTypesResponse get documentTypesResponse => _documentTypesResponse;

  DocumentTypeMetricResponse get documentTypeMetricResponse => _documentTypeMetricResponse;

  Media get selectedMediaType => _selectedMediaType;

  PostResponse? get searchPostResponse => _searchPostResponse;

  Future<Media> setSelectedMediaType(Media media) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('selectedMediaType', jsonEncode(media));
    _selectedMediaType = media;
    return _selectedMediaType;
  }

  Future<Media> getSelectedMediaType() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> json = jsonDecode(prefs.getString('selectedMediaType') ?? '');
    _selectedMediaType = json != ''
        ? Media.fromJson(json)
        : Media(code: 'MEDIA_TYPE_DOCUMENT', label: 'DOCUMENT', image: 'assets/images/document-bleu.svg');
    return _selectedMediaType;
  }

  Future<Post> getCurrentPost() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> json = jsonDecode(prefs.getString('currentPost') ?? '');
    return Post.fromJson(json);
  }

  Future<String> getCurrentPostId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('currentPostId') ?? '';
  }

  Future<Post> setCurrentPost(Post post) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('currentPost', jsonEncode(post));
    prefs.setString('currentPostId', post.uuid!);
    return post;
  }

  Future<PostResponse> fetchDashboardPosts(String accountId) async {
    return fetchDashboardPostsWithFilters(accountId, 0, 50);
  }

  Future<PostResponse> fetchProfilePosts(String accountId) async {
    return fetchProfilePostsWithFilters(accountId, 0, 50);
  }

  Future<PostResponse> fetchDashboardPostsWithFilters(String accountId, int offset, int limit) async {
    final url = Uri.parse(skillsmates_posts +
        '/posts/dashboard/account/' +
        accountId +
        '?offset=' +
        offset.toString() +
        '&limit=' +
        limit.toString());
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _postResponse = PostResponse.fromJson(body);
      notifyListeners();
      return PostResponse.fromJson(body);
    } catch (e) {
      throw e;
    }
  }

  Future<PostResponse> fetchProfilePostsWithFilters(String accountId, int offset, int limit) async {
    final url = Uri.parse(skillsmates_posts +
        '/posts/account/' +
        accountId +
        '?offset=' +
        offset.toString() +
        '&limit=' +
        limit.toString());
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _profilePostResponse = PostResponse.fromJson(body);
      notifyListeners();
      return PostResponse.fromJson(body);
    } catch (e) {
      throw e;
    }
  }

  Future<PostResponse> fetchPostById(String postId) async {
    final url = Uri.parse(skillsmates_posts + '/posts/' + postId);
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _profilePostResponse = PostResponse.fromJson(body);
      notifyListeners();
      return _profilePostResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<ProfileDocumentsResponse> fetchProfileDocuments(String accountId) async {
    final url = Uri.parse(skillsmates_posts + '/posts/documents/' + accountId);
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }
      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _profileDocumentsResponse = ProfileDocumentsResponse.fromJson(body);
      notifyListeners();
      return _profileDocumentsResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<DocumentTypesResponse> fetchDocumentTypes() async {
    final url = Uri.parse(skillsmates_posts + '/document-types');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _documentTypesResponse = DocumentTypesResponse.fromJson(body);
      notifyListeners();
      return _documentTypesResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<DocumentTypeMetricResponse> fetchProfileDocumentTypeMetric(String accountId) async {
    final url = Uri.parse(skillsmates_posts + '/document-types/metric/' + accountId);
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _documentTypeMetricResponse = DocumentTypeMetricResponse.fromJson(body);
      notifyListeners();
      return _documentTypeMetricResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<DocumentTypeMetricResponse> fetchDocumentTypeMetric() async {
    final url = Uri.parse(skillsmates_posts + '/document-types/metric');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _documentTypeMetricResponse = DocumentTypeMetricResponse.fromJson(body);
      notifyListeners();
      return _documentTypeMetricResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<PostResponse?> searchPosts(SearchParam searchParam) async {
    final url = Uri.parse(skillsmates_posts + '/posts/search?offset=0&limit=20');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'account': searchParam.account,
          'content': searchParam.content,
          'country': searchParam.country,
          'city': searchParam.city,
          'documentTypes': searchParam.documentTypes,
          'statutes': searchParam.statuses,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _searchPostResponse = PostResponse.fromJson(body);
      notifyListeners();
      return _searchPostResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<ProfileDocumentsResponse?> research(SearchParam searchParam, int page) async {
    final url = Uri.parse(skillsmates_posts + '/posts/research?offset=$page&limit=9');
    try {
      final response = await http.post(
        url,
        body: jsonEncode({
          'account': searchParam.account,
          'content': searchParam.content,
          'country': searchParam.country,
          'city': searchParam.city,
          'documentTypes': searchParam.documentTypes,
          'statutes': searchParam.statuses,
          'direction': searchParam.direction,
        }),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['errors'] != null) {
        throw HttpException(responseData['errors'][0]['detail']);
      }

      final Map<String, dynamic> body = json.decode(response.body) as Map<String, dynamic>;
      _profileDocumentsResponse = ProfileDocumentsResponse.fromJson(body);
      notifyListeners();
      return _profileDocumentsResponse;
    } catch (e) {
      throw e;
    }
  }

  Future<String> savePost(Post post, PlatformFile? file) async {
    var request = http.MultipartRequest('POST', Uri.parse(skillsmates_posts + '/posts'));
    if (file != null) {
      String? mimeType = mime(file.name);
      String? mimee = mimeType?.split('/')[0];
      String? type = mimeType?.split('/')[1];
      request.files.add(http.MultipartFile.fromBytes('file', file.bytes?.toList() ?? List.empty(),
          filename: file.name, contentType: MediaType(mimee!, type!)));
    } else {
      request.files.add(http.MultipartFile.fromBytes('file', List.empty(), filename: ''));
    }

    request.fields['title'] = post.title ?? '';
    request.fields['keywords'] = post.keywords ?? '';
    request.fields['description'] = post.description ?? '';
    request.fields['documentType'] = post.documentType ?? '';
    request.fields['link'] = post.link ?? '';
    request.fields['postType'] = post.postType ?? '';
    request.fields['account'] = post.account?.uuid ?? '';
    var response = await request.send();

    return response.toString();
  }

  Future<String> updatePost(Post post, PlatformFile? file) async {
    var request = http.MultipartRequest('PUT', Uri.parse(skillsmates_posts + '/posts/' + post.uuid!));
    if (file != null) {
      String? mimeType = mime(file.name);
      String? mimee = mimeType?.split('/')[0];
      String? type = mimeType?.split('/')[1];
      request.files.add(http.MultipartFile.fromBytes('file', file.bytes?.toList() ?? List.empty(),
          filename: file.name, contentType: MediaType(mimee!, type!)));
    } else {
      request.files.add(http.MultipartFile.fromBytes('file', List.empty(), filename: ''));
    }

    request.fields['title'] = post.title ?? '';
    request.fields['keywords'] = post.keywords ?? '';
    request.fields['description'] = post.description ?? '';
    request.fields['documentType'] = post.documentType ?? '';
    request.fields['link'] = post.link ?? '';
    request.fields['postType'] = post.postType ?? '';
    request.fields['account'] = post.account?.uuid ?? '';
    request.fields['document'] = post.document ?? '';
    var response = await request.send();

    return response.toString();
  }

  Future<String> deletePost(Post post) async {
    var request = http.MultipartRequest('DELETE', Uri.parse(skillsmates_posts + '/posts/' + post.uuid!));
    var response = await request.send();
    return response.toString();
  }

  Future<bool> checkSocialInteraction(String emitter, String receiver) async {
    final url = Uri.parse(skillsmates_interactions + '/check/follow/${emitter}/${receiver}');
    try {
      final response = await http.get(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );
      final responseData = jsonDecode(response.body);
      if (responseData['data'] != null) {
        notifyListeners();
        return responseData['data'];
      } else {
        throw HttpException('Erreur lors du traitement de la requette');
      }
    } catch (e) {
      throw e;
    }
  }
}
