// LOCAL
// const ms_skillsmates_accounts = 'http://localhost:7001';
// const ms_skillsmates_posts = 'http://localhost:7002';
// const skillsmates_url = 'http://localhost:61015';

// INT
const ms_skillsmates_accounts = 'http://3.138.167.82:8001';
const ms_skillsmates_posts = 'http://3.138.167.82:8002';
const skillsmates_url = 'http://skillsmates.cf.s3-website.us-east-2.amazonaws.com/';

// PROD
// const ms_skillsmates_accounts = 'https://api-skillsmates.com:9001';
// const ms_skillsmates_posts = 'https://api-skillsmates.com:9002';
// const skillsmates_url = 'https://skills-mates.com/';

const skillsmates_authenticate = ms_skillsmates_accounts + '/skillsmates/authenticate';
const skillsmates_accounts = ms_skillsmates_accounts + '/skillsmates/accounts';
const skillsmates_parameters = ms_skillsmates_accounts + '/skillsmates';
const skillsmates_password = ms_skillsmates_accounts + '/skillsmates/password';
const skillsmates_posts = ms_skillsmates_posts + '/skillsmates';
const skillsmates_interactions = ms_skillsmates_posts + '/skillsmates/interactions';

// VERSION
const skillsmates_version = '0.8.20';
