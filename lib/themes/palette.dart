import 'package:flutter/material.dart';

class Palette {
  static const Color scaffold = Color(0xFFF0F2F5);

  static const Color skillsmatesBlue = Color(0xFF16152D);

  static const LinearGradient createRoomGradient = LinearGradient(
    colors: [Color(0xFF496AE1), Color(0xFFCE48B1)],
  );

  static const Color online = Color(0xFF4BCB1F);
  static const Color offline = Color.fromARGB(255, 170, 172, 169);

  static const Color appGreen = Color.fromARGB(255, 43, 177, 150);
  static const Color appBlue = Color.fromARGB(255, 70, 112, 182);
  static const Color appRed = Color.fromARGB(255, 227, 65, 51);
  static const Color appYellow = Color.fromARGB(255, 241, 198, 0);

  static const LinearGradient storyGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Colors.transparent, Colors.black26],
  );
}
