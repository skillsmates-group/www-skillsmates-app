import 'package:flutter/material.dart';

class SmInputTheme{
  TextStyle _buildTextStyle(Color color, {double size = 16.0}){
    return TextStyle(
      color: color,
      fontSize: size,
    );
  }

  OutlineInputBorder _buildBorder(Color color){
    return OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      borderSide: BorderSide(
        color: color,
        width: 1.0,
      ),
    );
  }

  InputDecorationTheme theme() => InputDecorationTheme(
    contentPadding: const EdgeInsets.all(16),
    isDense: true,
    // "always" put the label at the top
    floatingLabelBehavior: FloatingLabelBehavior.auto,
    // this can be useful for putting TextFields in a row
    // However, it might be more desirable to wrap with Flexible
    // to make them grow to the availaconst ble width
    // constraints: const BoxConstraints(maxWidth: 500),

    /// Borders
    //Enabled and not showing error
    enabledBorder: _buildBorder(Colors.grey[600]!),
    // Has error but not focus
    errorBorder: _buildBorder(Colors.red),
    // Has error and focus
    focusedErrorBorder: _buildBorder(Colors.red),
    // Default value is if borders are null
    border: _buildBorder(Colors.yellow),
    // Enabled and focus
    focusedBorder: _buildBorder(Colors.blue),
    // disabled
    disabledBorder: _buildBorder(Colors.grey[400]!),

    /// TextStyles
    suffixStyle: _buildTextStyle(Colors.black),
    counterStyle: _buildTextStyle(Colors.grey, size: 12.0),
    floatingLabelStyle: _buildTextStyle(Colors.black),
    // Make error and helper the same size, so that the field
    // does not grow in height when there is an error text
    errorStyle: _buildTextStyle(Colors.red, size: 12.0),
    helperStyle: _buildTextStyle(Colors.black, size: 12.0),
    hintStyle: _buildTextStyle(Colors.grey),
    labelStyle: _buildTextStyle(Colors.black),
    prefixStyle: _buildTextStyle(Colors.black),
  );
}